import os
import sys
import datetime
import yaml
import uuid
from pymongo import InsertOne, DeleteMany
from jagereye_ng.io.db_engine import connect_db
from jagereye_ng.io.db_engine import DatabaseError
from jagereye_ng.util.generic import get_config


def get_license_config(config_file="license.yml"):
    dir_path = os.path.dirname(os.path.realpath(__file__))
    with open(os.path.join(dir_path, config_file), "r") as f:
        return yaml.load(f)

class License():

    def __init__(self,
                 db_name,
                 db_host=None,
                 db_authentication_source=None):
        self._db_client, self._db = self._setup_db(
            db_name,
            host=db_host,
            authentication_source=db_authentication_source,
        )

    def _setup_db(self, db_name, host=None, authentication_source=None):
        try:
            client, db = connect_db(
                db_name,
                host=host,
                authentication_source=authentication_source,
            )
        except DatabaseError as e:
            sys.stderr.write("Database error occured while connecting: {}\n"
                          .format(str(e)))
            raise

        return client, db

    def update(self):
        license_config = get_license_config('license.yml')['numbers_of_channel']

        application = [
            {'type': l, 'channel': channel}
            for l, channel in license_config.items()
            if channel > 0
        ]
        doc = {
            "uuid" : str(uuid.uuid4()),
            "date" : datetime.datetime.now(),
            "vendor" : "dev",
            "application" : application
        }
        self._db['licenses'].bulk_write([
            DeleteMany({}),
            InsertOne(doc)])
        print('dev license has been updated', doc)

dbconfig = get_config()["services"]["database"]
db_url = "mongodb://{}:{}@{}:27017".format(dbconfig['env']["username"],
                                           dbconfig['env']["password"],
                                           dbconfig['network']['ip'])
db_name = dbconfig['db_name']
l = License(db_name,
            db_host=db_url,
            db_authentication_source='admin')

l.update()
