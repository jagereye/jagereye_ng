#!/bin/bash

while true
do
  # keep all docker services running in a period of time
  /usr/local/bin/docker-compose -p jagereye -f /usr/local/jagereye/docker-compose.yml up
  sleep 15
done
