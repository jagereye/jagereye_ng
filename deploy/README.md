# Deployment 

### Disable auto upgrade of OS
The Ubuntu OS use cron-daily to do "unattended-upgrades". Once it leads to replace the nvidia driver.
Then the nvidia-docker cannot work with msg as below:
``` shell
nvidia-docker | 2018/01/11 15:09:04 Error: nvml: Driver/library version mismatch
```
So we need to disable "unattended-upgrades".

1. Edit /etc/apt/apt.conf.d/20auto-upgrades:  
change 2 properties from

``` 
APT::Periodic::Update-Package-Lists "1";
APT::Periodic::Unattended-Upgrade "1";
```
to

```
APT::Periodic::Update-Package-Lists "0";
APT::Periodic::Unattended-Upgrade "0";
```
---
### Enable & config syslog server
1. Edit /etc/rsyslog.conf:  
change 2 properties from

```
#module(load="imudp")
#input(type="imudp" port="514")
```
to

```
module(load="imudp")
input(type="imudp" port="514")
```
2. Create a file /etc/rsyslog.d/30-jager.conf  
It is the config to redirect specific log facility to a file.  
For example:

```
local1.info                     /var/log/jager/jager.info
local1.debug                    /var/log/jager/jager.debug
```

3. Restart the rsyslog service

```
sudo systemctl restart rsyslog
```

Then if you just inject logging code with the setting 'local1' as facility in logging library

### Enable syslog rotation
1. Create a file /etc/logrotate.d/jager

It is the configuration to rotate the syslog files.

```
/var/log/jager/jager.debug {
  rotate 3
  size 500M
  missingok
  notifempty
  copytruncate
}

/var/log/jager/jager.info {
  rotate 3
  size 500M
  missingok
  notifempty
  copytruncate
}
```

2. Add logrotate to crontabe

Edit the crontab jobs:

```
sudo crontab -e
```

Add following lines:

```
# Run logrotate on the 14th minute of every hour.
14 * * * * /usr/sbin/logrotate /etc/logrotate.d/jager
```
---
### For networking

##### Set network interface name to jager0 and jager1
Asign network interface name 'jager0' for control port, 'jager1' for data port
1. find the mac address of the ethernet ports

```
$ ip a
12: enp1s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether XX:XX:XX:XX:XX:XX brd ff:ff:ff:ff:ff:ff
13: enp2s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether XX:XX:XX:XX:XX:XX brd ff:ff:ff:ff:ff:ff
```

2. create a udev rule for network interfaces
```
/etc/udev/rules.d/10-network.rules
---
SUBSYSTEM=="net", ACTION=="add", ATTR{address}=="aa:bb:cc:dd:ee:ff", NAME="jager0"
SUBSYSTEM=="net", ACTION=="add", ATTR{address}=="ff:ee:dd:cc:bb:aa", NAME="jager1"
```

3. restart to apply

##### Add privilege for network setting 
make the user **'jager'** has previledge to execute the network setting cmds

1. edit /etc/sudoers:

``` $ sudo vim /etc/sudoers```  or ``` $ visudo ``` (more safe) 

2. add the configurations below:

``` 
Cmnd_Alias FLUSH_IP = /sbin/ip addr flush dev jager*
Cmnd_Alias IFCONFIG = /sbin/ifconfig jager* *
Cmnd_Alias DHCLIENT = /sbin/dhclient jager*
# at the end:
jager ALL=(ALL) NOPASSWD: FLUSH_IP, IFCONFIG, DHCLIENT
```

note:
there's only service api access host's network settings

##### Disable service 'network-manager'
The service 'network-manager' on Ubuntu 16.04 will auto-config each network interfaces for conncetivity. For example, After setting static IP for a interface, when unplug the cable, the IP will be lost. Whenever plugging the cable again, network-manager will auto-config IP. It   
may be not the IP u set originally.

``` shell
$ sudo systemctl disable network-manager.service
```

##### Make all network interface be static
Due to the issue https://gitlab.com/jagereye/jagereye_ng/issues/52, we need to make all network interface be static as default
1. edit /etc/network/interfaces, 'jager0' is control port, assume the fixed IP is '192.168.20.1', 'jager1' is data port

```
auto jager0
iface jager0 inet static
address 192.168.20.1
netmask 255.255.255.0

auto jager1
iface jager1 inet static
```
**note**: No need to assign gateway for jager0. The control port is only connect to the SI's computer, it doesn't connet to Internet. So it should not assign gateway 

---
### For Docker Container connect to Host's X server

1. create user jager on host

```shell
export uid=4020
export gid=4020
sudo groupadd -g $gid jager
sudo useradd -m -u $uid -g jager jager
```

* Currently this user is for `X Server` authority user maping between host and container

2. Set user jager as auto login user on gdm3 (ubuntu desktop default display manager)

edit `/etc/gdm3/custom.conf` for auto login setup.

```
AutomaticLoginEnable=true
AutomaticLogin=jager
```

**note**: When user `jager` gui login session, gdm will fork a xorg process to start listening socket on `/tmp/.X11-unix/X0`. please confirm the jager should be the first login user or make sure there's always a desktop gui login user session existed.

3. allow all users to access Xorg for docker container while jager login

edit `/etc/profile`
```
if [ "$DISPLAY" != "" ]
then
  xhost +
fi
```
