{% macro network(network) -%}
        {% if network.mode == "bridge" -%}
        networks:
            {{networks.name}}:
                ipv4_address: {{network.ip}}
        {%- else -%}
        network_mode: "{{network.mode}}"
        {%- endif -%}
{%- endmacro -%}
#
# This file was generated automatically, please don't modify it unless you know what you're doing
#
---
version: "2.3"
services:
    {%- if services %}
    api:
        {%- if build %}
        build:
            context: "{{services.api.buildpath}}"
            args:
                VERSION: {{jager_version}}
        {%- endif %}
        image: "jagereye/api:{{jager_version}}"
        {{ network(services.api.network) }}
        {%- if environ.JAGERENV == "development" %}
        command: run dev
        {%- endif %}
        volumes:
            - type: "bind"
              source: "/etc/localtime"
              target: "/etc/localtime"
            - type: "bind"
              source: "{{services.api.logging_mount.source}}"
              target: "{{services.api.logging_mount.target}}"
            - type: "bind"
              source: "{{shared_files.mount.source}}"
              target: "{{shared_files.mount.target}}"
            {%- if environ.JAGERENV == "development" %}
            - type: "bind"
              source: "{{environ.JAGERROOT}}/services/api"
              target: "/home/jager/jagereye_ng/services/api"
            {%- endif %}
        cap_add:
            {%- for cap in services.api.cap_add %}
            - "{{cap}}"
            {%- endfor %}
        ports:
            {%- for _, port in services.api.ports.items() %}
            - "{{port}}"
            {%- endfor %}
            {%- if environ.JAGERENV == "development" %}
            - "9229"
            {%- endif %}
        depends_on:
            - database
            - messaging
        environment:
            - JAGERENV={{environ.JAGERENV}}
        restart: always
    streamer:
        {%- if build %}
        build:
            context: "{{services.streamer.buildpath}}"
            args:
                VERSION: {{jager_version}}
        {%- endif %}
        image: "jagereye/streamer:{{jager_version}}"
        {{ network(services.streamer.network) }}
        ports:
            {%- for _, port in services.streamer.ports.items() %}
            - "{{port}}"
            {%- endfor %}
        volumes:
            - type: "bind"
              source: "/etc/localtime"
              target: "/etc/localtime"
        depends_on:
            - database
            - live_streaming
        environment:
            - JAGERENV={{environ.JAGERENV}}
        restart: always
    frontend:
        {%- if build %}
        build: "{{services.frontend.buildpath}}"
        {%- endif %}
        image: "jagereye/frontend:{{jager_version}}"
        {{ network(services.frontend.network) }}
        volumes:
            - type: "bind"
              source: "/etc/localtime"
              target: "/etc/localtime"
        ports: 
            {%- for key, port in services.frontend.ports.items() %}
            - "{{port}}"
            {%- endfor %}
        restart: always
    database:
        {%- if build %}
        build: "{{services.database.buildpath}}"
        {%- endif %}
        image: "jagereye/database:{{jager_version}}"
        {%- if services.database.mount.enabled %}
        volumes:
            - type: "bind"
              source: "/etc/localtime"
              target: "/etc/localtime"
            - type: "bind"
              source: "{{services.database.mount.source}}"
              target: "/data/db"
        {%- endif %}
        {{ network(services.database.network) }}
        ports:
            {%- for key, port in services.database.ports.items() %}
            - "{{port}}"
            {%- endfor %}
        environment:
            - JAGERENV={{environ.JAGERENV}}
            {%- if services.database.env %}
            - MONGO_INITDB_ROOT_USERNAME={{services.database.env.username}}
            - MONGO_INITDB_ROOT_PASSWORD={{services.database.env.password}}
            - MONGO_INITDB_DATABASE={{services.database.db_name}}
            {%- endif %}
        restart: always
    messaging:
        {%- if build %}
        build: "{{services.messaging.buildpath}}"
        {%- endif %}
        image: "jagereye/messaging:{{jager_version}}"
        {{ network(services.messaging.network) }}
        volumes:
            - type: "bind"
              source: "/etc/localtime"
              target: "/etc/localtime"
        ports:
            {%- for key, port in services.messaging.ports.items() %}
            - "{{port}}"
            {%- endfor %}
        environment:
            - JAGERENV={{environ.JAGERENV}}
        restart: always
    obj_storage:
        {%- if build %}
        build: "{{services.obj_storage.buildpath}}"
        {%- endif %}
        image: "jagereye/obj_storage:{{jager_version}}"
        {{ network(services.obj_storage.network) }}
        {%- if services.obj_storage.mount.enabled %}
        volumes:
            - type: "bind"
              source: "/etc/localtime"
              target: "/etc/localtime"
            - type: "bind"
              source: "{{services.obj_storage.mount.source}}"
              target: "/data"
        {%- endif %}
        ports:
            {%- for key, port in services.obj_storage.ports.items() %}
            - "{{port}}"
            {%- endfor %}
        environment:
            - JAGERENV={{environ.JAGERENV}}
            - ACCESS_KEY={{services.obj_storage.credentials.access_key}}
            - SECRET_KEY={{services.obj_storage.credentials.secret_key}}
        restart: always
    live_streaming:
        {%- if build %}
        build: "{{services.live_streaming.buildpath}}"
        {%- endif %}
        image: "jagereye/live_streaming:{{jager_version}}"
        {{ network(services.live_streaming.network) }}
        volumes:
            - type: "bind"
              source: "/etc/localtime"
              target: "/etc/localtime"
        ports:
            {%- for key, port in services.live_streaming.ports.items() %}
            - "{{port}}"
            {%- endfor %}
        restart: always
    prometheus:
        {%- if build %}
        build: "{{services.prometheus.buildpath}}"
        {%- endif %}
        image: "jagereye/prometheus:{{jager_version}}"
        {{ network(services.prometheus.network) }}
        volumes:
            - type: "bind"
              source: "/etc/localtime"
              target: "/etc/localtime"
        ports:
            {%- for key, port in services.prometheus.ports.items() %}
            - "{{port}}"
            {%- endfor %}
        restart: always
    host_agent:
        {%- if build %}
        build:
            context: "{{services.host_agent.buildpath}}"
            args:
                VERSION: {{jager_version}}
        {%- endif %}
        image: "jagereye/host_agent:{{jager_version}}"
        {%- if services.host_agent.network.mode == "bridge" %}
        networks:
            {{networks.name}}:
                ipv4_address: {{services.host_agent.network.ip}}
        {%- else %}
        network_mode: "{{services.host_agent.network.mode}}"
        {%- endif %}
        volumes:
            - type: "bind"
              source: "/etc/localtime"
              target: "/etc/localtime"
            - type: "bind"
              source: "/"
              target: "/host-root"
            - type: "bind"
              source: "{{shared_files.mount.source}}"
              target: "{{shared_files.mount.target}}"
        depends_on:
            - messaging
        environment:
            - JAGERENV={{environ.JAGERENV}}
        restart: always
    expiration:
        {%- if build %}
        build:
            context: "{{services.expiration.buildpath}}"
            args:
                VERSION: {{jager_version}}
        {%- endif %}
        image: "jagereye/expiration:{{jager_version}}"
        {{ network(services.expiration.network) }}
        volumes:
            - type: "bind"
              source: "/etc/localtime"
              target: "/etc/localtime"
        restart: always
    blackbox:
        {%- if build %}
        build: "{{services.blackbox.buildpath}}"
        {%- endif %}
        image: "jagereye/blackbox:{{jager_version}}"
        {{ network(services.blackbox.network) }}
        volumes:
            - type: "bind"
              source: "/etc/localtime"
              target: "/etc/localtime"
        restart: always
    {%- endif %}
    {%- if apps %}
    analyzer_manager:
        image: "jagereye/analyzer_manager:{{jager_version}}"
        {{ network(apps.base.network) }}
        ports: 
            {%- for key, port in apps.base.ports.items() %}
            - "{{port}}"
            {%- endfor %}
        {%- if services %}
        depends_on:
            - database
            - messaging
            - obj_storage
        {%- endif %}
        runtime: nvidia
        volumes:
            - type: "bind"
              source: "/etc/localtime"
              target: "/etc/localtime"
            - type: "bind"
              source: "{{x11_unix_socket.mount.source}}"
              target: "{{x11_unix_socket.mount.target}}"
        environment:
            - JAGERENV={{environ.JAGERENV}}
            - DISPLAY=unix:0
            - QT_X11_NO_MITSHM=1
        restart: always
        {%- if logging.transport ==  "syslog" %}
        logging:
            driver: syslog
            options:
                syslog-address: "udp://{{logging.transport_candidates.syslog.host}}:{{logging.transport_candidates.syslog.port}}"
                syslog-facility: {{logging.transport_candidates.syslog.facility}}
                tag: ":- INFO - analyzers - third_party"
        {%- endif %}
    {%- endif %}
{%- if networks %}
networks:
    {{networks.name}}:
        driver: bridge
        internal: {{networks.internal}}
        ipam:
            config:
                - subnet: {{networks.subnet}}
{%- endif %}
