class ObjectBuffer(object):
    """Base class for an object buffer."""

    def  __init__(self, obj, extension):
        """Create a new `ObjectBuffer`

        Args:
            obj (object): The object instance.
            extension (string): The extension type.
        """
        self._obj = obj
        self._extension = extension

    @property
    def obj(self):
        """object: The object instance."""
        return self._obj

    @property
    def extension(self):
        """string: The extension type."""
        return self._extension


class ImageBuffer(ObjectBuffer):
    """Object buffer for a image."""

    def __init__(self, image, extension = 'jpg'):
        """Create a new `ImageBuffer`

        Args:
            image (numpy `ndarray`): The image instance.
            extension (string): The extension type. Defaults to "jpg".
        """
        super().__init__(image, extension)

    @property
    def image(self):
        """object: The image instance."""
        return self._obj


class JsonBuffer(ObjectBuffer):
    """Object buffer for a JSON."""

    def __init__(self, json_val):
        """Create a new `JsonBuffer`

        Args:
            json_val (json-like object): The value of json. The type is
                json-like, including dict, list, tuple, string, int, float,
                int- & float-derived Enums, True, False and None.
        """
        super().__init__(json_val, "json")

    @property
    def json_val(self):
        """object: The value of json."""
        return self._obj


class FileBuffer(ObjectBuffer):
    """Object buffer for a file from file system."""

    def __init__(self, file_path, extension):
        """Create a new `FileBuffer`

        Args:
            file_path (string): The path to the file.
            extension (string): The extension type of the file.
        """
        super().__init__(file_path, extension)

    @property
    def file_path(self):
        """object: The path to the file."""
        return self._obj
