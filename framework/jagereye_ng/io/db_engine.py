import traceback
import sys

import mongoengine as mongo
from pymongo.errors import PyMongoError


_TYPE_FIELD_MAP = {
    "string": mongo.StringField,
    "number": mongo.FloatField,
    "date": mongo.DateTimeField,
    "boolean": mongo.BooleanField,
    "object": mongo.DictField,
    "objectid": mongo.ObjectIdField,
    "reference": mongo.ReferenceField,
}

_INDEX_OPTIONS = [
    "sparse",
    "unique",
    "expireAfterSeconds",
    "name",
    "collation",
]


class InvalidSchemaError(Exception):
    pass


class DatabaseError(Exception):
    pass


class NonExistentIdError(DatabaseError):
    pass


class NonUpdatableError(DatabaseError):
    pass


class _Schema_Transformer(object):
    """Inner class for schema transformation.

    The class transforms from Mongoose style to MongoEngine style.
    """
    def _gen_class_name(self, original_name, embedded=False):
        """Generate a class name for MongoEngine Document.

        Args:
            original_name (string): The original class name
            embedded (bool): The class is embedded or not. Default to False.

        Returns:
            string: The generated class name from the original name
        """
        titled = original_name.title()
        filterd = "".join([c for c in titled if c.isalpha() or c.isnumeric()])
        postfix = "EmbeddedDocument" if embedded else "Document"
        class_name = "{}{}".format(filterd, postfix)

        return class_name

    def _create_embedded_field(self, field_name="", sub_fields={}, **kwargs):
        """Create a embedded field.

        Args:
            field_name (string): The field name. Defaults to field_name.
            sub_fields (dict): The sub-fields for this embedded field. Defaults
                to {}.
            **kwargs: The keyword arguments that will be fed to
                `mongoengine.EmbeddedDocumentField`.

        Returns:
            `mongoengine.EmbeddedDocumentField`: The embedded field.
        """
        class_name = self._gen_class_name(field_name, embedded=True)
        EmbeddedDoc = type(class_name, (mongo.EmbeddedDocument,), sub_fields)
        field = mongo.EmbeddedDocumentField(EmbeddedDoc, **kwargs)

        return field

    def _to_primitive_field(self, field_name, schema_type, schema_dict={}):
        """Transform a primitive field from Mongoose style to MongoEngine style.

        Args:
            field_name (string): The field name.
            schema_type (string): The Mongoose schema type.
            schema_dict (dict): The Mongoose schema type options.

        Returns:
            `mongoengine.BaseField`: The MongoEngine type field.

        Raises:
            `InvalidSchemaError`: If the schema type is not supported.
        """
        type_name = schema_type.lower()

        kwargs = {
            "required": schema_dict.get("required", False),
            "default": schema_dict.get("default", None),
            "unique": schema_dict.get("unique", False),
        }

        if type_name not in _TYPE_FIELD_MAP:
            raise InvalidSchemaError("Non-Supported schema type: {}"
                                         .format(schema_type))
        if type_name == "string":
            # TODO(Su JiaKuan): Mongoose supports "trim", "lowercase" and
            # "uppercase" for string type, but native mongoengine does not.
            kwargs["regex"] = schema_dict.get("regex", None)
            kwargs["min_length"] = schema_dict.get("minlength", None)
            kwargs["max_length"] = schema_dict.get("maxlength", None)
            kwargs["choices"] = schema_dict.get("enum", None)
        elif type_name == "number":
            kwargs["min_value"] = schema_dict.get("min", None)
            kwargs["max_value"] = schema_dict.get("max", None)
        elif type_name == "date":
            # TODO(Su JiaKuan): Mongoose supports "min" and "max" for date
            # type, but native mongoengine does not.
            pass
        elif type_name == "objectid":
            ref = schema_dict.get("ref", None)
            if ref is not None:
                return _TYPE_FIELD_MAP["reference"](ref)

        return _TYPE_FIELD_MAP[type_name](**kwargs)

    def _transform_compound_indexes(self, indexes_desc, parent_field_name=None):
        """Transform compound indexes from Mongoose style to MongoEngine style.

        Args:
            indexes_desc (dict): The Mongose style indexes description.
            parent_field_name (string): The field name of parent. Defaults to
                None.

        Returns:
            dict: The transformed MongoEngine style indexes.

        Raises:
            `InvalidSchemaError`: If the schema field value is not supported.
        """
        compound_indexes = {
            "fields": [],
        }

        for index_name, index_value in indexes_desc["fields"].items():
            if index_value is False:
                continue
            elif index_value is True:
                prefix = ""
            elif index_value is 1:
                prefix = "+"
            elif index_value is -1:
                prefix = "-"
            else:
                raise InvalidSchemaError("Index value of '{}' is not True, "
                                         "False, 1 or -1".format(index_value))

            postfix = "{}.".format(parent_field_name) \
                      if parent_field_name is not None else ""
            field_name = "{}{}{}".format(prefix, postfix, index_name)

            compound_indexes["fields"].append(field_name)

        if "options" in indexes_desc:
            options = indexes_desc["options"]
            for index_option in _INDEX_OPTIONS:
                if index_option in options:
                    compound_indexes[index_option] = options[index_option]

        return compound_indexes

    def _transform_schema_field(self, field_name, field_value):
        """Transform a schema field from Mongoose style to MongoEngine style.

        Args:
            field_name (string): The schema field name.
            field_value (string, dict, list): The schema field value.

        Returns:
            `mongoengine.BaseField`: The MongoEngine type field.

        Raises:
            `InvalidSchemaError`: If the field value is not a string,
                dictionary or a one-length list.
        """
        indexes = []

        if isinstance(field_value, str):
            # If the field value is a string, then it is the type name.
            #
            # [Example]
            # Original field:
            # "number"
            # Transformed field:
            # mongoengine.FloatField()
            field = self._to_primitive_field(field_name, field_value)
        elif isinstance(field_value, dict):
            # If the field value is a dictionary, then it has 2 possible
            # choices.
            #
            # The first choice is it contains a "type" field and the "type" field
            # is a string.
            #
            # [Example]
            # Original schema:
            # { "type": "number" }
            # Transformed schema:
            # mongoengine.FloatField()
            #
            # The other choice is it an object.
            #
            # [Example]
            # Original schema:
            # {
            #   "type": { "type": "number" },
            #   "content": "string"
            # }
            # Transformed schema:
            # mongoengine.EmbeddedDocumentField(mongo.EmbeddedDocument(
            #   content=mongoengine.StringField()
            # ))
            is_leaf = ("type" in field_value and
                       isinstance(field_value["type"], str))

            if is_leaf:
                field = self._to_primitive_field(
                    field_name,
                    field_value["type"],
                    schema_dict=field_value,
                )

                with_index = ("index" in field_value and
                              field_value["index"] is True)
                if with_index:
                    indexes.append(field_name)
            else:
                sub_fields = {}
                for key, value in field_value.items():
                    if key == "DISCRIMINATOR":
                        continue
                    if key == "INDEX":
                        compound_indexes = self._transform_compound_indexes(
                            value,
                            parent_field_name=field_name,
                        )
                        indexes.append(compound_indexes)
                        continue

                    sub_field_name = "{}.{}".format(field_name, key)
                    sub_field_value = value
                    sub_field, sub_indexes = self._transform_schema_field(
                        sub_field_name,
                        sub_field_value,
                    )

                    sub_fields[key] = sub_field
                    indexes += sub_indexes

                field = self._create_embedded_field(field_name=field_name,
                                                    sub_fields=sub_fields)
        elif isinstance(field_value, list):
            # If the field value is a list, then it is array type.
            #
            # [Example]
            # Original schema:
            # [{ "type": "number" }]
            # Transformed schema:
            # mongoengine.ListField(mongoengine.FloatField())
            if len(field_value) is not 1:
                raise InvalidSchemaError("The list value length of '{}' is not"
                                         " 1".format(field_name))

            inside_field_name = field_name
            inside_field_value = field_value[0]
            inside_field, inside_indexes = self._transform_schema_field(
                inside_field_name,
                inside_field_value,
            )

            field = mongo.ListField(inside_field)
            indexes += inside_indexes
        else:
            raise InvalidSchemaError("The value of '{}' is not a string, "
                                     "dictionary or list".format(field_name))
        return field, indexes

    def _transform_schema(self, schema):
        """Transform a schema from Mongoose style to MongoEngine style.

        Args:
            schema (dict): The Mongoose style schema.

        Returns:
            `mongoengine.BaseField`: The MongoEngine type field.

        Raises:
            `InvalidSchemaError`: If the schema is not a dictionary.
        """
        # Schema must be a dictionary.
        if not isinstance(schema, dict):
            raise InvalidSchemaError("Schema must be a dict, but found: {}"
                                      .format(schema))

        fields = {}
        indexes = []

        for field_name, field_value in schema.items():
            if field_name == "ONE_SHOT":
                continue
            if field_name == "PROJECTIONS":
                continue
            if field_name == "DISCRIMINATOR":
                continue
            if field_name == "INDEX":
                compound_indexes = self._transform_compound_indexes(field_value)
                indexes.append(compound_indexes)
                continue

            field, field_indexes = self._transform_schema_field(field_name,
                                                                field_value)
            fields[field_name] = field
            indexes += field_indexes

        return fields, indexes

    def create_document(self,
                        collection_name,
                        schema):
        """Define a MongoDB document format for a collection.

        Args:
            collection_name (string): The collection name.
            schema (dict): The Mongoose style schema.

        Returns:
            `mongoengine.Document`: The document definition class.

        Raises:
            `InvalidSchemaError`: If the schema is not valid.
        """
        fields, indexes = self._transform_schema(schema)

        attributes = {
            "meta": {
                "collection": collection_name,
                "allow_inheritance": "DISCRIMINATOR" in schema,
                "index_cls": False,
                "indexes": indexes,
            },
            **fields,
        }

        class_name = self._gen_class_name(collection_name)

        Doc = type(class_name, (mongo.Document,), attributes)

        return Doc

    def create_discriminator(self, BaseDoc, schema, distinct_name):
        """Define a MongoDB document format based on a base document defintion.

        Args:
            BaseDoc (`mongoengine.Document`): The base document definition
                class.
            schema (dict): The Mongoose style schema.
            distinct_name (string): The name to be distinc from the base
                definition.

        Returns:
            `mongoengine.Document`: The document definition class.

        Raises:
            `InvalidSchemaError`: If the schema is not valid.
        """
        fields, indexes = self._transform_schema(schema)

        indexes += BaseDoc._meta["indexes"]

        attributes = {
            "meta": {
                "index_cls": False,
                "indexes": indexes,
            },
            **fields,
        }

        collection_name = BaseDoc._meta["collection"]
        original_name = "{}.{}".format(collection_name, distinct_name)
        class_name = self._gen_class_name(original_name)

        Doc = type(class_name, (BaseDoc,), attributes)

        return Doc


def connect_db(db_name, host=None, authentication_source=None):
    """Connect to a MongoDB database.

    Args:
        db_name (string): The database name for connection.
        host (string): The host URL for connection. Defaults to None.
        authentication_source (string): Database to authenticate against.
            Defaults to None.

    Returns:
        (`pymongo.MongoClient`, `pymongo.database.Database`): The MongoDB
            client and database instance.
    """
    try:
        client = mongo.connect(db=db_name,
                               host=host,
                               authentication_source=authentication_source)
        db = mongo.connection.get_db()
    except PyMongoError as e:
        raise DatabaseError(e)
    except mongo.MongoEngineConnectionError as e:
        raise DatabaseError(e)

    return client, db


def define_document(collection_name, schema):
    """Define a MongoDB document format for a collection.

    The document defintion is a Document class in MongoEngine. It provides an
    ORM-like interface for document reading, creation, updating, deleting and
    validation.

    The document only supports Mongoose style schema.

    Args:
        collection_name (string): The collection name.
        schema (dict): The Mongoose style schema.

    Returns:
        `mongoengine.Document`: The document definition class.

    Raises:
        `InvalidSchemaError`: If the schema is not valid.
    """
    transformer = _Schema_Transformer()
    Doc = transformer.create_document(collection_name, schema)

    return Doc


def define_discriminator(BaseDoc, schema, distinct_name):
    """Define a MongoDB document format based on a base document defintion.

    The document defintion is a Document class in MongoEngine. It provides an
    ORM-like interface for document reading, creation, updating, deleting and
    validation.

    The document only supports Mongoose style schema.

    Args:
        BaseDoc (`mongoengine.Document`): The base document definition class.
        schema (dict): The Mongoose style schema.
        distinct_name (string): The name to be distinc from the base definition.

    Returns:
        `mongoengine.Document`: The document definition class.

    Raises:
        `InvalidSchemaError`: If the schema is not valid.
    """
    transformer = _Schema_Transformer()
    Doc = transformer.create_discriminator(BaseDoc,
                                           schema,
                                           distinct_name)

    return Doc
