import numpy as np
import gi
import threading
from collections import deque
import time

gi.require_version('Gst', '1.0')
gi.require_version('GstRtspServer', '1.0')
from gi.repository import Gst, GstRtspServer, GObject
from jagereye_ng.util import logging
from jagereye_ng.util.generic import get_config

class StreamFactory(GstRtspServer.RTSPMediaFactory):
    def __init__(self, endpoint, pipe, fps=15, width=1920, height=1080, queue_size=5):
        super().__init__()
        self._queue = deque(maxlen=queue_size)
        self._endpoint = endpoint
        self._pipe = pipe
        self._fps = fps
        self._width = width
        self._height = height
        self._num_frames = {}
        self._blankframe = np.zeros((width, height, 3), np.uint8).tostring()
        self._latest_frame = self._blankframe
        self._event = threading.Event()
        self._event.clear()
        self._reader_thread = threading.Thread(target=self._receive_frame,
                                               daemon=True)
        self._reader_thread.start()
        self.duration = 1 / self._fps * Gst.SECOND  # duration of a frame in nanoseconds
        self.launch_string = 'appsrc name=source is-live=true block=true format=GST_FORMAT_TIME ' \
                             'caps=video/x-raw,format=BGR,width={},height={},framerate={}/1 ' \
                             '! videoconvert ! video/x-raw,format=(string)I420 ' \
                             '! x264enc noise-reduction=10000 speed-preset=ultrafast tune=zerolatency ' \
                             '! rtph264pay config-interval=1 name=pay0 pt=96'.format(width, height, self._fps)

    def _receive_frame(self):
        while True:
            if self._event.is_set():
                break
            try:
                if self._pipe.poll(1): # add timeout to release cpu resource
                    frame = self._pipe.recv() # blocking
                    self._queue.appendleft(frame)
            except (OSError, IOError, EOFError):
                '''
                Raises EOFError if there is nothing left to receive and the
                other end was closed.
                from https://docs.python.org/3.6/library/multiprocessing.html#multiprocessing.connection.Connection.recv
                '''
                logging.warn("The pipeline stream output frames to RTSP "
                             "server endpoint {} communication object "
                             "multiprocessing.Pipe has been "
                             "closed, Please check if there's something "
                             "wrong happens on the RTSP client".format(
                                  self._endpoint))
                break

    def on_need_data(self, src, length):
        if self._queue:
            data = self._queue.pop().tostring()
            self._latest_frame = data
        else:
            logging.debug("Out of video frame")
            data = self._latest_frame

        # keep record of each src of their frame counter, default value is 0
        num_frames = self._num_frames.get(src, 0)

        # prepare the frame
        buf = Gst.Buffer.new_allocate(None, len(data), None)
        buf.fill(0, data)
        buf.duration = self.duration
        timestamp = num_frames * self.duration
        buf.pts = buf.dts = int(timestamp)
        buf.offset = timestamp

        num_frames += 1
        self._num_frames[src] = num_frames

        # push frame to pipeline
        retval = src.emit('push-buffer', buf)
        if retval != Gst.FlowReturn.OK:
            logging.info(retval)
            self.on_eos(src)

    def on_eos(self, src):
        # end-of-stream signal receviced, remove the counter
        if src in self._num_frames:
            del self._num_frames[src]

    def do_create_element(self, url):
        return Gst.parse_launch(self.launch_string)

    def do_configure(self, rtsp_media):
        appsrc = rtsp_media.get_element().get_child_by_name('source')
        appsrc.connect('need-data', self.on_need_data)
        appsrc.connect('end-of-stream', self.on_eos)

    def release(self):
        self._event.set()
        self._queue.clear()

class GstServer(GstRtspServer.RTSPServer):
    def __init__(self):
        super().__init__()
        self.factory = {}
        rtsp_port = get_config()["apps"]["base"]["ports"]["rtsp"]
        self.set_service(rtsp_port)
        self.attach(None)

    def add_stream(self, pipe, endpoint, fps=15, width=1920, height=1080,
                   queue_size=5):
        self._remove_stream(endpoint)
        logging.info("adding RTSP endpoint: {}".format(endpoint))
        self.factory[endpoint] = StreamFactory(endpoint, pipe, fps,
                                               width, height, queue_size)
        self.factory[endpoint].set_shared(True)
        self.get_mount_points().add_factory("/" + endpoint,
                                            self.factory[endpoint])
    def remove_stream(self, endpoint):
        logging.info("removing RTSP endpoint: {}".format(endpoint))
        self._remove_stream(endpoint)

    def _remove_stream(self, endpoint):
        self.get_mount_points().remove_factory("/" + endpoint)
        if endpoint in self.factory:
            self.factory[endpoint].release()
        self.factory.pop(endpoint, None)
