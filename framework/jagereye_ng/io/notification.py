from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import datetime
import asyncio
from pytz import timezone
from dask.distributed import get_client, get_worker
from jagereye_ng import logging
from jagereye_ng.util.generic import get_config


def _push(category, message):
    worker = get_worker()
    assert hasattr(worker, "je_notification"), ("IO_WORKER have not been "
                                                "established yet.")
    asyncio.run_coroutine_threadsafe(
        worker.je_notification.push(category, message), worker.je_io_loop)


class Notification(object):
    CATEGORY_ANALYZER = "analyzer"

    def __init__(self):
        config = get_config()["apps"]["dask"]
        address = config["cluster_ip"] + ":" + config["scheduler_port"]
        try:
            # Get Dask client
            self._client = get_client(address=address)
        except ValueError:
            assert False, ("Should connect to Dask scheduler before"
                           " initializing this object.")

    def __del__(self):
        self._client.close()

    def push(self, category, message):
        """Push a real-time notifcation.

        Args:
            category (string): The notification category.
            message (dictr: The notification message.
        """
        def done_callback(future):
            if future.exception() is not None:
                logging.error("Failed to push notification: ({}, {}), error: {}"
                              .format(category, message, future.exception()))
                import traceback
                tb = future.traceback()
                traceback.format_tb(tb)

        future = self._client.submit(_push, category, message,
                                     resources={"IO": 1})
        future.add_done_callback(done_callback)

    def push_anal(self, event_id, anal_id, timestamp, event_type, content, notification=None):
        """Push a real-time notifcation whose category is "analyzer".

        Args:
            event_id (string, `bson.objectid.ObjectId`): The event ID.
            anal_id (string, `bson.objectid.ObjectId`): The analyzer ID that
                the event belongs to.
            timestamp (float): The event timestamp.
            event_type (string): The event type.
            content (dict): The event content according to the event type.
            notification (dict): Defines notification type and content.
        """

        # TODO: This is a temporary workaround for notification. In the future, we will
        # add more notification channels like slack or other apps. To serve different usage,
        # we should let apps decide how and what to notify. So this function is merely a
        # message proxy.
        message = {
            "_id": str(event_id),
            "analyzerId": str(anal_id),
            "timestamp": timestamp,
            "type": event_type,
            "content": content,
            "notification": notification,
        }

        # Construct date string from timestamp.
        mlsec = repr(timestamp).split(".")[1][:3]
        date_str = (datetime.datetime
                    .utcfromtimestamp(timestamp)
                    .replace(tzinfo=timezone("UTC"))
                    .strftime("%Y-%m-%dT%H:%M:%S.{}Z".format(mlsec)))
        message.update({"date": date_str})

        self.push(Notification.CATEGORY_ANALYZER, message)
