from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import asyncio
import datetime
import json
import os
import time
from bson.objectid import ObjectId
from mongoengine import DoesNotExist
from dask.distributed import get_worker
from nats.aio.client import Client as NATS
from nats.aio.errors import ErrConnectionClosed, ErrTimeout, ErrNoServers
from pytz import timezone

from jagereye_ng import logging
from jagereye_ng.io.db_engine import connect_db
from jagereye_ng.io.const import ONE_SHOT_STATUS
from jagereye_ng.io.db_engine import define_document
from jagereye_ng.io.db_engine import define_discriminator
from jagereye_ng.io.db_engine import DatabaseError
from jagereye_ng.io.db_engine import InvalidSchemaError
from jagereye_ng.io.db_engine import NonExistentIdError
from jagereye_ng.io.db_engine import NonUpdatableError
from jagereye_ng.io.obj_buffer import FileBuffer
from jagereye_ng.io.obj_buffer import ImageBuffer
from jagereye_ng.io.obj_buffer import JsonBuffer
from jagereye_ng.io.obj_buffer import ObjectBuffer
from jagereye_ng.io.obj_storage import ObjectStorageClient
from jagereye_ng.util.generic import get_config, get_messaging
from jagereye_ng.util.generic import get_static_path
from jagereye_ng.util.generic import nested_set
from jagereye_ng.util.generic import uuid

NATS_CHANNEL_NAME = get_messaging()["channels"]["NOTIFICATION"]

# Expired may contain files that will be saved in the object storage and the
# locations must be saved in the database at the same time. We need a
# all-or-nothing transaction mechanism to create/update/delete externals. We
# set a expiration period for every transaction. If the expiration service sees
# records that exceed the expiration period, then it will treat them as failed
# transactions and delete them (and their related files in the object storage).
# The expiration period is 600 seconds.
TRANSACTION_EXPIRED_PERIOD = 600


class _ParsedBuffer(object):
    def __init__(self, keys, buffer, obj_key_prefix=""):
        is_valid_type = (
            isinstance(buffer, FileBuffer) or
            isinstance(buffer, ImageBuffer) or
            isinstance(buffer, JsonBuffer)
        )

        if not is_valid_type:
            raise TypeError("Non-supported type for buffer: {}".format(buffer))

        self._keys = keys
        self._buffer = buffer
        self._obj_key = os.path.join(
            obj_key_prefix,
            "{}.{}".format(uuid(), buffer.extension),
        )

    @property
    def keys(self):
        return self._keys

    @property
    def buffer(self):
        return self._buffer

    @property
    def obj_key(self):
        return self._obj_key


class Database(object):
    def __init__(self, db_name, host=None, authentication_source=None):
        try:
            self._client, self._db = connect_db(
                db_name,
                host=host,
                authentication_source=authentication_source,
            )
        except DatabaseError as e:
            logging.error("Database error occured while connecting: {}"
                          .format(str(e)))
            raise

        try:
            self._obj_storage = ObjectStorageClient()
            self._obj_storage.connect()
        except Exception as e:
            logging.error("Fail to connect to object storage: {}"
                          .format(str(e)))
            raise

        # Load schemas.
        self._db_schema = self._load_schema("database.json")
        self._externals_schema = self._load_schema("externals.json")

        # Get all available external types.
        self._external_types = list(self._externals_schema.keys())

        try:
            # Create "Document" class for each collection.
            self._Docs = {
                "events": define_document("events", self._db_schema["events"]),
                "externals": define_document("externals",
                                             self._db_schema["externals"]),
                "external_types": {},
            }
            for name, inner_schema in self._externals_schema.items():
                inner_schema_cp = inner_schema.copy()
                inner_schema_cp.pop("ONE_SHOT", None)
                inner_schema_cp.pop("PROJECTIONS", None)
                schema = {
                    "content": inner_schema_cp,
                }
                self._Docs["external_types"][name] = define_discriminator(
                    self._Docs["externals"],
                    schema,
                    name,
                )
        except InvalidSchemaError as e:
            logging.error("Invalid schema: {}".format(str(e)))
            raise

        self._projections = {
            "externals": [
                "task.status",
                "task.reason",
                "type",
                "group",
                "content",
                "createdAt",
                "updatedAt",
            ],
        }

    def cleanup(self):
        logging.info("Destroying Database service")
        self._client.close()

    def __del__(self):
        self.cleanup()

    def _load_schema(self, filename):
        schema_url = get_static_path(filename)

        with open(schema_url) as f:
            schema = json.load(f)

        return schema

    def _plain_id(self, obj):
        if "_id" not in obj:
            return obj
        else:
            new_obj = obj.copy()
            new_obj["_id"] = str(obj["_id"])
            return new_obj

    def _plain_ids(self, objs):
        return [self._plain_id(obj) for obj in objs]

    def _save_buffer(self, parsed_buffer):
        key = parsed_buffer.obj_key
        buffer = parsed_buffer.buffer
        obj = buffer.obj

        if isinstance(buffer, FileBuffer):
            self._obj_storage.save_file_obj(key, obj)
        elif isinstance(buffer, ImageBuffer):
            self._obj_storage.save_image_obj(key, obj)
        elif isinstance(buffer, JsonBuffer):
            self._obj_storage.save_json_obj(key, obj)

    def _parse_buffers(self, content, obj_key_prefix=""):
        buffers = []

        if isinstance(content, dict):
            for key, value in content.items():
                sub_buffers = self._parse_buffers(
                    value,
                    obj_key_prefix=obj_key_prefix,
                )
                for sub_buffer in sub_buffers:
                    sub_buffer.keys.insert(0, key)
                    buffers.append(sub_buffer)
        elif isinstance(content, ObjectBuffer):
            buffers.append(
                _ParsedBuffer([], content, obj_key_prefix=obj_key_prefix),
            )

        return buffers

    def _create_task_field(self, external_type):
        name = self._externals_schema[external_type].get("ONE_SHOT", None)
        task = {
            "name": name,
            "createdAt": time.time(),
            "status": ONE_SHOT_STATUS.PENDING
                      if name is not None else ONE_SHOT_STATUS.DONE,
        }

        return task

    def _save_event(self, anal_id, timestamp, event_type, content):
        event_doc = self._Docs["events"](analyzerId=anal_id,
                                         timestamp=timestamp,
                                         type=event_type,
                                         content=content)
        event_doc.save()

        return str(event_doc.id)

    def _save_external_db(self,
                          task,
                          external_type,
                          content,
                          group=None,
                          expired=False,
                          expired_at=None,
                          reference=None):
        now_sec = time.time()

        Doc = self._Docs["external_types"][external_type]
        external_doc = Doc(
            expired=expired,
            expiredAt=expired_at,
            reference=reference,
            createdAt=now_sec,
            updatedAt=now_sec,
            task=task,
            type=external_type,
            content=content,
            group=group,
        )
        # MongoEngine saves "_cls" field automatically. We don't need this
        # field, so specify it to an empty string forcibly.
        external_doc._cls = ""
        external_doc.save()

        return external_doc

    def _save_tmp_external(self,
                           task,
                           external_type,
                           content,
                           group=None,
                           reference=None):
        expired_at = time.time() + TRANSACTION_EXPIRED_PERIOD
        external_doc = self._save_external_db(
            task,
            external_type,
            content,
            group=group,
            expired=True,
            expired_at=expired_at,
            reference=reference,
        )

        return external_doc

    def _save_external(self, external_type, content, group=None):
        if external_type not in self._external_types:
            raise ValueError("External type '{}' is not in supported list: "
                             "{}".format(external_type, self._external_types))

        task = self._create_task_field(external_type)

        # Parse buffers from content.
        parsed_buffers = self._parse_buffers(content,
                                             obj_key_prefix=external_type)

        if not parsed_buffers:
            external_doc = self._save_external_db(task,
                                                  external_type,
                                                  content,
                                                  group=group)
        else:
            # Set content keys and values for buffers.
            saved_content = content.copy()
            for parsed_buffer in parsed_buffers:
                nested_set(saved_content,
                           parsed_buffer.keys,
                           parsed_buffer.obj_key)

            # Save a external record with expiration tag and timestamp in the
            # database.
            external_doc = self._save_tmp_external(task,
                                                   external_type,
                                                   saved_content,
                                                   group=group)

            # Save buffers to the object storage.
            for parsed_buffer in parsed_buffers:
                self._save_buffer(parsed_buffer)

            # Unset the expiration tag and timestamp because the transaction
            # is success.
            external_doc.expired = None
            external_doc.expiredAt = None
            external_doc.save()

        return str(external_doc.id)

    def _get_external(self, external_id, projected=True):
        projection = self._projections["externals"] if projected else None
        result = self._db["externals"].find_one({
            "_id": ObjectId(external_id),
            "expired": False,
        }, projection=projection)

        if result is None:
            raise NonExistentIdError("External ID not found: {}"
                                     .format(external_id))

        return self._plain_id(result)

    def _query_externals(self, query):
        search = {
            "expired": False,
        }

        for key, value in query.items():
            if key == "_ids":
                query_ids = [ObjectId(x) for x in query["_ids"]]
                search["_id"] = {
                    "$in": query_ids,
                }
            elif key == "task_status":
                task_status_type = type(value)
                if task_status_type == str:
                    search["task.status"] = value
                elif task_status_type == list:
                    search["task.status"] = {
                        "$in": value,
                    }
                else:
                    raise TypeError("Invalid task status type for external"
                                    " query: {}".format(value))
            elif key == "type":
                search["type"] = value
            elif key == "group":
                group_type = type(value)
                if group_type == str:
                    search["group"] = ObjectId(value)
                elif group_type == list:
                    groups = [ObjectId(v) for v in value]
                    search["group"] = {
                        "$in": groups,
                    }
                elif group_type == ObjectId:
                    pass
                else:
                    raise TypeError("Invalid group type for external query: {}"
                                    .format(value))
            elif key.startswith("content."):
                search[key] = value

        result = list(self._db["externals"].find(
            search,
            projection=self._projections["externals"],
        ))

        return self._plain_ids(result)

    def _update_external(self, external_id, group=None, content=None):
        should_update = group is not None or content is not None

        if not should_update:
            return

        external = self._get_external(external_id, projected=False)

        is_updatable = (external["task"]["status"] == ONE_SHOT_STATUS.DONE or
                        external["task"]["status"] == ONE_SHOT_STATUS.FAILED)

        if not is_updatable:
            raise NonUpdatableError("External is still in pending or "
                                    "processing")

        external_type = external["type"]
        old_task = external["task"]
        old_group = external.get("group", None)
        old_content = external["content"]
        new_task = self._create_task_field(external_type)
        new_group = old_group if group is None else group
        new_content = old_content if content is None else content

        # Parse buffers from content.
        parsed_buffers = self._parse_buffers(content,
                                             obj_key_prefix=external_type)

        if not parsed_buffers:
            if content is not None:
                # If no buffers but content updated, create a temporal record for
                # old external

                # Create a temporal record for old external that references to
                # the orignal one.
                self._save_tmp_external(old_task,
                                        external_type,
                                        old_content,
                                        group=old_group,
                                        reference=external_id)
        else:
            # If the request contains files, we need a transaction to update
            # both the database and the files.

            # Set content keys and values for buffers.
            for parsed_buffer in parsed_buffers:
                nested_set(new_content,
                           parsed_buffer.keys,
                           parsed_buffer.obj_key)

            # Create a temporal record for new external that references to the
            # orignal one.
            self._save_tmp_external(new_task,
                                    external_type,
                                    new_content,
                                    group=new_group,
                                    reference=external_id)

            # Save buffers to the object storage.
            for parsed_buffer in parsed_buffers:
                self._save_buffer(parsed_buffer)

            # Create a temporal record for old external that references to the
            # orignal one.
            self._save_tmp_external(old_task,
                                    external_type,
                                    old_content,
                                    group=old_group,
                                    reference=external_id)

        search = {
            "id": external_id,
            "expired": False,
            "class_check": False,
        }
        updated = {
            "set__updatedAt": time.time(),
            "set__task": new_task,
            "set__group": new_group,
            "set__content": new_content,
        }

        # Update the new external to the original one.
        Doc = self._Docs["external_types"][external_type]
        updated_count = Doc.objects(**search).update_one(**updated)

        if updated_count == 0:
            raise NonExistentIdError("External ID not found: {}"
                                     .format(external_id))

    def _delete_external(self, external_id):
        search = {
            "id": external_id,
            "expired": False,
            "class_check": False,
        }
        updated = {
            "expired": True,
            "expiredAt": time.time(),
        }

        deleted_count = self._Docs["externals"] \
                            .objects(**search) \
                            .update_one(**updated)

        if deleted_count == 0:
            raise NonExistentIdError("External ID not found: {}"
                                     .format(external_id))

    def _get_external_group(self, external_group_id):
        result = self._db["externals/groups"].find_one({
            "_id": ObjectId(external_group_id)
        })

        if result is None:
            raise NonExistentIdError("External group ID not found: {}"
                                     .format(external_group_id))

        return self._plain_id(result)

    def _get_analyzer(self, analyzer_id):
        result = self._db["analyzers"].find_one({
            "_id": ObjectId(analyzer_id)
        })

        if result is None:
            raise NonExistentIdError("Analyzer ID not found: {}"
                                     .format(analyzer_id))

        return self._plain_id(result)

    def execute(self, fn, *args, **kwargs):
        try:
            output = fn(*args, **kwargs)
        except Exception as e:
            logging.error("Failed to execute: {}, args: {}, kwargs: {}, error:"
                          " {}".format(fn.__name__, args, kwargs, e))
            raise

        logging.debug("Success to execute: {}, args: {}, kwargs: {}"
                      .format(fn.__name__, args, kwargs))

        return output

    def save_event(self, anal_id, timestamp, event_type, content):
        return self.execute(self._save_event,
                            anal_id,
                            timestamp,
                            event_type,
                            content)

    def save_external(self, external_type, content, group=None):
        return self.execute(self._save_external,
                            external_type,
                            content,
                            group=group)

    def get_external(self, external_id):
        return self.execute(self._get_external, external_id),

    def query_externals(self, query):
        return self.execute(self._query_externals, query),

    def update_external(self, external_id, group=None, content=None):
        return self.execute(self._update_external,
                            external_id,
                            group=group,
                            content=content)

    def delete_external(self, external_id):
        return self.execute(self._delete_external, external_id)

    def get_external_group(self, external_group_id):
        return self.execute(self._get_external_group, external_group_id),

    def get_analyzer(self, analyzer_id):
        return self.execute(self._get_analyzer, analyzer_id),

class Notification(object):
    def __init__(self, nats_hosts):
        self._nats = NATS()
        self._io_loop = asyncio.get_event_loop()
        try:
            asyncio.run_coroutine_threadsafe(self._initialize_nats(nats_hosts),
                                                    self._io_loop)
        except Exception as e:
            logging.error("NATS initialization failed with exception: "
                          "{!r}".format(e))
            raise

    async def _initialize_nats(self, nats_hosts):
        options = {
            "servers": nats_hosts,
            "io_loop": self._io_loop,
            "max_reconnect_attempts": 60,
            "reconnect_time_wait": 2,
            "disconnected_cb": self._nats_disconnected_cb,
            "reconnected_cb": self._nats_reconnected_cb,
            "error_cb": self._nats_error_cb,
            "closed_cb": self._nats_closed_cb
        }
        try:
            await self._nats.connect(**options)
            logging.info("NATS connection for Notification is established.")
        except ErrNoServers as e:
            logging.error(str(e))
            raise
        except Exception as e:
            logging.error(str(e))
            raise

    async def _nats_disconnected_cb(self):
        logging.info("[NATS] disconnected")

    async def _nats_reconnected_cb(self):
        logging.info("[NATS] reconnected")

    async def _nats_error_cb(self, e):
        logging.error("[NATS] ERROR: {}".format(e))

    async def _nats_closed_cb(self):
        logging.info("[NATS] connection is closed")

    def cleanup(self):
        logging.info("Destroying Notification service")
        self._nats.close()

    def __del__(self):
        self.cleanup()

    async def push(self, category, message):
        logging.info("Pushing notification: ({}: {})".format(category, message))
        try:
            await self._nats.publish(NATS_CHANNEL_NAME, json.dumps(
                {"category": category, "message": message}).encode())
        except ErrConnectionClosed:
            logging.error("Connection closed prematurely.")
            raise
        except ErrTimeout:
            logging.error("Timeout occurred when publishing"
                          " event: {}".format(event))
            raise
        except Exception as e:
            logging.error("Failed to publish notification: {}".format(e))
            raise

def init_worker():
    worker = get_worker()
    if not hasattr(worker, 'init') and hasattr(worker, "name") and worker.name.startswith("IO_WORKER"):
        logging.info("Initializing worker: {}".format(worker.name))

        # Initiralize notification service
        nats_config = get_config()["services"]["messaging"]
        worker.je_notification = Notification(["nats://"+nats_config["network"]["ip"]+":4222"])
        worker.je_io_loop = asyncio.get_event_loop()

        # Initialize database service
        dbconfig = get_config()["services"]["database"]
        dbURL = "mongodb://"+dbconfig['env']["username"]+":"+dbconfig['env']["password"]+"@"+dbconfig['network']['ip']+":27017"
        worker.je_database = Database(dbconfig["db_name"],
                                      host=dbURL,
                                      authentication_source="admin")

        worker.init = True
