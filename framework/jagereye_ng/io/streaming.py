from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os
import abc
import time
import threading
import signal
import cv2
import numpy as np
from queue import Queue
from collections import deque
from urllib.parse import urlparse

from jagereye_ng.util import logging


DEFAULT_STREAM_BUFFER_SIZE = 64     # frames

# The default threshold full percent of buffer is 70%
DEFAULT_STREAM_BUFFER_THRES_PERCENT = 70
DEFAULT_FPS = 15

# StreamReader connection robustness
MAX_WRONG_SIZE_TIME = 10
DEFAULT_READ_TIMEOUT = 5 # in second

TYPE_THREADING_LOCK = type(threading.Lock())

class ConnectionError(Exception):
    pass


class EndOfVideoError(Exception):
    pass


class TaskTimeoutError(Exception):
    pass


def _is_livestream(url):
    """Check whether the source is a livestream or not.

    We check the protocol of the source url to determine whether
    it's a livestream, since currently we only support "RTSP",
    source url which not starts with "rtsp://" would be consider
    as not an livestream source.

    Returns:
        True if it's a livestream source and false otherwise.
    """
    # XXX: Not a very robust way for checking the source protocol.
    return urlparse(url).scheme.lower() == "rtsp"


def run_with_timeout(timeout, func, *args, **kwargs):
    """Execute a task with a timeout.

    Args:
        timeout (float): The timeout value.
        func (function): The task function to be executed.

    Returns:
        The task result.

    Raises:
        An TaskTimeoutError exception if the execution of the task exceeded
        the timeout, otherwise, raises the task exceptions, if any.
    """

    class Job(threading.Thread):
        def __init__(self, func, *args, **kwargs):
            threading.Thread.__init__(self)
            self.daemon = True
            self._func = func
            self._args = args
            self._kwargs = kwargs
            self.result = None
            self.exception = None

        def run(self):
            try:
                self.result = self._func(*self._args, **self._kwargs)
            except Exception as e:
                self.exception = e

        def terminate(self):
            signal.pthread_kill(self.ident, 0)

    job = Job(func, *args, **kwargs)
    job.start()
    job.join(timeout=timeout)

    if job.is_alive():
        # job's timeout has happened, cancel it and notify the caller
        job.terminate()
        raise TaskTimeoutError("The task has exceeded the timeout.")
    elif job.exception is not None:
        raise job.exception
    else:
        return job.result


class VideoFrame(object):
    def __init__(self, image_raw, timestamp=None):
        self.image = image_raw
        if timestamp is None:
            self.timestamp = time.time()
        else:
            self.timestamp = timestamp


class StreamReaderThread(threading.Thread):
    def __init__(self,
                 reader,
                 queue,
                 lock,
                 width,
                 height,
                 src,
                 timeout,
                 stop_event,
                 cap_interval,
                 is_livestream):
        super().__init__()
        self._reader = reader
        self._shape = (height, width, 3)
        self._queue = queue
        self._lock = lock
        self._stop_event = stop_event
        self._cap_interval = cap_interval / 1000.0
        self._is_livestream = is_livestream
        self._exception = None
        self._blankframe = np.zeros(self._shape, dtype=np.uint8)
        self._src = src
        self._timeout = timeout
        self._is_retrying = False
        self._retry_thread = None
        self._wrong_size_time = 0

    def run(self):
        last_timestamp = time.time()

        while not self._stop_event.is_set():
            frame = None
            try:
                if isinstance(self._lock, TYPE_THREADING_LOCK):
                    self._lock.acquire()
                if not self._is_retrying:
                    # drop buffer in VideoCapture to reduce latency
                    run_with_timeout(DEFAULT_READ_TIMEOUT, self._reader.grab)
                    # TODO use individual thread pool to handle this
                    # run_with_timeout.
                    timestamp = time.time()
                    if (timestamp - last_timestamp) >= self._cap_interval:
                        # `VideoSyncMultiStreamReader._sync` need blank frame
                        frame = VideoFrame(self._blankframe, timestamp)

                        # read from stream source to fit capture interval
                        success, image = run_with_timeout(
                            DEFAULT_READ_TIMEOUT,
                            self._reader.read,
                        )
                        # TODO use individual thread pool to handle this
                        # run_with_timeout.
                        if success:
                            if image.shape == self._shape:
                                frame.image = image
                                last_timestamp = timestamp
                                self._exception = None
                            else:
                                self._wrong_size_time += 1
                                if self._wrong_size_time > MAX_WRONG_SIZE_TIME:
                                    # TODO add last wrong size time check
                                    self._go_retry()
                        else:
                            self._go_retry()

            except Exception as ex:
                logging.info('Reader thread {} exception: {}'.format(self._src, str(ex)))
                if not self._is_retrying:
                    self._go_retry()
            finally:
                if frame is not None:
                    self._queue.appendleft(frame)
                if isinstance(self._lock, TYPE_THREADING_LOCK):
                    self._lock.release()
            time.sleep(0.01)

    def _go_retry(self):
        if self._is_livestream:
            self._exception = \
                VideoStreamReaderBasic.ERROR_CONNECTION
        else:
            self._exception = \
                VideoStreamReaderBasic.ERROR_ENDOFVIDEO
        self._wrong_size_time = 0
        self._is_retrying = True
        self._retry_thread = threading.Thread(target=self._retry)
        self._retry_thread.daemon = True
        self._retry_thread.start()

    def _retry(self):
        try:
            logging.info('Reader thread {} with retry start'.format(self._src))
            run_with_timeout(self._timeout, self._reader.open, self._src)
        except Exception as ex1:
            logging.info('Reader thread {} with retry exception {}'.format(
                            self._src, str(ex1)))
        finally:
            self._is_retrying = False
            logging.info('Reader thread {} with retry finish'.format(self._src))

    def get_exception(self):
        return self._exception

class VideoStreamReaderBasic(metaclass=abc.ABCMeta):
    STATUS_SRC_UP = 'up'
    STATUS_SRC_DOWN = 'down'
    ERROR_CONNECTION = ''
    ERROR_ENDOFVIDEO = ''
    ERROR_TASKTIMEOUTERROR = ''
    @abc.abstractmethod
    def open(self):
        return notimplemented
    @abc.abstractmethod
    def release(self):
        return notimplemented
    @abc.abstractmethod
    def get_video_info(self):
        return notimplemented
    @abc.abstractmethod
    def is_busy(self):
        return notimplemented
    @abc.abstractmethod
    def read(self):
        return notimplemented

class VideoSyncMultiStreamReader(VideoStreamReaderBasic):
    """The synchronized multiple video stream reader.

    First this create reader for every video stream source.
    The reader to read frames from a video stream source. The source can be a
    video file or a live stream such as RTSP, Motion JPEG. Each captured frame
    will be stored as a VideoFrame object with an image tensor and the
    timestamp of which the frame been captured.

    There is another thread keep checking sync frames from different video
    stream sources.

    The "image" tensor is a 3-dimensional numpy `ndarray` whose type is uint8
    and the shape format is:
    1. Image height.
    2. Image width.
    3. Number of channels, which is usually 3.

    The "timestamp" tensor is a 0-dimensional numpy `ndarray` whose type is
    string.
    """

    def __init__(self, src, fps=DEFAULT_FPS,
                    buffer_size=DEFAULT_STREAM_BUFFER_SIZE):
        """Initialize a `VideoSyncMultiStreamReader` object.

        Args:
            src (string): video stream source
            fps (int): streaming fps
            buffer_size (int): The maximum size to buffering the video stream.
        """

        self._src = src
        self._num_source = len(self._src)
        self._status_source = [VideoStreamReaderBasic.STATUS_SRC_DOWN
                                for _ in range(self._num_source)]
        self._readers = [cv2.VideoCapture() for _ in range(self._num_source)]
        self._thread = [None for _ in range(self._num_source)]
        self._lock = [threading.Lock() for _ in range(self._num_source)]
        self._fps = fps
        self._sync_wait_time = 0.001
        self._sync_interval = 2 / fps
        self._frame_queue = [deque(maxlen=buffer_size)
                                for _ in range(self._num_source)]
        self._stop_event = [threading.Event() for _ in range(self._num_source)]
        self._video_info = [{} for _ in range(self._num_source)]
    def open(self, timeout=15):
        """Open video streaming sources and create sync thread

        Args:
            timeout (int): open streaming connection timeout
        Returns:
            (list of string): video sources status
            (string): video stream error
        """
        # init status
        self._status_source = [VideoStreamReaderBasic.STATUS_SRC_DOWN
                                for i in range(self._num_source)]
        for i in range(self._num_source):
            logging.info("Opening video source: {}".format(self._src[i]))
            if self._readers[i].isOpened():
                self._status_source[i] = VideoStreamReaderBasic.STATUS_SRC_DOWN
                return self._status_source, \
                        VideoStreamReaderBasic.ERROR_CONNECTION

        # Open video source
        for i in range(self._num_source):
            try:
                run_with_timeout(timeout, self._readers[i].open, self._src[i])
            except:
                logging.error("Error occurred when opening video source"
                              " from {}".format(self._src[i]))
                self._status_source[i] = VideoStreamReaderBasic.STATUS_SRC_DOWN
                return self._status_source, \
                        VideoStreamReaderBasic.ERROR_CONNECTION

        capture_interval = 10 # in Millisecond
        # Advoid capture_interval greater than self._sync_interval
        assert capture_interval < 1000.0 * self._sync_interval
        for i in range(self._num_source):
            # Get video information
            success, image = self._readers[i].read()
            if not success:
                self._status_source[i] = \
                    VideoStreamReaderBasic.STATUS_SRC_DOWN
                return self._status_source, \
                        VideoStreamReaderBasic.ERROR_CONNECTION
            self._status_source[i] = VideoStreamReaderBasic.STATUS_SRC_UP

            height, width, _ = image.shape
            self._video_info[i]["frame_size"] = (width, height)
            # Start reader thread
            self._stop_event[i].clear()
            logging.info("Starting reader thread")
            self._thread[i] = StreamReaderThread(self._readers[i],
                                              self._frame_queue[i],
                                              self._lock[i],
                                              width,
                                              height,
                                              self._src[i],
                                              timeout,
                                              self._stop_event[i],
                                              capture_interval,
                                              _is_livestream(self._src[i]))
        for i in range(self._num_source):
            self._thread[i].daemon = True
            self._thread[i].start()
        return self._status_source, None

    def release(self):
        for i in range(self._num_source):
            self._stop_event[i].set()
            if hasattr(self, "_thread"):
                self._thread[i].join()
            self._readers[i].release()
            self._frame_queue[i].clear()

    def get_video_info(self):
        # TODO update this when developing stitching pipeline check
        return self._video_info[0]

    def _read(self, batch_size):
        sync_frames = []
        for _ in range(batch_size):
            sync_frame = self._sync()
            sync_frames.append(sync_frame)
        return list(map(list, zip(*sync_frames)))

    def is_busy(self):
        return False

    def read(self, batch_size=1):
        """The routine of synced multi video stream frames capturer capturation

        Returns:
            (list of list of `VideoFrame`): A list of synced VideoFrame objects
                from every streaming source. The length of the list is
                determined by the `len(_num_source)`, The second dimension
                length of the list is  `batch_size`.
            (list of string): video sources status
            (string): video stream error
        """
        count_err = 0
        for i in range(self._num_source):
            if self._thread[i].get_exception() is not None:
                self._status_source[i] = VideoStreamReaderBasic.STATUS_SRC_DOWN
                count_err += 1
        if count_err == self._num_source:
            return [[]], self._status_source, \
                    VideoStreamReaderBasic.ERROR_CONNECTION
        data = self._read(batch_size)
        return data, self._status_source, None

    def _newest_timestamp(self, frames):
        max_ts_id = 0
        max_ts = 0
        for i in range(len(frames)):
            if frames[i][1].timestamp > max_ts:
                max_ts = frames[i][1].timestamp
                max_ts_id = i
        return (max_ts_id, max_ts)

    def _sync(self):
        """Keep sync frame in queue from different sources"""
        for i in range(self._num_source):
            # check if queue from source is empty
            while True:
                if len(self._frame_queue[i]) != 0:
                    break
                time.sleep(self._sync_wait_time)
        # lock all source
        for i in range(self._num_source):
            self._lock[i].acquire()
        # try syncing frames
        frames = []
        cam_id_throw_frame = [i for i in range(self._num_source)]
        while True:
            # get last item from each queue
            for idx in cam_id_throw_frame:
                frame = self._frame_queue[idx].pop()
                frames.append((idx, frame))

            # find the newest timestamp
            max_ts_id, max_ts = self._newest_timestamp(frames)

            cam_id_throw_frame = []
            drop_index = []
            all_frames_synced = True
            for i in range(len(frames)):
                if max_ts - frames[i][1].timestamp > self._sync_interval:
                    if len(self._frame_queue[frames[i][0]]) > 0:
                        cam_id_throw_frame.append(frames[i][0])
                        drop_index.append(i)
                    all_frames_synced = False
            if all_frames_synced or len(cam_id_throw_frame) == 0:
                break
            # drop all item that is not in sync interval
            frames = [frames[i] for i in range(len(frames)) if i not in drop_index]

        # sort according to cam id
        sync_frames = sorted(frames, key=lambda x: x[0])
        sync_frames = [sync_frames[i][1] for i in range(len(sync_frames))]
        for i in range(self._num_source):
            self._lock[i].release()
        return sync_frames

class VideoStreamReader(VideoStreamReaderBasic):
    """The video stream reader.

    The reader to read frames from a video stream source. The source can be a
    video file or a live stream such as RTSP, Motion JPEG. Each captured frame
    will be stored as a VideoFrame object with an image tensor and the
    timestamp of which the frame been captured.

    The "image" tensor is a 3-dimensional numpy `ndarray` whose type is uint8
    and the shape format is:
    1. Image height.
    2. Image width.
    3. Number of channels, which is usually 3.

    The "timestamp" tensor is a 0-dimensional numpy `ndarray` whose type is
    string.
    """

    def __init__(self, src, fps=DEFAULT_FPS,
                    buffer_size=DEFAULT_STREAM_BUFFER_SIZE):
        """Initialize a `VideoStreamReader` object.

        Args:
            src (string): video stream source
            fps (int): streaming fps
            buffer_size (int): The maximum size to buffering the video stream.
        """
        self._src = src
        self._fps = fps
        self._interval = 1 / fps
        self._status_source = VideoStreamReaderBasic.STATUS_SRC_DOWN
        self._reader = cv2.VideoCapture()
        self._reader.set(cv2.CAP_PROP_BUFFERSIZE, 1)
        self._queue = deque(maxlen=buffer_size)
        self._stop_event = threading.Event()
        self._video_info = {}

    def open(self, timeout=15):
        """Open video streaming source

        Args:
            timeout (int): open streaming connection timeout
        Returns:
            (string): video source status
            (string): video stream error
        """
        logging.info("Opening video source: {}".format(self._src))

        self._status_source = VideoStreamReaderBasic.STATUS_SRC_DOWN
        if self._reader.isOpened():
            return self._status_source, \
                    VideoStreamReaderBasic.ERROR_CONNECTION

        # Open video source
        try:
            run_with_timeout(timeout, self._reader.open, self._src)
        except:
            logging.error("Error occurred when opening video source"
                          " from {}".format(self._src))
            return self._status_source, \
                    VideoStreamReaderBasic.ERROR_CONNECTION

        # Get video information
        success, image = self._reader.read()
        if not success:
            return self._status_source, \
                    VideoStreamReaderBasic.ERROR_CONNECTION
        self._status_source = VideoStreamReaderBasic.STATUS_SRC_UP

        height, width, _ = image.shape
        self._video_info["frame_size"] = (width, height)

        # Start reader thread
        self._stop_event.clear()
        capture_interval = 1000.0 / self._fps
        logging.info("Starting reader thread")
        self._thread = StreamReaderThread(self._reader,
                                          self._queue,
                                          None,
                                          width,
                                          height,
                                          self._src,
                                          timeout,
                                          self._stop_event,
                                          capture_interval,
                                          _is_livestream(self._src))
        self._thread.daemon = True
        self._thread.start()

        return self._status_source, None

    def release(self):
        self._stop_event.set()
        if hasattr(self, "_thread"):
            self._thread.join()
        self._reader.release()
        self._queue.clear()

    def get_video_info(self):
        return self._video_info

    def _read(self, batch_size):
        frames = [self._queue.pop() for _ in range(batch_size)]
        return frames

    def is_busy(self):
        full_percent = (len(self._queue) / self._queue.maxlen) * 100
        return full_percent > DEFAULT_STREAM_BUFFER_THRES_PERCENT

    def _get_q_size(self):
        cur_q_size = len(self._queue)
        return cur_q_size

    def read(self, batch_size=1):
        """The routine of video stream capturer capturation.

        Returns:
            (list of `VideoFrame`): A list of captured VideoFrame objects.
                The length of the list is determined by the `batch_size`.
            (string): video source status
            (string): video stream error
        """
        cur_q_size = self._get_q_size()
        while cur_q_size < batch_size:
            time.sleep(self._interval)
            cur_q_size = self._get_q_size()

        self._status_source = VideoStreamReaderBasic.STATUS_SRC_UP
        err = self._thread.get_exception()
        if err is not None:
            self._status_source = VideoStreamReaderBasic.STATUS_SRC_DOWN
            return [], self._status_source, err

        data = self._read(batch_size)
        return data, self._status_source, None

class StreamWriterThread(threading.Thread):
    def __init__(self, writer, queue, stop_event):
        super().__init__()
        self._writer = writer
        self._queue = queue
        self._stop_event = stop_event
        self._exception = None

    def run(self):
        try:
            while True:
                if self._queue.empty():
                    if self._stop_event.is_set():
                        break
                    else:
                        time.sleep(0.01)
                        continue
                frame = self._queue.get()
                self._writer.write(frame.image)
                self._queue.task_done()
                # TODO add time.sleep to reduce cpu loading?
            logging.info("Writer thread is terminated")
        except Exception as e:
            logging.error(str(e))
            self._exception = e

    def get_exception(self):
        return self._exception


class VideoStreamWriter(object):
    """Video Stream Writer
        currently use queue , then use OpenCV and GStreamer to save as video file.
    """
    def __init__(self, q_size=0):
        """Initialize an OpenCV VideoWriter object and frames queue
        Args:
            q_size (int): frames for recording queue size.
                default to 0 as infinite, which will make number of threads be
                automatically chosenand and also makes method write as a
                non-blocking operation.
        """
        self._writer = cv2.VideoWriter()
        self._queue = Queue(q_size) # use Queue WILL not lost frame
        self._stop_event = threading.Event()

    def open(self, filename, fps, size, threads=0):
        """Open OpenCV VideoWriter and start to get frame from the queue in a
            background thread.
        Args:
            filename (string): file name with path for OpenCV VideoWriter.
            fps (int): fps for OpenCV VideoWriter.
            size (tuple): tuple of witdh, height for OpenCV VideoWriter function.
            threads (int): Number of threads used by the x264enc.
                The more thread you use, the processing time for video
                recording is shorter. But it takes more core GStreamer pipeline
                will process at same time.
                Here's benchmark testing for this VideoWriter using x264enc
                with 2 threads, 10 fps on i7-8700 CPU @3.20GHz:
                ----------------------------------------------
                resolution | source   | process | writer (fps)
                720p       | 1800 sec | 423 sec | 42
                1080p      | 1800 sec | 708 sec | 25
                ----------------------------------------------
                # TODO maybe add optional parameters to use nvh264enc for 
                  higher encoding fps in the future?
            ref: 
                [x264enc](https://gstreamer.freedesktop.org/documentation/x264/index.html?gi-language=c#x264enc:threads)
                [nvh264enc](https://gstreamer.freedesktop.org/documentation/nvenc/nvh264enc.html)
        """
        if self._writer.isOpened():
            raise RuntimeError("Stream is already opened")

        _, ext = os.path.splitext(filename)
        if ext == ".mp4":
            '''
            Use I420, which is the most common YUV420p foramt, to make the
            generated videos widely supported.
            (If we don't specify, the output format may be YUV444p, which is
            not supported by some browsers on mobile phones)
            '''
            filename = ('appsrc ! videoconvert ! '
                        'video/x-raw,format=(string)I420 ! '
                        'x264enc threads={} ! mp4mux ! '
                        'filesink location={}'.format(threads, filename))

            fourcc = 0
        else:
            fourcc = cv2.VideoWriter_fourcc(*'XVID')

        if not self._writer.open(filename, fourcc, fps, size):
            raise RuntimeError("Can't open video file {}"
                               .format(filename))

        self._stop_event.clear()

        logging.info("Starting writer thread")
        self._thread = StreamWriterThread(self._writer,
                                          self._queue,
                                          self._stop_event)
        self._thread.daemon = True
        self._thread.start()

    def end(self):
        """release all resource"""
        try:
            self._stop_event.set()
            if hasattr(self, "_thread") and self._thread.is_alive():
                if not self._queue.empty():
                    self._queue.join()
                self._thread.join()
            self._writer.release()
        except Exception as e:
            logging.error(str(e))

    def write(self, frames):
        """Note: if queue size is not infinity, this function is a "blocking" operation"""
        if isinstance(frames, list):
            for frame in frames:
                self._queue.put(frame)
        else:
            self._queue.put(frames)
