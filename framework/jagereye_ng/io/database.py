from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from dask.distributed import get_client, get_worker
from jagereye_ng import logging
from jagereye_ng.util.generic import get_config


def _get_worker():
    worker = get_worker()
    assert hasattr(worker, "je_database"), ("IO_WORKER have not been "
                                            "established yet.")

    return worker


def _save_event(anal_id, timestamp, event_type, content):
    worker = _get_worker()
    event_id = worker.je_database.save_event(anal_id,
                                             timestamp,
                                             event_type,
                                             content)

    return event_id


def _save_external(external_type, content, group=None):
    worker = _get_worker()
    external_id = worker.je_database.save_external(external_type,
                                                   content,
                                                   group=group)

    return external_id


def _get_external(external_id):
    worker = _get_worker()
    external = worker.je_database.get_external(external_id)

    return external[0]


def _query_externals(query):
    worker = _get_worker()
    externals = worker.je_database.query_externals(query)

    return externals[0]


def _update_external(external_id, group=None, content=None):
    worker = _get_worker()
    worker.je_database.update_external(external_id,
                                       group=group,
                                       content=content)


def _delete_external(external_id):
    worker = _get_worker()
    worker.je_database.delete_external(external_id)

def _get_external_group(external_group_id):
    worker = _get_worker()
    external_group = worker.je_database.get_external_group(external_group_id)

    return external_group[0]

def _get_analyzer(analyzer_id):
    worker = _get_worker()
    analyzer = worker.je_database.get_analyzer(analyzer_id)

    return analyzer[0]

class Database(object):
    def __init__(self):
        config = get_config()["apps"]["dask"]
        address = config["cluster_ip"] + ":" + config["scheduler_port"]
        try:
            # Get Dask client
            self._client = get_client(address=address)
        except ValueError:
            assert False, ("Should connect to Dask scheduler before"
                           " initializing this object.")

    def __del__(self):
        self._client.close()

    def _submit(self, task, *args, **kwargs):
        def done_callback(future):
            if future.exception() is not None:
                logging.error("Failed to execute submitted task: {}, args: {},"
                              " kwargs: {}, error: {}"
                              .format(task.__name__,
                                      args,
                                      kwargs,
                                      future.exception()))
                import traceback
                tb = future.traceback()
                traceback.format_tb(tb)

        future = self._client.submit(task, *args, **kwargs, resources={"IO": 1})
        future.add_done_callback(done_callback)

        return future

    def save_event(self, anal_id, timestamp, event_type, content):
        """Save an event in the database.

        Args:
            anal_id (string): The analyzer ID that the event belongs to.
            timestamp (float): The event timestamp.
            event_type (string): The event type.
            content (dict): The event content according to the event type.

        Returns:
            string: The saved event ID.
        """
        return self._submit(_save_event,
                            anal_id,
                            timestamp,
                            event_type,
                            content)

    def save_external(self, external_type, content, group=None):
        """Save an external in the database.

        Args:
            external_type (string): The external type.
            content (dict): The external content.
            group (string): The group that the external belongs to.

        Returns:
            string: The saved external ID.
        """
        return self._submit(_save_external, external_type, content, group=group)

    def get_external(self, external_id):
        """Get an external from the database.

        Args:
            external_id (string): The target external ID.

        Returns:
            dict: The external.

        Raises:
            `NonExistentIdError`: If the external Id does not exist.
        """
        return self._submit(_get_external, external_id)

    def query_externals(self, query):
        """Query externals from the database.

        Args:
            query (dict): The query filters. Query can contains following keys:
                "_ids" (list of string): The external IDs.
                "task_status" (string, list of string): The task status of the
                    externals.
                "type" (string): The externals type.
                "group" (string, list of string, `bson.objectid.ObjectId`, list
                    of `bson.objectid.ObjectId`): The group IDs that the
                    externals belong to.
                "content.**.*" (object): The filters for content in external,
                    such as "content.number".

        Returns:
            list of dict: The queried externals.

        Raises:
            `TypeError`: If the query group type is not string or list.
        """
        return self._submit(_query_externals, query)

    def update_external(self, external_id, group=None, content=None):
        """Update an external from the database.

        Args:
            external_id (string): The target external ID.

        Raises:
            `NonExistentIdError`: If the external Id does not exist.
        """
        return self._submit(_update_external,
                            external_id,
                            group=group,
                            content=content)

    def delete_external(self, external_id):
        """Delete an external from the database.

        Args:
            external_id (string): The target external ID.

        Raises:
            `NonExistentIdError`: If the external Id does not exist.
        """
        return self._submit(_delete_external, external_id)

    def get_external_group(self, external_group_id):
        """Get a group from the database.

        Args:
            external_group_id (string): The target external group ID.

        Returns:
            dict: The external group.

        Raises:
            `NonExistentIdError`: If the external group Id does not exist.
        """
        return self._submit(_get_external_group, external_group_id)

    def get_analyzer(self, analyzer_id):
        """Get an analyzer from the database.

        Args:
            analyzer_id (string): The target analyzer ID.

        Returns:
            dict: The analyzer.

        Raises:
            `NonExistentIdError`: If the analyzer Id does not exist.
        """
        return self._submit(_get_analyzer, analyzer_id)