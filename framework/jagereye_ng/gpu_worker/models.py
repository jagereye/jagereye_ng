from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import abc
import os
import pickle
import sys
import shutil
import hashlib

import cv2
import tensorflow as tf
import numpy as np
from matplotlib import path as mpl_path
from PIL import Image
from sklearn.preprocessing import normalize

from jagereye_ng.gpu_worker.label import OBJECT_DETECTION_LABELS
from jagereye_ng.gpu_worker.label import VEHICLE_DETECTION_LABELS
from jagereye_ng.gpu_worker.label import LICENSE_PLATE_DETECTION_LABELS
from jagereye_ng.gpu_worker.label import UNATTENDED_OBJECT_CLASSIFICATION_CATEGORIES
from jagereye_ng.gpu_worker.util import yolo_v3
from jagereye_ng.gpu_worker.util.onnx import OnnxTRTExecutor
from jagereye_ng.gpu_worker.util.retinaface import RetinaFace
from jagereye_ng.util.generic import get_static_path
from jagereye_ng.util.generic import uuid
from jagereye_ng.util.math import clamp
from jagereye_ng.util.math import sigmoid
from jagereye_ng.util.math import softmax
from jagereye_ng.util.shape import BoundingBox
from jagereye_ng.util.shape import Point


FORMAT_FROZEN_GRAPH = "frozen_graph"
FORMAT_SAVED_MODEL = "saved_model"
FORMAT_CUSTOM = 'custom'


class InvalidFormatError(Exception):
    pass


class ModelBase(object):
    """The base class for model inference."""

    def __init__(self, name, version, model_format):
        """Initialize a `ModelBase` instance.

        Args:
            name (string): The model name.
            version (string): The model version.
            model_format (string): Format of the model file.
        """
        self._name = name
        self._version = version
        self._format = model_format
        if os.environ["JAGERENV"] == "production":
            self._uuid = str(uuid())
            model_dir = os.path.join("/tmp", self._uuid)
        else:
            model_dir = os.path.join(get_static_path(""), "models")

        self._path = self._get_path(model_dir, name, version, model_format)

    def _get_path(self, model_dir, name, version, model_format):
        """Get model path.

        Args:
            model_dir (string): The path to model directory.
            name (string): The model name.
            version (string): The model version.
            model_format (string): The model format.

        Returns:
            string: Path to the model (file or directory).
        """
        path = os.path.join(model_dir,
                            name,
                            version)

        if model_format == FORMAT_FROZEN_GRAPH:
            path = os.path.join(path, "frozen_inference_graph.pb")

        return path

    def _decrypt_model(self, model_name):
        base_dir = get_static_path("")
        path = os.path.join(base_dir,
                            "models",
                            model_name)
        sha = hashlib.sha1()
        sha.update("{}jager0402".format(model_name).encode('utf-8'))
        os.system('unzip -q -P {} -d /tmp/{}/ {}.zip'.format(sha.hexdigest(), self._uuid, path))

    def _clean_model(self, model_name):
        shutil.rmtree('/tmp/{}/{}'.format(self._uuid, model_name))

    def load(self, gpu_idx, gpu_fraction):
        """Load the model into memory.

        Args:
            gpu_idx (int): The index of GPU to be used.
            gpu_fraction (float): The GPU memory fraction to be used.
        """
        os.environ["CUDA_VISIBLE_DEVICES"] = "{}".format(gpu_idx)

    def get_info(self):
        """Get information of the model.

        Returns:
            dict: The model information.
        """
        return {
            "name": self._name,
            "version": self._version,
            "format": self._format,
            "path": self._path,
        }

    @abc.abstractmethod
    def run(self):
        """Run model inference."""
        raise NotImplementedError()


class TensorFlowModel(ModelBase):
    """The class for TensorFlow model inference."""

    def __init__(self, name, version, model_format):
        """Initialize a `TensorFlowModel` instance.

        Args:
            name (string): The model name.
            version (string): The model version.
            model_format (string): Format of the model file.

        Raises:
            `InvalidFormatError`: If model format is not "frozen_model" or
                "saved_model".
        """
        if model_format not in [FORMAT_FROZEN_GRAPH, FORMAT_SAVED_MODEL]:
            raise InvalidFormatError("Invalid model TensorFlow format: {}"
                                     .format(model_format))

        super().__init__(name, version, model_format)

    def load(self, gpu_idx, gpu_fraction):
        """Load the model into memory.

        Args:
            gpu_idx (int): The index of GPU to be used.
            gpu_fraction (float): The GPU memory fraction to be used.
        """
        super().load(gpu_idx, gpu_fraction)

        self._graph = tf.Graph()
        gpu_options = tf.GPUOptions(per_process_gpu_memory_fraction=gpu_fraction)
        sess_config = tf.ConfigProto(allow_soft_placement=True,
                                     gpu_options=gpu_options)
        self._session = tf.Session(graph=self._graph, config=sess_config)
        if os.environ["JAGERENV"] == "production":
            self._decrypt_model(self._name)
        if self._format == FORMAT_FROZEN_GRAPH:
            with self._graph.as_default():
                od_graph_def = tf.GraphDef()
                with tf.gfile.GFile(self._path, "rb") as f:
                    serialized_graph = f.read()
                    od_graph_def.ParseFromString(serialized_graph)
                    tf.import_graph_def(od_graph_def, name=self._name, input_map=None,
                    return_elements=None, producer_op_list=None)
        elif self._format == FORMAT_SAVED_MODEL:
            tf.saved_model.loader.load(self._session,
                                       [tf.saved_model.tag_constants.SERVING],
                                       self._path,
                                       import_scope=self._name)
        if os.environ["JAGERENV"] == "production":
            self._clean_model(self._name)

    def get_tensor(self, tensor_name):
        """Get tensor from model graph.

        Args:
            tensor_name (string): The tensor name.

        Returns:
            tensorflow `Tensor`: The tensor instance.
        """
        full_tensor_name = "{}/{}".format(self._name, tensor_name)
        tensor = self._graph.get_tensor_by_name(full_tensor_name)

        return tensor


class CustomModel(ModelBase):
    """The class for customized model inference."""

    def __init__(self, name, version, model_format):
        """Initialize a `CustomModel` instance.

        Args:
            name (string): The model name.
            version (string): The model version.
            model_format (string): Format of the model file.

        Raises:
            `InvalidFormatError`: If model format is not "custom".
        """
        if model_format != FORMAT_CUSTOM:
            raise InvalidFormatError("Invalid customized model format: {}"
                                     .format(model_format))

        super().__init__(name, version, model_format)


class ObjectDetectionBase(TensorFlowModel):
    def __init__(self,
                 name,
                 version,
                 model_format,
                 model_image_size,
                 batch_size,
                 class_num,
                 labels,
                 anchors):
        super().__init__(name, version, model_format)
        self._model_image_size = model_image_size
        self._batch_size = batch_size
        self._class_num = class_num
        self._labels = labels
        self._anchors = anchors

    def _pre_process(self, images):
        processeds = []

        for image in images:
            pil_image = Image.fromarray(image)
            # Resize the image to model_image_size with padding.
            boxed_image = yolo_v3._letterbox_image(
                pil_image,
                tuple(reversed(self._model_image_size)),
            )
            np_boxed_image = np.array(boxed_image, dtype="float32")
            np_boxed_image /= 255.
            # Add batch dimension.
            np_boxed_image = np.expand_dims(np_boxed_image, 0)
            processeds.append(np_boxed_image)
        processeds = np.vstack(processeds)

        return processeds

    def _post_process(self, netouts, images):
        out_bboxes, out_scores, out_classes = netouts
        processeds = []

        for idx in range(len(out_bboxes)):
            im_width = images[idx].shape[1]
            im_height = images[idx].shape[0]

            bboxes = [BoundingBox(clamp(int(b[1]), 0, im_width - 1),
                                  clamp(int(b[0]), 0, im_height - 1),
                                  clamp(int(b[3]), 0, im_width - 1),
                                  clamp(int(b[2]), 0, im_height - 1))
                      for b in out_bboxes[idx]]
            scores = out_scores[idx].tolist()
            labels = [self._labels[c] for c in out_classes[idx]]
            processed = [bboxes, scores, labels]
            processeds.append(processed)

        return processeds

    def load(self, gpu_idx, gpu_fraction):
        super().load(gpu_idx, gpu_fraction)

        generated = self._generate()
        self._input_images_tensor = generated[0]
        self._input_shape_tensor = generated[1]
        self._input_score_thd_tensor = generated[2]
        self._input_iou_thd_tensor = generated[3]
        self._input_max_bboxes_tensor = generated[4]
        self._output_tensors = generated[5]

        # warmup the model
        # the input image shape should be [n, n],
        # where n is integer and lager than model image size.

        warmup_frame_size = (self._model_image_size[0] + 100,
                             self._model_image_size[1]+100)
        warmup_frames = np.zeros((self._batch_size,
                                  warmup_frame_size[0],
                                  warmup_frame_size[1],
                                  3))

        self._session.run(
            self._output_tensors,
            feed_dict={
                self._input_images_tensor: warmup_frames,
                self._input_shape_tensor: [warmup_frame_size[0],
                                           warmup_frame_size[1]],
                self._input_score_thd_tensor: [0.6],
                self._input_iou_thd_tensor: [0.5],
                self._input_max_bboxes_tensor: [20],
        })

    def run(self,
            images,
            detect_threshold=0.6,
            iou_threshold=0.5,
            max_bboxes=20):
        assert self._graph, "Should load the model first before running it"

        # We assume all images have same shape.
        image_w = images[0].shape[1]
        image_h = images[0].shape[0]

        input_images = self._pre_process(images)
        netouts = self._session.run(
            self._output_tensors,
            feed_dict={
                self._input_images_tensor: input_images,
                self._input_shape_tensor: [image_h, image_w],
                self._input_score_thd_tensor: [detect_threshold],
                self._input_iou_thd_tensor: [iou_threshold],
                self._input_max_bboxes_tensor: [max_bboxes],
            })

        results = self._post_process(netouts, images)

        return results

    def _generate(self):
        yolo_outputs = [self.get_tensor("conv2d_59/BiasAdd:0"),
                        self.get_tensor("conv2d_67/BiasAdd:0"),
                        self.get_tensor("conv2d_75/BiasAdd:0")]

        # Generate input tensors.
        with self._graph.as_default():
            input_images_tensor = self.get_tensor("input_1:0")
            input_shape_tensor = tf.placeholder(tf.float32, shape=(2,))
            input_score_thd_tensor = tf.placeholder(tf.float32, shape=(1,))
            input_iou_thd_tensor = tf.placeholder(tf.float32, shape=(1,))
            input_max_bboxes_tensor = tf.placeholder(tf.int32, shape=(1,))

        # Generate output tensors for filtered bounding boxes.
        output_tensors = yolo_v3._yolo_eval(
            self._graph,
            yolo_outputs,
            self._anchors,
            self._class_num,
            input_shape_tensor,
            self._batch_size,
            input_score_thd_tensor,
            input_iou_thd_tensor[0],
            input_max_bboxes_tensor[0],
        )

        return (
            input_images_tensor,
            input_shape_tensor,
            input_score_thd_tensor,
            input_iou_thd_tensor,
            input_max_bboxes_tensor,
            output_tensors,
        )


class ObjectDetection(ObjectDetectionBase):
    def __init__(self,
                 name,
                 version,
                 model_format,
                 model_image_size=(416, 416),
                 batch_size=5):
        super().__init__(name,
                         version,
                         model_format,
                         model_image_size,
                         batch_size,
                         80,
                         OBJECT_DETECTION_LABELS,
                         self._get_anchors())

    def _get_anchors(self):
        return np.array([[10., 13.], [16., 30.], [33., 23.],
                        [30., 61.], [62., 45.], [59., 119.],
                        [116., 90.], [156., 198.], [373., 326.]])


class VehicleDetection(ObjectDetectionBase):
    def __init__(self,
                 name,
                 version,
                 model_format,
                 model_image_size=(416, 416),
                 batch_size=5):
        super().__init__(name,
                         version,
                         model_format,
                         model_image_size,
                         batch_size,
                         5,
                         VEHICLE_DETECTION_LABELS,
                         self._get_anchors())

    def _get_anchors(self):
        return np.array([[11., 8.], [15. ,20.], [23., 13.],
                        [33., 24.], [38., 54.], [64., 34.],
                        [92., 72.], [173., 145.], [422., 292.]])


class LicensePlateDetection(ObjectDetectionBase):
    def __init__(self,
                 name,
                 version,
                 model_format,
                 model_image_size=(416, 416),
                 batch_size=5):
        super().__init__(name,
                         version,
                         model_format,
                         model_image_size,
                         batch_size,
                         1,
                         LICENSE_PLATE_DETECTION_LABELS,
                         self._get_anchors())

    def _get_anchors(self):
        return np.array([[17., 8.], [23., 13.], [36., 16.],
                        [54., 22.], [65., 44.], [92., 29.],
                        [121., 59.], [228., 111.], [675., 343.]])

    def run(self,
            images,
            detect_threshold=0.6,
            iou_threshold=0.5,
            max_bboxes=20):
        detections = super().run(images,
                                 detect_threshold=detect_threshold,
                                 iou_threshold=iou_threshold,
                                 max_bboxes=max_bboxes)
        # The object detection result of each image: [bboxes, scores, classes],
        # but # of objects is 1 (i.e., license plate only), so we can ignore the
        # third item (i.e., classes) of the result.
        plates = [detection[0:2] for detection in detections]

        return plates


class LicensePlateRecognition(TensorFlowModel):
    GAUSSIAN_SMOOTH_FILTER_SIZE = (1, 3)
    ADAPTIVE_THRESH_BLOCK_SIZE = 33
    ADAPTIVE_THRESH_WEIGHT = 9
    STRUCTURE_SIZE = (15, 15)

    def __init__(self,
                 name,
                 version,
                 model_format,
                 batch_size=10,
                 model_image_height=60):
        super().__init__(name, version, model_format)
        self.batch_size = batch_size
        self._model_image_height = model_image_height

    def _extract_value(self, image):
        copied = image.copy()

        if image.shape == (image.shape[0], image.shape[1]):
            copied = cv2.cvtColor(image, cv2.COLOR_GRAY2BGR)
        image_hsv = cv2.cvtColor(copied, cv2.COLOR_BGR2HSV)
        image_hue, image_saturation, image_value = cv2.split(image_hsv)

        return image_value

    def _maximize_contrast(self, image_grayscale, structure_size):
        structuring_element = cv2.getStructuringElement(cv2.MORPH_RECT,
                                                        structure_size)

        image_top_hat = cv2.morphologyEx(image_grayscale,
                                         cv2.MORPH_TOPHAT,
                                         structuring_element)
        image_black_hat = cv2.morphologyEx(image_grayscale,
                                           cv2.MORPH_BLACKHAT,
                                           structuring_element)

        image_grayscale_plus_top_hat = cv2.add(image_grayscale, image_top_hat)
        image_grayscale_plus_top_hat_minus_black_hat = \
            cv2.subtract(image_grayscale_plus_top_hat, image_black_hat)

        return image_grayscale_plus_top_hat_minus_black_hat

    def _pre_process(self, images):
        lic_images = []
        for image in images:
            # Resize the input image.
            re_h = self._model_image_height
            re_w = int(image.shape[1] * re_h / image.shape[0])
            img = cv2.resize(image,
                               (re_w, re_h),
                               interpolation=cv2.INTER_AREA)
            image_grayscale = self._extract_value(img)
            image_max_contrast_grayscale = \
                self._maximize_contrast(image_grayscale, self.STRUCTURE_SIZE)
            image_blurred = cv2.GaussianBlur(image_max_contrast_grayscale,
                                             self.GAUSSIAN_SMOOTH_FILTER_SIZE,
                                             0)
            image_thresh = cv2.adaptiveThreshold(image_blurred,
                                                 255.0,
                                                 cv2.ADAPTIVE_THRESH_GAUSSIAN_C,
                                                 cv2.THRESH_BINARY_INV,
                                                 self.ADAPTIVE_THRESH_BLOCK_SIZE,
                                                 self.ADAPTIVE_THRESH_WEIGHT)
            image_thresh = 255 - image_thresh

            retval, buffer = cv2.imencode(".jpg", image_thresh)
            lic_image = buffer.tostring()
            # Add batch dimension.
            lic_images.append(lic_image)

        return lic_images

    def load(self, gpu_idx, gpu_fraction):
        super().load(gpu_idx, gpu_fraction)
        self.input_tensor = self.get_tensor("input_image_as_bytes:0")
        self.output_tensors = [
            self.get_tensor("prediction:0"),
            self.get_tensor("probability:0"),
        ]

        # Warmup the model.
        plates = []
        for _ in range(self.batch_size):
            plate_shape = (self._model_image_height,
                           self._model_image_height * 3,
                           3)
            plate = np.random.random(plate_shape) * 255
            plates.append(plate.astype(np.uint8))

        input_images = self._pre_process(plates)
        self._session.run(
            self.output_tensors,
            feed_dict={ self.input_tensor: input_images },
        )

    def run(self, plates_list, pred_threshold=0.9):
        """ run plate recognition model to generate predict number and score

        Args:
            plates_list(list of list of `numpy.ndarray`): first dimension means
                frame, secondary dimension means plate image on every frame.
                plate numpy ndarray shape: (img_height, img_width, 3)

        Returns:
            list of list of `list of prediction, list of score`:  first
            dimension means frame, secondary dimension means predictions and
            score ordered by input image on every frame. examples:
            [[['XZ6502', 'XZ6502'], [0.9998790026857591, 0.9998790026857591]]]
        """
        assert self._graph, "Should load the model first before running it"

        plates = []
        for i in range(len(plates_list)):
            for j in range(len(plates_list[i])):
                # We feed both original and negative plate images into the
                # model, and pickup one that has higher score. Our model can
                # only recognize plates whose texts are black. This is useful
                # for plates whose texts are white because the texts become
                # black in the negative versions.
                plates.append(plates_list[i][j])
                plates.append(255 - plates_list[i][j])

        plate_numbers = []
        plate_preds = []
        plate_scores = []
        for i in range(0, len(plates), self.batch_size):
            # Preprocessing image by thresholding.
            input_images = self._pre_process(plates[i: i+self.batch_size])
            # Run the model inference to get the predicted plate numbers and score.
            preds, scores = self._session.run(
                self.output_tensors,
                feed_dict={ self.input_tensor: input_images },
            )
            for j in range(len(input_images)): # batch_size 1 ~ infinite
                # Decode the predicted plate numbers for Python 3.
                pred, score = bytes("", "iso-8859-1"), 0
                if len(input_images) == 1: # for single input output from tensorflow
                    pred, score = preds, scores
                elif len(input_images) > 1: # for batch output
                    pred, score = preds[j], scores[j]
                else:
                    pass
                if sys.version_info >= (3,):
                    pred = pred.decode("iso-8859-1")
                # Filter the plate numbers with low score.
                plate_number = pred if score > pred_threshold else None
                plate_numbers.append(plate_number)
                plate_preds.append(pred)
                plate_scores.append(score)

        idx_plate_numbers = 0
        predictions = []
        for i in range(len(plates_list)):
            preds = []
            scores = []
            chunk_len = len(plates_list[i])

            for j in range(chunk_len):
                # Pick one from original and negative plates by comparing
                # their scores.
                idx_orig = idx_plate_numbers + 2 * j
                idx_neg = idx_orig + 1
                if plate_scores[idx_orig] > plate_scores[idx_neg]:
                    idx = idx_orig
                else:
                    idx = idx_neg
                preds.append(plate_preds[idx])
                scores.append(plate_scores[idx])
            predictions.append([preds, scores])

            idx_plate_numbers += (chunk_len * 2)

        return predictions


class FaceDetection(CustomModel):
    def __init__(self,
                 name,
                 version,
                 model_format,
                 model_image_size=(711, 400),
                 max_batch_size=5):
        super().__init__(name, version, model_format)

        self._model_image_size = model_image_size
        self._max_batch_size = max_batch_size

    def load(self, gpu_idx, gpu_fraction):
        super().load(gpu_idx, gpu_fraction)

        if os.environ["JAGERENV"] == "production":
            self._decrypt_model(self._name)

        self._detector = RetinaFace(os.path.join(self._path, "base.onnx"),
                                    os.path.join(self._path, "regress.pt"),
                                    gpu_idx,
                                    self._model_image_size,
                                    self._max_batch_size)

        # Warmup the model.
        warmup_frames = np.zeros((self._max_batch_size,
                                  self._model_image_size[1],
                                  self._model_image_size[0],
                                  3))
        self._detector.detect(warmup_frames, 0.5, 0.3)

        if os.environ["JAGERENV"] == "production":
            self._clean_model(self._name)

    def _is_valid_size(self, bbox, min_width, min_height):
        return bbox.width() >= min_width and bbox.height() >= min_height

    def _is_boundary_exceeded(self, landmark, im_width, im_height):
        for l in landmark:
            if l.x < 0 or l.x >= im_width or l.y < 0 or l.y >= im_height:
                return True

        return False

    def _is_extreme_angle(self, landmark):
        bbpath = mpl_path.Path(np.array([
            [landmark[0].x - 2, landmark[0].y],
            [landmark[1].x + 2, landmark[1].y],
            [landmark[4].x + 2, landmark[4].y - 2],
            [landmark[3].x - 2, landmark[3].y - 2],
        ]))

        return not bbpath.contains_point(np.array([landmark[2].x,
                                                   landmark[2].y]))

    def _post_process(self,
                      model_outs,
                      images,
                      min_width,
                      min_height,
                      filter_boundary_exceeded,
                      filter_extreme_angle):
        processeds = []
        for model_out, image in zip(model_outs, images):
            bboxes = []
            landmarks = []
            scores = []

            im_width = image.shape[1]
            im_height = image.shape[0]

            model_bboxes, model_landmarks, model_scores = model_out

            for m_bbox, m_landmark, m_score in zip(model_bboxes, model_landmarks, model_scores):
                bbox = BoundingBox(
                    clamp(int(m_bbox[0]), 0, im_width - 1),
                    clamp(int(m_bbox[1]), 0, im_height - 1),
                    clamp(int(m_bbox[2]), 0, im_width - 1),
                    clamp(int(m_bbox[3]), 0, im_height - 1),
                )
                landmark = [Point(int(l[0]), int(l[1])) for l in m_landmark.reshape(5, 2)]
                score = m_score[0]

                if not self._is_valid_size(bbox, min_width, min_height):
                    continue

                if (filter_boundary_exceeded and
                    self._is_boundary_exceeded(landmark, im_width, im_height)):
                    continue

                if filter_extreme_angle and self._is_extreme_angle(landmark):
                    continue

                bboxes.append(bbox)
                landmarks.append(landmark)
                scores.append(score)

            processeds.append((bboxes, landmarks, scores))

        return processeds

    def run(self,
            images,
            min_width=40,
            min_height=40,
            score_threshold=0.5,
            iou_threshold=0.3,
            filter_boundary_exceeded=True,
            filter_extreme_angle=True):
        assert self._detector, "Should load the model first before running it"

        model_outs = []
        for idx in range(0, len(images), self._max_batch_size):
            faces_list, landmarks_list, scores_list = self._detector.detect(
                images[idx:idx+self._max_batch_size],
                score_threshold,
                iou_threshold,
            )
            for faces, landmarks, scores in zip(faces_list, landmarks_list, scores_list):
                model_outs.append((faces, landmarks, scores))

        results = self._post_process(model_outs,
                                     images,
                                     min_width,
                                     min_height,
                                     filter_boundary_exceeded,
                                     filter_extreme_angle)

        return results


class FaceRecognition(CustomModel):
    def __init__(self,
                 name,
                 version,
                 model_format,
                 max_batch_size=10,
                 model_image_size=(112, 112)):
        super().__init__(name, version, model_format)
        self._model_image_size = model_image_size
        self._max_batch_size = max_batch_size

    def _pre_process(self, images):
        processeds = []
        for image in images:
            resized = cv2.resize(image, self._model_image_size)
            image_rgb = cv2.cvtColor(resized, cv2.COLOR_BGR2RGB)
            image_t = np.transpose(image_rgb, (2, 0, 1))
            processeds.append(image_t)

        return np.array(processeds, dtype=np.float32)

    def load(self, gpu_idx, gpu_fraction):
        super().load(gpu_idx, gpu_fraction)

        # Load model ONNX file and allocate an executor for TensorRT.
        model_path = os.path.join(self._path, "model.onnx")
        self._model = OnnxTRTExecutor(model_path, gpu_idx, self._max_batch_size)

        # Above model lacks last layer, which is one-dimensional batch
        # normalization layer.
        fc1_bn_path = os.path.join(self._path, "fc1_bn.pickle")
        with open(fc1_bn_path, "rb") as handle:
            self._fc1_bn = pickle.load(handle)

        # Warmup the model.
        faces = []
        for _ in range(self._max_batch_size):
            faces_shape = (self._model_image_size[0], self._model_image_size[1], 3)
            face = np.random.random(faces_shape) * 255
            face = face.astype(np.uint8)
            faces.append(face)
        input_images = self._pre_process(faces)
        self._model.run(input_images)

    def run(self, faces_list):
        """ run face recognition model to generate face feature

        Args:
            faces_list(list of list of `numpy.ndarray`): first dimension means
                frame, secondary dimension means faces image on every frame.
                face numpy ndarray shape: (face_height, face_img_width, 3)

        Returns:
            list of list of `numpy.ndarray`:  first dimension means frame,
            secondary dimension means face features on every frame. feature
            numpy ndarray shape: (512,)
        """
        assert self._model, "Should load the model first before running it"

        faces = []
        for i in range(len(faces_list)):
            for j in range(len(faces_list[i])):
                faces.append(faces_list[i][j])
        features = []
        for i in range(0, len(faces), self._max_batch_size):
            input_images = self._pre_process(faces[i:i+self._max_batch_size])
            output = self._model.run(input_images)[0]
            output = np.array(output)
            # Implement the batch normalization layer:
            # y = ((x - mean) / std) * scale + bias.
            # Note that the scale is 1, so we can ignore it here.
            output = (output - self._fc1_bn["mean"]) / self._fc1_bn["std"] \
                     + self._fc1_bn["bias"]
            for k in range(output.shape[0]):
                features.append(normalize(output[k:k+1]).flatten())

        idx_features = 0
        features_list = []
        for i in range(len(faces_list)):
            features_list.append([])
            for j in range(len(faces_list[i])):
                features_list[len(features_list)-1].append(features[idx_features])
                idx_features += 1

        return features_list


class UnattendedObjectClassification(TensorFlowModel):
    def __init__(self,
                 name,
                 version,
                 model_format,
                 model_image_size=(200, 200),
                 batch_size=1):
        super().__init__(name, version, model_format)
        self._model_image_size = model_image_size
        self._batch_size = batch_size
        self._categories = UNATTENDED_OBJECT_CLASSIFICATION_CATEGORIES

    def load(self, gpu_idx, gpu_fraction):

        super().load(gpu_idx, gpu_fraction)

        self._input_tensor = self.get_tensor("ExpandDims:0")
        self._output_tensor = self.get_tensor("final_result:0")
        # Warmup the model.
        obj_shape = (self._model_image_size[0], self._model_image_size[1], 3)
        images = []
        for _ in range(self._batch_size):
            images.append(np.random.random(obj_shape) * 255)

        input_images = self._pre_process(images)
        self._session.run(
            self._output_tensor,
            feed_dict={self._input_tensor: input_images},
        )

    def _pre_process(self, images):
        processeds = []

        for image in images:
            # Add batch dimension.
            image = np.expand_dims(image, 0)
            processeds.append(image)
        processeds = np.vstack(processeds)

        return processeds

    def run(self, images):
        """ run object classification for unattended object

        Args:
            images(list of `numpy.ndarray`): images with unattended object
            on every frame. object numpy ndarray shape:
            (obj_height, obj_img_width, 3)

        Returns:
            (`numpy.ndarray`): unattened object classification probability for
            index of corresponding category. 0, 1, 2, ..., N-1.
            numpy ndarray shape: (N,)
        """
        assert self._graph, "Should load the model first before running it"
        input_images = self._pre_process(images)
        probabilities = self._session.run(
                self._output_tensor,
                feed_dict={self._input_tensor: input_images},
            )

        return probabilities
