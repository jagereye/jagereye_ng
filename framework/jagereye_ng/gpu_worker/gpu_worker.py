from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

from dask.distributed import get_worker

from jagereye_ng.util import logging
from jagereye_ng.util.string import snake_to_pascal


def init_worker():
    """Initialize a GPU worker."""
    worker = get_worker()
    if not hasattr(worker, 'init') and hasattr(worker, "name") and worker.name.startswith("GPU_WORKER"):
        logging.info("Initializing worker: {}".format(worker.name))

        subnames = worker.name.split(':')
        model_name = subnames[1].lower()
        model_version = subnames[2]
        model_format = subnames[3]
        gpu_idx = int(subnames[4])
        gpu_fraction = float(subnames[5])

        from jagereye_ng import models
        model_class = getattr(models, snake_to_pascal(model_name))
        worker.global_models = dict()
        worker.global_models[model_name] = model_class(
            model_name,
            model_version,
            model_format,
        )
        worker.global_models[model_name].load(gpu_idx, gpu_fraction)

        worker.init = True


def _run_model(name, *args, **kwargs):
    return get_worker().global_models[name].run(*args, **kwargs)


def run_model(client, name, *args, **kwargs):
    """Run model on a GPU worker.

    Args:
        client (dask.dsitributed `Client`): The Dask client object.
        name (string): The name of model to run.

    Returns:
        The result of model running.
    """
    resources = {
        "GPU:{}".format(name.upper()): 1,
    }
    result = client.submit(_run_model,
                           name,
                           *args,
                           **kwargs,
                           resources=resources)

    return result
