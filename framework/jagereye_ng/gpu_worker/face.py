import cv2
import numpy as np
from skimage import transform as trans

from jagereye_ng.util.shape import BoundingBox


_ALIGNED_OUTPUT_SIZE = (112, 112)
_TRANS_SRC = np.array([
    [38.2946, 51.6963],
    [73.5318, 51.5014],
    [56.0252, 71.7366],
    [41.5493, 92.3655],
    [70.7299, 92.2041],
], dtype=np.float32)


def _align_face(image, landmark=None, bbox=None, margin=44):
    M = None

    if landmark is not None:
        dst = landmark.astype(np.float32)
        tform = trans.SimilarityTransform()
        tform.estimate(dst, _TRANS_SRC)
        M = tform.params[0:2, :]

    if M is None:
        if bbox is None:
            # Use center crop.
            det = np.zeros(4, dtype=np.int32)
            det[0] = int(image.shape[1] * 0.0625)
            det[1] = int(image.shape[0] * 0.0625)
            det[2] = image.shape[1] - det[0]
            det[3] = image.shape[0] - det[1]
        else:
            det = bbox

        bb = np.zeros(4, dtype=np.int32)
        bb[0] = np.maximum(det[0] - margin / 2, 0)
        bb[1] = np.maximum(det[1] - margin / 2, 0)
        bb[2] = np.minimum(det[2] + margin / 2, image.shape[1])
        bb[3] = np.minimum(det[3] + margin / 2, image.shape[0])
        ret = image[bb[1]:bb[3], bb[0]:bb[2], :]

        ret = cv2.resize(ret, _ALIGNED_OUTPUT_SIZE)

        return ret
    else:
        # Do align using landmark.
        warped = cv2.warpAffine(image, M, _ALIGNED_OUTPUT_SIZE, borderValue=0.0)

        return warped


def crop_align_faces(image,
                     bboxes,
                     landmarks,
                     expand_top=0.2,
                     expand_bottom=0.2,
                     expand_left=0.2,
                     expand_right=0.2):
    """Crop and align faces from an image.

    Args:
        image (`numpy.ndarray`): The image to be cropped.
        bboxes (list of `BoundingBox`): The bounding boxes of faces.
        landmarks (list of list of `Point`): The landmarks of faces.
        expand_top (float): The ratio to image height to expand top margin.
            Defaults to 0.2
        expand_bottom (float): The ratio to image height to expand bottom
            margin. Defaults to 0.2
        expand_left (float): The ratio to image width to expand left margin.
            Defaults to 0.2
        expand_right (float): The ratio to image width to expand right margin.
            Defaults to 0.2

    Returns:
        list of `numpy.ndarray`: The cropped and aligned faces.
    """
    im_width = image.shape[1]
    im_height = image.shape[0]
    aligned_faces = []

    for bbox, landmark in zip(bboxes, landmarks):
        # Expand the face bounding box.
        bbox_width = bbox.width()
        bbox_height = bbox.height()
        bbox_expan = BoundingBox(
            max(int(bbox.xmin - bbox_width * expand_left), 0),
            max(int(bbox.ymin - bbox_height * expand_top), 0),
            min(int(bbox.xmax + bbox_width * expand_right), im_width - 1),
            min(int(bbox.ymax + bbox_height * expand_bottom), im_height - 1),
        )

        # Calculate the landmark positions relative to the expanded face
        # bounding box.
        landmark_rel = [[kp.x - bbox_expan.xmin, kp.y - bbox_expan.ymin]
                        for kp in landmark]
        landmark_rel = np.array(landmark_rel)

        # Crop the face.
        face = image[bbox_expan.ymin:bbox_expan.ymax,
                     bbox_expan.xmin:bbox_expan.xmax]

        # Align the face.
        aligned_face = _align_face(face, landmark=landmark_rel)

        aligned_faces.append(aligned_face)

    return aligned_faces


def match_faces(targets, candidates, threshold=0.4):
    """Match faces that are similar.

    Args:
        targets (`numpy.ndarray` or list of `numpy.ndarray`): The target faces.
        candidates (`numpy.ndarray` or list of `numpy.ndarray`): The candidate
            faces.

    Returns:
        `numpy.ndarray`, `numpy.ndarray`: Two matrices, the first matrix shows
            similarity scores for each target to each candidate, and the second
            matrix shows matches or not for each target to each candidate.
    """
    x = np.array(targets)
    y = np.array(candidates)
    scores_matrix = np.dot(x, y.T)
    matches_matrix = scores_matrix > threshold

    return scores_matrix, matches_matrix
