import cv2
import numpy as np
import onnx
import torch
import torch.nn as nn
import torch.nn.functional as F
import torchvision.ops as ops

from jagereye_ng.gpu_worker.util.onnx import OnnxTRTExecutor
from jagereye_ng.image.image import letterbox_image


def _generate_anchors(base_size=16, ratios=None, scales=None):
    if ratios is None:
        ratios = np.array([1, 1, 1])

    if scales is None:
        scales = np.array([2 ** 0, 2 ** (1.0 / 3.0), 2 ** (2.0 / 3.0)])

    num_anchors = len(scales)

    # Initialize output anchors
    anchors = np.zeros((num_anchors, 4))

    # Scale base_size
    anchors[:, 2:] = base_size * np.tile(scales, (2, 1)).T

    # Transform from (x_ctr, y_ctr, w, h) -> (x1, y1, x2, y2)
    anchors[:, 0::2] -= np.tile(anchors[:, 2] * 0.5, (2, 1)).T
    anchors[:, 1::2] -= np.tile(anchors[:, 3] * 0.5, (2, 1)).T

    return anchors


def _shift(shape, stride, anchors):
    shift_x = (np.arange(0, shape[1]) + 0.5) * stride
    shift_y = (np.arange(0, shape[0]) + 0.5) * stride

    shift_x, shift_y = np.meshgrid(shift_x, shift_y)

    shifts = np.vstack((
        shift_x.ravel(), shift_y.ravel(),
        shift_x.ravel(), shift_y.ravel()
    )).transpose()

    # add A anchors (1, A, 4) to
    # cell K shifts (K, 1, 4) to get
    # shift anchors (K, A, 4)
    # reshape to (K * A, 4) shifted anchors
    A = anchors.shape[0]
    K = shifts.shape[0]
    all_anchors = (anchors.reshape((1, A, 4)) + \
                  shifts.reshape((1, K, 4)).transpose((1, 0, 2)))
    all_anchors = all_anchors.reshape((K * A, 4))

    return all_anchors


class _Anchors(nn.Module):

    def __init__(self,
                 pyramid_levels=None,
                 strides=None,
                 sizes=None,
                 ratios=None,
                 scales=None):
        super().__init__()

        if pyramid_levels is None:
            self._pyramid_levels = [3, 4, 5]
        else:
            self._pyramid_levels = pyramid_levels
        if strides is None:
            self._strides = [2 ** x for x in self._pyramid_levels]
        else:
            self._strides = strides
        if sizes is None:
            self._sizes = [2 ** 4.0, 2 ** 6.0, 2 ** 8.0]
        else:
            self._sizes = sizes
        if ratios is None:
            self._ratios = np.array([1, 1, 1])
        else:
            self._ratios = ratios
        if scales is None:
            self._scales = np.array([2 ** 0, 2 ** (1/2.0) , 2 ** 1.0 ])
        else:
            self._scales = scales

    def forward(self, image):
        image_shape = image.shape[2:]
        image_shape = np.array(image_shape)
        image_shapes = [(image_shape + 2 ** x - 1) // (2 ** x)
                        for x in self._pyramid_levels]

        # Compute anchors over all pyramid levels
        all_anchors = np.zeros((0, 4)).astype(np.float32)

        for idx, p in enumerate(self._pyramid_levels):
            anchors = _generate_anchors(base_size=self._sizes[idx],
                                        ratios=self._ratios,
                                        scales=self._scales)
            shifted_anchors = _shift(image_shapes[idx],
                                     self._strides[idx],
                                     anchors)
            all_anchors = np.append(all_anchors, shifted_anchors, axis=0)

        all_anchors = np.expand_dims(all_anchors, axis=0)

        return torch.from_numpy(all_anchors.astype(np.float32)).cuda()


class _RegressionTransform(nn.Module):

    def __init__(self, std_box=None, std_ldm=None):
        super().__init__()

        if std_box is None:
            self._std_box = torch.from_numpy(np.array([
                0.1,
                0.1,
                0.2,
                0.2,
            ]).astype(np.float32)).cuda()
        else:
            self._std_box = std_box
        if std_ldm is None:
            self._std_ldm = (torch.ones(1,10) * 0.1).cuda()
        else:
            self._std_ldm = std_ldm

    def forward(self, anchors, bbox_deltas, ldm_deltas, img):
        widths  = anchors[:, :, 2] - anchors[:, :, 0]
        heights = anchors[:, :, 3] - anchors[:, :, 1]
        ctr_x   = anchors[:, :, 0] + 0.5 * widths
        ctr_y   = anchors[:, :, 1] + 0.5 * heights

        # Rescale.
        ldm_deltas = ldm_deltas * self._std_ldm
        bbox_deltas = bbox_deltas * self._std_box

        bbox_dx = bbox_deltas[:, :, 0]
        bbox_dy = bbox_deltas[:, :, 1]
        bbox_dw = bbox_deltas[:, :, 2]
        bbox_dh = bbox_deltas[:, :, 3]

        # Get predicted boxes.
        pred_ctr_x = ctr_x + bbox_dx * widths
        pred_ctr_y = ctr_y + bbox_dy * heights
        pred_w     = torch.exp(bbox_dw) * widths
        pred_h     = torch.exp(bbox_dh) * heights

        pred_boxes_x1 = pred_ctr_x - 0.5 * pred_w
        pred_boxes_y1 = pred_ctr_y - 0.5 * pred_h
        pred_boxes_x2 = pred_ctr_x + 0.5 * pred_w
        pred_boxes_y2 = pred_ctr_y + 0.5 * pred_h

        pred_boxes = torch.stack([
            pred_boxes_x1,
            pred_boxes_y1,
            pred_boxes_x2,
            pred_boxes_y2
        ], dim=2)

        # Get predicted landmarks.
        pt0_x = ctr_x + ldm_deltas[:,:,0] * widths
        pt0_y = ctr_y + ldm_deltas[:,:,1] * heights
        pt1_x = ctr_x + ldm_deltas[:,:,2] * widths
        pt1_y = ctr_y + ldm_deltas[:,:,3] * heights
        pt2_x = ctr_x + ldm_deltas[:,:,4] * widths
        pt2_y = ctr_y + ldm_deltas[:,:,5] * heights
        pt3_x = ctr_x + ldm_deltas[:,:,6] * widths
        pt3_y = ctr_y + ldm_deltas[:,:,7] * heights
        pt4_x = ctr_x + ldm_deltas[:,:,8] * widths
        pt4_y = ctr_y + ldm_deltas[:,:,9] * heights

        pred_landmarks = torch.stack([
            pt0_x,
            pt0_y,
            pt1_x,
            pt1_y,
            pt2_x,
            pt2_y,
            pt3_x,
            pt3_y,
            pt4_x,
            pt4_y,
        ], dim=2)

        # Clip bboxes and landmarks.
        B,C,H,W = img.shape

        pred_boxes[:,:,::2] = torch.clamp(pred_boxes[:,:,::2],
                                          min=0,
                                          max=int(W))
        pred_boxes[:,:,1::2] = torch.clamp(pred_boxes[:,:,1::2],
                                           min=0,
                                           max=int(H))
        pred_landmarks[:,:,::2] = torch.clamp(pred_landmarks[:,:,::2],
                                              min=0,
                                              max=int(W))

        return pred_boxes, pred_landmarks


class _RegressionModel(nn.Module):

    def __init__(self):
        super().__init__()

        self._regress_bboxes = _RegressionTransform()

    def forward(self,
                bbox_regressions,
                ldm_regressions,
                classifications,
                anchors,
                img_batch):
        classifications_act = F.softmax(classifications, dim=-1)

        bboxes, landmarks = self._regress_bboxes(anchors,
                                                 bbox_regressions,
                                                 ldm_regressions,
                                                 img_batch)

        return classifications_act, bboxes, landmarks


class RetinaFace(object):

    def __init__(self,
                 base_model_path,
                 regress_model_path,
                 gpu_idx,
                 model_image_size,
                 max_batch_size):
        self._base_model_path = base_model_path
        self._regress_model_path = regress_model_path
        self._gpu_idx = gpu_idx
        self._model_image_size = model_image_size
        self._max_batch_size = max_batch_size

        self._base_model = OnnxTRTExecutor(self._base_model_path,
                                           self._gpu_idx,
                                           self._max_batch_size)

        self._regress_model = self._load_regress_model()
        self._anchors = self._create_anchors()

    def _load_base_model(self):
        import onnx_tensorrt.backend as backend

        onnx_model = onnx.load(self._base_model_path)
        model = backend.prepare(onnx_model,
                                device="CUDA:{}".format(self._gpu_idx),
                                max_batch_size=self._max_batch_size)

        return model

    def _load_regress_model(self):
        model = _RegressionModel()
        pre_state_dict = torch.load(self._regress_model_path)
        pretrained_dict = {k[7:]: v
                           for k, v in pre_state_dict.items()
                           if k[7:] in model.state_dict()}
        model.load_state_dict(pretrained_dict)

        return model

    def _create_anchors(self):
        input_width, input_height = self._model_image_size
        size = (1, 3, input_height, input_width)

        dummy_image = torch.zeros(size=size).float().cuda()
        anchors_module = _Anchors().cuda().eval()
        anchors = anchors_module(dummy_image)

        return anchors

    def _prepare_input_images(self, images):
        input_width, input_height = self._model_image_size

        input_images = []
        scales = []
        shifts = []
        for image in images:
            input_image, scale, shift = letterbox_image(
                image,
                self._model_image_size,
            )
            input_image = np.transpose(input_image, (2, 0, 1))
            input_images.append(input_image)
            scales.append(scale)
            shifts.append(shift)

        return np.array(input_images, dtype=np.float32, order='C'), scales, shifts

    def detect(self, images, score_threshold, iou_threshold):
        assert len(images) <= self._max_batch_size

        input_images, scales, shifts = self._prepare_input_images(images)
        bbox_regressions, ldm_regressions, classifications = self._base_model.run(
            input_images,
        )
        classifications_act, bboxes, landmarks = self._regress_model(
            torch.from_numpy(bbox_regressions).cuda(),
            torch.from_numpy(ldm_regressions).cuda(),
            torch.from_numpy(classifications).cuda(),
            self._anchors,
            torch.from_numpy(input_images).cuda(),
        )

        batch_size = classifications_act.shape[0]
        picked_boxes = []
        picked_landmarks = []
        picked_scores = []

        for i in range(batch_size):
            classification = classifications_act[i,:,:]
            bbox = bboxes[i,:,:]
            landmark = landmarks[i,:,:]
            scale = scales[i]
            shift_x = shifts[i][0]
            shift_y = shifts[i][1]

            # Choose positive and scores > score_threshold
            scores, argmax = torch.max(classification, dim=1)
            argmax_indice = argmax==0
            scores_indice = scores > score_threshold
            positive_indices = argmax_indice & scores_indice

            scores = scores[positive_indices]

            if scores.shape[0] == 0:
                picked_boxes.append([])
                picked_landmarks.append([])
                picked_scores.append([])
                continue

            bbox = bbox[positive_indices]
            landmark = landmark[positive_indices]

            keep = ops.boxes.nms(bbox, scores, iou_threshold)
            keep_boxes = bbox[keep]
            keep_landmarks = landmark[keep]
            keep_scores = scores[keep]
            keep_scores.unsqueeze_(1)

            for j in range(2):
                keep_boxes[:,j*2] = (keep_boxes[:,j*2] - shift_x) / scale
                keep_boxes[:,j*2+1] = (keep_boxes[:,j*2+1] - shift_y) / scale
            for j in range(5):
                keep_landmarks[:,j*2] = (keep_landmarks[:,j*2] - shift_x) / scale
                keep_landmarks[:,j*2+1] = (keep_landmarks[:,j*2+1] - shift_y) / scale

            picked_boxes.append(keep_boxes.cpu().detach().numpy())
            picked_landmarks.append(keep_landmarks.cpu().detach().numpy())
            picked_scores.append(keep_scores.cpu().detach().numpy())

        return picked_boxes, picked_landmarks, picked_scores
