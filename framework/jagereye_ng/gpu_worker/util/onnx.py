from concurrent.futures import ThreadPoolExecutor


class OnnxTRTExecutor(object):
    """The class for ONNX TensorRT execution."""

    def __init__(self, model_path, gpu_idx, max_batch_size):
        """Initialize a `OnnxTRTExecutor` instance.

        Args:
            model_path (string): The path to ONNX model file.
            gpu_idx (int): The index of GPU to run model.
            max_batch_size (int): The maximum batch size for inference.
        """
        self._model_path = model_path
        self._gpu_idx = gpu_idx
        self._max_batch_size = max_batch_size

        # XXX(feabries su):
        # The base model must be loaded and runned in another fixed thread
        # because it uses PyCuda. Contexts in CUDA are a per-thread affair.
        # Load and run the model in a thread pool executor whose maximum worker
        # is 1 will make sure it is always executed in the same thread.
        # Reference:
        # https://wiki.tiker.net/PyCuda/FrequentlyAskedQuestions#How_does_PyCUDA_handle_threading.3F
        self._executor = ThreadPoolExecutor(max_workers=1)
        self._model = self._executor.submit(self._load_model).result()

    def _load_model(self):
        import onnx
        import onnx_tensorrt.backend as backend

        onnx_model = onnx.load(self._model_path)
        model = backend.prepare(onnx_model,
                                device="CUDA:{}".format(self._gpu_idx),
                                max_batch_size=self._max_batch_size)

        return model

    def run(self, *args, **kwargs):
        return self._executor.submit(self._model.run, *args, **kwargs).result()
