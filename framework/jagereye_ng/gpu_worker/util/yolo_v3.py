"""Miscellaneous utility functions."""

from PIL import Image
import tensorflow as tf


def _letterbox_image(image, size):
    '''resize image with unchanged aspect ratio using padding'''
    image_w, image_h = image.size
    w, h = size
    new_w = int(image_w * min(w*1.0/image_w, h*1.0/image_h))
    new_h = int(image_h * min(w*1.0/image_w, h*1.0/image_h))
    resized_image = image.resize((new_w,new_h), Image.BICUBIC)

    boxed_image = Image.new('RGB', size, (128,128,128))
    boxed_image.paste(resized_image, ((w-new_w)//2,(h-new_h)//2))
    return boxed_image


def _yolo_head(feats, anchors, num_classes, input_shape, calc_loss=False):
    """Convert final layer features to bounding box parameters."""
    num_anchors = len(anchors)
    # Reshape to batch, height, width, num_anchors, box_params.

    anchors_tensor = tf.reshape(tf.constant(anchors, dtype=tf.float32), [1, 1, 1, num_anchors, 2])

    grid_shape = tf.shape(feats)[1:3] # height, width
    grid_y = tf.tile(tf.reshape(tf.range(start=0, limit=grid_shape[0], delta=1), [-1, 1, 1, 1]),
        [1, grid_shape[1], 1, 1])
    grid_x = tf.tile(tf.reshape(tf.range(start=0, limit=grid_shape[1], delta=1), [1, -1, 1, 1]),
        [grid_shape[0], 1, 1, 1])
    grid = tf.concat([grid_x, grid_y], axis=-1)
    grid = tf.cast(grid, feats.dtype)


    feats = tf.reshape(
        feats, [-1, grid_shape[0], grid_shape[1], num_anchors, num_classes + 5])

    # Adjust preditions to each spatial grid point and anchor size.
    box_xy = (tf.sigmoid(feats[..., :2]) + grid) / tf.cast(grid_shape[::-1], feats.dtype)
    box_wh = tf.exp(feats[..., 2:4]) * anchors_tensor / tf.cast(input_shape[::-1], feats.dtype)
    box_confidence = tf.sigmoid(feats[..., 4:5])
    box_class_probs = tf.sigmoid(feats[..., 5:])

    if calc_loss == True:
        return grid, feats, box_xy, box_wh
    return box_xy, box_wh, box_confidence, box_class_probs


def _yolo_correct_boxes(box_xy, box_wh, input_shape, image_shape):
    '''Get corrected boxes'''
    box_yx = box_xy[..., ::-1]
    box_hw = box_wh[..., ::-1]
    input_shape = tf.cast(input_shape, box_yx.dtype)
    image_shape = tf.cast(image_shape, box_yx.dtype)

    new_shape = tf.round(image_shape * tf.reduce_min(input_shape/image_shape))
    offset = (input_shape-new_shape)/2./input_shape
    scale = input_shape/new_shape
    box_yx = (box_yx - offset) * scale
    box_hw *= scale

    box_mins = box_yx - (box_hw / 2.)
    box_maxes = box_yx + (box_hw / 2.)
    boxes =  tf.concat([
        box_mins[..., 0:1],  # y_min
        box_mins[..., 1:2],  # x_min
        box_maxes[..., 0:1],  # y_max
        box_maxes[..., 1:2]  # x_max
    ], axis=-1)

    # Scale boxes back to original image shape.
    boxes *= tf.concat([image_shape, image_shape], axis=-1)
    return boxes


def _yolo_boxes_and_scores(feats, anchors, num_classes, input_shape, image_shape):
    '''Process Conv layer output'''
    box_xy, box_wh, box_confidence, box_class_probs = _yolo_head(feats,
                                                                 anchors,
                                                                 num_classes,
                                                                 input_shape)
    boxes = _yolo_correct_boxes(box_xy, box_wh, input_shape, image_shape)
    boxes = tf.reshape(boxes, [-1, 4])
    box_scores = box_confidence * box_class_probs
    box_scores = tf.reshape(box_scores, [-1, num_classes])
    return boxes, box_scores


def _yolo_eval(graph,
              yolo_outputs,
              anchors,
              num_classes,
              image_shape,
              BATCH_SIZE,
              score_threshold,
              iou_threshold,
              max_boxes):
    """Evaluate YOLO model on given input and return filtered boxes."""
    num_layers = len(yolo_outputs)

    anchor_mask = [[6,7,8], [3,4,5], [0,1,2]] if num_layers==3 else [[3,4,5], [1,2,3]] # default setting
    input_shape = tf.shape(yolo_outputs[0])[1:3] * 32

    boxes_list_ = []
    scores_list_ = []
    classes_list_ = []

    with tf.Session(graph=graph):
        for b in range(BATCH_SIZE):
            boxes = []
            box_scores = []
            for l in range(num_layers):
                feat = yolo_outputs[l][b]
                feat = tf.expand_dims(feat, axis=0)
                _boxes, _box_scores = _yolo_boxes_and_scores(feat,
                                                             anchors[anchor_mask[l]],
                                                             num_classes,
                                                             input_shape,
                                                             image_shape)
                boxes.append(_boxes)
                box_scores.append(_box_scores)
            boxes = tf.concat(boxes, axis=0)
            box_scores = tf.concat(box_scores, axis=0)

            mask = box_scores >= score_threshold

            boxes_ = []
            scores_ = []
            classes_ = []
            for c in range(num_classes):
                # TODO: use keras backend instead of tf.
                class_boxes = tf.boolean_mask(boxes, mask[:, c])
                class_box_scores = tf.boolean_mask(box_scores[:, c], mask[:, c])

                nms_index = tf.image.non_max_suppression(
                    class_boxes, class_box_scores, max_boxes, iou_threshold=iou_threshold)
                class_boxes = tf.gather(class_boxes, nms_index)
                class_box_scores = tf.gather(class_box_scores, nms_index)
                classes = tf.ones_like(class_box_scores, 'int32') * c
                boxes_.append(class_boxes)
                scores_.append(class_box_scores)
                classes_.append(classes)
            boxes_list_.append(tf.concat(boxes_, axis=0))
            scores_list_.append(tf.concat(scores_, axis=0))
            classes_list_.append(tf.concat(classes_, axis=0))

        return boxes_list_, scores_list_, classes_list_
