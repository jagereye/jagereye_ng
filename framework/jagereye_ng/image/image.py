from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import cv2
import numpy as np

from jagereye_ng.io import obj_storage


def shrink_image(image, max_width=0):
    """Shrink image to a smaller size.

    Args:
        image: A 3 dimensional numpy array of the image to be saved.
        max_width (int): The maximum width of image. If the given image
            width is larger than max_width, the width of image file will be
            shrunk to max_width, and the height will be shrunk proportionally.
            When max_width <= 0, the maximum width of image is unlimited.
            Defaults to 0.
    """
    orig_width = image.shape[1]
    if max_width > 0 and orig_width > max_width:
        ratio = max_width / orig_width
        shrunk_image = cv2.resize(image, (0, 0), fx=ratio, fy=ratio)
    else:
        shrunk_image = image.copy()

    return shrunk_image


def draw_bbox(image, bbox, color, thickness):
    """Draw a bounding box on an image.

    Args:
        image (`numpy.ndarray`): The image to be cropped.
        bbox (`jagereye_ng.shape.BoundingBox`): The boudning box of cropped
            region.
        color (tuple): The bounding box color of format (B, G, R).
        thickness (int): The line thickness of be drawn.
    """
    drawn = image.copy()
    cv2.rectangle(drawn,
                  (int(bbox.xmin), int(bbox.ymin)),
                  (int(bbox.xmax), int(bbox.ymax)),
                  color,
                  thickness=thickness)

    return drawn


def draw_region(image, region, color, alpha=0.5):
    """Draw a region on a image.

    Args:
        image (`numpy.ndarray`): The image to draw.
        region (tuple of tuple): The region to be drawn onto the image. It's a
            tuple list of tuple, such as ((X1, Y1), (X2, Y2), ...), the point in
            the list should be type of `int32`.
        color (tuple): The region color of format (B, G, R).
        alpha (float): The level for alpha blending. Default to 0.5.

    Returns:
        The drawn image.
    """
    drawn_image = image.copy()
    overlay = image.copy()
    cv2.fillPoly(overlay, np.array([region], np.int32), color)
    cv2.addWeighted(overlay, alpha, image, 1 - alpha, 0, drawn_image)
    return drawn_image


def crop(image, bbox):
    """Crop from an iamge.

    Args:
        image (`numpy.ndarray`): The image to be cropped.
        bbox (`jagereye_ng.shape.BoundingBox`): The boudning box of cropped
            region.

    Returns:
        The cropped image.
    """
    return image[bbox.ymin:bbox.ymax, bbox.xmin:bbox.xmax]


def letterbox_image(image, size, filled=128, interpolation=cv2.INTER_NEAREST):
    """Resize image with unchanged aspect ratio using padding

    Args:
        image (`numpy.ndarray`): The original image.
        size (tuple of int): The size of letterbox image. The format is
            (wdith, heihgt).
        filled (int): The grayscale color to be filled in the background. Range
            from 0 to 255. Defaults to 128.
        interpolation (int): The interpolation method. Defaults to
            cv2.INTER_NEAREST.

    Returns:
        `numpy.ndarray`, float, tuple of int: The letterbox image, the scale,
            and the shift (format: (dx, dy)).
    """
    iw, ih = image.shape[0:2][::-1]
    w, h = size
    scale = min(w / iw, h / ih)
    nw = int(iw * scale)
    nh = int(ih * scale)
    image = cv2.resize(image, (nw, nh), interpolation=interpolation)
    new_image = np.zeros((size[1], size[0], 3), np.uint8)
    new_image.fill(filled)
    dx = (w - nw) // 2
    dy = (h - nh) // 2
    new_image[dy:dy+nh,dx:dx+nw,:] = image

    return new_image, scale, (dx, dy)
