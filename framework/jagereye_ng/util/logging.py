"""Logging utilities."""

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import yaml
from jagereye_ng.util.generic import get_static_path
import sys
import logging as _logging
import logging.config
from logging import DEBUG # pylint: disable=unused-import
from logging import ERROR # pylint: disable=unused-import
from logging import FATAL # pylint: disable=unused-import
from logging import INFO # pylint: disable=unused-import
from logging import WARN # pylint: disable=unused-import

from jagereye_ng.util.generic import get_config

VALID_TRANSPORTS = {'SYSLOG': 'syslog', 'STDOUT': 'stdout'}

class InvalidLoggingTransport(Exception):
    def __init__(self, value):
        self.value = value
    def __str__(self):
        return(repr(self.value))

def _get_logging_config():
    config = get_config()['logging']
    with open(get_static_path('logging.template.yml'), 'r') as f:
        log_template = yaml.load(f)

    if config['dev_config']['enabled']:
        log_level = config['dev_config']['loglevel']
    else:
        log_level = 'INFO'

    # compose log config
    log_conf = log_template.copy()
    log_conf['handlers'] = dict()
    # determine the transport is syslog or stdout
    if config['transport'] == VALID_TRANSPORTS['SYSLOG']:
        syslog_transport = config['transport_candidates'][config['transport']]
        host = syslog_transport['host']
        port = syslog_transport['port']
        facility = syslog_transport['facility']
        # add syslog handler for jagereye's logging
        log_conf['handlers']['jagereye_syslog_handler'] = log_template['handlers']['jagereye_syslog_handler']
        log_conf['handlers']['jagereye_syslog_handler']['address'] = [host, port]
        log_conf['handlers']['jagereye_syslog_handler']['facility'] = facility
        log_conf['handlers']['jagereye_syslog_handler']['level'] = log_level
        log_conf['loggers']['jagereye_logger']['handlers'].append('jagereye_syslog_handler')
        # add syslog handler for 3rd party's logging
        log_conf['handlers']['third_party_syslog_handler'] = log_template['handlers']['third_party_syslog_handler']
        log_conf['handlers']['third_party_syslog_handler']['address'] = [host, port]
        log_conf['handlers']['third_party_syslog_handler']['facility'] = facility
        log_conf['handlers']['third_party_syslog_handler']['level'] = log_level
        log_conf['root']['handlers'].append('third_party_syslog_handler')

    elif config['transport'] == VALID_TRANSPORTS['STDOUT']:
        log_conf['handlers']['jagereye_stdout_handler'] = log_template['handlers']['jagereye_stdout_handler']
        log_conf['handlers']['third_party_stdout_handler'] = log_template['handlers']['third_party_stdout_handler']
        log_conf['handlers']['third_party_stdout_handler']['level'] = log_level
        log_conf['handlers']['jagereye_stdout_handler']['level'] = log_level
        log_conf['root']['handlers'].append('third_party_stdout_handler')
        log_conf['loggers']['jagereye_logger']['handlers'].append('jagereye_stdout_handler')
    else:
        raise InvalidLoggingTransport('Invalid logging transport: ' + config['transport'])
    return log_conf

config = _get_logging_config()
_logging.config.dictConfig(config)

_logger = _logging.getLogger('jagereye_logger')
_logging.getLogger('botocore').setLevel(_logging.INFO)
_logging.getLogger('matplotlib').setLevel(_logging.INFO)
_logging.getLogger('shapely').setLevel(_logging.INFO)
_logging.getLogger('asyncio').setLevel(_logging.INFO)


def log(level, msg, *args, **kwargs):
    """Log message for a given level.

    Args:
      level (int): The log level.
      msg (string): The message to log.
    """
    _logger.log(level, msg, *args, **kwargs)


def debug(msg, *args, **kwargs):
    """Log debug level message.

    Args:
      msg (string): The message to log.
    """
    _logger.debug(msg, *args, **kwargs)


def error(msg, *args, **kwargs):
    """Log error level message.

    Args:
      msg (string): The message to log.
    """
    _logger.error(msg, *args, **kwargs)


def fatal(msg, *args, **kwargs):
    """Log fatal level message.

    Args:
      msg (string): The message to log.
    """
    _logger.fatal(msg, *args, **kwargs)


def info(msg, *args, **kwargs):
    """Log info level message.

    Args:
      msg (string): The message to log.
    """
    _logger.info(msg, *args, **kwargs)


def warn(msg, *args, **kwargs):
    """Log warn level message.

    Args:
      msg (string): The message to log.
    """
    _logger.warn(msg, *args, **kwargs)


def warning(msg, *args, **kwargs):
    """Log warn level message.

    Args:
      msg (string): The message to log.
    """
    _logger.warning(msg, *args, **kwargs)


def set_debug_mode(params):
    if params['debugMode'] == True:
        _logger.setLevel(_logging.DEBUG)
        _logger.handlers[0].setLevel(_logging.DEBUG)
    elif params['debugMode'] == False:
        _logger.setLevel(_logging.INFO)
        _logger.handlers[0].setLevel(_logging.INFO)

def exception(msg, *args, **kwargs):
    """Log error level message with an exception

    Args:
      msg (string): The message to log.
    """
    _logger.exception(msg, *args, **kwargs)


# Controls which methods from pyglib.logging are available within the project.
_allowed_symbols_ = [
    'DEBUG',
    'ERROR',
    'FATAL',
    'INFO',
    'WARN'
    'debug',
    'error',
    'fatal',
    'info',
    'log',
    'warn',
    'warning',
    'exception'
]
