from typing import List, Any

class AdaptiveListSelection():
    """Adaptive list index selection

    This is a adaptive list index selection depends on input length and output
    length. Sometimes we face the choice of list selection that length of input
    list is not equals to length output list, that's take list of (`VideoFrame`)
    smooth frame selection as our example:

    Case 1: Video stream reader fps is 20, video stream writer fps is 10, such
    that we expect 20 frames input from video stream reader and video stream
    writer 10 frames per second.
    The input list index would be like:
        [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19]
    The output list index we need shoud be properly picked like:
        [0, 2, 4, 6, 10, 12, 14, 16, 18, 19]

    Case 2: Video stream reader fps is 10, video stream writer fps is 15, such
    that we expect 10 frames input from video stream reader and video stream
    writer 15 frames per second.
    The input list index would be like:
        [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
    The output list index we need shoud be properly picked like:
        [0, 0, 1, 1, 2, 2, 3, 3, 4, 4, 5, 5, 6, 7, 8, 9]
    """

    def sample_index(self, indexes: List[int], len_output_list: int) -> List[int]:
        """
        Args:
            indexes (list of int): stores index of a list
        Returns:
            list of int: selected index from indexes.
        """
        # Step 1: pick head, tail and sampling
        indexes2 = []
        for i in range(len(indexes)):
            if i == 0 or i == len(indexes) - 1 or \
               i % (len(indexes) // len_output_list) == 0:
                indexes2.append(i)
        # Step 2: remove in the middle if needed
        if len(indexes2) > len_output_list:
            jump = 0
            if len(indexes2) - len_output_list == 1:
                jump = len(indexes2) // 2 + 1
            else:
                jump = round(len(indexes2) / (len(indexes2) - len_output_list)) - 1
            counter = 1
            while True:
                left = counter * jump
                if left == 0 and len(indexes2) > 0:
                    left = 1
                elif left == len(indexes2) - 1 and len(indexes2) > 2:
                    left = len(indexes2) -2
                indexes2 = indexes2[:left] + indexes2[left+1:]
                if len(indexes2) <= len_output_list:
                    break
                right = len(indexes2) - 1 - counter * jump
                if right == len(indexes2) - 1 and len(indexes2) > 2:
                    right = len(indexes2) - 2
                elif right == 0 and len(indexes2) > 0:
                    right = 1
                indexes2 = indexes2[:right] + indexes2[right+1:]
                if len(indexes2) <= len_output_list:
                    break
                counter += 1

        return indexes2

    def flatten_index(self, indexes: List[int], len_output_list: int) -> List[int]:
        """
        Args:
            indexes (list of int): stores index of a list
        Returns:
            list of int: selected index from indexes.
        """
        freqs = {}
        j = 0
        # Step 1: sumarize first selected index frequency
        for i in range(len_output_list):
            if i % (len_output_list // len(indexes)) == 0 and i != 0:
                j = min(j + 1, len(indexes) - 1)
            freqs[j] = freqs.get(j, 0) + 1
        # Step 2: nomalize the frequency if needed
        if freqs[indexes[len(indexes)-1]] != len_output_list and \
           freqs[indexes[len(indexes)-1]] > freqs[0] + 1:
            count = 0
            for i in range(len(indexes)):
                freqs[i] += 1
                if count >= freqs[indexes[len(indexes)-1]] - 1:
                    break
                count += 1
            freqs[indexes[len(indexes)-1]] = 1

        indexes2 = []
        for idx, freq in freqs.items():
            for _ in range(freq):
                indexes2.append(idx)
        return indexes2

    def select_index(self, lists: List[Any], len_output_list: int) -> List[int]:
        indexes = [i for i in range(len(lists))]
        if len(indexes) < len_output_list:
            return self.flatten_index(indexes, len_output_list)
        elif len(indexes) == len_output_list:
            return indexes
        elif len(indexes) > len_output_list:
            return self.sample_index(indexes, len_output_list)

    def select(self, lists: List[Any], len_output_list: int) -> List[Any]:
        indexes = self.select_index(lists, len_output_list)
        return [lists[indexes[j]] for j in range(len(indexes))]
