"""Generic utilities."""

import os
import yaml
from uuid import uuid4

import jagereye_ng
import json
import os

def get_static_path(file_name):
    """Get path of a static file.

    file_name (string): The static file name.
    """
    file_dir = os.path.join(os.path.dirname(jagereye_ng.__file__), "static")
    file_path = os.path.join(file_dir, file_name)

    return file_path

def get_messaging(messaging_file="messaging.json"):
    """Get the messaging format file

    messaging_file (string): The path to the messaging format file. Defaults to
      "messaging.json".
    """
    with open(get_static_path(messaging_file), "r") as f:
        return json.load(f)

def get_config(config_file="config.yml"):
    """Get the configuration.

    config_file (string): The path to the configuration file. Defaults to
      "config.yml".
    """
    if os.getenv("JAGERENV", 'production') == "production" and config_file == "config.yml":
        from jagereye_ng.static.config import data as config
        return config
    with open(get_static_path(config_file), "r") as f:
        return yaml.load(f)


def nested_set(dict_obj, keys, value):
    """Set a dictionary value with nested key.

    Args:
        dict_obj (dict): The dictionary object.
        keys (list of string): The nested key which is represented by a list of
            keys.
        value (object): The value to be set.
    """
    if not keys:
        return

    current = dict_obj
    for key in keys[0 : -1]:
        if key not in current or not isinstance(current[key], dict):
            current[key] = {}
        current = current[key]
    current[keys[-1]] = value

def is_env_production():
    """
    Returns:
        bool: return true if environment "JAGERENV" == "production"
    """
    return os.environ["JAGERENV"] == "production"


def is_env_development():
    """
    Returns:
        bool: return true if environment "JAGERENV" == "development"
    """
    return os.environ["JAGERENV"] == "development"


def uuid():
    """Generate a UUID.

    Returns:
        string: The generated UUID.
    """
    return uuid4()
