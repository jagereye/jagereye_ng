import numpy as np


def clamp(x, min_val, max_val):
    return max(min(x, max_val), min_val)


def sigmoid(x):
    x_ = x.astype(np.float128)

    return 1. / (1. + np.exp(-x_))


def softmax(x, axis=-1, t=-100.):
    x_ = x - np.max(x)

    if np.min(x_) < t:
        x_ = x_ / np.min(x_) * t

    e_x = np.exp(x_)

    return e_x / e_x.sum(axis, keepdims=True)
