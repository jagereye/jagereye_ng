class Point(object):
    """Class for a 2-dimensional point."""

    def __init__(self, x, y):
        """Initialize a `Point` instance.

        Args:
            x (float): X-axis of the point.
            y (float): Y-axis of the point.
        """
        self._x = x
        self._y = y

    @property
    def x(self):
        """float: X-axis of the point."""
        return self._x

    @x.setter
    def x(self, x):
        self._x = x

    @property
    def y(self):
        """float: Y-axis of the point."""
        return self._y

    @y.setter
    def y(self, y):
        self._y = y

    def tolist(self):
        """Get the list version of the point.

        Returns:
            list of float: The list version of the point. The format is [x, y].
        """
        return [self._x, self._y]


class BoundingBox(object):
    """Class for a bounding box.

    A bounding box is a axis-aligned rectangle that is defined by 2 points:
    min-point and max-point. The min-point has less or equal (x, y) value than
    max-point.
    """

    def __init__(self, xmin, ymin, xmax, ymax):
        """Initialize a `BoundingBox` instance.

        Args:
            xmin (float): X in min-point.
            ymin (float): Y in min-point.
            xmax (float): X in max-point.
            ymax (float): Y in max-point.
        """
        self._xmin = xmin
        self._ymin = ymin
        self._xmax = xmax
        self._ymax = ymax

    @property
    def xmin(self):
        """float: X in min-point."""
        return self._xmin

    @xmin.setter
    def xmin(self, xmin):
        self._xmin = xmin

    @property
    def ymin(self):
        """float: Y in min-point."""
        return self._ymin

    @ymin.setter
    def ymin(self, ymin):
        self._ymin = ymin

    @property
    def xmax(self):
        """float: X in max-point."""
        return self._xmax

    @xmax.setter
    def xmax(self, xmax):
        self._xmax = xmax

    @property
    def ymax(self):
        """float: Y in max-point."""
        return self._ymax

    @ymax.setter
    def ymax(self, ymax):
        self._ymax = ymax

    def tolist(self):
        """Get the list version of the bounding box.

        Returns:
            list of float: The list version of the bounding box. The format
                is [xmin, ymin, xmax, ymax].
        """
        return [self._xmin, self._ymin, self._xmax, self._ymax]

    def width(self):
        """Get width of the bounding box.

        Returns:
            float: Width of the bounding box.
        """
        return self._xmax - self._xmin

    def height(self):
        """Get height of the bounding box.

        Returns:
            float: Height of the bounding box.
        """
        return self._ymax - self._ymin

    def area(self):
        """Get area of the bounding box.

        Returns:
            float: Area of the bounding box.
        """
        return self.width() * self.height()

    def intersection(self, bbox):
        """Get intersection area of the bounding box and another one.

        bbox (`BoundingBox`): Another bounding box.

        Returns:
            float: Intersection area.
        """
        area = max(0.0, min(self._xmax, bbox.xmax) - max(self._xmin, bbox.xmin)) \
               * max(0.0, min(self._ymax, bbox.ymax) - max(self._ymin, bbox.ymin))

        return area

    def union(self, bbox):
        """Get union area of the bounding box and another one.

        bbox (`BoundingBox`): Another bounding box.

        Returns:
            float: Union area.
        """
        return self.area() + bbox.area() - self.intersection(bbox)

    def iou(self, bbox):
        """Get intersection over union of the bounding box and another one.

        bbox (`BoundingBox`): Another bounding box.

        Returns:
            float: Intersection over union.
        """
        return float(self.intersection(bbox)) / self.union(bbox)

    def scale(self, xscale, yscale):
        """Scale the Bounding box.

        xscale (float): Scale over x.
        yscale (float): Scale over y.

        Returns:
            `BoundingBox`: The sclaed bounding box.
        """
        xmin = self._xmin * xscale
        ymin = self._ymin * yscale
        xmax = self._xmax * xscale
        ymax = self._ymax * yscale

        return BoundingBox(xmin, ymin, xmax, ymax)
