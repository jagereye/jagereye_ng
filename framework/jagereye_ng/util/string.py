import re

def snake_to_pascal(snake_str):
    """Convert snake case string to camel case.

    Args:
        snake_str (string): The snake case string.

    Returns:
        string: The camel case string.
    """
    components = snake_str.split("_")
    pascal_str = ''.join(x.title() for x in components)

    return pascal_str

def pascal_to_snake(pascal_str):
    s1 = re.sub('(.)([A-Z][a-z]+)', r'\1_\2', pascal_str)
    return re.sub('([a-z0-9])([A-Z])', r'\1_\2', s1).lower()
