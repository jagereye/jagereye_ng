import multiprocessing
import GPUtil

def _get_gpus_info():
    gpus_info = []
    local_device_protos = GPUtil.getGPUs()

    for local_device in local_device_protos:
        gpus_info.append({
            "index": str(local_device.id),
            "memory_total": float(local_device.memoryTotal),
            "memory_free": float(local_device.memoryFree)
        })

    return gpus_info


def get_gpus_info():
    """Get information of available GPUs.

    Returns:
        list of dict: Information of available GPUs.
    """
    with multiprocessing.Pool(1) as p:
        return p.apply(_get_gpus_info)
