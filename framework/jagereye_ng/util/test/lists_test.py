from jagereye_ng.util.lists import AdaptiveListSelection

class TestAdaptiveSelection(object):
    def test_flatten(self):
        selector = AdaptiveListSelection()
        len_outputs = [ nn for nn in range(60)]
        for len_output in len_outputs:
            len_inputs = [k for k in range(1, len_output)]
            for i in range(len(len_inputs)):
                lists = [j for j in range(len_inputs[i])]
                selected_idx = selector.flatten_index(lists, len_output)
                selected_list = []
                for j in range(len(selected_idx)):
                    selected_list.append(lists[selected_idx[j]])
                # 0:
                selected = selector.select(lists, len_output)
                for j in range(len(selected_list)):
                    assert selected_list[j] == selected[j]

                freqs = {}
                for i in range(len(selected_idx)):
                    freqs[i] = freqs.get(i, 0) + 1

                # 1. last idx freqs can larger than other idx's frequency
                freq_last_idx = freqs[len(lists)-1]
                condition1 = True
                for k, v in freqs.items():
                    if k != len(lists) - 1:
                        if freq_last_idx <= v:
                            condition1 = False
                condition2 = False

                # 2. freqs of last idx - 1 <= freqs of other idx
                if freqs[0] >= freqs[len(lists)-1]:
                    condition2 = True
                assert condition1 is True or condition2 is True

    def test_sampling(self):
        selector = AdaptiveListSelection()
        len_outputs = [ nn for nn in range(1, 60 + 1)]
        for len_output in len_outputs:
            if len_output == 1:
                len_inputs = [k for k in range(len_output, len_output * 2)]
            else:
                len_inputs = [k for k in range(len_output, len_output * 5 + 1)]
            for i in range(len(len_inputs)):
                lists = [j for j in range(len_inputs[i])]
                selected_idx = selector.sample_index(lists, len_output)
                selected_list = []
                for j in range(len(selected_idx)):
                    selected_list.append(lists[selected_idx[j]])
                # 0:
                selected = selector.select(lists, len_output)
                for j in range(len(selected_list)):
                    assert selected_list[j] == selected[j]
                # 1. idx 0 and last should exist
                assert lists[0] == selected_list[0]
                assert lists[len(lists)-1] == selected_list[len(selected_list)-1]

                # 2. idx increase
                idx_inc = -1
                for j in range(len(selected_idx)):
                    assert selected_idx[j] >= idx_inc
                    idx_inc = selected_idx[j]

                # 3. len_outputs duplicate idx
                s = set()
                for j in range(len(selected_idx)):
                    assert selected_idx[j] not in s
                    s.add(selected_idx[j])

    def test_equal(self):
        selector = AdaptiveListSelection()
        len_output = 10
        lists = [j for j in range(len_output)]
        assert lists == selector.select(lists, len_output)