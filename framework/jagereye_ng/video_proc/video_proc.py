from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import cv2
import numpy as np

def roi_to_coords(height, width, roi):
    if roi is not None:
        coords = []
        for roi_corner in roi:
            coords.append([width*roi_corner["x"], height*roi_corner["y"]])
        return coords
    else:
        return None

def get_polygon_area(coords):
    """Return the area of the polygon whose vertices are given by the
    sequence points. It is the implementation of  Shoelace formula.
    """
    if coords is not None:
        area = 0
        q = coords[-1]
        for p in coords:
            area += p[0] * q[1] - p[1] * q[0]
            q = p
        return abs(area / 2)
    else:
        return None

def get_mask(shape, coords):
    """Generate a mask to mask the area of roi composed of coords

    Args:
        shape: The shape of generated mask
        coords: The array of coordinates, e.g. [[0,1], [1,2], [3,4]]
    Returns:
        A mask which is a cv matrix; if coord is None, then return None
    """
    height = shape[0]
    width = shape[1]

    # i.e. 3 or 4 depending on your image
    channel = shape[2]

    if coords is not None:
        mask = np.zeros(shape, dtype=np.uint8)
        coords = np.array(coords, dtype=np.int32)
        ignore_mask_color = (255,)*channel
        cv2.fillPoly(mask, [coords], ignore_mask_color)
        return mask
    else:
        return None

def detect_motion(frames, sensitivity=80, roi=None):
    """Detect motion between frames.

    Args:
        frames: A list of VideoFrame objects.
        sensitivity: The sensitivity of motion detection, range from 1
                     to 100. Defaults to 80.
    Returns:
        A dict with the following keys:
            frames (list): frames that have motion difference against the first
                frame in the input frames.
            index (list): the index of the detected frames in the input frame
                list.
    """
    sensitivity_clamp = max(1, min(sensitivity, 100))
    threshold = (100 - sensitivity_clamp) * 0.05
    num_frames = len(frames)
    if num_frames < 1:
        return []

    results = {"frames": [], "index": []}

    def add_to_results(frame, index):
        results["frames"].append(frame)
        results["index"].append(index)

    coords = roi_to_coords(frames[0].image.shape[0], frames[0].image.shape[1], roi)
    mask = get_mask(frames[0].image.shape, coords)
    roi_area = get_polygon_area(coords)
    img_size = frames[0].image.shape[1] * frames[0].image.shape[0]
    base_area = roi_area if roi_area is not None else img_size

    if mask is not None:
        # apply the mask
        last = cv2.bitwise_and(frames[0].image, mask)
    else:
        last = frames[0].image

    last = cv2.cvtColor(last, cv2.COLOR_BGR2GRAY)


    for i in range(1, num_frames):

        if mask is not None:
            # apply the mask
            current = cv2.bitwise_and(frames[i].image, mask)
        else:
            current = frames[i].image

        current = cv2.cvtColor(current, cv2.COLOR_BGR2GRAY)

        res = cv2.absdiff(last, current)
        # Remove the noise and do the threshold.
        res = cv2.blur(res, (5, 5))
        res = cv2.morphologyEx(res, cv2.MORPH_OPEN, None)
        res = cv2.morphologyEx(res, cv2.MORPH_CLOSE, None)
        ret, res = cv2.threshold(res, 10, 255, cv2.THRESH_BINARY_INV)  # pylint: disable=unused-variable
        # Count the number of black pixels.
        num_black = np.count_nonzero(res == 0)
        # Calculate the average of black pixel in the image.
        avg_black = (num_black * 100.0) / base_area
        # Detect moving by testing whether the average of black exceeds the
        # threshold or not.
        if avg_black >= threshold:
            add_to_results(frames[i], i)
        last = current
    return results
