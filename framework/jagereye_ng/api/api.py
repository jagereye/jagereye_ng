from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import json
from json import JSONDecodeError
from nats.aio.client import Client as NATS
from nats.aio.errors import ErrConnectionClosed, ErrTimeout, ErrNoServers
from jagereye_ng.util import logging
from jagereye_ng.util.generic import get_config


class APIConnector(object):
    def __init__(self, ch_name, io_loop, nats_hosts):
        self._io_loop = io_loop
        self._nats = NATS()
        natsconfig = get_config()["services"]["messaging"]
        nats_hosts=["nats://"+ natsconfig['network']['ip'] +":4222"]
        self._io_loop.run_until_complete(self._setup(ch_name, nats_hosts))

    async def _setup(self, ch_name, nats_hosts):
        options = {
            "servers": nats_hosts,
            "io_loop": self._io_loop,
            "max_reconnect_attempts": 60,
            "reconnect_time_wait": 2,
            "disconnected_cb": self._disconnected_cb,
            "reconnected_cb": self._reconnected_cb,
            "error_cb": self._error_cb,
            "closed_cb": self._closed_cb
        }
        try:
            await self._nats.connect(**options)
            logging.info("NATS connection for APIConnector '{}' is "
                         "established.".format(self.__class__.__name__))
        except ErrNoServers as e:
            logging.error(e)
            raise
        else:
            await self._nats.subscribe(ch_name,
                                       cb=self._api_handler)

    async def _disconnected_cb(self):
        logging.info("[NATS] disconnected")

    async def _reconnected_cb(self):
        logging.info("[NATS] reconnecting to {}".format(
            self._nats.connected_url.netloc))

    async def _error_cb(self, e):
        logging.error("[NATS] {}".format(e))

    async def _closed_cb(self):
        logging.info("[NATS] connection is closed")

    async def _api_handler(self, recv):
        subject = recv.subject
        reply = recv.reply
        try:
            msg = json.loads(recv.data.decode())
        except JSONDecodeError:
            raise

        response = {}
        try:
            if msg["command"] == "CREATE":
                self.on_create(msg["params"])
                response["result"] = "success"
            elif msg["command"] == "READ":
                result = self.on_read(msg["params"])
                response["result"] = result
            elif msg["command"] == "UPDATE":
                self.on_update(msg["params"])
            elif msg["command"] == "DELETE":
                self.on_delete(msg["params"])
            elif msg["command"] == "START":
                self.on_start(msg["params"])
            elif msg["command"] == "STOP":
                self.on_stop(msg["params"])
            elif msg["command"] == "IS_READY":
                result = self.on_is_ready()
                response["result"] = result
            elif msg["command"] == "CHANGE_DEBUG_MODE":
                self.on_change_debug_mode(msg["params"])
            elif msg["command"] == "GET_GPU_RESOURCE":
                result = self.on_get_gpu_resource()
                response["result"] = result
            elif msg["command"] == "RELOAD_GPU_WORKER":
                self.on_reload_gpu_worker()
        except RuntimeError as e:
            response["error"] = {"message": str(e)}
        except Exception as e:
            logging.error(e)

        try:
            await self._nats.publish(reply, json.dumps(response).encode())
        except ErrConnectionClosed as e:
            logging.error("Error ocurred when publishing response: {}, "
                          "ERROR: {}".format(response, e))
        except ErrTimeout as e:
            logging.error("Timeout ocurred when publishing response: {} "
                          "ERROR:: {}".format(response, e))

    def on_create(self, params):
        pass

    def on_read(self, params):
        pass

    def on_update(self, params):
        pass

    def on_delete(self):
        pass

    def on_start(self):
        pass

    def on_stop(self):
        pass

    def on_is_ready(self):
        pass

    def on_change_debug_mode(self, params):
        pass
    
    def on_get_gpu_resource(self):
        pass

    def on_reload_gpu_worker(self):
        pass
