import math
import json

from jagereye_ng.util.generic import get_config, uuid
from jagereye_ng.util.gpus import get_gpus_info
from jagereye_ng.util.string import pascal_to_snake
from jagereye_ng.io.db_engine import connect_db
from jagereye_ng.io.db_engine import DatabaseError


class ConfigurationError(Exception):
    pass


class OutOfResourcesError(Exception):
    pass

def find_requirement():
    """Find the requirement of licensed applications.

    The function finds the application requirement configuraion of models.

    Returns:
        dict: The details of required models, contains model version, format, 
        and GPU memory usage.

    Raises:
        ConfigurationError: If the model settings for applications are not
            correct.
    """

    # Initialize database service
    dbconfig = get_config()["services"]["database"]
    dbURL = "mongodb://{}:{}@{}:27017".format(dbconfig['env']["username"],
                                              dbconfig['env']["password"],
                                              dbconfig['network']['ip'])
    try:
        client, db = connect_db(
            dbconfig["db_name"],
            host=dbURL,
            authentication_source="admin"
        )
    except DatabaseError as e:
        raise("Database error occured while connecting: {}".format(str(e)))

    # get license record if any
    licenses = db["licenses"].find()

    client.close()

    if licenses is None:
        return {}

    # Read configuration of applications and models.
    apps_config = get_config()["apps"]
    models_config = get_config(config_file="models.yml")["models"]

    requirement = {}

    # find resource req of application
    for entry in licenses:
        for app in entry['application']:
            app_name = pascal_to_snake(app["type"])
            if app_name == 'stitching':
                # TODO pre allocate GPU for app stitching and config ...
                continue
            if not app_name in apps_config.keys():
                raise ConfigurationError("Application \"{}\" is not supported".format(app_name))

            # find resource requirement of the model
            for model_name in apps_config[app_name]["models"]:
                # Get the number of required channels and the GPU
                # memory resource and version for each model instance..
                if requirement.get(model_name) is None:
                    requirement[model_name] = {
                        "channel": app["channel"],
                        "per_model_amount": apps_config[app_name]["models"][model_name]["per_model_amount"],
                        "gpu_usage": models_config[model_name]["gpu_usage"],
                        "version": models_config[model_name]["version"],
                        "format": models_config[model_name]["format"],
                        "worker": []
                    }
                else:
                    requirement[model_name]["channel"] += app["channel"]

    return requirement

def find_allocation_settings(requirement):
    """Find the best allocation for GPU workers.

    The function determines the best allocation by looking-up the configuraion
    of applications and models, and the available GPUs resources.

    Returns:
        list of dict: The best allocation settings found by scheduler.

        Each item of the allocation settings is a dictionary that contains the
        name and resources of a GPU worker.

    Raises:
        OutOfResourcesError: If the GPU resources are enough for allocation.
    """

    # Get the GPUs information for allocation.
    gpus_info = get_gpus_info()
    for gpu_info in gpus_info:
        gpu_info["memory_left"] = gpu_info["memory_total"]

    # Initialize allocation settings.
    allocation_settings = []

    for model_name in requirement:
        # Add an allocation for each model instance. If the GPUs resources
        # are not enough, an exception will raise.
        model_req = requirement[model_name]

        instance_num = math.ceil(model_req["channel"] / model_req["per_model_amount"])

        for _ in range(instance_num):
            gpu_idx = -1
            for gpu_info in gpus_info:
                if gpu_info["memory_left"] > model_req["gpu_usage"]:
                    gpu_idx = gpu_info["index"]
                    gpu_fraction = \
                        float(model_req["gpu_usage"]) / gpu_info["memory_total"]
                    gpu_info["memory_left"] = \
                        gpu_info["memory_left"] - model_req["gpu_usage"]
                    break

            if gpu_idx == -1:
                raise OutOfResourcesError("GPUs resources are not enough"
                                            " for allocation")

            allocation_settings.append({
                "model_name": model_name,
                "name": "GPU_WORKER_{}:{}:{}:{}:{}:{}".format(
                    uuid(),
                    model_name.upper(),
                    model_req["version"],
                    model_req["format"],
                    gpu_idx,
                    gpu_fraction,
                ),
                "resources": {
                    "GPU:{}".format(model_name.upper()): model_req["per_model_amount"]
                }
            })

    return allocation_settings


def get_gpu_resource():
    return get_gpus_info()
