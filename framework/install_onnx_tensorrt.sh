#!/bin/sh

set -o errexit

readonly TENSORRT_VERSION=6.0
readonly CUDA_HOME=/usr/local/cuda

readonly ZIP_FILE=${TENSORRT_VERSION}.zip
readonly SRC_FOLDER=onnx-tensorrt-release-${TENSORRT_VERSION}

# Download the source zip file and unzip it.
wget -N https://github.com/onnx/onnx-tensorrt/archive/release/${ZIP_FILE}
unzip -o ${ZIP_FILE}

# Replace "/build" with "/usr/lib/x86_64-linux-gnu/" in setup.py.
# This will make sure the location of dependent shared library be correct.
sed -i 's/build\//\/usr\/lib\/x86_64-linux-gnu\//g' ${SRC_FOLDER}/setup.py

# Install the onnx_tensorrt
cd ${SRC_FOLDER} && python3 setup.py install
