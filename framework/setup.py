import glob
import os
from setuptools import Command
from setuptools import find_packages
from setuptools import setup
from setuptools.command.install import install
from setuptools.command.test import test
from shutil import copy
from shutil import copytree
from shutil import rmtree
import shlex
import subprocess
import sys
import tarfile
import urllib.request

# Metadata about the package
NAME = 'jagereye_ng'
DESCRIPTION = 'A large distributed scale video analysis framework.'


# Dependencies for testing
TESTS_REQUIRED=[
    'pytest',
    'pytest-mock',
    'pytest-cov',
]


# The framwork directory path.
FRAMEWORK_DIR = os.path.dirname(os.path.join(os.getcwd(), __file__))


def install_packages(target, user_space, cached):
    cmd = ['pip3', 'install'] + target

    if not cached:
        cmd.append('--no-cache-dir')
    if user_space:
        cmd.append('--user')

    subprocess.check_call(cmd)


def install_requirements(user_space, cached):
    install_packages(['-r', './requirements.txt'], user_space, cached)


def download_models(cached):
    """Download models that are defined in configuration file."""
    # XXX(SuJiaKuan): We should import yaml after it is installed.
    import yaml

    # Load models configuration.
    with open('{}/static/models.yml'.format(NAME)) as f:
        models_config = yaml.load(f)

    base_url = models_config["common"]["base_url"]

    # Download Models one by one.
    for model_name, model_config in models_config["models"].items():
        model_version = model_config["version"]
        file_basename = '{}.tar.gz'.format(model_version)
        cache_dirname = os.path.join('{}/.cache/{}/models/{}/{}'
                                     .format(os.path.expanduser('~'),
                                             NAME,
                                             model_name,
                                             model_version))
        target_dirname = os.path.join('{}/static/models/{}/{}'
                               .format(NAME, model_name, model_version))
        target_filename = os.path.join(target_dirname, file_basename)
        download_url = os.path.join(base_url, model_name, file_basename)

        if cached and os.path.exists(cache_dirname):
            # Remove the old directory.
            rmdir(target_dirname)

            # Copy the cached model to model directory.
            print('Copying cached model from {} to {}'
                  .format(cache_dirname, target_dirname))
            copytree(cache_dirname, target_dirname)
        else:
            # Create the directory that stores the model file.
            mkdir(target_dirname)

            # Download the model.
            print('Downloading {}'.format(download_url))
            urllib.request.urlretrieve(download_url, target_filename)

            # Extract the model tarball.
            print('Extracting {}'.format(target_filename))
            tar = tarfile.open(target_filename)
            tar.extractall(path=target_dirname)
            tar.close()

            # Delete the tarball because it won't be used any more.
            print('Deleting {}'.format(target_filename))
            os.remove(target_filename)

            if cached:
                # Copy to the cached directory.
                print('Copying {} -> {}'.format(target_dirname, cache_dirname))
                copytree(target_dirname, cache_dirname)

def encrypt_models(model_path):
    model_path = os.path.join(model_path, 'models')
    subprocess.check_call(['./encrypt_models.sh', model_path])

class InstallCommand(install):
    """Command to install framework."""

    user_options = install.user_options + [
        ('requirements', None, 'Packages installation from requirements.txt'),
        ('model', None, 'Jagereye_ng Model installation'),
        ('no-cache', None, 'Use cache for packages, model installation'),
        ('framework', None, 'Framework installation'),
    ]

    def initialize_options(self):
        install.initialize_options(self)
        self.requirements = False
        self.model = False
        self.no_cache = False
        self.framework = False

    def finalize_options(self):
        install.finalize_options(self)
        self.user = bool(self.user)
        self.requirements = bool(self.requirements)
        self.model = bool(self.model)
        self.no_cache = bool(self.no_cache)
        self.framework = bool(self.framework)
        self.install_all = self.requirements is False and \
                           self.model is False and self.framework is False

    def run(self):
        if self.install_all is True or self.requirements is True:
            install_requirements(self.user, not self.no_cache)
        
        if self.install_all is True or self.model is True:
            download_models(not self.no_cache)
            if os.getenv('ENCRYPT_MODEL') == 'true':
                encrypt_models(os.path.join(NAME, 'static'))
	
        if self.install_all is True or self.framework is True:
            install.run(self)

class TestCommand(test):
    """Command to test framework."""

    user_options = test.user_options + [
        ('no-cache', None, 'Use cache for installation'),
    ]

    def initialize_options(self):
        test.initialize_options(self)
        self.no_cache = False

    def finalize_options(self):
        test.finalize_options(self)
        self.no_cache = bool(self.no_cache)

    def run(self):
        # Install reuiqred packages and models for framework.
        install_requirements(True, not self.no_cache)
        download_models(not self.no_cache)

        # Install required packeges (pytest) for testing.
        install_packages(TESTS_REQUIRED, True, not self.no_cache)

        # XXX(SuJiaKuan): We should import pytest after it is installed.
        import pytest

        # Start running the testing.
        pytest_args = '''
            --cov-report html
            --cov-report term
            --cov={}
            {}
        '''.format(NAME, NAME)
        errno = pytest.main(shlex.split(pytest_args))
        sys.exit(errno)


class DocCommand(Command):
    """Command to generate documentation."""

    description = 'Generate documentation.'
    user_options = []

    def initialize_options(self):
        pass

    def finalize_options(self):
        pass

    def run(self):
        subprocess.check_call(['doxygen', '.Doxyfile'])


class LintCommand(Command):
    """Command to lint source code."""

    description = 'Lint source code.'
    user_options = []

    def initialize_options(self):
        pass

    def finalize_options(self):
        pass

    def run(self):
        subprocess.check_call(['pylint', 'jagereye_ng'])


class DockerCommand(Command):
    """Command to build docker images."""

    description = 'Build docker images.'
    user_options = []

    def initialize_options(self):
        pass

    def finalize_options(self):
        pass

    def run(self):
        version = get_version()
        docker_file = 'Dockerfile.dev'
        image_name = 'jagereye/framework:{}'.format(version)
        # this is to avoid using JAGERENV to determine whether we should encrypt models
        # for JAGERENV sometimes changed during build time.
        encrypt_arg = 'ENCRYPT_MODEL=true' \
            if os.getenv('JAGERENV') == 'production' else 'ENCRYPT_MODEL=false'
        self._build(docker_file, image_name, [encrypt_arg])

        # if building for production, we need a framework image without any 
        # jager source code.
        if os.getenv('JAGERENV') == 'production':
            docker_file = 'Dockerfile.pro'
            image_name = 'jagereye/framework_production:{}'.format(version)
            self._build(docker_file, image_name)

    def _build(self, docker_file, image_name, build_args=None):
        docker_cmd = [
            'docker', 'build',
            '-f', docker_file,
            '-t', image_name,
            '.'
        ]
        if build_args is not None:
            for arg in build_args:
                docker_cmd.append('--build-arg')
                docker_cmd.append(arg)
        print('Building Docker: file = {}, image = {}'
              .format(docker_file, image_name))
        subprocess.check_call(docker_cmd)


def get_version():
    """Get version of JagerEye."""
    config_file_path = os.path.join(NAME, 'static/config.yml')

    for line in open(config_file_path, 'r'):
        if line.startswith('version'):
            version = line.split(' ')[1]
            for replaced in ['\'', '\"', '\n']:
                version = version.replace(replaced, '')

            return version


def abspath(path):
    """Get the absolute path of a file or a directory.

    Args:
      path (string): The path relative to framework directory.
    """
    return os.path.join(FRAMEWORK_DIR, path)


def cp_static_files(static_files):
    """Copy files to static folder.

    Args:
      static_files (list of string): The files to be copied.
    """
    static_dir = os.path.join(NAME, 'static')
    # Create the static directory if necessary.
    if not os.path.exists(static_dir):
        os.makedirs(static_dir)
    # Now, copy files into the static directory.
    for static_file in static_files:
        if os.path.isfile(static_file):
            if os.getenv('JAGERENV') == 'production' and \
                static_file.endswith('config.yml'):
                import yaml
                with open(static_file, 'r') as f:
                    config = yaml.load(f)
                with open(os.path.join(static_dir, 'config.py'), 'w') as f:
                    f.write('data = ')
                    f.write(str(config))
                print('Transform {} into {}'.format(
                    static_file, 
                    os.path.join(static_dir, 'config.py')))

            copy(static_file, static_dir)
            print('Copy {} into {}'.format(static_file, static_dir))


def rmdir(dirname):
    if os.path.exists(dirname) and os.path.isdir(dirname):
        rmtree(dirname)

def mkdir(dirname):
    if not os.path.exists(dirname):
        os.makedirs(dirname)


def main():
    # Copy static files into framework library directory.
    cp_static_files([
        abspath('../shared/messaging.json'),
        abspath('../shared/database.json'),
        abspath('../shared/externals.json'),
        abspath('../shared/config.yml'),
        abspath('../shared/models.yml'),
        abspath('./jagereye_ng/util/logging.template.yml')
    ])

    # Now, run the setup script.
    setup(
        name=NAME,
        version=get_version(),
        description=DESCRIPTION,
        packages=find_packages(include=[NAME, '{}.*'.format(NAME)]),
        include_package_data=True,
        zip_safe=False,
        cmdclass = {
            'doc': DocCommand,
            'docker': DockerCommand,
            'install': InstallCommand,
            'lint': LintCommand,
            'test': TestCommand
        }
    )


if __name__ == '__main__':
    main()
