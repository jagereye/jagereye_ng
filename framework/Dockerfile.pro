FROM jagereye/tensorflow:1.13.1-gpu-py3

MAINTAINER SuJiaKuan <feabries@gmail.com>

ENV NUMPY_VERSION 1.14.3
ENV OPENCV_VERSION 3.3.1
ENV CUDA_VERSION 10.0
ENV CUDNN_MAJOR_VERSION 7
ENV CUDNN_VERSION 7.6
ENV CUDNN_FULL_VERSION 7.6.3.30-1+cuda10.0
ENV TENSORRT_VERSION 6.0.1.5
ENV JAGERENV production

WORKDIR /root

# Install system packages.
RUN apt-get update && apt-get install -y --no-install-recommends --allow-downgrades \
        build-essential \
        cmake \
        libcudnn${CUDNN_MAJOR_VERSION}=${CUDNN_FULL_VERSION} \
# gstreamer
        gstreamer1.0-libav \
        gstreamer1.0-plugins-bad \
        gstreamer1.0-plugins-base \
        gstreamer1.0-plugins-good \
        gstreamer1.0-plugins-ugly \
        libgstreamer1.0-0 \
        libgstreamer1.0-dev \
        libgstreamer-plugins-base1.0-dev \
# gst RTSP server
        gir1.2-gst-rtsp-server-1.0 \
# ffmpeg
        libavcodec-dev \
        libavformat-dev \
        libswscale-dev \
# QT:   https://doc.qt.io/Qt-5/linux-requirements.html
        libfontconfig1-dev \
        libfreetype6-dev \
        libx11-dev \
        libxext-dev \
        libxfixes-dev \
        libxi-dev \
        libxrender-dev \
        libxcb1-dev \
        libx11-xcb-dev \
        libxcb-glx0-dev \
        libxcb-keysyms1-dev \
        libxcb-image0-dev \
        libxcb-shm0-dev \
        libxcb-icccm4-dev \
        libxcb-sync0-dev \
        libxcb-xfixes0-dev \
        libxcb-shape0-dev \
        libxcb-randr0-dev \
        libxcb-render-util0-dev \
        python3-dev \
        swig \
	git \
        unzip \
        wget \
        pkg-config \
# for debug in dev
        vim \
# for packaging compression
        upx-ucl \
        libgeos-dev \
        && \
    apt-get clean && \
    rm -rf /var/lib/apt/lists/*

# Get pip.
RUN curl -O https://bootstrap.pypa.io/get-pip.py && \
    python3 get-pip.py && \
    rm get-pip.py

RUN pip3 --no-cache-dir install \
        numpy==${NUMPY_VERSION}

# Install OpenCV.
# TODO(JiaKuan Su):
# 1. Remove unnecessary dependencies.
# 2. Check what dependencies should be added for acceleration.
RUN wget https://github.com/opencv/opencv/archive/${OPENCV_VERSION}.zip
RUN unzip ${OPENCV_VERSION}.zip
WORKDIR opencv-$OPENCV_VERSION/build
RUN cmake \
        -D BUILD_DOCS=OFF \
        -D BUILD_EXAMPLES=OFF \
        -D BUILD_PERF_TESTS=OFF \
        -D BUILD_TESTS=OFF \
        -D BUILD_PYTHON_SUPPORT=ON \
        -D CMAKE_BUILD_TYPE=RELEASE \
        -D CMAKE_INSTALL_PREFIX=$(python3 -c "import sys; print(sys.prefix)") \
        -D CV_TRACE=OFF \
        -D INSTALL_C_EXAMPLES=OFF \
        # calibration
        -D WITH_GTK=ON \
        -D WITH_GTK_2_X=ON \
        # use GSTREAMER
        -D WITH_FFMPEG=OFF \
        -D WITH_GSTREAMER=ON \
        -D PYTHON_EXECUTABLE=$(which python3) \
        -D PYTHON_INCLUDE_DIR=$(python3 -c "from distutils.sysconfig import get_python_inc; print(get_python_inc())") \
        -D PYTHON_PACKAGES_PATH=$(python3 -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())") \
        ..

# TODO add opencv contirb to build
RUN make install -j$(nproc)
WORKDIR /root
RUN rm -r \
        ${OPENCV_VERSION}.zip \
        opencv-${OPENCV_VERSION}

# Install TensorRT.
ENV TENSORRT_TAR TensorRT-${TENSORRT_VERSION}.Ubuntu-18.04.x86_64-gnu.cuda-${CUDA_VERSION}.cudnn${CUDNN_VERSION}.tar.gz
ENV TENSORRT_URL https://jagereye-production.s3-ap-northeast-1.amazonaws.com/packages/tensorrt/${TENSORRT_TAR}
ENV TENSORRT_FOLDER TensorRT-${TENSORRT_VERSION}
RUN wget ${TENSORRT_URL} && \
    tar -xvf ${TENSORRT_TAR} && \
    cd ${TENSORRT_FOLDER} && \
    cp -r lib/* /usr/lib/x86_64-linux-gnu/ && \
    rm /usr/lib/x86_64-linux-gnu/libnv*.a && \
    cp -r include/* /usr/include/x86_64-linux-gnu/ && \
    pip3 install python/tensorrt-${TENSORRT_VERSION}-cp36-none-linux_x86_64.whl && \
    pip3 install uff/uff-*-py2.py3-none-any.whl && \
    cd ../ && \
    rm -rf ${TENSORRT_FOLDER} ${TENSORRT_TAR}

# Install onnx-tensort.
COPY install_onnx_tensorrt.sh .
RUN ./install_onnx_tensorrt.sh
RUN rm install_onnx_tensorrt.sh

# Add a user: "jager" and switch to it.
RUN groupadd -g 999 jager && \
    useradd -r -m -u 999 -g jager jager

USER jager
ENV HOME /home/jager
WORKDIR ${HOME}
