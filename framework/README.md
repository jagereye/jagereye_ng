# JagerEye Framework

The directory contains framework library for developing JagerEye applications.

## Installation

You can install framework on your host machine, or build Docker images that contain the framwork.

### On the Host

* Install Python 3 (>= 3.5).

* Install Python 3 binding for OpenCV (>=2.4.0) that is compiled with [Gstreamer](https://gstreamer.freedesktop.org/).

* Install pip3.

* Install cuda 10.0 and cudnn 7.6.

* Install TensorRT 6.0.

* Install [onnx-tensorrt](https://github.com/onnx/onnx-tensorrt) for TensorRT 6.0, by running the customized script.

```bash
sudo ./install_onnx_tensorrt.sh
```

* Run the installation script.

```bash
# The following instruction installs framework on your own home directory. You can also
# run 'sudo python3 setup.py install' to install framework on system.
python3 setup.py install --user
```

* Alternatively, you can also use `jager` command (as described in [here](..)) to install.

```bash
# The following instruction installs framework on your own home directory. You can also
# run 'sudo jager install' to install framework on system.
jager install --user
```

### On Docker

* Install Python 3 (>= 3.5).

* Install Docker (>=17.09.0).

* Install nvidia-docker2.

* Run the Docker building script.

```bash
python3 setup.py docker --target=worker
```

* Now, you have a new image called `jagereye/framework` that contains the framework library.

## Testing

You can run unit testing by following steps:

* Install the dependencies as described in [Installation on the host](#on-the-host).

* Run the testing script.

```bash
python3 setup.py test
```

After the testing script ends, it will show success / failed test cases and the code coverage report. You can also open `htmlcov/index.html` via browser to see the code coverage report.`
