#!/bin/sh

cd $1

for i in */;
do
    PASSWD=$(echo -n ${i%/}"jager0402" | sha1sum | awk '{print $1}') && \
    zip -r -P $PASSWD "${i%/}.zip" "$i" && \
    rm -rf ${i%/}; \
done

exit 0