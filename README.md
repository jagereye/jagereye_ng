# JagerEye

JagerEye is a large distributed scale video analysis framework.

## Installation

### Prerequisites

The following packages are required before installation
- docker>=17.09.0 ([docker-ce](https://docs.docker.com/install/linux/docker-ce/ubuntu/#install-using-the-repository))
- nvidia-docker2 ([github repo](https://github.com/NVIDIA/nvidia-docker#ubuntu-140416041804-debian-jessiestretch))
- docker-compose>=1.19.0 ([link](https://docs.docker.com/compose/install/#install-compose))
- python>=3.5
- pip3

### Steps

* Clone the project and go to the directory. (You may be prompt with gitlab username/password)
```bash
git clone https://gitlab.com/jagereye/jagereye_ng
cd jagereye_ng
```

* Export environment variables.
```bash
# The root directory of the project.
export JAGERROOT=$(pwd)
# The mode to build JagerEye, it can be 'development' or 'production'
export JAGERENV=development
# Path to the binary folder.
export PATH=$JAGERROOT/bin:$PATH

# gitlab access token

get the personal access token with scope api [ref](https://gitlab.com/profile/personal_access_tokens)

export GITLAB_DEPLOY_ACCOUNT=your_gitlab_account
export GITLAB_DEPLOY_TOKEN=your_gitlab_access_token
```

* Setup the configuration file.
```bash
pushd shared
ln -sfn config.${JAGERENV}.yml config.yml
popd
```

* Login to gitlab registry (You may need to create a deploy token on [jagereye/jagereye_webapp](https://gitlab.com/jagereye/jagereye_webapp/-/settings/repository))
```bash
docker login registry.gitlab.com -u <USER_ID> -p <DEPLOY_TOKEN>
```

* Install the dependencies for building services.
```bash
pip3 install -r deploy/requirements.txt
```

* Build the base docker images for services.
```bash
jager build servicebase
```

* Build the base docker images for applications.
```bash
jager build appbase
```

* Build the docker images for services and applications.
```bash
# You can also build services and applications separately by running
# 'jager build services' and 'jager build apps'.
jager build all
```

* Now, we can start running applications and services.
```bash
# You can also start services and applications separately by running
# 'jager start services' and 'jager start apps'.
jager start all
```

* Alternatively, we can also deploy JagerEye as a [systemd](https://en.wikipedia.org/wiki/Systemd) service.
```bash
# Deploy JagerEye as a systemd service. This command will also make
# JagerEye starts automatically when system boots up. If you need to
# disable the startup mechanism, please run
# 'sudo systemctl disable jagereye'.
sudo -E env "PATH=$PATH" jager deploy
# Run node_manager, an service to report resource usages.
sudo systemctl start node_exporter
# Start running JagerEye service.
sudo systemctl start jagereye
```

## Compatibility table with other services
| JagerEye version | FaceUI version | Tripwire UI version| License Server |
|------------------|----------------|--------------------|----------------|
| ~V1.5            | N/A            | V1.0               | N/A            |
| V1.6             | V1.0           | N/A                | V1.0           |
| V1.7             | V1.1           | N/A                | V1.0           |
| V1.7.1           | V1.2           | N/A                | V1.0           |

## Contributing

### Coding Style Guildline

#### Python

* Follow [Google Python Style Guide](https://google.github.io/styleguide/pyguide.html).

* To learn how to write docstrings, [Example Google Style Python Docstrings](http://sphinxcontrib-napoleon.readthedocs.io/en/latest/example_google.html) is a good example.

#### Node.js

```bash
TODO
```

## Templates

Please use following templates first when you open a new issue or merge request:

- [Issue Template](https://gitlab.com/jagereye/gitlab_templates/raw/master/jagereye_ng/issue.md)
- [Merge Request Template](https://gitlab.com/jagereye/gitlab_templates/raw/master/jagereye_ng/merge_request.md)
