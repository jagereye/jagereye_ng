#!/bin/bash
#
# A command-line tool for JagerEeye

set -o errexit

readonly BUILD_FOLDER="${JAGERROOT}/build"
readonly REGISTRY_ENDPOINT="registry.gitlab.com"

jager::main_usage() {
    echo "
USAGE: jager COMMAND [arg...]
       jager [ -h | --help | -v | --version ]

Command line tool for JagerEye

Options:
    -h, --help      Show this screen
    -v, --version   Print version information and quit

Commands:
    build           Build docker images
    deploy          Deploy JagerEye to system
    bundle          Generate a bundle that contains docker images and related files
    push            Push all images to GitLab container registry
    info            Display system-wide information
    license         Generate license for development from config
    install         Install framework to host without building docker images
    start           Start services/applications docker containers

Run 'jager COMMAND --help' for more information on a command.

[NOTE] Please make sure the docker daemon is up and running before starting.
"
}

jager::error() {
    echo -e "\033[0;31mERROR\033[0m: ${1}"
    exit 1
}

jager::build() {
    local usage="
USAGE: jager build TARGET [OPTIONS]

Build JagerEye docker images

This command is equivalent to command 'docker-compose build'.

Options:
    -*, --*         Please reference the 'Options' section from 'docker-compose build --help'

Targets:
    all             Build all targets without base images (Note: it does NOT take any arguments) (default)
    apps            Only build applications
    appbase         Only build application base images (Note: it does NOT take any arguments)
    services        Only build services
    servicebase     Only build service base images (Note: it does NOT take any arguments)

[NOTE] 1. 'apps', including the apps part in 'all', is built by 'docker' instead of 'docker-compose', so beware of
          the use of arguments.
       2. Make sure you have the base images built before building apps/services images.
    "
    local target="all"
    local args=""

    if (( $# > 0 )); then
        target=${1}
        case "${target}" in
            all|apps|appbase|services|servicebase)
                shift
                ;;
            -h|--help)
                echo "${usage}"
                exit 0
                ;;
            *)
                jager::error "Invalid target: ${target}
                ${usage}"
                ;;
        esac

        while (( $# > 0 ))
        do
            args="${args} ${1}"
            shift
        done
    fi

    if [ "${target}" == "all" ]; then
        jager::prepare_build apps services
        jager::build_all "${args}"
    elif [ "${target}" == "apps" ]; then
        jager::prepare_build apps
        jager::build_apps "${args}"
    elif [ "${target}" == "appbase" ]; then
        jager::prepare_build docker
        jager::build_tensorflow
        jager::prepare_build framework
        jager::build_appbase
    elif [ "${target}" == "services" ]; then
        jager::prepare_build services
        jager::build_services "${args}"
    elif [ "${target}" == "servicebase" ]; then
        jager::prepare_build docker
        jager::build_servicebase
    fi
}

jager::prepare_build() {
    local targets="${@}"
    jager::show_info

    # Reset build folder
    rm -rf ${BUILD_FOLDER}
    mkdir ${BUILD_FOLDER}

    cd ${BUILD_FOLDER}

    # Copy shared files to build folder
    cp -r ${JAGERROOT}/shared .
    cp -r ${JAGERROOT}/modules .

    # Create a symbolic link, named 'config.yml', for service config file, and it will be used for:
    # 1) deploy/docker_gen.py, which will look for 'config.yml' to generate docker-compose.yml.
    # 2) When building 'appbase', framework/setup.py will copy 'config.yml' to framework static folder.
    local src_config_file="${BUILD_FOLDER}/shared/config.${JAGERENV}.yml"
    if [ ! -f ${src_config_file} ]; then
        jager::error "Config file '${src_config_file}' was not found!"
    fi
    ln -sfn ${src_config_file} ${BUILD_FOLDER}/shared/config.yml

    # Copy target files to build folder
    for item in ${targets}
    do
        cp -r ${JAGERROOT}/${item} .
    done
}

jager::build_services() {
    local args="${1}"

    cd ${BUILD_FOLDER}

    # Copy shared files to each service folder
    local shared_folder="${BUILD_FOLDER}/shared"
    local modules_folder="${BUILD_FOLDER}/modules"
    # TODO: Do more fine-grained control about which shared file is required
    #       by which service
    local service_list=$(ls -d ./services/*/)
    for item in ${service_list}; do
        # XXX: It's ugly, but currently there is no way to copy/add files outside
        #      the build context in Dockerfile, even for symbolic linked files, ie.,
        #      can't write something like 'COPY ../some_file .'
        #      For more details, please read https://github.com/moby/moby/issues/18789
        cp ${shared_folder}/* ${item}
        cp -r ${modules_folder} ${item}

        # if there are multiple Dockerfile, we will use Dockerfile.dev for development purpose, 
        # and Dockerfile.pro for production
        if [ -e ${item}/Dockerfile.dev -a "${JAGERENV}" != "production" ]; then
            cp ${item}/Dockerfile.dev ${item}/Dockerfile
        fi
        if [ -e ${item}/Dockerfile.pro -a "${JAGERENV}" == "production" ]; then
            cp ${item}/Dockerfile.pro ${item}/Dockerfile
        fi
    done

    python3 ${JAGERROOT}/deploy/docker_gen.py --workdir=${BUILD_FOLDER} --is_build services
    echo "Run 'docker-compose build ${args}'"
    docker-compose build ${args}
}

jager::build_apps() {
    local args="${1}"
    local appname="analyzer_manager"

    cd ${BUILD_FOLDER}/apps
    echo "Run 'docker build -t jagereye/${appname}:${VERSION} --build-arg VERSION=${VERSION} --build-arg GITLAB_DEPLOY_ACCOUNT=${GITLAB_DEPLOY_ACCOUNT} --build-arg GITLAB_DEPLOY_TOKEN=${GITLAB_DEPLOY_TOKEN}  ${args} .'"
    df=""
    if [ ${JAGERENV} == "development" ]; then
        df=".dev"
    elif [ ${JAGERENV} == "production" ]; then
        df=".pro"
    fi
    docker build -f "Dockerfile${df}" -t jagereye/${appname}:${VERSION} --build-arg VERSION=${VERSION} --build-arg GITLAB_DEPLOY_ACCOUNT=${GITLAB_DEPLOY_ACCOUNT} --build-arg GITLAB_DEPLOY_TOKEN=${GITLAB_DEPLOY_TOKEN} ${args} .
}

jager::build_all() {
    jager::build_services
    jager::build_apps
}

jager::build_tensorflow() {
    cd ${BUILD_FOLDER}/docker
    docker build -t jagereye/tensorflow:1.13.1-gpu-py3 -f Dockerfile.tensorflow .
    echo "Run 'docker build -t jagereye/tensorflow -f appbase/Dockerfile.tensorflow .'"
}

jager::build_appbase() {
    cd ${BUILD_FOLDER}/framework
    python3 setup.py docker
}

jager::build_servicebase() {
    cd ${BUILD_FOLDER}/docker
    docker build -t jagereye/nodejs:${VERSION} -f Dockerfile.nodejs .
    echo "Run 'docker build -t jagereye/nodejs -f services/Dockerfile.nodejs .'"
}

jager::bundle() {
    local usage="
USAGE: jager bundle [OPTIONS]

Generate a bundle that contains the docker images and related files.

Options:
    -h, --help      Show this screen
    "
    if (( $# > 0 )); then
        case "${1}" in
            -h|--help)
                echo "${usage}"
                exit 0
                ;;
            *)
                jager::error "Invalid option: ${1}
                ${usage}"
                ;;
        esac
    fi

    jager::gen_bundle true bundle_path

    echo "The generated bundle is at ${bundle_path}"
}

jager::gen_bundle() {
    local -r output="jagereye-${VERSION}"
    local -r output_file="${output}.tar.gz"
    local -r output_dir="${BUILD_FOLDER}/${output}"

    local -r gzip=${1}
    local resultvar=${2}

    jager::prepare_build

    cd ${BUILD_FOLDER}

    python3 ${JAGERROOT}/deploy/docker_gen.py --workdir=${BUILD_FOLDER} all

    # Collect the image names.
    for img in $(docker-compose config | awk '{if ($1 == "image:") print $2;}'); do
        imgs="$imgs $img"
    done

    # Generate the docker images.
    echo "Run 'docker ${imgs} | gzip > images.tar.gz'"
    docker save ${imgs} | gzip > images.tar.gz

    # Create a output directory that contains the necessary files.
    mkdir ${output}
    mkdir ${output}/units
    mkdir ${output}/network
    mkdir ${output}/bin
    mkdir ${output}/script
    echo ${VERSION} > ${output}/VERSION
    mv docker-compose.yml ${output}
    mv images.tar.gz ${output}
    cp ${JAGERROOT}/deploy/units/jagereye.service ${output}/units
    cp ${JAGERROOT}/deploy/units/node_exporter.service ${output}/units
    cp ${JAGERROOT}/deploy/bin/jager-deploy ${output}/bin
    cp ${JAGERROOT}/deploy/bin/node_exporter ${output}/bin
    cp ${JAGERROOT}/deploy/script/* ${output}/script

    if [ ${gzip} == false ]; then
        # Return value.
        eval $resultvar="'${output_dir}'"
    else
        # Now, create the bundle.
        echo "Run 'tar zcvf ${output_file} ${output}'"
        tar zcvf ${output_file} ${output}

        # Remove the output directory.
        rm -rf ${output}

        # Return value.
        eval $resultvar="'${BUILD_FOLDER}/${output_file}'"
    fi
}

jager::deploy() {
    local usage="
USAGE: jager deploy [OPTIONS]

Deploy JagerEye related files to system.

Options:
    -h, --help      Show this screen
    "
    if (( $# > 0 )); then
        case "${1}" in
            -h|--help)
                echo "${usage}"
                exit 0
                ;;
            *)
                jager::error "Invalid option: ${1}
                ${usage}"
                ;;
        esac
    fi

    # Generate the files for deployment.
    jager::gen_bundle false bundle_dir

    # The path to the deployment executable.
    local -r deploy_bin="${bundle_dir}/bin/jager-deploy"

    # Install to system by running the deployment executable.
    # XXX: We don't install images here, because we assume the images have been
    #      installed by running 'jager build'.
    bash ${deploy_bin} install package
    bash ${deploy_bin} install folders
    bash ${deploy_bin} install service

    # The build folder will be unused, clean it.
    rm -rf ${BUILD_FOLDER}
}

jager::start() {
    local usage="
USAGE: jager start TARGET [OPTIONS]

Start JagerEye docker containers.

This command is equivalent to command 'docker-compose up'.

Unlike 'docker-compose up' we won't build the docker images if they don't exist, please
make sure you have the TARGET docker images ready, by running 'jager build TARGET', before
starting TARGET.

Options:
    -*, --*         Please reference the 'Options' section from 'docker-compose up --help'

Targets:
    all             Start all targets (default)
    services        Only start services
    apps            Only start applications
    "
    local target="all"
    local args=""

    if (( $# > 0 )); then
        target=${1}
        case "${target}" in
            all|services|apps)
                shift
                ;;
            -h|--help)
                echo "${usage}"
                exit 0
                ;;
            *)
                jager::error "Invalid target: ${target}
                ${usage}"
                ;;
        esac

        while (( $# > 0 ))
        do
            args="${args} ${1}"
            shift
        done
    fi

    jager::prepare_build

    cd ${BUILD_FOLDER}

    python3 ${JAGERROOT}/deploy/docker_gen.py --workdir=${BUILD_FOLDER} ${target}
    echo "Run 'docker-compose up ${args}'"
    docker-compose up ${args}
}

jager::install() {
    local usage="
USAGE: jager install [OPTIONS]

Install framework to host without building docker images

This command is equivalent to command 'python3 setup.py install [options]'
in 'framework' directory.

Options:
    -h, --help      Show this screen
    --user          Install framework in user's own directory
    --requirements  Install framework packages from requirements.txt
    --model         Install jagereye_ng model
    --no-cache      Use cache for packages, model installation
    --framework     Install jagereye_ng framework
    "
    local target=""

    while (( $# > 0 ))
    do
        case "${1}" in
            -h|--help)
                echo "${usage}"
                exit 0
                ;;
            --user|--model|--requirements|--framework)
                target="${target} ${1}"
                shift
                ;;
            *)
                jager::error "Invalid option: ${1}
                ${usage}"
                ;;
        esac
    done

    jager::prepare_build framework

    cd ${BUILD_FOLDER}/framework

    echo "Run 'python3 setup.py install ${target}'"
    python3 setup.py install ${target}
}

jager::push() {
    local usage="
USAGE: jager push TARGET [OPTIONS] [ARG]

This automatically parse all JagerEye docker images and push to GitLab container registry.

Options:
    -h, --help      Show this screen
    -t, --tag       Docker image tag [ARG] (defaults to current JagerEye version)
    -e, --env       The Docker container environment (defaults to \"development\")

For instance:
docker tag jagereye/api:1.7.0 jagereye/api:tag
docker tag jagereye/api:tag registry.gitlab.com/jagereye/jagereye_ng/development/tag:api
docker push registry.gitlab.com/jagereye/jagereye_ng/development/tag:api
    "
    local args=""
    local tag=""
    local container_env="development"
    while (( $# > 0 )); do
        case "${1}" in
            -h|--help)
                echo "${usage}"
                exit 0
                ;;
            -t|--tag)
                tag=${2}
                shift 2
                ;;
            -e|--env)
                container_env=${2}
                shift 2
                ;;
            *)
                jager::error "Invalid arguments: ${1}
                ${usage}"
                ;;
        esac
    done

    if [ "${tag}" = "" ]; then
        tag="v${VERSION}"
    fi

    if !([[ -z "${GITLAB_DEPLOY_ACCOUNT}" ]] && [[ -z "${GITLAB_DEPLOY_TOKEN}" ]]);then
        docker login $REGISTRY_ENDPOINT -u $GITLAB_DEPLOY_ACCOUNT -p $GITLAB_DEPLOY_TOKEN
    else
        docker login $REGISTRY_ENDPOINT
    fi

    local images=$(ls -d ./services/*/ | awk -F'/' '{print $3}')
    for image in ${images}; do
        docker::tag_and_push $image $tag
    done
    docker::tag_and_push "analyzer_manager" $tag

    docker logout $REGISTRY_ENDPOINT
}

docker::tag_and_push() {
    local -r image=${1}
    local -r tag=${2}
    local -r group="jagereye"
    local -r project="jagereye_ng"
    local -r repository="jagereye"
    local -r registry_url="$REGISTRY_ENDPOINT/$group/$project/$container_env/$tag"

    echo "docker tag $repository/$image:$VERSION $repository/$image:$tag"
    docker tag $repository/$image:$VERSION $repository/$image:$tag
    echo "docker tag $repository/$image:$tag $registry_url:$image"
    docker tag $repository/$image:$tag $registry_url:$image
    echo "docker push $registry_url:$image"
    docker push $registry_url:$image
}

jager::show_info() {
    echo "
Environment Variables:
    JAGERROOT: ${JAGERROOT}
    JAGERENV: ${JAGERENV}
    "
}

jager::license() {
    local license_config="license.yml"
    local license_dev_path="dev/license"
    local usage="
USAGE: jager license [OPTIONS]

For our jager application development environment. to simplify license process with license server, this command will generate license entry in database directly."
    local config="
we provide a license config: ${JAGERROOT}/${license_config} in ${license_dev_path},
you can change depends on your apps requirement.

for example:
"
    local options="
Options:
    -h, --help      Show this screen"
    if (( $# > 0 )); then
        case "${1}" in
            -h|--help)
                echo "${usage}"
		echo "${config}"
		cat "$JAGERROOT/${license_dev_path}/${license_config}"
		echo "${options}"
                exit 0
                ;;
            *)
                jager::error "Invalid option: ${1}
                ${usage}"
                ;;
        esac
    fi
    python3 $JAGERROOT/dev/license/license.py
}

jager::show_version() {
    echo "Jager version ${VERSION}"
}

#########################
# Sanity Check
#########################

if [ "${JAGERROOT}" == "" ]; then
    jager::error "Environment variable 'JAGERROOT' is not defined!"
fi
if [ ! "${JAGERENV}" == "" ]; then
    if [ "${JAGERENV}" != "production" ] && [ "${JAGERENV}" != "development" ] && [ "${JAGERENV}" != "test" ]; then
        jager::error " Invalid value of environment variable 'JAGERENV': ${JAGERENV}.
        The value should be one of the following: 'production', 'development', 'test'."
    fi
else
    export JAGERENV=production
fi

readonly VERSION=$(grep -m 1 'version:' $JAGERROOT/shared/config.$JAGERENV.yml | awk '{print $2}' | tr -d \'\")

#########################
# Main
#########################

if [ "$#" == "0" ]; then
    jager::main_usage
    exit 1
fi

opt=${1}
shift
case "${opt}" in
    build)
        jager::build "${@}"
        ;;
    push)
        jager::push "${@}"
        ;;
    bundle)
        jager::bundle "${@}"
        ;;
    deploy)
        jager::deploy "${@}"
        ;;
    start)
        jager::start "${@}"
        ;;
    install)
        jager::install "${@}"
        ;;
    info)
        jager::show_info
        ;;
    license)
	jager::license "${@}"
	;;
    -v|--version)
        jager::show_version
        ;;
    *)
        jager::main_usage
        ;;
esac
