const express = require('express')
const proxy = require('express-http-proxy')
const bodyParser = require('body-parser')
const cors = require('cors')

const { authenticate } = require('./auth/passport')
const relay = require('./routes/relay')
const streamingManager = require('./streaming/manager')
const { API_ENTRY, HTTP_FLV_HOST, SUPPORTED_CODEC } = require('./constants')

const app = express()

app.use(cors({
    // Some legacy browsers (IE11, various SmartTVs) choke on 204.
    optionsSuccessStatus: 200,
}))
app.use(bodyParser.json())
app.use(bodyParser.urlencoded({
    extended: false,
}))

// Proxy live streaming service for http-flv.
app.use(
    `/${SUPPORTED_CODEC.H264}`,
    streamingManager.traceRequest(),
    proxy(HTTP_FLV_HOST),
)

// Setup API authentication.
app.use(API_ENTRY, authenticate)

// Initialize API.
app.use(API_ENTRY, relay)

// Logging errors.
app.use((err, req, res, next) => {
    console.error(err)
    next(err)
})

// Catch-all error handling.
app.use((err, req, res, next) => {
    const error = {
        code: err.code,
        message: err.message
    }
    res.status(err.status).send({error: error})
})

module.exports = app
