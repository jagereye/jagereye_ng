const config = require('jagereye_ng/config')

const streamerConfig = config.services.streamer
const liveStreamingConfig = config.services.live_streaming

const API_ENTRY = `/${streamerConfig.api_base_url}`

const RTMP_BASE_URL =
    `rtmp://${liveStreamingConfig.network.ip}:${liveStreamingConfig.ports.rtmp}`
const HTTP_FLV_HOST =
    `${liveStreamingConfig.network.ip}:${liveStreamingConfig.ports.http}`

const SUPPORTED_CODEC = Object.freeze({
    H264: 'h264',
})

module.exports = {
    API_ENTRY,
    RTMP_BASE_URL,
    HTTP_FLV_HOST,
    SUPPORTED_CODEC,
}
