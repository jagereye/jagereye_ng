const app = require('./app')
const config = require('jagereye_ng/config')

const port = parseInt(config.services.streamer.ports.client)

app.listen(port, (err) => {
    if (err) {
        console.error(err)
    } else {
        console.info(`==> Listening on port ${port}`)
    }
})

