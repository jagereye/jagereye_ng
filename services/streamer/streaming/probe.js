const ffmpeg = require('fluent-ffmpeg');
const Promise = require('bluebird');
const find = require('lodash/find');

const ffprobe = Promise.promisify(ffmpeg.ffprobe);

const { ProbeError } = require('../errors')

async function probeStreamMetadata(url) {
    let stream;

    const metadata = await ffprobe(url).catch(() => {
        throw new ProbeError(`Stream not found: ${url}`)
    })

    stream = find(metadata.streams, (stream) => (
        stream.width > 0 && stream.height > 0
    ))

    if (!stream) {
        stream = find(metadata.streams, (stream) => (
            stream.coded_width > 0 && stream.coded_height > 0
        ))

        if (!stream) {
            throw new ProbeError(`Can not get streaming size: ${url}`)
        }
    }

    return {
        width: stream.coded_width,
        height: stream.coded_height,
        codec: stream.codec_name,
    }
}

module.exports = {
    probeStreamMetadata,
}
