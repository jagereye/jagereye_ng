const urljoin = require('url-join')
const find = require('lodash/find')
const forEach = require('lodash/forEach')
const isEqual = require('lodash/isEqual')

const { probeStreamMetadata } = require('./probe')
const { convertToFlv } = require('./convert')
const { genUUID } = require('../utils/common')
const { CodecError } = require('../errors')
const { RTMP_BASE_URL, SUPPORTED_CODEC } = require('../constants')

class RelayWorker {
    constructor(url, metadata, relayOut) {
        this.url = url
        this.metadata = metadata
        this.relayOut = relayOut

        this.numConnections = 0
    }

    start() {}

    kill() {}
}

class H264RelayWorker extends RelayWorker {
    constructor(url, metadata, relayOut, rtmpUrl) {
        super(url, metadata, relayOut)

        this._rtmpUrl = rtmpUrl

        this._instance = null
    }

    start() {
        if (!this._instance) {
            this._instance = convertToFlv(this.url, this._rtmpUrl)
            this._instance.start()
        }
    }

    kill() {
        if (this._instance) {
            this._instance.autoRestart = false
            this._instance.kill()
            this._instance = null
        }
    }
}

class StreamingManager {
    constructor() {
        this._workers = {}
    }

    _updateWorker(url, worker) {
        this._removeWorker(url)
        this._addWorker(url, worker)
    }

    _addWorker(url, worker) {
        if (!this._workers[url]) {
            this._workers[url] = worker

            worker.start()
        }
    }

    _removeWorker(url) {
        if (this._workers[url]) {
            this._workers[url].kill()

            delete this._workers[url]
        }
    }

    _createH264RelayWorker(url, metadata) {
        const sid = genUUID()
        const relayOut = urljoin(
            SUPPORTED_CODEC.H264,
            SUPPORTED_CODEC.H264,
            `${sid}.flv`,
        )
        const rtmpUrl = urljoin(RTMP_BASE_URL, SUPPORTED_CODEC.H264, sid)

        return new H264RelayWorker(url, metadata, relayOut, rtmpUrl)
    }

    async _clearOutdated() {
        forEach(this._workers, async (worker, url) => {
            if (worker.numConnections <= 0) {
                await this._removeWorker(url)
            }
        })
    }

    startClearOutdatedCron(period = 600 * 1000) {
        setInterval(this._clearOutdated.bind(this), period)
    }

    traceRequest() {
        return (req, res, next) => {
            const worker = find(this._workers, (worker) => (
                `/${worker.relayOut}` === urljoin(req.baseUrl, req.path)
            ))

            if (worker) {
                worker.numConnections += 1
                req.on('close', () => {
                    worker.numConnections -= 1
                })
            }

            next()
        }
    }

    async relay(url) {
        let worker = this._workers[url]

        const metadata = await probeStreamMetadata(url)
        const needCreate = !worker || !isEqual(worker.metadata, metadata)

        if (needCreate) {
            if (metadata.codec === SUPPORTED_CODEC.H264) {
                worker = this._createH264RelayWorker(url, metadata)
            } else {
                this._removeWorker(url)

                throw new CodecError(`Non-Supported codec: ${metadata.codec}`)
            }

            this._updateWorker(url, worker)
        }

        return worker
    }
}

const streamingManager = new StreamingManager()

streamingManager.startClearOutdatedCron()

module.exports = streamingManager
