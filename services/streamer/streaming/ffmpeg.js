const { spawn } = require('child_process')

class FFmpeg {
    constructor(args, autoRestart = true, restartDelay = 3000) {
        this._args = args
        this.autoRestart = autoRestart
        this.restartDelay = restartDelay

        this._process = null
    }

    start() {
        if (!this._process) {
            this._process = spawn('ffmpeg', this._args)

            this._process.on('exit', (code) => {
                console.debug(`FFmpeg exits, code = ${code}, args = ${this._args}`)

                if (this.autoRestart) {
                    setTimeout(() => {
                        this.kill()
                        this.start()
                    }, this.restartDelay)
                }
            })
            this._process.stderr.on('data', (data) => {
                console.debug(data.toString())
            })
            this._process.stdout.on('data', (data) => {
                console.debug(data.toString())
            })
        }
    }

    kill() {
        if (this._process) {
            this._process.stdout.destroy()
            this._process.stderr.destroy()
            this._process.kill()

            this._process = null
        }
    }
}

module.exports = {
    FFmpeg,
}
