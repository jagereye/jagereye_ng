const { FFmpeg } = require('./ffmpeg')

function argsToFlv(sourceUrl, rtmpUrl) {
    return [
        '-nostdin',
        '-stimeout', '10000000',
        '-rtsp_transport', 'tcp',
        '-fflags', 'nobuffer',
        '-flags', 'low_delay',
        '-i', sourceUrl,
        '-vcodec', 'copy',
        '-an',
        '-tune', 'zerolatency',
        '-f', 'flv',
        rtmpUrl,
    ]
}

function convertToFlv(sourceUrl, rtmpUrl) {
    const args = argsToFlv(sourceUrl, rtmpUrl)
    const instance = new FFmpeg(args)

    return instance
}

module.exports = {
    convertToFlv,
}
