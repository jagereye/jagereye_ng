ARG VERSION
FROM jagereye/nodejs:${VERSION} as builder

LABEL maintainer="feabries@gmail.com"

WORKDIR ${HOME}

USER jager

# Create services structure
RUN mkdir -p jagereye_ng/services/streamer && \
    mkdir -p jagereye_ng/shared && \
    mkdir -p jagereye_ng/modules/jagereye_ng

# Install common module
WORKDIR ${HOME}/jagereye_ng/modules/jagereye_ng
COPY --chown=jager:jager modules/jagereye_ng/ .
RUN npm install

# Copy shared files
# [NOTE] Shared files should be copied in to the build context before
#        running docker build
WORKDIR ${HOME}/jagereye_ng/shared
COPY --chown=jager:jager config.yml .
COPY --chown=jager:jager database.json .
COPY --chown=jager:jager externals.json .
COPY --chown=jager:jager pipeline.json .
COPY --chown=jager:jager systems.json .

# transform config.yml into config.js so it can be built as binary
RUN echo "module.exports = " > ${HOME}/jagereye_ng/modules/jagereye_ng/config.js && \
    npm install -g js-yaml && \
    js-yaml ${HOME}/jagereye_ng/shared/config.yml >> ${HOME}/jagereye_ng/modules/jagereye_ng/config.js

# Install service
WORKDIR ${HOME}/jagereye_ng/services/streamer
COPY --chown=jager:jager . .
RUN npm install --no-cache && npm install -g pkg && pkg package.json --output streamer

FROM jagereye/nodejs:${VERSION}

# Install ffmpeg.
USER root
RUN apt-get update && apt-get install -y --no-install-recommends \
        ffmpeg \
        && \
        apt-get clean && \
        rm -rf /var/lib/apt/lists/*

USER jager

# Create services structure
RUN mkdir -p jagereye_ng/services/streamer && \
    mkdir -p jagereye_ng/shared

# Copy shared files
# [NOTE] Shared files should be copied in to the build context before
#        running docker build
WORKDIR ${HOME}/jagereye_ng/shared
COPY --chown=jager:jager database.json .
COPY --chown=jager:jager externals.json .
COPY --chown=jager:jager pipeline.json .
COPY --chown=jager:jager systems.json .

WORKDIR ${HOME}/jagereye_ng/services/streamer
COPY --from=builder --chown=jager:jager ${HOME}/jagereye_ng/services/streamer/streamer .
ENTRYPOINT ["/home/jager/jagereye_ng/services/streamer/streamer"]
