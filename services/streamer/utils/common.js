const expressValidator = require('express-validator')
const httpError = require('http-errors')
const uuidv4 = require('uuid/v4')

function createError(status, message, origErrObj) {
    const error = new Error()

    error.status = status

    if (message) {
        error.message = message
    } else {
        error.message = httpError(status).message
    }

    if (origErrObj) {
        if (origErrObj.kind === 'ObjectId') {
            error.status = 400
            error.message = 'Invalid ObjectId format'
        }
        error.stack = origErrObj.stack
    }

    return error
}

function genUUID() {
    return uuidv4()
}

function validate(req, res, next) {
    const errors = expressValidator.validationResult(req)

    if (!errors.isEmpty()) {
        return next(createError(400, errors.array()[0]['msg']))
    }

    next()
}

module.exports = {
    createError,
    genUUID,
    validate,
}

