const express = require('express')
const expressValidator = require('express-validator')
const isString = require('lodash/isString')

const streamingManager = require('../streaming/manager')
const { createError, validate } = require('../utils/common')

const router = express.Router()

const relayToValidator = [
    expressValidator.check('url')
        .isString().withMessage('URL must be a string')
        .isLength({ min: 1 }).withMessage('The length of URL must be > 1'),
]

async function relayTo(req, res, next) {
    const { url } = req.body

    try {
        const worker = await streamingManager.relay(url)

        return res.status(201).send({
            relayOut: worker.relayOut,
            metadata: worker.metadata,
        })
    } catch (err) {
        if (err.name === 'CodecError' || err.name === 'ProbeError') {
            return next(createError(400, err.message))
        }

        return next(createError(500, null, err))
    }
}

router.post('/relay', relayToValidator, validate, relayTo)

module.exports = router

