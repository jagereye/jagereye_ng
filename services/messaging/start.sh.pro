#!/usr/local/bin/bash

# start the prometheus exporter
/nats_exporter -varz "http://localhost:8222" &

# Run via the configuration file
/gnatsd -c gnatsd.conf
