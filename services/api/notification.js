const WebSocket = require('ws')
const async = require('async')
const cloneDeep = require('lodash/cloneDeep')
const fs = require('fs')
const logger = require('./logger')
const request = require('request-promise-native')
const { getObject } = require('./objectStorage')
const config = require('jagereye_ng/config')
const models = require('jagereye_ng/database')
const P = require('bluebird');
const licenseModel = P.promisifyAll(models['licenses'])

const msg = JSON.parse(fs.readFileSync('../../shared/messaging.json', 'utf8'))

// TODO: should use const instead of var
var settings

var server
var wsServer

let LICENSE_EXPIRED = false

// Get configurations.
const { linebot_url } = config.services.api.params
const LINEBOT_URL = linebot_url

async function notifying(event, callback) {
    let parsedEvent
    try {
        parsedEvent = JSON.parse(event)
    } catch(e) {
        return callback(e)
    }
    const parsedEventNotification = cloneDeep(parsedEvent)
    // Delete the field that event doesn't need
    delete parsedEvent.message.notification

    // Regular websocket messages, this is default notification
    event = JSON.stringify(parsedEvent)
    async.each(wsServer.clients, (client, callback) => {
        client.send(event, (err) => {
            if (err) { return callback(err) }
            callback()
        })
    }, (err) => {
        if (err) { return callback(err) }
        callback()
    })

    const { timestamp, notification } = parsedEventNotification.message
    // Special notification channels like line, slack, or email are handled here
    if (notification) {
        for (const notify in notification) {
            /**
             * Currently line notification is only applied to face apps (both single and 
             * cross cam). Face apps only send text (name, camera, and time) and picture 
             * (face and scene). If other apps also use line for notification, probably
             * need to add video handler.
             */
            if(notify === 'line') {
                try {
                    let formData = notification[notify]
                    for (const element in formData) {
                        if(typeof element === 'string' &&
                            (formData[element].endsWith('.jpg') || formData[element].endsWith('.jpeg'))) {
                            const jpg = await getObject(formData[element])
                            formData[element] = {
                                value: jpg.Body,
                                options: {
                                    filename: formData[element],
                                    contentType: 'image/jpg'
                                }
                            }
                        }
                    }
                    formData.time = new Date(timestamp*1000).toLocaleString()
                    await request.post({
                        uri: LINEBOT_URL,
                        formData
                    })
                } catch(e) {
                    logger.error(e)
                }
            }
        }
    }
}

function notificationHandler(request) {
    if(LICENSE_EXPIRED === true) {
        return logger.error('License has expired')
    }
    logger.debug(`Publishing notification: ${request}`)
    notifying(request, (err) => {
        if (err) { return logger.error(err) }
    })
}

function init(_server, _settings) {
    server = _server
    settings = _settings
}

function start() {
    let path = settings.path || '/notification'
    wsServer = new WebSocket.Server({server: server, path: path, headers: {
        "Access-Control-Allow-Origin": "*"
    }})
    wsServer.on('connection', (ws) => {
        logger.debug('notification: websocket connected')
        ws.isAlive = true
        ws.on('pong', () => {
            ws.isAlive = true
        })
        ws.on('close', () => {
            logger.debug('notification: websocket closed')
        })
        ws.on('error', (err) => {
            logger.error(err)
        })
    })
    wsServer.on('error', (err) => {
        logger.error(err)
    })
    nats.subscribe(msg['channels']['NOTIFICATION'], notificationHandler)
    const heartbeatTimer = setInterval(function ping() {
        wsServer.clients.forEach((client) => {
            if (client.isAlive === false) {
                return client.terminate()
            }
            client.isAlive = false
            client.ping('', false, true)
        })
    }, settings.websocketKeepAliveTime || 10000)

    // check license expiration every 10 seconds
    setInterval( async () => {
        const licenses = await licenseModel.find({}).lean()
        for(const license of licenses) {
            if(license.expiredAt < Date.now()/1000) {
                LICENSE_EXPIRED = true
            }
        }
        LICENSE_EXPIRED = false
    }, 10000)

}

function stop() {
    if (wsServer) {
        wsServer.close()
        wsServer = null
    }
}


module.exports = {
    init: init,
    start: start,
    stop: stop
}
