const express = require('express')
const router = express.Router()
const { routesWithAuth } = require('./auth')
const { createError } = require('./utils')
const { getSystemStatus } = require('./systemUtils')
const logger = require('./logger')

async function getStatus(req, res, next) {
    let result
    try {
        result = await getSystemStatus()
    } catch(e) {
        logger.error(e)
        return next(createError(500, e))
    }
    return res.send(result)
}

/*
 * Routing Table
 */
routesWithAuth(
    router,
    ['get', '/system/status', getStatus],
)

module.exports = router
