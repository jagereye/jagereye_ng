const os = require('os')
const P = require('bluebird');
const execAsync = P.promisify(require('child_process').exec);
const format = require('util').format;

const logger = require('../logger');

const shellTimeout = 4000

class SetNetworkError extends Error {
    constructor (message, status) {
        super(message);
        this.name = this.constructor.name;
        // Capturing stack trace, excluding constructor call from it.
        Error.captureStackTrace(this, this.constructor);
        this.status = status || 500;
    }
};

async function setNetworkInterface(networkInterface, mode, address, netmask, gateway) {
    // flush the dataport ip
    await execAsync(format('sudo ip addr flush dev %s', networkInterface), {timeout: shellTimeout});
    if(mode === 'static') {
        await execAsync(format('sudo ifconfig %s %s', networkInterface, address), {timeout: shellTimeout});
    } else if (mode === 'dhcp') {
        try {
            await execAsync(format('sudo dhclient %s', networkInterface), {timeout: shellTimeout});
        } catch(e){
            // it happened when the dataport cannot find dhcp server
            logger.error(e);
            throw new SetNetworkError('dhcp failed');
        }
    }
}

function getInterfaceIp(interfaceName) {
    const interfaces = os.networkInterfaces();
    try {
        const ip = interfaces[interfaceName][0]['address'];
        return ip;
    } catch(e) {
        logger.error(e);
        return;
    }
}

module.exports = {
    setNetworkInterface: setNetworkInterface,
    getInterfaceIp: getInterfaceIp,
    SetNetworkError: SetNetworkError
};
