const merge = require('lodash/merge')

const { SYSTEM_STATE } = require('./constants')

const request = require('request-promise-native')
const config = require('jagereye_ng/config')
const models = require('jagereye_ng/database')
const logger = require('./logger')
const assert = require('assert')
const { analyzerStatus } = require('./constants')

const PROMETHEUS_URL = config.services.prometheus.params.endpoint_url + '/' +
                       config.services.prometheus.params.api_version
const PROMETHEUS_PREFIX = PROMETHEUS_URL + '/query?query='

var memTotal = 0, systemDiskSize = 0, dataDiskSize = 0

async function getSystemRecord(id) {
    return await models.systems.findById(id)
}

async function updateSystemRecord(id, content, condition = null, upsert = true) {
    const search = { _id: id }
    const updated = { content }
    const options = {
        upsert,
        runValidators: true,
    }

    if (condition) {
        search.content = condition
    }

    return await models.systems.updateOne(search, updated, options)
}

async function getSystemState() {
    const result = await getSystemRecord('state')

    if (!result) {
        return undefined
    } else {
        return result.content.state
    }
}

async function updateSystemState(state) {
    if (state === SYSTEM_STATE.READY) {
        // If the new state is "ready", we don't need to consider what the
        // current state is.
        await updateSystemRecord('state', { state })
    } else {
        // If the new state is not "ready", we should make sure the current
        // state is "ready".
        const result = await updateSystemRecord(
            'state',
            { state },
            { state: SYSTEM_STATE.READY },
            false,
        )

        if (result.nModified !== 1) {
            return false
        }
    }

    return true
}

async function getSystemInfo() {
    const result = await getSystemRecord('information')

    if (!result) {
        return undefined
    } else {
        return result.content
    }
}

async function updateSystemInfo(info) {
    const oldInfo = await getSystemInfo()
    const newInfo = merge({}, oldInfo, info)

    await updateSystemRecord('information', newInfo)
}

async function getSystemStatus() {
    let cpu = 0, memAvail = 0, systemDiskFree = 0, dataDiskFree = 0, analyzers, targets, camera

    try {
        cpu = await request(PROMETHEUS_PREFIX + '100 - (avg by (instance) (irate(node_cpu{job="node",mode="idle"}[5m])) * 100)', { json: true })
        if( memTotal === 0 ) {
            memTotal = await request(PROMETHEUS_PREFIX + '(avg_over_time(node_memory_MemTotal[5m]))/1024/1024/1024', { json: true })
            memTotal = getPrometheusReturnValue(memTotal)
        }
        memAvail = await request(PROMETHEUS_PREFIX + '(avg_over_time(node_memory_MemAvailable[5m]))/1024/1024/1024', { json: true })
        if( systemDiskSize === 0 ) {
            systemDiskSize = await request(PROMETHEUS_PREFIX + '(node_filesystem_size{mountpoint="/"})/1024/1024/1024', { json: true })
            systemDiskSize = getPrometheusReturnValue(systemDiskSize)
        }
        systemDiskFree = await request(PROMETHEUS_PREFIX + '(node_filesystem_free{mountpoint="/"})/1024/1024/1024', { json: true })
        if( dataDiskSize === 0 ) {
            dataDiskSize = await request(PROMETHEUS_PREFIX + '(node_filesystem_size{mountpoint="/data"})/1024/1024/1024', { json: true })
            dataDiskSize = getPrometheusReturnValue(dataDiskSize)
        }
        dataDiskFree = await request(PROMETHEUS_PREFIX + '(node_filesystem_free{mountpoint="/data"})/1024/1024/1024', { json: true })
        camera = await models['analyzers'].countDocuments({})
        analyzers = await request(PROMETHEUS_PREFIX + 'analyzer_status', { json: true })
        targets = await request(PROMETHEUS_URL + '/targets', { json: true })
    } catch(e) {
        throw e
    }
    let result = {
        timestamp: new Date().getTime(),
        systemState: await getSystemState(),
        usage: [],
        analyzers: [],
        services: []
    }
    // cpu
    if (cpu['status'] === 'success') {
        result.usage.push({
            name: 'cpu',
            unit: '%',
            used: getPrometheusReturnValue(cpu),
            total: 100
        })
    } else {
        result.usage.push({
            name: 'cpu',
            unit: '%',
            used: NaN,
            total: 100
        })
    }
    // memory
    if (memAvail['status'] === 'success') {
        memAvail = getPrometheusReturnValue(memAvail)
        let memUsed = memTotal - memAvail
        result.usage.push({
            name: 'memory',
            unit: 'GB',
            used: memUsed,
            total: memTotal
        })
    } else {
        result.usage.push({
            name: 'memory',
            unit: 'GB',
            used: NaN,
            total: memTotal
        })
    }
    // systemDisk
    if (systemDiskFree['status'] === 'success') {
        systemDiskFree = getPrometheusReturnValue(systemDiskFree)
        let systemDiskUsed = systemDiskSize - systemDiskFree
        result.usage.push({
            name: 'systemDiskCapacity',
            unit: 'GB',
            used: systemDiskUsed,
            total: systemDiskSize
        })
    } else {
        result.usage.push({
            name: 'systemDiskCapacity',
            unit: 'GB',
            used: NaN,
            total: systemDiskSize
        })
    }
    // dataDisk
    if (dataDiskFree['status'] === 'success') {
        dataDiskFree = getPrometheusReturnValue(dataDiskFree)
        let dataDiskUsed = dataDiskSize - dataDiskFree
        result.usage.push({
            name: 'dataDiskCapacity',
            unit: 'GB',
            used: dataDiskUsed,
            total: dataDiskSize
        })
    } else {
        result.usage.push({
            name: 'dataDiskCapacity',
            unit: 'GB',
            used: NaN,
            tal: dataDiskSize
        })
    }
    // camera
    const license = await models.licenses.find({})
    let licCounter = 0
    for(const entry of license) {
        for(const app of entry.application) {
            licCounter += app.channel
        }
    }
    result.usage.push({
        name: 'numOfCameras',
        unit: '',
        used: camera,
        total: licCounter
    })
    // analyzers
    if (analyzers['status'] === 'success' && analyzers['data'] && analyzers['data']['result']) {
        analyzers = analyzers['data']['result']
        assert(Array.isArray(analyzers))
        for(let i = 0 ; i < analyzers.length ; i++) {
            let statusCode = parseInt(analyzers[i]['value'][1])
            result.analyzers.push({
                name : analyzers[i]['metric']['analyzer'],
                status : statusCode === -1 ? 'unknown' : analyzerStatus[statusCode]
            })
        }
    }
    // targets
    if (targets['status'] === 'success' && targets['data'] && targets['data']['activeTargets']) {
        targets = targets['data']['activeTargets']
        assert(Array.isArray(targets))
        for(let i = 0 ; i < targets.length ; i++) {
            result.services.push({
                name: targets[i].labels.job,
                status: targets[i].health
            })
        }
    }

    return result
}

function getPrometheusReturnValue(ret) {
    try {
        const result = parseFloat(ret.data.result[0].value[1])
        return result
    } catch(e) {
        logger.error(e)
        return 0
    }
}


async function getUpgradeContent() {
    const result = await getSystemRecord('upgrade')

    if (!result) {
        return undefined
    } else {
        return result.content
    }
}

async function updateUpgradeContent(status, message) {
    const content = { status }

    if (message) {
        content.message = message
    }

    await updateSystemRecord('upgrade', content)
}

async function getNetworkContent() {
    const result = await getSystemRecord('network')

    if (!result) {
        return undefined
    } else {
        return result.content
    }
}

async function updateNetworkContent(network) {
    await updateSystemRecord('network', network)
}

module.exports = {
    getSystemRecord,
    updateSystemRecord,
    getSystemState,
    updateSystemState,
    getSystemInfo,
    updateSystemInfo,
    getSystemStatus,
    getUpgradeContent,
    updateUpgradeContent,
    getNetworkContent,
    updateNetworkContent,
}
