const express = require('express')
const router = express.Router()
const { getSystemInfo, updateSystemInfo } = require('./systemUtils')
const logger = require('./logger')
const { createError } = require('./utils')
const { routesWithAuth } = require('./auth')
const config = require('jagereye_ng/config')

async function getInfo(req, res, next) {
    /**
     * Middleware of GET /system/info
     */

    // get information from db
    try {
        let info = await getSystemInfo()
        // response back
        info.softwareVersion = config.version
        info.hardwareModel = config.model
        res.status(200).send(info)
    } catch (err) {
        logger.error(err);
        return next(createError(500));
    }
}

async function setupInfo() {
    /**
     * Setup info when API server start
     */

    // check if there exist info in DB
    try {
        let info = await getSystemInfo()
        if(!info) {
            // if not existed, create default info
            const defaultSetting = {
                timeZone: "Asia/Taipei UTC+8",
                lastUpgradeTime: new Date()
            }

            try {
                await updateSystemInfo(defaultSetting)
            } catch (err) {
                logger.error(err)
            }
        }
    } catch (err) {
        logger.error(err)
    }
}

/*
 * Routing Table
 */
routesWithAuth(
    router,
    ['get', '/system/info', getInfo],
)

module.exports = {
    infoRouter: router,
    setupInfo
}
