const express = require('express')
const router = express.Router()
const multer = require('multer')
const NATS = require('nats')
const Promise = require('bluebird')
const { checkSchema } = require('express-validator/check')

const logger = require('./logger')
const sharedFiles = require('./sharedFiles')
const { stopAnalyzers } = require('./analyzers')
const { createError, validate } = require('./utils')
const {
    updateSystemState,
    updateSystemInfo,
    getUpgradeContent,
    updateUpgradeContent,
} = require('./systemUtils')
const { routesWithAuth } = require('./auth')
const { SYSTEM_STATE, UPGRADE_STATUS, RESTART_TARGET } = require('./constants')

const {
    channels: CHANNELS,
    ch_api_host: COMMAND_TYPE,
    ch_host_api_report: REPORT_TYPE,
} = require('../../shared/messaging.json')
const DEFAULT_REQUEST_TIMEOUT = 15000
const RESTART_RETRY_INTERVAL = 10000

const upgradeFileParser = multer({
    storage: multer.diskStorage({
        destination: (req, file, cb) => {
            const dir = `upgrade/${new Date().getTime()}`
            const fullPath = sharedFiles.mkdir(dir, '-p')

            cb(null, fullPath)
        },
        filename: (req, file, cb) => {
            cb(null, file.originalname)
        },
    }),
}).single('upfile')

const restartValidator = checkSchema({
    target: {
        isIn: {
            options: [[
                RESTART_TARGET.SERVICE,
                RESTART_TARGET.SYSTEM,
            ]],
            errorMessage: `Target must be "${RESTART_TARGET.SERVICE}" ` +
            `or "${RESTART_TARGET.SYSTEM}"`,
        },
    },
})

function requestHostAgent(command, params, timeout = DEFAULT_REQUEST_TIMEOUT) {
    return new Promise((resolve, reject) => {
        const request = JSON.stringify({
            command,
            params,
        })

        nats.requestOne(CHANNELS.API_TO_HOST, request, {}, timeout, (reply) => {
            if (reply instanceof NATS.NatsError) {
                const err =
                    (reply.code === NATS.REQ_TIMEOUT) ?
                    new Error('Timeout for requesting host agent') :
                    reply

                reject(err)
            }

            try {
                resolve(JSON.parse(reply))
            } catch (err) {
                reject(err)
            }
        })
    })
}

async function requestRestart(target, retryInterval = RESTART_RETRY_INTERVAL) {
    let sid

    try {
        sid = listenHostAgentReport(COMMAND_TYPE.RESTART, (report) => {
            const { type, message } = report

            if (type === REPORT_TYPE.ERROR) {
                logger.error(`Restart error: ${message}`)

                unlistenHostAgentReport(sid)
                setTimeout(() => requestRestart(target), retryInterval)
            }
        })

        const reply = await requestHostAgent(COMMAND_TYPE.RESTART, { target })

        if (reply.type !== REPORT_TYPE.START) {
            throw new Error('Failed to request host agent for restarting')
        }
    } catch (err) {
        logger.error(err)

        if (sid) {
            unlistenHostAgentReport(sid)
        }
        setTimeout(() => requestRestart(target), retryInterval)
    }
}

async function getUpgradeStatus(req, res, next) {
    try {
        const content = await getUpgradeContent()

        if (!content) {
            throw Error('No content for upgrade status')
        }

        return res.send(content)
    } catch (err) {
        return next(createError(500, null, err))
    }
}

function listenHostAgentReport(from, callback) {
    const sid = nats.subscribe(CHANNELS.HOST_TO_API, async (msg) => {
        let report

        try {
            report = JSON.parse(msg)
        } catch (err) {
            logger.error(err)
            return
        }

        if (from !== report.from) {
            return
        }

        await callback(report)
    })

    return sid
}

function unlistenHostAgentReport(sid) {
    nats.unsubscribe(sid)
}

function handleUpgradeReport() {
    const sid = listenHostAgentReport(COMMAND_TYPE.UPGRADE, async (report) => {
        try {
            const { type, message } = report

            switch (type) {
                case REPORT_TYPE.NEW_STEP:
                    logger.info(`Upgrade new step: ${message}`)

                    await updateUpgradeContent(UPGRADE_STATUS.PROCESSING, message)

                    break
                case REPORT_TYPE.FINISH:
                    const newMsg = 'Restart the service'

                    // Update the last upgrade time.
                    await updateSystemInfo({
                        lastUpgradeTime: new Date(),
                    })
                    // Unlisten the host report for upgrading.
                    unlistenHostAgentReport(sid)

                    logger.info(`Upgrade new step: ${newMsg}`)

                    // Go to next step: restart the service.
                    await updateUpgradeContent(UPGRADE_STATUS.PROCESSING, newMsg)
                    // Request host agent to restart the service.
                    requestRestart(RESTART_TARGET.SERVICE)

                    break
                case REPORT_TYPE.ERROR:
                    logger.error(`Upgrade error: ${message}`)

                    await updateUpgradeContent(UPGRADE_STATUS.FAILED, message)
                    // Update system state to be 'ready'.
                    await updateSystemState(SYSTEM_STATE.READY)
                    // Unlisten the host report.
                    unlistenHostAgentReport(sid)

                    break
                default:
                    logger.error(`Unkown report type: ${type}`)
                    break
            }
        } catch (err) {
            logger.error(err)
        }
    })

    return sid
}

async function startUpgradeProcess(req, res, next) {
    let file

    try {
        file = req.file

        // Check the upgrade file exists and its type is gzip or not.
        if (!file) {
            return next(createError(400, 'Upgrade file is required'))
        }
        if (file.mimetype !== 'application/x-gzip' && file.mimetype !== 'application/gzip') {
            return next(createError(400, 'Upgrade file must be a gzip file'))
        }

        // Update the system state to be 'upgrading'.
        const updateSuccess = await updateSystemState(SYSTEM_STATE.UPGRADING)
        if (!updateSuccess) {
            return next(createError(400, 'System is not in ready state'))
        }

        // Update the upgrade status to be 'processing'.
        await updateUpgradeContent(
            UPGRADE_STATUS.PROCESSING,
            'Prepare for installation'
        )

        res.status(204).send()
    } catch (err) {
        sharedFiles.rm(file.destination, '-rf', true)

        return next(createError(500, null, err))
    }

    let sid

    try {
        sid = handleUpgradeReport()

        const reply = await requestHostAgent(COMMAND_TYPE.UPGRADE, {
            filePath: file.path,
        })

        if (reply.type !== REPORT_TYPE.START) {
            throw new Error('Failed to request host agent')
        }
    } catch (err) {
        logger.error(err)

        if (sid) {
            unlistenHostAgentReport(sid)
        }

        sharedFiles.rm(file.destination, '-rf', true)
        await updateUpgradeContent(
            UPGRADE_STATUS.FAILED,
            `Error occurs while preparation (${err.message})`,
        )
    }
}

async function restart(req, res, next) {
    let target

    try {
        const updateSuccess = await updateSystemState(SYSTEM_STATE.RESTARTING)

        if (!updateSuccess) {
            return next(createError(400, 'System is not in ready state'))
        }

        target = req.body.target

        res.status(204).send()
    } catch (err) {
        return next(createError(500, null, err))
    }

    try {
        // Stop all analyzers.
        await stopAnalyzers()
    } catch (err) {
        logger.error(err)
    }

    // Request host agent to restart.
    requestRestart(target)
}

async function setupSystemRecords() {
    try {
        // TODO(JiaKuan Su):
        // 1. We should also consider use "initial" state before updating to
        //    "ready" state. For example, the analyzers APIs are only available
        //    after all existed analyzers restart.
        // 2. Currently, we assume ALL services are restarted. In the future,
        //    we should handle the other case: only API service is restarted by
        //    some reasons. In such case, we should not update the state to
        //    "ready" directly.
        await updateSystemState(SYSTEM_STATE.READY)
    } catch (err) {
        logger.error(err)
    }

    try {
        // TODO(JiaKuan Su):
        // Handle the case for service termination while upgrading.
        await updateUpgradeContent(UPGRADE_STATUS.DONE)
    } catch (err) {
        logger.error(err)
    }
}

routesWithAuth(
    router,
    ['get', '/system/upgrade', getUpgradeStatus],
    ['post', '/system/upgrade', upgradeFileParser, startUpgradeProcess],
    ['post', '/system/restart', restartValidator, validate, restart],
)

module.exports = {
    systems: router,
    setupSystemRecords,
}
