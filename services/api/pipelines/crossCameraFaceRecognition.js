const express = require('express')
const { checkSchema } = require('express-validator/check')

const concat = require('lodash/concat')
const filter = require('lodash/filter')
const groupBy = require('lodash/groupBy')
const includes = require('lodash/includes')
const isInteger = require('lodash/isInteger')
const isNumber = require('lodash/isNumber')
const isPlainObject = require('lodash/isObject')
const isString = require('lodash/isString')
const isBoolean = require('lodash/isBoolean')
const map = require('lodash/map')
const merge = require('lodash/merge')
const slice = require('lodash/slice')

const models = require('jagereye_ng/database')
const { routesWithAuth } = require('../auth')
const {
    createError,
    validate,
    isValidId,
    isArrayOfId,
} = require('../utils')
const externalsSchema = require('../../../shared/externals.json')

const router = express.Router()

const EVENT_TYPE = 'cross_camera_face_recognition.alert'
const EXTERNAL_TYPE = 'cross_camera_face_recognition.face'

/*
 * Projections
 */
const getEventsProjection = {
    expired: 0,
}
const listExternalsSelect = concat([
    '_id',
    'task.status',
    'task.reason',
    'type',
    'group',
    'createdAt',
    'updatedAt',
], map(externalsSchema[EXTERNAL_TYPE].PROJECTIONS, (field) => `content.${field}`))

const baseValidations = {
    timestamp: {
        custom: {
            options: async (timestamp) => {
                if (timestamp) {
                    if (!isPlainObject(timestamp)) {
                        throw new Error('Timestamp must be an object')
                    }

                    const { start, end } = timestamp

                    if (start) {
                        if (!isNumber(start) || start <= 0) {
                            throw new Error(
                                'Timestamp start must be a number > 0',
                            )
                        }
                    }
                    if (end) {
                        if (!isNumber(end) || end <= 0) {
                            throw new Error(
                                'Timestamp end must be a number > 0',
                            )
                        }
                    }
                }
            },
        },
    },
    analyzers: {
        custom: {
            options: async (analyzers) => {
                if (analyzers && !isArrayOfId(analyzers)) {
                    throw new Error('Analyzers must be an array of IDs')
                }
            },
        },
    },
    group: {
        custom: {
            options: async (group) => {
                if (group && (!isValidId(group) && !isArrayOfId(group))) {
                    throw new Error(
                        'Group must be an ID or an array of IDs',
                    )
                }
            },
        },
    },
    name: {
        custom: {
            options: async (name) => {
                if (name) {
                    if (isPlainObject(name)) {
                        if (!isString(name.partialSearch)) {
                            throw Error('Name partial search must be a string')
                        }
                    } else {
                        if (!isString(name)) {
                            throw Error('Name must be an object or a string')
                        }
                    }
                }
            },
        },
    },
    limit: {
        custom: {
            options: async (limit) => {
                if (limit && !isInteger(limit)) {
                    throw new Error('Limit must be an integer')
                }
                if (limit < 1) {
                    throw new Error('Limit must larger than 0')
                }
            },
        },
    },
    page: {
        custom: {
            options: async (page) => {
                if (page && !isInteger(page)) {
                    throw new Error('Page must be an integer')
                }
                if (page < 1) {
                    throw new Error('Page must larger than 0')
                }
            },
        },
    },
    matched: {
        custom: {
            options: async (matched) => {
                if (matched !== undefined && !isBoolean(matched)) {
                    throw new Error('Matched must be a boolean')
                }
            }
        },
    },
}

const queryEventsValidator = checkSchema(merge(baseValidations, {
    sortBy: {
        custom: {
            options: async (sortBy) => {
                if (sortBy && !isPlainObject(sortBy)) {
                    throw new Error('SortBy must be an object')
                }
                const sortTypes = ['asc', 'ascending', '1', 'desc', 'descending', '-1']
                const sortProperties = ['timestamp']
                for (const key in sortBy) {
                    if (!includes(sortProperties, key)) {
                        throw new Error(
                            `Key "${key}" is not supported for sort: [${sortProperties}]`,
                        )
                    }
                    if (!includes(sortTypes, sortBy[key])) {
                        throw new Error(
                            `Sort type "${sortBy[key]}" is not supported: [${sortTypes}] `
                        )
                    }
                }
            },
        },
    },
}))

const queryAttendancesValidator = checkSchema(merge(baseValidations, {
    sortBy: {
        custom: {
            options: async (sortBy) => {
                if (sortBy && !isPlainObject(sortBy)) {
                    throw new Error('SortBy must be an object')
                }
                const sortTypes = ['asc', 'ascending', '1', 'desc', 'descending', '-1']
                const sortProperties = ['createdAt', 'updatedAt']
                for (const key in sortBy) {
                    if (!includes(sortProperties, key)) {
                        throw new Error(
                            `Key "${key}" is not supported for sort: [${sortProperties}]`,
                        )
                    }
                    if (!includes(sortTypes, sortBy[key])) {
                        throw new Error(
                            `Sort type "${sortBy[key]}" is not supported: [${sortTypes}] `
                        )
                    }
                }
            },
        },
    },
}))

async function queryEvents(req, res, next) {
    try {
        const {
            timestamp,
            analyzers,
            group,
            name,
            sortBy,
            limit,
            page,
            matched,
        } = req.body

        if (page && !limit) {
            // Must give page with limit
            return next(createError(400, 'Option "page" must come with "limit"'))
        }

        const eventsQuery = {
            type: EVENT_TYPE,
            expired: {
                $ne: true,
            },
        }
        const eventsOptions = {}
        const externalsQuery = {
            expired: {
                $ne: true,
            }, 
        }

        if (timestamp) {
            const { start, end } = timestamp
            const timestampQuery = {}

            if (start) {
                timestampQuery.$gte = start
            }
            if (end) {
                timestampQuery.$lte = end
            }

            eventsQuery.timestamp = timestampQuery
        }
        if (analyzers) {
            eventsQuery.analyzerId = {
                $in: analyzers,
            }
        }
        if(matched !== undefined) {
            eventsQuery['content.matched'] = matched
        }
        if (group) {
            externalsQuery.group =
                isArrayOfId(group) ?
                { $in: group } :
                group
        }
        if (name) {
            externalsQuery['content.name'] =
                isPlainObject(name) ?
                new RegExp(name.partialSearch, 'i') :
                name
        }

        if (sortBy) {
            eventsOptions.sort = sortBy
        }

        let list = await models.events.find(
            eventsQuery,
            getEventsProjection,
            eventsOptions,
        ).populate({
            path: 'content.triggered',
            select: listExternalsSelect,
            match: externalsQuery,
            model: 'externals',
        }).exec()

        // FIXME(feabries su): Population by Mongoose does not filter events
        // whose `content.triggered` field is null, so we can only filter
        // manually. To resolve such problems, we may use "aggregate" instead
        // of "populate" for database join.
        list = filter(list, 'content.triggered')

        const total = list.length

        if (limit) {
            const sliceStart = page ? (page - 1) * limit : 0
            const sliceEnd = page ? page * limit : limit

            list = slice(list, sliceStart, sliceEnd)
        }

        return res.status(200).send({
            total,
            result: list,
        })
    } catch (err) {
        return next(createError(500, null, err))
    }
}

async function queryAttendances(req, res, next) {
    try {
        const {
            timestamp,
            analyzers,
            group,
            name,
            sortBy,
            limit,
            page,
            matched,
        } = req.body

        if (page && !limit) {
            // Must give page with limit
            return next(createError(400, 'Option "page" must come with "limit"'))
        }

        const externalsQuery = {
            type: EXTERNAL_TYPE,
            expired: {
                $ne: true,
            },
        }
        const externalsOptions = {}

        if (group) {
            externalsQuery.group =
                isArrayOfId(group) ?
                { $in: group } :
                group
        }
        if (name) {
            externalsQuery['content.name'] =
                isPlainObject(name) ?
                new RegExp(name.partialSearch, 'i') :
                name
        }

        if (sortBy) {
            externalsOptions.sort = sortBy
        }

        if (limit) {
            externalsOptions.limit = limit
        }

        if (page) {
            externalsOptions.skip = (page - 1) * limit
        }

        const total = await models.externals.find(
            externalsQuery,
            externalsOptions,
        ).count()
        const externals = await models.externals.find(
            externalsQuery,
            listExternalsSelect,
            externalsOptions,
        )
        const externalIds = map(externals, (external) => (
            external._id.toString()
        ))

        const eventsQuery = {
            type: EVENT_TYPE,
            expired: {
                $ne: true,
            },
            'content.triggered': {
                $in: externalIds,
            },
            'content.matched': true
        }
        const eventsOptions = {
            sort: {
                timestamp: 'asc',
            },
        }

        if (timestamp) {
            const { start, end } = timestamp
            const timestampQuery = {}

            if (start) {
                timestampQuery.$gte = start
            }
            if (end) {
                timestampQuery.$lte = end
            }

            eventsQuery.timestamp = timestampQuery
        }
        if (analyzers) {
            eventsQuery.analyzerId = {
                $in: analyzers,
            }
        }
        if(matched !== undefined) {
            eventsQuery['content.matched'] = matched
        }

        const events = await models.events.find(
            eventsQuery,
            getEventsProjection,
            eventsOptions,
        )
        const eventGroups = groupBy(events, 'content.triggered')

        const attendances = map(externals, (external) => {
            const relatedEvents = eventGroups[external._id]
            const appearance = {
                first: null,
                last: null,
            }

            if (relatedEvents) {
                appearance.first = relatedEvents[0]
                appearance.last = relatedEvents[relatedEvents.length - 1]
            }

            return merge(external.toObject(), {
                appearance,
            })

        })

        return res.status(200).send({
            total,
            result: attendances,
        })
    } catch (err) {
        return next(createError(500, null, err))
    }
}

/*
 * Routing Table
 */
routesWithAuth(
    router,
    ['post', '/cross_camera_face_recognition/events', queryEventsValidator, validate, queryEvents],
    ['post', '/cross_camera_face_recognition/attendances', queryAttendancesValidator, validate, queryAttendances],
)

module.exports = router
