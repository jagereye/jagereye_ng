const express = require('express')

const crossCameraFaceRecognition = require('./crossCameraFaceRecognition')

const router = express.Router()

router.use('/pipelines', crossCameraFaceRecognition)

module.exports = router
