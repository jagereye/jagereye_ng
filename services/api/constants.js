const ROLES = Object.freeze({
    ADMIN: 'admin',
    WRITER: 'writer',
    READER: 'reader',
})

// analyzer status mapping
const analyzerStatus = Object.freeze([
    'starting',
    'running',
    'source_down',
    'internal_error',
    'stopped',
])

// One-Shot task status mapping
const ONE_SHOT_STATUS = Object.freeze({
    PENDING: 'pending',
    PROCESSING: 'processing',
    DONE: 'done',
    FAILED: 'failed',
})

// System state mapping.
const SYSTEM_STATE = Object.freeze({
    READY: 'ready',
    RESTARTING: 'restarting',
    UPGRADING: 'upgrading',
})

const UPGRADE_STATUS = Object.freeze({
    PROCESSING: 'processing',
    DONE: 'done',
    FAILED: 'failed',
})

const RESTART_TARGET = Object.freeze({
    SERVICE: 'service',
    SYSTEM: 'system',
})

module.exports = {
    ROLES,
    analyzerStatus,
    ONE_SHOT_STATUS,
    SYSTEM_STATE,
    UPGRADE_STATUS,
    RESTART_TARGET,
}
