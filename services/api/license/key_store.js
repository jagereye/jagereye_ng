const NodeRSA = require('node-rsa')
const { getSystemRecord, updateSystemRecord } = require('../systemUtils')

class ServerKeyError extends Error {
    constructor(message, name) {
        super(message)

        this.name = name

        Error.captureStackTrace(this, this.constructor)
    }
}

// key placeholder
let RSAKey

async function init() {
    let result = await getSystemRecord('jagerKey')

    if(result && result.content.key) {
        RSAKey = new NodeRSA()
        RSAKey.importKey(result.content.key)
    } else {
        RSAKey = await createNewKey()
    }
}

function getPrivateKey() {
    return RSAKey.exportKey('pkcs8-private')
}

function getPublicKey() {
    return RSAKey.exportKey('pkcs8-public')
}

async function getServerPublicKey() {
    let result = await getSystemRecord('serverKey')

    if(result && result.content.key) {
        return result.content.key
    } else {
        throw new ServerKeyError('No server public key found, please upload a server public key.')
    }
}

async function createNewKey() {
    let key = new NodeRSA()
    // generate key pair(default 2048 bits)
    key.generateKeyPair()

    await updateSystemRecord('jagerKey', { key: key.exportKey('pkcs8-private') } )

    return key
}

async function saveServerPublicKey(serverKey) {
    await updateSystemRecord('serverKey', { key: serverKey } )
}

init()

module.exports = {
    getPrivateKey: getPrivateKey,
    getPublicKey: getPublicKey,
    getServerPublicKey: getServerPublicKey,
    saveServerPublicKey: saveServerPublicKey,
    ServerKeyError
}
