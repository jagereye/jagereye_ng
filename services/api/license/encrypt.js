'use strict'

const crypto = require('crypto')
const randGen = require('seed-random')

const { getPrivateKey, getServerPublicKey } = require('./key_store')

/**
 * The encryption is done by combining AES, RSA, and random shuffle. First, encrypt the 
 * text with AES key, then encrypt the AES key with RSA public key. Then shuffle the 
 * encrypted text and key with Fisher-Yates shuffle algorithm while taking IV as the 
 * shuffle seed. The result string should become messed up enough while still can be
 * decrypted.
 */
const IV_LENGTH = 16 // For AES, this is always 16
const SEED_LENGTH = IV_LENGTH * 2 // Seed is IV as hex string, so 2 times of iv length

async function encrypt(text) {
  // prepare AES key
  const aesKey = crypto.randomBytes(32)

  // prepare cipher
  const iv = crypto.randomBytes(IV_LENGTH)
  const cipher = crypto.createCipheriv('aes-256-cbc', aesKey, iv)

  // encrypt text
  let encryptedData = cipher.update(text)
  encryptedData = Buffer.concat([encryptedData, cipher.final()]).toString('hex')

  // encrypt AES key with RSA public key
  const serverKey = await getServerPublicKey()
  const encryptedKey = crypto.publicEncrypt(serverKey, aesKey).toString('hex')

  // shuffle data and key with iv as seed
  const shuffledText = shuffle( encryptedData + encryptedKey, iv.toString('hex') )
  return iv.toString('hex') + shuffledText
}

function decrypt(text) {
  // get iv
  const iv = Buffer.from(text.substring(0, SEED_LENGTH), 'hex')

  // get unshuffled text and key
  const unshuffledString = unshuffle(text.substring(SEED_LENGTH), iv.toString('hex'))

  // separate encrypted text and key
  const cipherLength = unshuffledString.length - 512 // 512 is the aesKey length
  const encryptedText = Buffer.from(unshuffledString.substring(0, cipherLength), 'hex')
  const encryptedAESKey = Buffer.from(unshuffledString.substring(cipherLength), 'hex')
  
  // use jager private key to decrypt AES key
  const decryptedAESKey = crypto.privateDecrypt(getPrivateKey(), encryptedAESKey)
  const decipher = crypto.createDecipheriv('aes-256-cbc', decryptedAESKey, iv)

  // decrypted text
  let decrypted = decipher.update(encryptedText)
  decrypted = Buffer.concat([decrypted, decipher.final()])

  return decrypted.toString()
}

function shuffle(text, seed) {
  const rand = randGen(seed)
  let array = text.split('')
  let currentIndex = array.length

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {
    // Pick a remaining element...
    let randomIndex = Math.floor(rand() * (currentIndex --))

    // And swap it with the current element.
    let temporaryValue = array[currentIndex]
    array[currentIndex] = array[randomIndex]
    array[randomIndex] = temporaryValue
  }

  return array.join('')
}

function unshuffle(text, seed) {
  const rand = randGen(seed)
  let array = text.split('')
  let currentIndex = array.length
  let map = []

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {
    // Pick a remaining element...
    let randomIndex = Math.floor(rand() * (currentIndex --))

    // keep record of swap order
    map.push([currentIndex, randomIndex])
  }

  // reverse the swap
  while(map.length > 0) {
    let swap = map.pop()
    let temporaryValue = array[swap[0]]
    array[swap[0]] = array[swap[1]]
    array[swap[1]] = temporaryValue
  }

  return array.join('')
}

module.exports = { decrypt, encrypt }