const express = require('express')
const router = express.Router()
const mongoose = require('mongoose');
const os = require('os')
const { routesWithAuth } = require('../auth')
const { checkSchema } = require('express-validator/check')
const { createError, validate, isValidId, genUUID, now } = require('../utils')
const { machineId } = require('node-machine-id')
const { getPublicKey, 
        getServerPublicKey,
        saveServerPublicKey,
        ServerKeyError } = require('./key_store')
const { encrypt, decrypt } = require('./encrypt')
const { template } = require('./license_template')
const licenseFile = require('nodejs-license-file')
const isString = require('lodash/isString')
const models = require('jagereye_ng/database')
const isEqual = require('lodash/isEqual')
const P = require('bluebird');
const licenseModel = P.promisifyAll(models['licenses'])
const { getGPUResource, getAnalyzersSum } = require('../analyzers')
const config = require('jagereye_ng/config')
const fs = require('fs')
const yaml = require('js-yaml')
// Path to the model yaml file.
const path = require('path')
const MODEL_PATH = path.join(__dirname, '../../../shared/models.yml')
const snakeCase = require('snake-case')
const dbSchema = JSON.parse(fs.readFileSync('../../shared/database.json'))

const validNicNameRegex = '(^enp)|(^eth[0-9]+$)|(^ens)|(^eno)|(^enx)'
/**
 * Network interface default name
 *  1. Names incorporating Firmware/BIOS provided index numbers for on-board
 *      devices (example: eno1)
 *  2. Names incorporating Firmware/BIOS provided PCI Express hotplug slot
 *      index numbers (example: ens1)
 *  3. Names incorporating physical/geographical location of the connector
 *      of the hardware (example: enp2s0)
 *  4. Names incorporating the interfaces's MAC address (example:
 *      enx78e7d1ea46da)
 *  5. Classic, unpredictable kernel-native ethX naming (example: eth0)
 *
 *  ref: https://www.freedesktop.org/wiki/Software/systemd/PredictableNetworkInterfaceNames/ 
 */

class GPURequestError extends Error {
    constructor(message, name) {
        super(message)

        this.name = name

        Error.captureStackTrace(this, this.constructor)
    }
}

const licenseRequestValidator = checkSchema({
    'application.*.type': {
        isIn: {
            options: [dbSchema['subdoc_pipelines']['type']['enum']],
            errorMessage: `Application type is not valid`,
        },
        exists: true
    },
    'application.*.channel': {
        custom: {
            options: async (chan) => {
                if (typeof chan === "undefined") {
                    throw new Error('Channel is required')
                }
                if (!Number.isInteger(chan)) {
                    throw new Error('Channel must be an integer')
                }
                if (chan <= 0) {
                    throw new Error('Channel must greater than 0')
                }
            }
        }
    }
})

const licenseValidator = checkSchema({
    'license': {
        custom: {
            options: async (license) => {
                if (typeof license === "undefined") {
                    throw new Error('license is required')
                }
                if (!isString(license)) {
                    throw new Error('license must be an encrypted string')
                }
            }
        }
    }
})

const publicKeyValidator = checkSchema({
    'publicKey': {
        custom: {
            options: async (publicKey) => {
                if (typeof publicKey === "undefined") {
                    throw new Error('publicKey is required')
                }
                if (!isString(publicKey)) {
                    throw new Error('publicKey must be an encrypted string')
                }
            }
        }
    }
})

const getLicenseProjection = {
    '_id': 1,
    'application': 1,
    'createdAt': 1,
    'expiredAt': 1,
    'vendor': 1,
}

async function generateLicenseRequest(req, res, next) {
    const { project, application, token } = req.body
    try {
        await checkGPUMemory(application)
    } catch(e) {
        if(e instanceof GPURequestError) {
            return next(createError(400, e.message))
        } else {
            return next(createError(500, e.message))
        }
    }

    // prepare encrypted license request
    try {
        const data = {
            project,
            application,
            machineID: await machineId(),
            mac: JSON.stringify(getSystemMac()),
            key: getPublicKey(),
            uuid: genUUID(),
            token
        } 
        // encrypt request data
        const encryptedReq = await encrypt(JSON.stringify(data))
        return res.status(200).send({ "licenseRequest": encryptedReq })
    } catch(e) {
        if(e instanceof ServerKeyError) {
            return next(createError(400, e.message))
        } else {
            return next(createError(500, e.message))
        }
    }
    
}

async function checkGPUMemory(requestedApps) {
    // get GPU memory from analyzer manager
    let gpusInfo
    let licensedApps
    try {
        gpusInfo = await getGPUResource()

        // get existing license records for current GPU memory consumption
        licensedApps = await licenseModel.aggregate([
            {
                $unwind: "$application"
            }, {
                $group: {
                    "_id": "$application.type", 
                    "channel": {
                        $sum: "$application.channel"
                    }
                }
            }
        ])
    } catch(e) {
        throw e
    }

    if(!gpusInfo) {
        throw new Error('Get GPU resource failed')
    }

    // total app requirements
    let apps = {}
    for (const app of licensedApps) {
        if(!apps.hasOwnProperty(app._id)) {
            apps[app._id] = 0
        }
        apps[app._id] += app.channel
    }

    for (const app of requestedApps) {
        if(!apps.hasOwnProperty(app.type)) {
            apps[app.type] = 0
        }
        apps[app.type] += app.channel
    }

    // calculate GPU memory request
    const appConfig = config.apps
    const modelConfig = yaml.safeLoad(fs.readFileSync(MODEL_PATH))
    for (const app in apps) {
        const appName = snakeCase(app)
        if (appName == 'stitching') {
            // TODO pre allocate GPU for app stitching and config ...
            continue
        }
        if(!appConfig[appName]) {
            throw new GPURequestError('Invalid application name')
        }
        for(const modelName of Object.keys(appConfig[appName].models)) {
            if(!modelConfig.models[modelName]) {
                continue
            }

            const modelInstancesNum = Math.ceil(apps[app] / appConfig[appName].models[modelName].per_model_amount)
            const modelGPUUsage = modelConfig.models[modelName].gpu_usage
            for(let i = 0 ; i < modelInstancesNum ; i++) {
                let allocated = false
                for(let gpu of gpusInfo) {
                    if(gpu["memory_total"] > modelGPUUsage) {
                        gpu["memory_total"] -= modelGPUUsage
                        allocated = true
                        break
                    }
                }
                if(!allocated) {
                    throw new GPURequestError('Insufficient GPU memory')
                }
            }
        }
    }
}

async function uploadServerPublicKey(req, res, next) {
    const { publicKey } = req.body
    try {
        await saveServerPublicKey(publicKey)
    } catch(e) {
        return next(createError(500, e.message))
    }
    return res.status(204).send()
}

async function uploadLicense(req, res, next) {
    const { license } = req.body
    let parsedData
    try {
        // decrypt license file
        const decryptedLicense = decrypt(license)
        // validate the license file
        parsedData = licenseFile.parse({
            publicKey: await getServerPublicKey(),
            licenseFile: decryptedLicense,
            template
        })
    } catch(e) {
        if(e instanceof ServerKeyError) {
            return next(createError(400, e.message))
        } else {
            return next(createError(500, e.message))
        }
    }

    const mac = JSON.parse(parsedData.data.mac)
    if(parsedData.valid && 
        isEqual( parsedData.data.machineID, await machineId() ) &&
        isEqual( mac.sort(), getSystemMac().sort() ) ) {

        const application = JSON.parse(parsedData.data.application)
        try {
            const newLicense = await licenseModel.create({
                application,
                uuid: parsedData.data.uuid,
                createdAt: now('sec'),
                expiredAt: parsedData.data.expiredAt === 'undefined' ? 
                            undefined : 
                            parsedData.data.expiredAt,
                vendor: parsedData.data.vendor,
            })
            res.status(201).send({ _id: newLicense._id })
        } catch(e) {
            if(e.code === 11000) {
                return next(createError(400, 'License already existed'))
            } else {
                return next(createError(500, null, e))
            }
        }
    } else {
        next(createError(400, 'License validation failed'))
    }
}

async function getLicense(req, res, next) {
    const { id } = req.params

    if (!isValidId(id)) {
        return next(createError(400, 'Invalid ID'))
    }

    try {
        const result = await licenseModel.findById(id, getLicenseProjection)

        if (!result) {
            return next(createError(404, 'License not found'))
        }

        res.send(result)
    } catch (err) {
        return next(createError(500, null, err))
    }
}

async function getAllLicenses(req, res, next) {
    try {
        const list = await licenseModel.find({}, getLicenseProjection)

        res.send(list)
    } catch (err) {
        return next(createError(500, null, err))
    }
}

async function deleteLicense(req, res, next) {
    const { id } = req.params

    if (!isValidId(id)) {
        return next(createError(400, 'Invalid ID'))
    }

    try {
        // get the actual amount of analyzers
        const analyzerList = await getAnalyzersSum()
        // get the amount of allowed analyzer excluding the to-be-deleted license
        const licenseList = await licenseModel.aggregate([
            {
                $match: {
                    _id: {
                        $ne: mongoose.Types.ObjectId(id)
                    }
                }
            },
            {
                $unwind: "$application"
            },
            {
                $group: {
                    "_id": "$application.type",
                    "sum": {
                        "$sum": "$application.channel"
                    }
                }
            }
        ])

        // check if we have enough license
        for (const analyzer of analyzerList) {
            let found = false
            for (const license of licenseList) {
                if(analyzer._id === license._id) {
                    found = true
                    if(license.sum >= analyzer.sum) {
                        break
                    } else {
                        return next(createError(400, 'License of ' + analyzer._id + ' will be insufficient.'))
                    }
                }
            }
            if(!found) {
                return next(createError(400, 'License of ' + analyzer._id + ' will be insufficient.'))
            }
        }

        const result = await licenseModel.findByIdAndRemove(id)

        if (!result) {
            return next(createError(404, 'License not found'))
        }

        return res.status(204).send()
    } catch (err) {
        return next(createError(500, null, err))
    }
}

function getSystemMac() {
    const nics = os.networkInterfaces()
    const arr = []
    for (const key in nics) {
        if (key.match(validNicNameRegex)) {
            arr.push(nics[key][0]['mac'])
        }
    }
    return arr
}

async function trialLicenseValidator(req, res, next) {
    const licenses = await licenseModel.find({}).lean()
    for(const license of licenses) {
        if(license.expiredAt < Date.now()/1000) {
            return next(createError(400, 'License has expired.'))
        }
    }
    next()
}

/*
 * Routing Table
 */
routesWithAuth(
    router,
    ['get', '/licenses', getAllLicenses],
    ['get', '/license/:id', getLicense],
    ['delete', '/license/:id', deleteLicense],
    ['post', '/license/request', licenseRequestValidator, validate, generateLicenseRequest],
    ['post', '/license/upload', licenseValidator, validate, uploadLicense],
    ['post', '/license/upload_server_key', publicKeyValidator, validate, uploadServerPublicKey]
)

module.exports = {
    license: router,
    trialLicenseValidator
}
