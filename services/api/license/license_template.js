const template = [
    '====BEGIN LICENSE====',
    '{{&application}}',
    '{{&vendor}}',
    '{{&machineID}}',
    '{{&mac}}',
    '{{&uuid}}',
    '{{&expiredAt}}',
    '{{&serial}}',
    '=====END LICENSE====='
].join('\n');

module.exports = { template }