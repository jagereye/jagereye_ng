const http = require('http')
const NATS = require('nats')
const config = require('jagereye_ng/config')
const process = require('process')
const app = require('./app')
const notification = require('./notification')
const logger = require('./logger')

process.on('unhandledRejection', (reason, promise) => {
    logger.error(
        `Unhandle rejection, reason: ${reason}, promise: ${JSON.stringify(promise)}`
    )
}).on('uncaughtException', (err) => {
    logger.error(`Uncaught exception: \n${err.stack}`)
    process.exit(1)
})

// Initialize NATS
const natsServers = ['nats://'+config.services.messaging.network.ip+':4222']
global.nats = NATS.connect({
    'maxReconnectAttempts': -1,
    'reconnectTimeWait': 250,
    'servers': natsServers
})
nats.on('error', (err) => {
    logger.error('NATS connected error, NATS servers: '
        + natsServers.toString()
        + ', error: '+ err.toString())
})
nats.on('connect', (nc) => {
    logger.info('NATS connected, NATS servers: ' + natsServers.toString())
})
nats.on('disconnect', () => {
    logger.info('NATS disconnected, NATS servers: ' + natsServers.toString())
})
nats.on('reconnecting', () => {
    logger.info('NATS reconnectinig, NATS servers: ' + natsServers.toString())
})
nats.on('close', () => {
    logger.info('NATS connection closed, NATS servers: ' + natsServers.toString())
})


const httpServer = http.createServer(app)
// Initialize notification
notification.init(httpServer, {})
notification.start()

const port = 5000
httpServer.listen(port, (error) => {
    if (error) {
        logger.error(error)
    } else {
        logger.info(`==> Listening on port ${port}`)
    }
})
