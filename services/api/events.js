const express = require('express')
const router = express.Router()
const mongoose = require('mongoose');
const Ajv = require('ajv');
const forEach = require('lodash/forEach');

const { createError } = require('./utils')
const { routesWithAuth } = require('./auth')

const { ObjectId } = mongoose.Types;
const models = require('jagereye_ng/database')
const eventModel = models['events'];

const logger = require('./logger');
const startsWith = require('lodash/startsWith')

const { trialLicenseValidator } = require('./license/license')
/*
 * Projections
 */
const getEventsProjection = {
    expired: 0,
}

const ajv = new Ajv();
const eventQuerySchema = {
    type: 'object',
    properties: {
        timestamp: {
            type: 'object',
            properties: {
                start: {type: 'number'},
                end: {type: 'number'}
            },
            additionalProperties: false
        },
        events: {
            type: 'object',
            properties: {
                gt: {type: 'string'},
                lt: {type: 'string'},
                gte: {type: 'string'},
                lte: {type: 'string'}
            },
            minProperties: 1,
            additionalProperties: false
        },
        analyzers: {
            type: 'array',
            items: {
                type: 'string'
            },
        },
        types: {
            type: 'array',
            items: {
                type: 'string'
            },
        },
        limit: {
            type: 'number'
        },
        page: {
            type: 'number'
        },
        sortBy: {
            type: 'object',
            properties: {
                _id: {
                    type: 'string',
                    enum: ['asc', 'ascending', '1', 'desc', 'descending', '-1']
                },
                timestamp: {
                    type: 'string',
                    enum: ['asc', 'ascending', '1', 'desc', 'descending', '-1']
                },
                analyzerId: {
                    type: 'string',
                    enum: ['asc', 'ascending', '1', 'desc', 'descending', '-1']
                }
            },
            additionalProperties: false
        }
    }
    ,additionalProperties: true
}

const eventQueryValidator = ajv.compile(eventQuerySchema);

function validateEventQuery(req, res, next) {
    if(!eventQueryValidator(req.body)) {
        return next(createError(400, eventQueryValidator.errors));
    }
    next();
}

async function searchEvents(req, res, next) {
    let query = {
        expired: {
            $ne: true,
        },
    }
    let body = req.body

    if(body['timestamp']) {
        let timestampQuery = {};
        if (body['timestamp']['start']) {
            timestampQuery.$gte = body['timestamp']['start']
        }
        if (body['timestamp']['end']) {
            timestampQuery.$lte = body['timestamp']['end']
        }
        query['timestamp'] = timestampQuery
    }

    if(body['events']) {
        let eventIdQuery = {};
        if (body['events']['gt']) {
            // TODO: need error handling,
            // when body['events']['gt'] is not object id format
            // it will throw error.
            // All of the below
            eventIdQuery.$gt = ObjectId(body['events']['gt'])
        }
        if (body['events']['lt']) {
            eventIdQuery.$lt = ObjectId(body['events']['lt'])
        }
        if (body['events']['gte']) {
            eventIdQuery.$gte = ObjectId(body['events']['gte'])
        }

        if (body['events']['lte']) {
            eventIdQuery.$lte = ObjectId(body['events']['lte'])
        }
        query['_id'] = eventIdQuery
    }

    if (body['analyzers']) {
        query['analyzerId'] = {'$in': body['analyzers']}
    }

    if (body['types']) {
        query['type'] = {'$in': body['types']}
    }

    forEach(req.body, (value, key) => {
        if (startsWith(key, 'content.')) {
            query[key] = value
        }
    })

    let options = {}
    if (body['limit']) {
        options['limit'] = body['limit']
    }

    if (body['page']) {
        // must give page with limit
        if(!body['limit']) {
            return next(createError(400, 'Option "page" must come with "limit"'))
        }

        options['skip'] = ( body['page'] - 1 ) * body['limit']
    }

    if (body['sortBy']) {
        options['sort'] = body['sortBy']
    }

    try {
        const list = await eventModel.find(query, getEventsProjection, options)
        const total = await eventModel.find(query).count()
        res.send({ total: total, result: list})
    } catch(err) {
        logger.error(err)
        return next(createError(500, null))
    }
}

function deleteEvent(req, res, next) {
    let eventId = null;
    try {
        eventId = ObjectId(req.params['id']);
    }
    catch(err) {
        logger.error(err);
        return next(createError(404, {msg: 'invalid event id'}))
    }

    eventModel.findOne({
        _id: eventId,
        expired: {
            $ne: true,
        },
    })
    .then((event) => {
        if (!event) {
            return next(createError(404, `Event not found: ${eventId}`))
        }

        return eventModel.findByIdAndUpdate(eventId, {
            $set: {
                expired: true,
            },
        })
        .then(() => res.status(204).send())
    })
    .catch((err) => {
        logger.error(err)
        return next(createError(500, null))
    });
}

/*
 * Routing Table
 */
routesWithAuth(
    router,
    ['post', '/events', validateEventQuery, trialLicenseValidator, searchEvents],
    ['delete', '/event/:id', deleteEvent],
)

module.exports = router
