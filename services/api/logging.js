const express = require('express');
const router = express.Router()
const path = require('path');
const Ajv = require('ajv');
const format = require('util').format;
const P = require('bluebird');
const execAsync = P.promisify(require('child_process').exec);

const config = require('jagereye_ng/config')
const models = require('jagereye_ng/database')
const logger = require('./logger');
const sysModel = P.promisifyAll(models['systems']);
const writeFile = P.promisify(require('fs').writeFile)
const unlink = P.promisify(require('fs').unlink)
const { getSystemInfo, getSystemStatus } = require('./systemUtils')

const { createError } = require('./utils')
const { routesWithAuth } = require('./auth')

// TODO: It may need be configurable
const NUM_OF_BRAINS = 1
const DEFAULT_REQUEST_TIMEOUT = 15000

class PackLogsError extends Error {
/**
 * An error class will be throw when packLogs() failed
 */
    constructor (message, status) {
        super(message);
        this.name = this.constructor.name;
        // Capturing stack trace, excluding constructor call from it.
        Error.captureStackTrace(this, this.constructor);
        this.status = status || 500;
    }
};

async function packLogs(outputZipFile) {
    /* Zip the syslog and jager's log
     * @param   {string} outputZipFile  the path of output zip file
     *
     * @throw   {PackLogsError}
     */
    try {
        let timestamp = new Date().getTime().toString()

        let status = await getSystemStatus()
        const statusPath = '/tmp/status_' + timestamp
        await writeFile(statusPath, JSON.stringify(status))

        let info = await getSystemInfo()
        info.softwareVersion = config.version
        info.hardwareModel = config.model
        const infoPath = '/tmp/info_' + timestamp
        await writeFile(infoPath, JSON.stringify(info))

        const syslogPath = path.join(config.services.api.logging_mount.target, 'syslog*')
        const jagerlogPath = path.join(config.services.api.logging_mount.target, 'jager/*')
        await execAsync(format('sudo zip %s %s %s %s %s', outputZipFile, syslogPath, jagerlogPath, infoPath, statusPath), {timeout: DEFAULT_REQUEST_TIMEOUT})
        // remove tmp files after zip done
        await unlink(statusPath)
        await unlink(infoPath)
    } catch(err) {
        throw new PackLogsError(err);
    }
}

async function getLoggingBundle(req, res, next) {
    /**
     * Middleware of GET /system/logging/bundle
     */
    const zipFile = 'logs.zip'
    try {
        await packLogs(zipFile);
        res.status(200).sendFile(path.join(__dirname, zipFile));
    } catch(err) {
        logger.error(err);
        return next(createError(500));
    }
}

const ajv = new Ajv();
const patchSchema = {
    type: 'object',
    properties: {
        debugMode: {type: 'boolean'}
    },
    required:['debugMode'],
    additionalProperties: false
}
const patchValidator = ajv.compile(patchSchema);

function validatePatch(req, res, next) {
    /**
     * Middleware to validate body for PATCH /system/logging
     */

    if(!patchValidator(req.body)) {
        return next(createError(400, patchValidator.errors));
    }
    next();
}

// TODO: The function is same as requestBackend() in analyzers.js.
//       may need to be refactored to inprove the reusibilty
function requestBackend(request, timeout, callback) {
    let reqTimeout = timeout
    let cb = callback
    let ignore = false
    let count = 0

    if (typeof reqTimeout === 'function') {
        reqTimeout = DEFAULT_REQUEST_TIMEOUT
        cb = timeout
    }

    // Set a timeout for aggregating the replies
    const timer = setTimeout(() => {
        ignore = true
        cb({ code: NATS.REQ_TIMEOUT })
    }, reqTimeout)

    function closeResponse() {
        ignore = true
        clearTimeout(timer)
    }

    nats.request('api.analyzer', request, {'max': NUM_OF_BRAINS}, (reply) => {
        if (!ignore) {
            count += 1
            let isLastReply = count === NUM_OF_BRAINS
            if (isLastReply) {
                // All replies are received, cancel the timeout
                clearTimeout(timer)
            }
            try {
                const replyJSON = JSON.parse(reply)
                if (replyJSON['code'] &&
                    replyJSON['code'] === msg['ch_api_analyzer_reply']['NOT_AVAILABLE']) {
                    const errReply = {
                        error: {
                            code: msg['ch_api_analyzer_reply']['NOT_AVAILABLE'],
                            message: 'Runtime instance is not available to accept request right now'
                        }
                    }
                    return cb(errReply, isLastReply, closeResponse)
                }
                cb(replyJSON, isLastReply, closeResponse)
            } catch (e) {
                const errReply = { error: { message: e } }
                cb(errReply, isLastReply, closeResponse)
            }
        }
    })
}


async function patchLogging(req, res, next) {
    /**
     * Middleware of PATCH /system/logging
     */

    // save new setting into DB
    const setting = req.body;
    try {
        await sysModel.updateOne({_id: 'logging'}, {content: setting});
        if (req.body.debugMode) {
            logger.changeLevel('debug');
        }
        else {
            logger.changeLevel('info');
        }

        // pass the new setting throw messaging to analyzers
        const request = JSON.stringify({
            command: 'CHANGE_DEBUG_MODE',
            params: setting
        });
        requestBackend(request, (reply, isLastReply, closeResponse) => {
            if (reply['code'] && reply['code'] === NATS.REQ_TIMEOUT) {
                let error = new Error('Timeout Error: Request: changing debug mode')
                return next(createError(500, null, error))
            }
            if (reply['error']) {
                return next(createError(500, reply['error']['message']))
            }
            closeResponse()
            return res.status(200).send()
        });
    } catch (err) {
        logger.error(err);
        return next(createError(500));
    }
}


async function getLogging(req, res, next) {
    /**
     * Middleware of GET /system/logging
     */

    // get logging config from db
    try {
        let setting = await sysModel.findOne({'_id': 'logging'});
        // response back
        res.status(200).send(setting.content);
    } catch (err) {
        logger.error(err);
        return next(createError(500));
    }
}


async function setupLogger() {
    /**
     * Setup the logger when API server start
     */

    // check if default logging config is enabled
    if (config.logging.dev_config.enabled) {
        logger.changeLevel(config.logging.dev_config.loglevel);
        return;
    }

    // check if there exist logging config in DB
    try {
        let setting = await sysModel.findOne({'_id': 'logging'});
        if(!setting) {
            // if not existed, create default network setting
            const defaultSetting = {};
            const content = {};
            content.debugMode = false;

            defaultSetting._id = 'logging';
            defaultSetting.content = content;
            try {
                await sysModel.create(defaultSetting);
            } catch (err) {
                logger.error(err)
            }
            setting = defaultSetting;
        }
        // setup debug level of the logger
        if (setting.content.debugMode) {
            logger.changeLevel('debug');
        }
        else {
            logger.changeLevel('info');
        }
    } catch (err) {
        logger.error(err)
    }
}

/*
 * Routing Table
 */
routesWithAuth(
    router,
    ['get', '/system/logging/bundle', getLoggingBundle],
    ['get', '/system/logging', getLogging],
    ['patch', '/system/logging', validatePatch, patchLogging],
)

module.exports = {
    loggingRouter: router,
    setupLogger
}
