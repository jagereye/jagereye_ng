const express = require('express');
const router = express.Router();
const Ajv = require('ajv');
const { checkSchema } = require('express-validator/check')

const config = require('jagereye_ng/config').services.api.settings
const {
    getNetworkContent,
    updateNetworkContent,
    getSystemRecord,
    updateSystemRecord
} = require('./systemUtils');
const { createError, validate } = require('./utils');
const { routesWithAuth } = require('./auth');
const { setNetworkInterface, getInterfaceIp, SetNetworkError } = require('./settings_utils');
const logger = require('./logger');

const isPlainObject = require('lodash/isPlainObject')
const includes = require('lodash/includes')
const isString = require('lodash/isString')
const isArray = require('lodash/isArray')
const every = require('lodash/every')
const isIp = require('is-ip')

const dataPortInterface = config.data_port.device;

const ajv = new Ajv();
const settingPatchSchema = {
    'anyOf': [
        {
            type: 'object',
            properties: {
                mode: {
                    type: 'string',
                    pattern: '^static$',
                },
                address: {
                    type: 'string',
                    format: 'ipv4'
                },
                gateway: {
                    type: 'string',
                    format: 'ipv4'
                },
                netmask: {
                    type: 'string',
                    format: 'ipv4'
                }
            },
            additionalProperties: false,
            required: ['mode', 'address', 'gateway', 'netmask']
        },
        {
            type: 'object',
            properties: {
                mode: {
                    type: 'string',
                    pattern: '^dhcp$',
                },
            },
            additionalProperties: false,
            required: ['mode']
        },
    ]
}

const setNotificationValidator = checkSchema({
    line: {
        custom: {
            options: async (line) => {
                if (line && !isPlainObject(line)) {
                    throw new Error('line must be an object')
                }
                const lineProperties = ['chatbotId', 'qrcode']
                for (const key in line) {
                    if (!includes(lineProperties, key)) {
                        throw new Error(
                            `Key "${key}" is not supported: [${lineProperties}]`,
                        )
                    }
                }
                for (const property of lineProperties) {
                    if (!isString(line[property]) || !line[property]) {
                        throw new Error(`${property} must be a non-empty string`)
                    }
                }
            },
        },
    },
})

const setClusterValidator = checkSchema({
    role: {
        custom: {
            options: async (role) => {
                const roles = ['master', 'slave']
                if (role && !includes(roles, role)) {
                    throw new Error(
                        `Role "${role}" is not supported: [${roles}]`,
                    )
                }
            },
        },
    },
    name: {
        custom: {
            options: async (name) => {
                if (name && !isString(name)) {
                    throw new Error(`Name must be a string`)
                }
            },
        },
    },
    slaveList: {
        custom: {
            options: async (slaveList) => {
                if (slaveList) {
                    if(!isArray(slaveList)) {
                        throw new Error(`${slaveList} should be a list of IP addresses`)
                    }
                    if(!every(slaveList, isIp)) {
                        throw new Error(`slaveList contains invalid IP`)
                    }
                }
            },
        },
    },
})

const settingPatchValidator = ajv.compile(settingPatchSchema);

function validateSettingPatch(req, res, next) {
    if(!settingPatchValidator(req.body)) {
        // TODO: returned msg should be refine
        return next(createError(400, settingPatchValidator.errors));
    }
    next();
}

async function patchSettings(req, res, next) {
    const body = req.body

    const mode = body.mode;
    const addr = body.address;
    const netmask = body.netmask;
    const gateway = body.gateway;

    const newSettings = {};
    newSettings.mode = mode;
    newSettings.address = addr;
    newSettings.netmask = netmask;
    newSettings.gateway = gateway;
    newSettings.status = 'processing';
    // First, update record in db and response
    await updateNetworkContent(newSettings)
    res.status(204).send();
    // start configure network interface
    try {
        await resetNetworkInterface(dataPortInterface, mode, addr, netmask, gateway);
        // update the status in db
        newSettings.status = 'done';
        // get the dhcp ip and update
        if (mode === 'dhcp') {
            const dhcpAddress = getInterfaceIp(dataPortInterface);
            if (!dhcpAddress) {
                throw new ResetNetworkError('dhcp failed');
            }
            newSettings.address = 'None';
            newSettings.netmask = 'None';
            newSettings.gateway = 'None';
        }
        await updateNetworkContent(newSettings)
    } catch (err) {
        newSettings.status = 'failed';
        if (mode === 'dhcp') {
            newSettings.address = 'None';
            newSettings.netmask = 'None';
            newSettings.gateway = 'None';
        }
        await updateNetworkContent(newSettings)
        logger.error(err);
    };
}

async function getSettings(req, res, next) {
    let result = await getNetworkContent()
    if (result.mode === 'dhcp') {
        result.netmask = undefined;
        result.gateway = undefined;

        // Ray: when users set dhcp without port connected,
        // the port cannot get IP.
        // whenever user connect the port, the port will receive IP soon.
        // then the status of the port should be update

        // on the other hand, whenever the port disconnected,
        // the dhcp ip will be invalid, then the status should be updated

        const dhcpAddress = getInterfaceIp(dataPortInterface);
        if (dhcpAddress) {
            result.address = dhcpAddress;
            result.status = 'done';
            await updateNetworkContent({mode: 'dhcp', status: 'done'})
        } else {
            result.address = 'None';
            if (result.status != 'processing') {
                result.status = 'failed';
                await updateNetworkContent({mode: 'dhcp', status: 'failed'})
            }
        }
    }
    res.status(200).send(result);
}


async function initNetworkSetting() {
    const result = await getNetworkContent()
    if (result) {
        // restore network setting according to db record
        const {
            mode: mode,
            address: addr,
            netmask: netmask,
            gateway: gateway
        } = result
        try {
            await setNetworkInterface(dataPortInterface, mode, addr, netmask, gateway);
        } catch (err) {
            logger.error(err)
            throw new SetNetworkError('Error setting network interface')
        }
    } else {
        // create default network setting
        let defaultSettings = {};
        defaultSettings.mode = 'dhcp';
        defaultSettings.address = 'None';
        defaultSettings.status = 'processing';
        try {
            await updateNetworkContent(defaultSettings)
            await setNetworkInterface(dataPortInterface ,'dhcp');
        } catch (err) {
            defaultSettings.status = 'failed';
            await updateNetworkContent(defaultSettings)
            logger.error(err)
        }
    }
}

async function setNotification(req, res, next) {
    const { line } = req.body
    const result = await getSystemRecord('notification')
    const content = result.content
    if(line) {
        content.line = line
    }
    await updateSystemRecord('notification', content)
    res.status(204).send()
}

async function getNotification(req, res, next) {
    const result = await getSystemRecord('notification')
    if(!result) {
        return res.status(204).send()
    }
    res.status(200).send(result.content)
}

async function initNotification() {
    const result = await getSystemRecord('notification')
    if(!result) {
        const content = {
            line: {
                chatbotId: "",
                qrcode: ""
            }
        }
        await updateSystemRecord('notification', content)
    }
}

async function getClusterSettings(req, res, next) {
    const result = await getSystemRecord('clustering')
    if(!result) {
        return res.status(204).send()
    }

    const { role, name, slaveList } = result.content
    let status = []
    if(role === 'master') {
        // TODO: if role is master, it should ping each slave for status.
        // For now we respond a fake status
        slaveList.forEach(slaveIP => {
            status.push({ip: slaveIP, name: slaveIP, alive: true})
        })
        res.status(200).send({ role, name, slaveList: status })
    } else {
        res.status(200).send({ role, name })
    }
}

async function setClusterSettings(req, res, next) {
    const { role, name, slaveList } = req.body
    const result = await getSystemRecord('clustering')
    const content = result.content
    if(name) {
        content.name = name
    }
    if(role) {
        content.role = role
    }
    if(role === 'master' && slaveList) {
        content.slaveList = slaveList
    }

    await updateSystemRecord('clustering', content)
    res.status(204).send()
}

async function initClustering() {
    const result = await getSystemRecord('clustering')
    if(!result) {
        const content = {
            role: "master",
            slaveList: [],  // TODO: when a slave is setup, save its ip in this list
        }
        await updateSystemRecord('clustering', content)
    }
}

/*
 * Routing Table
 */
routesWithAuth(
    router,
    ['get', '/system/networking', getSettings],
    ['patch', '/system/networking', validateSettingPatch, patchSettings],
    ['get', '/system/notification', getNotification],
    ['patch', '/system/notification', setNotificationValidator, validate, setNotification],
    ['get', '/system/clustering', getClusterSettings],
    ['patch', '/system/clustering', setClusterValidator, validate, setClusterSettings],
)

module.exports = {
    settings: router,
    initNetworkSetting,
    initNotification,
    initClustering,
}
