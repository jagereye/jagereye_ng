const express = require('express')
const { checkSchema } = require('express-validator/check')

const isString = require('lodash/isString')

const config = require('jagereye_ng/config')
const models = require('jagereye_ng/database')
const { createError, validate, isValidId } = require('./utils')
const { jwt, jwtOptions } = require('./auth/passport')
const { routesWithAuth } = require('./auth')
const { ROLES } = require('./constants')
const logger = require('./logger')
/*
 * Projections
 */
const getUserProjection = {
    '_id': 1,
    'username': 1,
    'role': 1,
    'passwordLastUpdated': 1,
}

const router = express.Router()

const userValidator = checkSchema({
    username: {
        exists: true,
        errorMessage: 'Username is required',
    },
    password: {
        exists: true,
        errorMessage: 'Password is required',
    },
})

const roleValidator = checkSchema({
    role: {
        matches: {
            errorMessage: `Role should be "${ROLES.WRITER}" or "${ROLES.READER}"`,
            options: new RegExp(`\\b(${ROLES.WRITER}|${ROLES.READER})\\b`),
        },
    }
})

const changePasswordValidator = checkSchema({
    newPassword: {
        exists: true,
        errorMessage: 'New password is required',
    },
})

function isSelfOrAdmin(req, res, next) {
    if (config.services.api.token.enabled) {
        const { id: targetId } = req.params
        const { _id: requesterId, role: requesterRole } = req.user

        if (requesterRole !== ROLES.ADMIN && requesterId.toString() !== targetId) {
            return next(createError(400, 'Request non-self user'))
        }
    }

    next()
}

async function getAllUsers(req, res, next) {
    try {
        const list = await models.users.find({}, getUserProjection)

        return res.send(list)
    } catch (err) {
        return next(createError(500, null, err))
    }
}

async function createUser(req, res, next) {
    const { username, password, role } = req.body

    try {
        const { _id } = await models.users.create({
            username,
            password,
            role,
        })

        return res.status(201).send({ _id })
    } catch (err) {
        if (err.name === 'MongoError') {
            if (err.code === 11000) {
                return next(createError(409, 'Username already exists'))
            }
        }

        return next(createError(500, null, err))
    }
}

async function getUser(req, res, next) {
    const { id } = req.params

    if (!isValidId(id)) {
        return next(createError(400, 'Invalid ID'))
    }

    try {
        const result = await models.users.findById(id, getUserProjection)

        if (!result) {
            return next(createError(404, 'User not existed'))
        }

        res.send(result)
    } catch (err) {
        return next(createError(500, null, err))
    }
}

async function deleteUser(req, res, next) {
    const { id } = req.params

    if (!isValidId(id)) {
        return next(createError(400, 'Invalid ID'))
    }

    try {
        const targetUser = await models.users.findById(id)

        if (!targetUser) {
            return next(createError(404, 'User not existed'))
        }

        if (targetUser.role === ROLES.ADMIN) {
            // Deletion of admin user is not allowed.
            return next(createError(400, 'Deletion of admin user is not allowed'))
        }

        const result = await models.users.findByIdAndRemove(id)

        if (!result) {
            return next(createError(404, 'User not existed'))
        }

        return res.status(204).send()
    } catch (err) {
        return next(createError(500, null, err))
    }
}

async function changePassword(req, res, next) {
    const { id: targetId } = req.params
    const { _id: requesterId } = req.user
    const { oldPassword, newPassword } = req.body

    if (!isValidId(targetId)) {
        return next(createError(400, 'Invalid ID'))
    }

    try {
        const targetUser = await models.users.findById(targetId)

        if (!targetUser) {
            return next(createError(404, 'User not existed'))
        }

        // If the user changes its own password and it is not the first time
        // to be changed, then the request also needs old password.
        if (requesterId.toString() === targetId && targetUser.passwordLastUpdated) {
            if (!isString(oldPassword) || !targetUser.checkPassword(oldPassword)) {
                return next(createError(400, 'Incorrect old password'))
            }
        }

        targetUser.password = newPassword
        targetUser.passwordLastUpdated = new Date()
        await targetUser.save()

        return res.status(204).send()
    } catch (err) {
        return next(createError(500, null, err))
    }
}

async function updateRole(req, res, next) {
    const { id: targetId } = req.params
    const { role } = req.body
    const { _id: requesterId } = req.user

    try {
        const targetUser = await models.users.findById(targetId)

        if (!targetUser) {
            return next(createError(404, 'User not existed'))
        }

        if (targetUser.role === ROLES.ADMIN) {
            return next(createError(400, 'Updating admin role is not allowed'))
        }

        const updated = { role }
        const options = {
            new: true,
            runValidators: true,
        }
        const result = await models.users.findByIdAndUpdate(targetId, updated, options)

        if (!result) {
            return next(createError(404, 'User not existed'))
        }

        return res.status(204).send()
    } catch (err) {
        return next(createError(500, null, err))
    }
}

async function login(req, res, next) {
    const { username, password } = req.body

    try {
        const result = await models.users.findOne({
            username,
        })

        if (!result) {
            return next(createError(401, 'Incorrect username'))
        }

        if (!result.checkPassword(password)) {
            return next(createError(401, 'Incorrect password'))
        }

        const { _id } = result
        const payload = { _id }
        const token = jwt.sign(payload, jwtOptions.secretOrKey)
        const requirePasswordReset = result.passwordLastUpdated ? false : true

        return res.status(200).send({ _id, token, requirePasswordReset })
    } catch (err) {
        return next(createError(500, null, err))
    }
}

async function createAdminUser() {
    const {
        username,
        default_password: password,
    } = config.services.api.admin;

    try {
        const user = await models.users.findOne({ username })

        if (!user) {
            const result = await models.users.create({
                username,
                password,
                role: ROLES.ADMIN,
            })

            logger.info(`Admin user is added, id: ${result.id}`)
        }
    } catch (err) {
        logger.error(err)
    }
}

/*
 * Routing Table
 */
routesWithAuth(
    router,
    ['get', '/users', getAllUsers],
    ['post', '/users', userValidator, roleValidator, validate, createUser],
)
routesWithAuth(
    router,
    ['get', '/user/:id', isSelfOrAdmin, getUser],
    ['delete', '/user/:id', deleteUser],
    ['patch', '/user/:id/password', isSelfOrAdmin, changePasswordValidator, validate, changePassword],
    ['patch', '/user/:id/role', roleValidator, validate, updateRole],
)
router.post('/login', userValidator, validate, login)

module.exports = {
    users: router,
    createAdminUser,
}
