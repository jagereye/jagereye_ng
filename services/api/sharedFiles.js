const path = require('path')
const shell = require('shelljs')

const config = require('jagereye_ng/config')

const SHARED_DIR = path.resolve(
    process.env.SHARED_DIR || config.shared_files.mount.target
)

class FilesShareManager {
    constructor(sharedDir) {
        this.sharedDir = sharedDir
    }

    getFullPath(dirPath) {
        return path.resolve(this.sharedDir, dirPath)
    }

    mkdir(dirPath, options, absolute = false) {
        const fullPath =
            absolute ?
            path.resolve(dirPath) :
            this.getFullPath(dirPath)
        let curPath = fullPath

        shell.mkdir(options, fullPath)

        while (curPath !== this.sharedDir && curPath !== '/') {
            shell.chmod('777', curPath)
            curPath = path.resolve(curPath, '..')
        }

        return fullPath
    }

    rm(dirPath, options, absolute = false) {
        const fullPath =
            absolute ?
            path.resolve(dirPath) :
            this.getFullPath(dirPath)

        shell.rm(options, fullPath)

        return fullPath
    }
}

const filesShareManager = new FilesShareManager(SHARED_DIR)

module.exports = filesShareManager
