const { parseContentKey }  = require('../externals')

describe('External unittest ', () => {
    describe('parseContentKey', () => {
        test('single_camera_face_recognition.face', async () => {
            const externalType = 'single_camera_face_recognition.face'
            const target = parseContentKey(externalType, 'content[image]')
            expect(target).toEqual(['image'])
        })
        test('cross_camera_face_recognition.face', async () => {
            const externalType = 'cross_camera_face_recognition.face'
            const target = parseContentKey(externalType, 'content[image]')
            expect(target).toEqual(['image'])
        })
        test('stitching.calibration', async () => {
            /* TODO update this test case for field update
            const externalType = 'stitching.calibration'
            let wellFormat = []
            for (let i = 0; i < 6; i++){
                for (let j = 0; j < 10; j++) {
                    const result = parseContentKey(externalType, `content[source][${i}][images][${j}]`)
                    expect(result).toEqual(['source', `${i}`, 'images', `${j}`])
                }
            }
            let badFormat = [
                'content[!]',
                'content[source][images][0]',
                'content[source][0][images][a]',
                'content[source][0][images][0]1'
            ]
            for (let i in badFormat) {
                expect(() => {
                    parseContentKey(externalType, badFormat[i])
                }).toThrow()
            }
            */
        })
    })
})
