const express = require('express')
const jwt = require('jsonwebtoken')
const passport = require('passport')
const passportJWT = require("passport-jwt")

const { createError } = require('../utils')
const config = require('jagereye_ng/config')
const models = require('jagereye_ng/database')
const P = require('bluebird')
const userModel = P.promisifyAll(models['users'])

const router = express.Router()

/*
 * Projections
 */
const getUserProjection = {
    _id: 1,
    role: 1,
}

const jwtOptions = {
    jwtFromRequest: passportJWT.ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: config.services.api.token.secret,
}

passport.use(new passportJWT.Strategy(jwtOptions, (payload, done) => {
    return models.users.findById(payload._id, getUserProjection, (err, user) => {
        if (err) {
            return done(createError(500, null, err), false)
        }

        if (!user) {
            return done(createError(401, 'Unauthenticated'), false)
        }

        return done(null, user.toObject())
    })
}))

const authenticate = passport.authenticate('jwt', { session: false })

// It is for the situation when config.services.api.token.enabled = false
async function fakeAuthenticate(req, res, next) {
    const username = config.services.api.admin.username;
    result = await userModel.find({username: username});
    admin = result[0]
    req.user = {_id: admin._id, role: admin.role};
    next();
}

module.exports = {
    authenticate,
    fakeAuthenticate,
    jwt,
    jwtOptions,
}
