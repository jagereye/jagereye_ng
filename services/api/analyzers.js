const express = require('express')
const { validationResult } = require('express-validator/check')
const models = require('jagereye_ng/database')
const P = require('bluebird');
const analModel = P.promisifyAll(models['analyzers']);
const licenseModel = P.promisifyAll(models['licenses']);

const { createError } = require('./utils')
const { routesWithAuth } = require('./auth')
const NATS = require('nats')
const fs = require('fs')
const forEach = require('lodash/forEach');
const isBoolean = require('lodash/isBoolean');
const router = express.Router()

const msg = JSON.parse(fs.readFileSync('../../shared/messaging.json', 'utf8'))
const NUM_OF_BRAINS = 1
const DEFAULT_REQUEST_TIMEOUT = 15000

const logger = require('./logger');

//  setup for prometheus
const prometheusClient = require('prom-client')
const { analyzerStatus } = require('./constants')
const register = prometheusClient.register
const Gauge = prometheusClient.Gauge
const g = new Gauge({
    name: 'analyzer_status',
    help: 'Analyzer status',
    labelNames: ['analyzer']
});
// When scraping metrics, prometheus needs at least some value for exporter status
// to be UP, here we set the Gauge to 1
g.set(1)

/*
 * Projections
 */
const getConfProjection = {
    '_id': 1,
    'name': 1,
    'enabled': 1,
    'source': 1,
    'pipelines': 1,
    'streamOutput': 1
}

async function postReqValidator(req, res, next) {
    const errors = validationResult(req)
    if (!errors.isEmpty()) {
        return next(createError(400, null))
    }

    const pipelines = req.body['pipelines']

    try {
        // check license for analyzer quotation
        const analyzers = await analModel.find({})
        const license = await licenseModel.find({})

        // find allowed channel quota
        let allowed_channel = {}
        for(const entry of license) {
            // if a license is expired, just skip it
            if(entry.expiredAt < Date.now()/1000) {
                continue
            }
            for(const app of entry.application) {
                if(!allowed_channel[app.type]) {
                    allowed_channel[app.type] = 0
                }
                allowed_channel[app.type] += app.channel
            }
        }

        // find existing channel setting
        let existing_channel = {}
        for(const anal of analyzers) {
            for(const entry of anal.pipelines) {
                if(!existing_channel[entry.type]) {
                    existing_channel[entry.type] = 0
                }
                if(entry.type === 'Stitching') {
                    existing_channel[entry.type] += calculateStitchingNumber(entry)
                } else {
                    existing_channel[entry.type] += 1
                }
            }
        }

        let request_channel = {}
        // check if requested application can be contained
        for(const req of pipelines) {
            if(allowed_channel[req.type]) {
                if(existing_channel[req.type]) {
                    if(req.type === 'Stitching') {
                        existing_channel[req.type] += calculateStitchingNumber(req)
                    } else {
                        existing_channel[req.type] += 1
                    }
                    if(existing_channel[req.type] <= allowed_channel[req.type]) {
                        break
                    }
                } else {
                    if(!request_channel[req.type]) {
                        request_channel[req.type] = 0
                    }
                    if(req.type === 'Stitching') {
                        request_channel[req.type] += calculateStitchingNumber(req)
                    } else {
                        request_channel[req.type] += 1
                    }
                    if(request_channel[req.type] <= allowed_channel[req.type]) {
                        break
                    }
                }
            }
            return next(createError(403, 'Quota exceeded.'))
        }
    } catch(err) {
        return next(createError(500, null, err))
    }
    next()
}

function calculateStitchingNumber(pipeline) {
    const cameraGrps = pipeline.params.stitchConfig.panoCamGrouping
    let total = 0
    for (const grp in cameraGrps) {
        total += cameraGrps[grp].length - 1
    }
    return total
}

function requestBackend(request, timeout, callback) {
    let reqTimeout = timeout
    let cb = callback
    let ignore = false
    let count = 0

    if (typeof reqTimeout === 'function') {
        reqTimeout = DEFAULT_REQUEST_TIMEOUT
        cb = timeout
    }

    // Set a timeout for aggregating the replies
    const timer = setTimeout(() => {
        ignore = true
        cb({ code: NATS.REQ_TIMEOUT })
    }, reqTimeout)

    function closeResponse() {
        ignore = true
        clearTimeout(timer)
    }

    nats.request('api.analyzer', request, {'max': NUM_OF_BRAINS}, (reply) => {
        if (!ignore) {
            count += 1
            let isLastReply = count === NUM_OF_BRAINS
            if (isLastReply) {
                // All replies are received, cancel the timeout
                clearTimeout(timer)
            }
            try {
                const replyJSON = JSON.parse(reply)
                if (replyJSON['code'] &&
                    replyJSON['code'] === msg['ch_api_analyzer_reply']['NOT_AVAILABLE']) {
                    const errReply = {
                        error: {
                            code: msg['ch_api_analyzer_reply']['NOT_AVAILABLE'],
                            message: 'Runtime instance is not available to accept request right now'
                        }
                    }
                    return cb(errReply, isLastReply, closeResponse)
                }
                cb(replyJSON, isLastReply, closeResponse)
            } catch (e) {
                const errReply = { error: { message: e } }
                cb(errReply, isLastReply, closeResponse)
            }
        }
    })
}

function expireEvents(target) {
    const search = {
        analyzerId: {
            $in: target,
        }
    }
    const updated ={
        $set: {
            expired: true,
        },
    }

    models['events'].updateMany(search, updated, (err) => {
        if (err) {
            logger.error(err)
        }
    })
}

function requestBackendDeletion(target) {
    const request = JSON.stringify({
        command: 'DELETE',
        params: target,
    })

    requestBackend(request, (reply, isLastReply, closeResponse) => {
        if (reply['code'] && reply['code'] === NATS.REQ_TIMEOUT) {
            let error = new Error(`Timeout Error: Request: deleting analyzers ${target}`)
            logger.error(error)
            return
        }
        if (reply['error']) {
            closeResponse()
            logger.error(reply['error']['message'])
            return
        }
        closeResponse()
    })
}

function getAnalyzers(req, res, next) {
    models['analyzers'].find({}, getConfProjection, (err, list) => {
        if (err) { return next(createError(500, null, err)) }
        if (list.length === 0) { return res.status(200).send([]) }
        const request = JSON.stringify({
            command: 'READ',
            params: list.map(x => x['_id'])
        })
        requestBackend(request, (reply, isLastReply, closeResponse) => {
            if (reply['code'] && reply['code'] === NATS.REQ_TIMEOUT) {
                let error = new Error('Timeout Error: Request: getting analyzers')
                return next(createError(500, null, error))
            }
            if (reply['error']) {
                closeResponse()
                return next(createError(500, reply['error']['message']))
            }
            // TODO: rollback saved record if any error occurred
            closeResponse()
            result = list.map(x => {
                data = x.toJSON()
                const received = JSON.parse(reply['result'][x['_id']])
                data.status = received.status
                data.source.status = received.status_source
                return data
            })
            return res.status(200).send(result)
        })
    })
}

async function createAnalyzer(req, res, next) {
    /* Validate request */
    req.checkBody('name', 'name is required').notEmpty()
    req.checkBody('source', 'source is required').notEmpty()
    req.checkBody('pipelines', 'pipeline is required').notEmpty()
    const errors = req.validationErrors()
    if (errors) {
        return next(createError(400, errors[0]['msg']))
    }

    name = req.body['name']
    source = req.body['source']
    pipelines = req.body['pipelines']
    streamOutput = req.body['streamOutput'] ? true : false

    const config = {
        name,
        enabled: false,
        source,
        pipelines,
        streamOutput,
    }
    const analyzer = new models['analyzers'](config)
    analyzer.save((err, saved) => {
        if (err) {
            if (err.name === 'ValidationError') {
                // TODO: make error message frontend friendly
                return next(createError(400, err.message, err))
            }
            if (err.name === 'MongoError' && err.code === 11000) {
                let dupKey = err.errmsg.slice(err.errmsg.lastIndexOf('dup key:') + 14, -3)
                return next(createError(400, `Duplicate key error: ${dupKey}`, err))
            }
            return next(createError(500, null, err))
        }
        const request = JSON.stringify({
            command: 'CREATE',
            params: { id: saved.id, name, source, pipelines, stream_output: streamOutput }
        })
        requestBackend(request, (reply, isLastReply, closeResponse) => {
            if (reply['code'] && reply['code'] === NATS.REQ_TIMEOUT) {
                logger.error(`Timeout Error: Request: creating analyzer "${saved.id}"`)
            } else {
                if (reply['error']) {
                    logger.error(reply['error']['message'])
                }
                closeResponse()
            }
        })

        res.status(201).send({_id: saved.id})
    })
}

function deleteAnalyzers(req, res, next) {
    models['analyzers'].find({}, (err, list) => {
        if (err) { return next(createError(500, null, err)) }
        if (list.length === 0) { return res.status(204).send() }

        models['analyzers'].remove({}, (err) => {
            if (err) { return next(createError(500, null, err)) }

            const target = list.map(x => x['_id'])

            // Expire all related events.
            expireEvents(target)
            // Request backend to delete all analyzers.
            requestBackendDeletion(target)
            // Send response.
            res.status(204).send()
        })
    })
}

function getAnalyzer(req, res, next) {
    const id = req.params['id']
    models['analyzers'].findById(id, getConfProjection, (err, result) => {
        if (err) { return next(createError(500, null, err)) }
        if (result === null) { return next(createError(404)) }
        let data = result.toJSON()
        const request = JSON.stringify({
            command: 'READ',
            params: id
        })
        requestBackend(request, (reply, isLastReply, closeResponse) => {
            if (reply['code'] && reply['code'] === NATS.REQ_TIMEOUT) {
                let error = new Error(`Timeout Error: Request: getting analyzer "${id}"`)
                return next(createError(500, null, error))
            }
            if (reply['error']) {
                closeResponse()
                return next(createError(500, reply['error']['message']))
            }
            closeResponse()
            const received = JSON.parse(reply['result'])
            data.status = received.status
            data.source.status = received.status_source
            return res.status(200).send(data)
        })
    })
}

function deleteAnalyzer(req, res, next) {
    const id = req.params['id']
    models['analyzers'].findByIdAndRemove(id, (err, result) => {
        if (err) {
            return next(createError(500, null, err))
        }
        if (result === null) {
            return next(createError(404))
        }

        // Expire all related events.
        expireEvents([id])
        // Request backend to delete the analyzer.
        requestBackendDeletion(id)
        // Send response.
        res.status(204).send()
    })
}

function updateAnalyzer(req, res, next) {
    const id = req.params['id']
    const update = {}
    if (req.body.hasOwnProperty('name')) {
        update['name'] = req.body['name']
    }
    if (req.body.hasOwnProperty('source')) {
        update['source'] = req.body['source']
    }
    if (req.body.hasOwnProperty('pipelines')) {
        update['pipelines'] = req.body['pipelines']
    }
    if (isBoolean(req.body['streamOutput'])) {
        update['streamOutput'] = req.body['streamOutput']
    }
    const options = {
        new: true,
        runValidators: true
    }
    models['analyzers'].findByIdAndUpdate(id, update, options, (err, result) => {
        if (err) {
            if (err.name === 'ValidationError') {
                return next(createError(400, null, err))
            }
            if (err.name === 'MongoError' && err.code === 11000) {
                let dupKey = err.errmsg.slice(err.errmsg.lastIndexOf('dup key:') + 14, -3)
                return next(createError(400, `Duplicate key error: ${dupKey}`, err))
            }
            return next(createError(500, null, err))
        }
        if (result === null) {
            return next(createError(404))
        }

        if (update.hasOwnProperty('streamOutput')) {
            update['stream_output'] = update['streamOutput']
            delete update['streamOutput']
        }

        const request = JSON.stringify({
            command: 'UPDATE',
            params: { id, params: update }
        })
        requestBackend(request, (reply, isLastReply, closeResponse) => {
            if (reply['code'] && reply['code'] === NATS.REQ_TIMEOUT) {
                logger.error(`Timeout Error: Request: updating analyzer "${id}"`)
            } else {
                if (reply['error']) {
                    logger.error(reply['error']['message'])
                }
                closeResponse()
            }
        })

        res.status(204).send()
    })
}

function startAnalyzer(req, res, next) {
    const id = req.params['id']
    const updated = {
        enabled: true,
    }
    const options = {
        runValidators: true,
    }
    models['analyzers'].findByIdAndUpdate(id, updated, options, (err, result) => {
        if (err) {
            return next(createError(500, null, err))
        }
        if (result === null) {
            return next(createError(404))
        }

        const request = JSON.stringify({
            command: 'START',
            params: id
        })
        requestBackend(request, (reply, isLastReply, closeResponse) => {
            if (reply['code'] && reply['code'] === NATS.REQ_TIMEOUT) {
                logger.error(`Timeout Error: Request: starting analyzer "${id}"`)
            } else {
                if (reply['error']) {
                    logger.error(reply['error']['message'])
                }
                closeResponse()
            }
        })

        res.status(204).send()
    })
}

function stopAnalyzer(req, res, next) {
    const id = req.params['id']
    const updated = {
        enabled: false,
    }
    const options = {
        runValidators: true,
    }
    models['analyzers'].findByIdAndUpdate(id, updated, options, (err, result) => {
        if (err) {
            return next(createError(500, null, err))
        }
        if (result === null) {
            return next(createError(404))
        }

        const request = JSON.stringify({
            command: 'STOP',
            params: id
        })
        requestBackend(request, (reply, isLastReply, closeResponse) => {
            if (reply['code'] && reply['code'] === NATS.REQ_TIMEOUT) {
                logger.error(`Timeout Error: Request: stopping analyzer "${id}"`)
            } else {
                if (reply['error']) {
                    logger.error(reply['error']['message'])
                }
                closeResponse()
            }
        })

        res.status(204).send()
    })
}

function getMetrics(req, res, next) {
    models['analyzers'].find({}, getConfProjection, (err, list) => {
        if (err) { return next(createError(500, null, err)) }
        if (list.length === 0) {
            // if no analyzers in DB, simply return for heartheat
            register.resetMetrics()
            res.set('Content-Type', register.contentType);
            res.status(200).send(register.metrics())
            return
        }
        const request = JSON.stringify({
            command: 'READ',
            params: list.map(x => x['_id'])
        })
        requestBackend(request, (reply, isLastReply, closeResponse) => {
            if (reply['code'] && reply['code'] === NATS.REQ_TIMEOUT) {
                logger.error('Request timeout.')
            } else {
                if (reply['error']) {
                    logger.error(reply['error'])
                }
                closeResponse()
            }
            register.resetMetrics()
            result = list.map(x => {
                let status = 'unknown'
                if(reply['result'] && reply['result'][x['_id']]) {
                    const received = JSON.parse(reply['result'][x['_id']])
                    status = received.status
                }
                g.labels(x['name']).set(analyzerStatus.indexOf(status))
                return
            })

            res.set('Content-Type', register.contentType);
            res.status(200).send(register.metrics())
            return
        })
    })
}
function stopAnalyzers(force = true) {
    return new P(async (resolve, reject) => {
        // The variable to count current # of stop analyzers.
        let stoppedCount = 0
        // Get info of all analyzers.
        const analyzers = await analModel.find({})
            .catch((err) => reject(err))

        if (analyzers.length === 0) {
            return resolve()
        }

        // Stop each analyzer separately.
        forEach(analyzers, (analyzer) => {
            const id = analyzer._id
            const request = JSON.stringify({
                command: 'STOP',
                params: id,
            });

            // Reuqest analyzer manager to stop the analyzer by ID.
            requestBackend(request, (reply, isLastReply, closeResponse) => {
                let errMsg

                // Error handling.
                if (reply['code'] && reply['code'] === NATS.REQ_TIMEOUT) {
                    errMsg = `Timeout Error: Request: stopping analyzer "${id}"`
                } else {
                    if (reply['error']) {
                        errMsg =
                            `Stop Error: stopping analyzer "${id}": ` +
                            `${JSON.stringify(reply['error'])}`
                    }
                    closeResponse()
                }

                // Handle error message for force or non-force mode.
                if (errMsg) {
                    if (force) {
                        logger.error(errMsg)
                    } else {
                        return reject(errMsg)
                    }
                }

                logger.info(`Analyzer ${id} stopped`)

                // Increase the count of stopped analyzers.
                stoppedCount += 1
                // If all analyzers stop, then we can resolve it.
                if (stoppedCount === analyzers.length) {
                    logger.info('All analyzers stopped')
                    return resolve()
                }
            })
        })
    })
}

function getGPUResource() {
    return new P((resolve, reject) => {
        const request = JSON.stringify({
            command: 'GET_GPU_RESOURCE',
        })
        requestBackend(request, (reply, isLastReply, closeResponse) => {
            if (reply['code'] && reply['code'] === NATS.REQ_TIMEOUT) {
                logger.error('Request timeout.')
                return reject(new Error('Timeout getting GPU resources'))
            } else {
                closeResponse()
            }
            
            if (reply['error']) {
                logger.error(reply['error'])
                return reject(reply['error'])
            }
            
            return resolve(reply['result'])
        })
    })
}

function getAnalyzersSum() {
    return new P((resolve, reject) => {
        // sum pipeline types as an array of { _id: "IntrusionDetection", sum: 1 }
        models["analyzers"].aggregate([
            {
                $unwind: "$pipelines"
            }, 
            {
                $group: {
                    "_id": "$pipelines.type",
                    "sum": { $sum: 1 }
                }
            }
        ]).exec((err, list) => {
            if(err) { return reject(err) }
            resolve(list)
        })
    })
}

function reloadAnalyzers() {
    return new P((resolve, reject) => {
        const request = JSON.stringify({
            command: 'RELOAD_GPU_WORKER',
        })
        requestBackend(request, 30000, (reply, isLastReply, closeResponse) => {
            if (reply['code'] && reply['code'] === NATS.REQ_TIMEOUT) {
                logger.error('Request timeout.')
                return reject(new Error('Timeout reloading analyzers'))
            } else {
                closeResponse()
            }

            if (reply['error']) {
                logger.error(reply['error'])
            }
            return resolve()
        })
    })
}

/*
 * Routing Table
 */
routesWithAuth(
    router,
    ['get', '/analyzers', getAnalyzers],
    ['post', '/analyzers', postReqValidator, createAnalyzer],
    ['delete', '/analyzers', deleteAnalyzers],
)

routesWithAuth(
    router,
    ['get', '/analyzer/:id', getAnalyzer],
    ['patch', '/analyzer/:id', updateAnalyzer],
    ['delete', '/analyzer/:id', deleteAnalyzer],
    ['post', '/analyzer/:id/start', startAnalyzer],
    ['post', '/analyzer/:id/stop', stopAnalyzer],
)

// TODO: internal communication should go through internal network(i.e. bridge)
router.get('/metrics', getMetrics)

module.exports = {
    analyzers: router,
    stopAnalyzers,
    getGPUResource,
    reloadAnalyzers,
    getAnalyzersSum
}
