const express = require('express')
const bodyParser = require('body-parser')
const expressValidator = require('express-validator')
const cors = require('cors')

const config = require('jagereye_ng/config')
const logger = require('./logger');

const { users, createAdminUser } = require('./users')
const status = require('./status')
const { analyzers } = require('./analyzers')
const events = require('./events')
const { externals }= require('./externals')
const pipelines = require('./pipelines')
const { loggingRouter, setupLogger } = require('./logging.js')
const { infoRouter, setupInfo } = require('./info.js')
const helpers = require('./helpers')
const { setupObjectStorage } = require('./objectStorage')
const { 
    settings,
    initNetworkSetting, 
    initNotification,
    initClustering,
} = require('./settings')
const { systems, setupSystemRecords } = require('./systems')
const { license } = require('./license/license')

const app = express()

app.use(cors({ optionsSuccessStatus: 200 /* some legacy browsers (IE11, various SmartTVs) choke on 204 */ }))
app.use(bodyParser.json())
app.use(bodyParser.text())
app.use(bodyParser.urlencoded({ extended: false }))
app.use(expressValidator())

// Initialize API
const API_ENTRY = `/${config.services.api.base_url}`
app.use(API_ENTRY, users)
app.use(API_ENTRY, analyzers)
app.use(API_ENTRY, status)
app.use(API_ENTRY, events)
app.use(API_ENTRY, externals)
app.use(API_ENTRY, pipelines)
app.use(API_ENTRY, loggingRouter)
app.use(API_ENTRY, infoRouter)
app.use(API_ENTRY, helpers)
app.use(API_ENTRY, settings)
app.use(API_ENTRY, systems)
app.use(API_ENTRY, license)

// Logging errors
app.use((err, req, res, next) => {
    logger.error(err)
    next(err)
})

// Catch-all error handling
app.use((err, req, res, next) => {
    const error = {
        code: err.code,
        message: err.message
    }
    res.status(err.status).send({error: error})
})

// Create admin user if it is not existed
createAdminUser()

// Recreate network interfaces according to database record, or create
// default network if no record existed
initNetworkSetting()

// create default notification settings
initNotification()

// create default cluster settings
initClustering()

// Set up logger
setupLogger()

// Set up info
setupInfo()

// Setup object storage
setupObjectStorage()
.then((createdBucket) => {
    if (createdBucket) {
        logger.info(`Create new bucket: ${createdBucket}`)
    }
})
.catch((err) => logger.error(err))

// Setup system records
setupSystemRecords()

module.exports = app
