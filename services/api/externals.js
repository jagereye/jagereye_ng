const express = require('express')
const mime = require('mime-types')
const multer = require('multer')
const { checkSchema } = require('express-validator/check')

const assign = require('lodash/assign')
const filter = require('lodash/filter')
const forEach = require('lodash/forEach')
const includes = require('lodash/includes')
const isString = require('lodash/isString')
const isPlainObject = require('lodash/isPlainObject')
const keys = require('lodash/keys')
const map = require('lodash/map')
const mapValues = require('lodash/mapValues')
const pick = require('lodash/pick')
const replace = require('lodash/replace')
const set = require('lodash/set')
const toUpper = require('lodash/toUpper')
const cloneDeep = require('lodash/cloneDeep')
const startsWith = require('lodash/startsWith')

const models = require('jagereye_ng/database')
const {
    NamedError,
    createError,
    validate,
    genUUID,
    now,
    isValidId,
    isArrayOfId,
    isArrayOfString,
} = require('./utils')
const { routesWithAuth } = require('./auth')
const { saveObject } = require('./objectStorage')
const { ONE_SHOT_STATUS } = require('./constants')
const externalsSchema = require('../../shared/externals.json')

// Expired may contain files that will be saved in the object storage and the
// locations must be saved in the database at the same time. We need a
// all-or-nothing transaction mechanism to create/update/delete externals. We
// set a expiration period for every transaction. If the expiration service sees
// records that exceed the expiration period, then it will treat them as failed
// transactions and delete them (and their related files in the object storage).
// The expiration period is 600 seconds.
const TRANSACTION_EXPIRED_PERIOD = 600

const router = express.Router()

const externalTypes = keys(externalsSchema)
const externalContentKeysMap = mapValues(externalsSchema, (schema) => (
    filter(keys(schema), (key) => toUpper(key) !== key)
))

/*
 * Projections
 */
const getExternalProjection = {
    '_id': 1,
    'task.status': 1,
    'task.reason': 1,
    'type': 1,
    'group': 1,
    'content': 1,
    'createdAt': 1,
    'updatedAt': 1
}

const getGroupProjection = {
    '_id': 1,
    'type': 1,
    'name': 1,
    'notification': 1
}

const externalContentProjections = getExternalContentProjections()

const externalFormDataParser = multer().any()

const createExternalValidator = checkSchema({
    type: {
        custom: {
            options: async (type) => {
                if (!type) {
                    throw new Error('Type is required')
                }
                if (!isString(type)) {
                    throw new Error('Type must be a string')
                }
                if (!includes(externalTypes, type)) {
                    throw new Error(
                        `Type "${type}" is not supported: [${externalTypes}]`,
                    )
                }
            },
        },
    },
    group: {
        custom: {
            options: async (group) => {
                if (group && !isValidId(group)) {
                    throw new Error('Group must be a valid ID')
                }
            },
        },
    },
    content: {
        custom: {
            options: async (content) => {
                if (!content) {
                    throw new Error('Content is required')
                }
                if (!isPlainObject(content)) {
                    throw new Error('Content must be an object')
                }
            },
        },
    },
})

const updateExternalValidator = checkSchema({
    group: {
        custom: {
            options: async (group) => {
                if (group && !isValidId(group)) {
                    throw new Error('Group must be a valid ID')
                }
            },
        },
    },
    content: {
        custom: {
            options: async (content) => {
                if (content && !isPlainObject(content)) {
                    throw new Error('Content must be an object')
                }
            },
        },
    },
})

const queryExternalsValidator = checkSchema({
    _ids: {
        custom: {
            options: async (_ids) => {
                if (_ids && !isArrayOfId(_ids)) {
                    throw new Error('IDs must be an array of object ID')
                }
            },
        },
    },
    taskStatus: {
        custom: {
            options: async (taskStatus) => {
                const isInvalid =
                    taskStatus &&
                    (!isString(taskStatus) && !isArrayOfString(taskStatus))
                if (isInvalid) {
                    throw new Error(
                        'Task status must be a string or an array of string',
                    )
                }
            },
        },
    },
    type: {
        custom: {
            options: async (type) => {
                if (type && !includes(externalTypes, type)) {
                    throw new Error(
                        `Type "${type}" is not supported: [${externalTypes}]`,
                    )
                }
            },
        },
    },
    group: {
        custom: {
            options: async (group) => {
                if (group && (!isValidId(group) && !isArrayOfId(group))) {
                    throw new Error(
                        'Group must be a string or an array of string',
                    )
                }
            },
        },
    },
    sortBy: {
        custom: {
            options: async (sortBy) => {
                if (sortBy && !isPlainObject(sortBy)) {
                    throw new Error('sortBy must be an object')
                }
                const sortTypes = ['asc', 'ascending', '1', 'desc', 'descending', '-1']
                const sortProperties = ['_id', 'group', 'createdAt', 'updatedAt']
                for (const key in sortBy) {
                    if (!includes(sortProperties, key) && !key.startsWith('content.')) {
                        throw new Error(
                            `Key "${key}" is not supported for sort: [${sortProperties}, content.*]`,
                        )
                    }
                    if (!includes(sortTypes, sortBy[key])) {
                        throw new Error(
                            `Sort type "${sortBy[key]}" is not supported: [${sortTypes}] `
                        )
                    }
                }
            }
        }
    },
    limit: {
        custom: {
            options: async (limit) => {
                if (limit && !Number.isInteger(limit)) {
                    throw new Error('limit must be an integer')
                }
                if (limit < 1) {
                    throw new Error('limit must larger than 0')
                }
            }
        }
    },
    page: {
        custom: {
            options: async (page) => {
                if (page && !Number.isInteger(page)) {
                    throw new Error('page must be an integer')
                }
                if (page < 1) {
                    throw new Error('page must larger than 0')
                }
            }
        }
    }
})


const createGroupValidator = checkSchema({
    type: {
        custom: {
            options: async (type) => {
                if (!type) {
                    throw new Error('Type is required')
                }
                if (!isString(type)) {
                    throw new Error('Type must be a string')
                }
                if (!includes(externalTypes, type)) {
                    throw new Error(
                        `Type "${type}" is not supported: [${externalTypes}]`,
                    )
                }
            },
        },
    },
    name: {
        custom: {
            options: async (name) => {
                if (!name) {
                    throw new Error('name is required')
                }
                if (!isString(name)) {
                    throw new Error('name must be a string')
                }
            },
        },
    },
    notification: {
        custom: {
            options: notificationValidator
        }
    },
})

const updateGroupValidator = checkSchema({
    name: {
        custom: {
            options: async (name) => {
                if (name && !isString(name)) {
                    throw new Error('name must be a string')
                }
            },
        },
    },
    notification: {
        custom: {
            options: notificationValidator
        }
    },
})

async function notificationValidator(notification) {
    if (notification && !isPlainObject(notification)) {
        throw new Error('notification must be an object')
    }
    const notificationProperties = ['line']
    for (const key in notification) {
        if (!includes(notificationProperties, key)) {
            throw new Error(
                `Key "${key}" is not supported for notification: [${notificationProperties}]`,
            )
        }
        if (typeof notification[key] !== 'boolean') {
            throw new Error(
                `Value of notification["${key}"] must be a boolean`
            )
        }
    }
}

function getExternalContentProjections() {
    const projections = mapValues(externalsSchema, (schema) => (
        schema.PROJECTIONS
    ))

    return projections
}

function projectExternalContent(external) {
    const projection = externalContentProjections[external.type]

    if (!projection) {
        return external
    } else {
        return assign(external, {
            content: pick(external.content, projection),
        })
    }
}

/**
 * to parse multipart form-data field and return content key:
 *
 * @param { String } externalType external type
 * @param { String } formKey is file.fieldname in multer req.files
 *
 * example: content[image] => image,
 *          content[source][0][images] => source.0.images
 */
function parseContentKey(externalType, formKey) {
    const isValid = /^content(\[[a-zA-Z0-9]*\])+$/g.test(formKey)
    // matches content[image] or content[source][0][images]

    if (!isValid) {
        throw new NamedError(
            `Invalid content formdata key: "${formKey}"`,
            'InvalidContentKeyError',
        )
    }

    let parsed = formKey.substring('content'.length)
    while (includes(parsed, '][')){
        parsed = replace(parsed, '][', '.')
    }
    parsed = replace(parsed, /^\[(.*?)\]$/g, '$1')

    // prevent unnecessary field save in schema
    const target = parsed.split('.')
    const { schema } = models.externals.discriminators[externalType]
    schemaObj = JSON.parse(JSON.stringify(schema.obj))
    let existed = false
    try {
        schemaObj = schemaObj['content'][target[0]]
        for (let i = 1; i < target.length; i++) {
            if (/^[0-9]+$/g.test(target[i])) {
                schemaObj = schemaObj[0]
            } else {
                schemaObj = schemaObj[target[i]]
            }
        }
        if (schemaObj) {
            existed = true
        }
    } catch(err) {

    }
    if (!existed) {
        throw new NamedError(
            `Content key "${parsed}" is not for "${externalType}"`,
            'InvalidContentKeyError',
        )
    }
    return target
}

function genFileKey(externalType, file) {
    const filename = genUUID()
    const ext = mime.extension(file.mimetype)
    if (!ext) {
        throw new NamedError(
            `file format undefined: ${filename}.${file.mimetype}`
        )
    }
    const fileKey = `${externalType}/${filename}.${ext}`
    return fileKey
}

/*
 * @param { String } externalType external type
 * @param { Array } files in multer req.filesa
 *
 * @typedef {object} parsedFile - { contentKey: '', fileKey: '', obj: Buffer }
 * @return { Array.<parsedFile> }
 */
function parseContentFiles(externalType, files) {
    const parsedFiles = map(files, (file) => ({
        contentKey: parseContentKey(externalType, file.fieldname),
        fileKey: genFileKey(externalType, file),
        obj: file.buffer
    }))
    return parsedFiles
}

function validateContentKeys(externalType, content) {
    const externalContentKeys = externalContentKeysMap[externalType]

    for (key in content) {
        if (!includes(externalContentKeys, key)) {
            throw new NamedError(
                `Content key "${key}" is not for "${externalType}"`,
                'InvalidContentKeyError',
            )
        }
    }
}

async function createTemporalExternal(task, type, group, content, reference) {
    return await models.externals.create({
        expired: true,
        expiredAt: now('sec') + TRANSACTION_EXPIRED_PERIOD,
        reference,
        task,
        type,
        group,
        content,
    })
}

function createTaskField(type) {
    const name = externalsSchema[type].ONE_SHOT
    const task = {
        name,
        createdAt: now('sec'),
        status: name ? ONE_SHOT_STATUS.PENDING : ONE_SHOT_STATUS.DONE,
    }

    return task
}

async function createExternal(req, res, next) {
    const { type, group, content } = req.body
    const hasFiles = Boolean(req.files && req.files.length > 0)
    const task = createTaskField(type)
    try {
        // Content keys validation.
        validateContentKeys(type, content)

        if( group ) {
            const grp = await models["externals/groups"].findById(group)
            if (!grp) {
                return next(createError(400, `Group ${group} does not exist.`))
            }
            if (grp.type !== type) {
                return next(createError(400, 'Group type does not match with external type.'))
            }
        }
        if (!hasFiles) {
            /* If no files, just create the database record. */
            const createdAt = updatedAt = now('sec')
            const { _id } = await models.externals.create({
                task,
                type,
                group,
                content,
                createdAt,
                updatedAt
            })
            return res.status(201).send({ _id })
        } else {
            /*
             * If the request contains files, we need a transaction to store
             * both the database and the files.
             */

            // Parse the files in request andCheck field
            const parsedFiles = parseContentFiles(type, req.files)

            for(const parsedFile of parsedFiles) {
                // use lodash.set to update content
                set(content, parsedFile.contentKey, parsedFile.fileKey)
            }

            // database.
            const external = await createTemporalExternal(
                task,
                type,
                group,
                content,
            )

            for(const parsedFile of parsedFiles) {
                // Save new files to the object storage.
                await saveObject(parsedFile.fileKey, parsedFile.obj)
            }

            // Unset the expiration tag and timestamp because the transaction
            // is success.
            external.expired = false
            external.expiredAt = undefined
            external.createdAt = external.updatedAt = now('sec')
            await external.save()
            return res.status(201).send({ _id: external._id })
        }
    } catch (err) {
        if (err.name === 'ValidationError' ||
            err.name === 'InvalidContentKeyError') {
            return next(createError(400, err.message))
        }
        return next(createError(500, null, err))
    }
}

async function queryExternals(req, res, next) {
    const { _ids, taskStatus, type, group, sortBy, limit, page } = req.body

    try {
        const query = {
            expired: false,
        }

        if (_ids) {
            query._id = { $in: _ids }
        }
        if (type) {
            query.type = type
        }
        if (group && isValidId(group)) {
            query.group = group
        } else if (group && isArrayOfId(group)) {
            query.group = { $in: group }
        }
        if (isString(taskStatus)) {
            query['task.status'] = taskStatus
        } else if (isArrayOfString(taskStatus)) {
            query['task.status'] = { $in: taskStatus }
        }

        forEach(req.body, (value, key) => {
            if (startsWith(key, 'content.')) {
                if (typeof value === 'object' && value.hasOwnProperty('partialSearch')) {
                    query[key] = new RegExp(value['partialSearch'], 'i')
                } else {
                    query[key] = value
                }
            }
        })

        let options = {}
        if (limit) {
            options['limit'] = limit
        }

        if (page) {
            // must give page with limit
            if(!limit) {
                return next(createError(400, 'Option "page" must come with "limit"'))
            }

            options['skip'] = ( page - 1 ) * limit
        }

        if (sortBy) {
            options['sort'] = sortBy
        }
        const list = await models.externals.find(query, getExternalProjection, options)
        const total = await models.externals.find(query).count()
        const projectedList = map(list, projectExternalContent)

        return res.status(200).send({total: total, result: projectedList})
    } catch (err) {
        return next(createError(500, null, err))
    }
}

async function getExternal(req, res, next) {
    const { id } = req.params

    if (!isValidId(id)) {
        return next(createError(400, 'Invalid ID'))
    }

    try {
        const result = await models.externals.findOne({
            _id: id,
            expired: false,
        }, getExternalProjection)

        if (!result) {
            return next(createError(404, 'External not existed'))
        }

        res.send(projectExternalContent(result))
    } catch (err) {
        return next(createError(500, null, err))
    }
}

async function updateExternal(req, res, next) {
    // TODO sync with createExternal
    const { id } = req.params
    const { group } = req.body
    const { content } = req.body
    const hasFiles = Boolean(req.files && req.files.length > 0)

    if (!isValidId(id)) {
        return next(createError(400, 'Invalid ID'))
    }

    try {
        const external = await models.externals.findOne({
            _id: id,
            expired: false,
        })

        if (!external) {
            return next(createError(404, 'External not existed'))
        }

        const isUpdatable =
            external.task.status === ONE_SHOT_STATUS.DONE ||
            external.task.status === ONE_SHOT_STATUS.FAILED

        if (!isUpdatable) {
            return next(createError(
                400,
                'External is still in pending or processing',
            ))
        }
        
        if( group ) {
            const grp = await models["externals/groups"].findById(group)
            if (!grp) {
                return next(createError(400, `Group ${group} does not exist.`))
            }
            if (grp.type !== external.type) {
                return next(createError(400, 'Group type does not match with external type.'))
            }
        }
        const {
            task: oldTask,
            type,
            group: oldGroup,
            content: oldContent,
        } = external.toObject()
        const newTask = createTaskField(type)
        const newGroup = group ? group : oldGroup
        let newContent

        if (!hasFiles) {
            if (!content) {
                /*
                 * If no files and no content updated, just update the database
                 * record.
                 */
                newContent = oldContent
            } else {
                /*
                 * If no files but content updated, create a temporal record for
                 * old external.
                 */
                newContent = content

                // Create a temporal record for old external that references to
                // the orignal one.
                await createTemporalExternal(
                    oldTask,
                    type,
                    oldGroup,
                    oldContent,
                    id,
                )
            }
        } else {
            /*
             * If the request contains files, we need a transaction to update
             * both the database and the files.
             */
            newContent = content ? content : {}

            // Parse the files in request and check field
            const parsedFiles = parseContentFiles(type, req.files)

            for(const parsedFile of parsedFiles) {
                // use lodash.set to update content
                set(content, parsedFile.contentKey, parsedFile.fileKey)
            }

            // Create a temporal record for new external that references to the
            // orignal one.
            await createTemporalExternal(
                newTask,
                type,
                newGroup,
                newContent,
                id,
            )

            for(const parsedFile of parsedFiles) {
                // Save new files to the object storage.
                await saveObject(parsedFile.fileKey, parsedFile.obj)
            }

            // Create a temporal record for old external that references to the
            // orignal one.
            await createTemporalExternal(
                oldTask,
                type,
                oldGroup,
                oldContent,
                id,
            )
        }

        // Content keys validation.
        validateContentKeys(type, newContent)

        // Update the new external to the orignal one.
        external.task = newTask
        external.group = newGroup
        external.content = newContent
        external.updatedAt = now('sec')
        await external.save()

        return res.status(204).send()
    } catch (err) {
        if (err.name === 'ValidationError' ||
            err.name === 'InvalidContentKeyError') {
            return next(createError(400, err.message))
        }
        return next(createError(500, null, err))
    }
}

async function deleteExternal(req, res, next) {
    const { id } = req.params

    if (!isValidId(id)) {
        return next(createError(400, 'Invalid ID'))
    }

    try {
        const search = {
            _id: id,
            expired: false,
        }
        const updated = {
            $set: {
                expired: true,
                expiredAt: now('sec'),
            },
        }
        const result = await models.externals.findOneAndUpdate(search, updated)

        if (!result) {
            return next(createError(404, 'External not existed'))
        }

        return res.status(204).send()
    } catch (err) {
        return next(createError(500, null, err))
    }
}

async function createGroup(req, res, next) {
    const { type, name, notification } = req.body

    const { _id } = await models["externals/groups"].create({
        type,
        name,
        notification,
    })
    return res.status(201).send({ _id })
}

async function getGroup(req, res, next) {
    const { id } = req.params

    if (!isValidId(id)) {
        return next(createError(400, 'Invalid ID'))
    }

    try {
        const result = await models["externals/groups"].findById(id, getGroupProjection)

        if (!result) {
            return next(createError(404, 'Group not existed'))
        }

        res.send(result)
    } catch (err) {
        return next(createError(500, null, err))
    }
}

async function getAllGroup(req, res, next) {
    try {
        const result = await models["externals/groups"].find({}, getGroupProjection)
        res.send(result)
    } catch (err) {
        return next(createError(500, null, err))
    }
}

async function deleteGroup(req, res, next) {
    const { id } = req.params

    if (!isValidId(id)) {
        return next(createError(400, 'Invalid ID'))
    }

    try {
        // delete externals belong to the group first
        const search = {
            group: id,
            expired: false,
        }
        const updated = {
            $set: {
                expired: true,
                expiredAt: now('sec'),
            },
        }
        const options = {
            multi: true
        }
        await models.externals.updateMany(search, updated, options)

        // then delete group itself
        const result = await models["externals/groups"].remove({ _id: id })

        if (!result) {
            return next(createError(404, 'Group not existed'))
        }

        return res.status(204).send()
    } catch (err) {
        return next(createError(500, null, err))
    }
}

async function updateGroup(req, res, next) {
    const { id } = req.params
    const { name, notification } = req.body

    if (!isValidId(id)) {
        return next(createError(400, 'Invalid ID'))
    }

    try {
        const update = {
            name,
            notification,
        }
        const result = await models["externals/groups"].updateOne({ _id: id }, update)
        if(result.n < 1) {
            return next(createError(404, 'Group not existed'))
        }
        return res.status(204).send()
    } catch (err) {
        return next(createError(500, null, err))
    }
}

/*
 * Routing Table
 */
routesWithAuth(
    router,
    ['post', '/externals', externalFormDataParser, createExternalValidator, validate, createExternal],
    ['post', '/externals/query', queryExternalsValidator, validate, queryExternals],
    ['get', '/external/:id', getExternal],
    ['patch', '/external/:id', externalFormDataParser, updateExternalValidator, validate, updateExternal],
    ['delete', '/external/:id', deleteExternal],
)

routesWithAuth(
    router,
    ['post', '/externals/groups', createGroupValidator, validate, createGroup],
    ['get', '/externals/groups', getAllGroup],
    ['get', '/externals/groups/:id', getGroup],
    ['delete', '/externals/groups/:id', deleteGroup],
    ['patch', '/externals/groups/:id', updateGroupValidator, validate, updateGroup]
)

//module.exports = router
module.exports = {
    externals: router,
    parseContentKey
}
