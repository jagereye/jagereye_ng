const { validationResult } = require('express-validator/check')
const httpError = require('http-errors')
const uuidv4 = require('uuid/v4')

const every = require('lodash/every')
const isArray = require('lodash/isArray')
const isString = require('lodash/isString')

class NamedError extends Error {
    constructor(message, name) {
        super(message)

        this.name = name

        Error.captureStackTrace(this, this.constructor)
    }
}

function createError(status, message, origErrObj) {
    const error = new Error()

    error.status = status

    if (message) {
        error.message = message
    } else {
        error.message = httpError(status).message
    }

    if (origErrObj) {
        if (origErrObj.kind === 'ObjectId') {
            error.status = 400
            error.message = 'Invalid ObjectId format'
        }
        error.stack = origErrObj.stack
    }

    return error
}

function validate(req, res, next) {
    const errors = validationResult(req)

    if (!errors.isEmpty()) {
        return next(createError(400, errors.array()[0]['msg']))
    }

    next()
}

function genUUID() {
    return uuidv4()
}

function now(unit = 'ms') {
    switch (unit) {
        case 'ms':
            return Date.now()
        case 'sec':
            return Date.now() / 1000
        default:
            throw new Error(`Non-supported unit: ${unit}`)
    }
}

function isValidId(id) {
    // The length of ID must be 24 and the characters must be numbers, "a" to
    // "f", or "A" to "F".
    // Valid examples: "5ae68a54b6e9db31bd1e0a2c", "6acf3cf4e138230eb11a3db9".
    // Invalid examples: "6acf3cf4e138230e", "4be68a54b6e9db31bd1e0aZX".
    return isString(id) && id.match(/^[0-9a-fA-F]{24}$/)
}

function isArrayOfId(x) {
    return isArray(x) && every(x, isValidId)
}

function isArrayOfString(x) {
    return isArray(x) && every(x, isString)
}

module.exports = {
    NamedError,
    createError,
    validate,
    genUUID,
    now,
    isValidId,
    isArrayOfId,
    isArrayOfString,
}
