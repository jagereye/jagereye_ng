const S3 = require('aws-sdk/clients/s3')
const config = require('jagereye_ng/config')

// Get configurations.
const { obj_storage: objectStorageConfig } = config.services
const {
    endpoint_url: endpoint,
    bucket_name: bucketName,
} = objectStorageConfig.params

const {
    access_key: accessKeyId,
    secret_key: secretAccessKey,
} = objectStorageConfig.credentials

const publicReadPolicy = JSON.stringify({
    Version: '2012-10-17',
    Statement: [{
        Sid: '',
        Effect: 'Allow',
        Principal: {
            AWS: ['*']
        },
        Action: ['s3:GetObject'],
        Resource: [`arn:aws:s3:::${bucketName}/*`],
    }],
})

// Connect to the object storage.
const objectStorage = new S3({
    endpoint,
    accessKeyId,
    secretAccessKey,
    s3ForcePathStyle: true,
})

async function setupObjectStorage() {
    try {
        // Check the bucket exists or not.
        await objectStorage.headBucket({
            Bucket: bucketName,
        }).promise()
    } catch (headErr) {
        if (headErr.code !== 'NotFound') {
            throw headErr
        }

        // If the bucket does not exist, create one and make it as public
        // readable.
        await objectStorage.createBucket({
            Bucket: bucketName,
        }).promise()
        // TODO(Su JiaKuan): Need to handle failure on putting bucket policy.
        await objectStorage.putBucketPolicy({
            Bucket: bucketName,
            Policy: publicReadPolicy,
        }).promise()

        return bucketName
    }
}

async function saveObject(key, obj) {
    await objectStorage.putObject({
        Bucket: bucketName,
        Body: obj,
        Key: key,
    }).promise()
}

async function getObject(key) {
    return objectStorage.getObject({
        Bucket: bucketName,
        Key: key
    }).promise()
}

module.exports = {
    setupObjectStorage,
    saveObject,
    getObject
}
