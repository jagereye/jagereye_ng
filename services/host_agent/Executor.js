const shell = require('shelljs')

const { ch_host_api_report: REPORT_TYPE } = require('../../shared/messaging.json')

class Executor {
    constructor(chrootDir) {
        this.chrootDir = chrootDir
    }

    async execute() {}

    runCmdWithChroot(cmd) {
        return shell.exec(`chroot ${this.chrootDir} ${cmd}`)
    }

    report(type, message) {
        process.send({
            type,
            message,
        })
    }

    reportNewStep(message) {
        this.report(REPORT_TYPE.NEW_STEP, message)
    }

    reportFinish(message) {
        this.report(REPORT_TYPE.FINISH, message)
    }

    reportError(message) {
        this.report(REPORT_TYPE.ERROR, message)
    }
}

module.exports = Executor
