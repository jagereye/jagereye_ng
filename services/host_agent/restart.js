const Promise = require('bluebird')

const Executor = require('./Executor')
const { RESTART_TARGET } = require('./constants')

class RestartExecutor extends Executor {
    constructor(chrootDir, target) {
        super(chrootDir)

        this.target = target

        this.execute = this.execute.bind(this)
    }

    async execute() {
        let cmd = ''

        if (this.target === RESTART_TARGET.SERVICE) {
            /*
                Q: Why change original cmd = 'systemctl restart jagereye'?
                A: Since the command NOT working on host OS 'Ubuntu 18.04',
                it always return 'Failed to connect to bus: No data available'


                Here we stop all the jagereye related docker
                container, and our service `jagereye` keep docker services
                running in a period of time. The behavior is acting like 
                RESTART.

                ps:
                Q: Why not use docker restart?
                A: We need our `jagereye` service keep track with log of
                docker-compose, the logs will be stored in syslog.
            */
            cmd = 'bash /usr/local/jagereye/script/stop_jagereye.sh'
        } else if (this.target === RESTART_TARGET.SYSTEM) {
            cmd = 'reboot'
        }

        this.runCmdWithChroot(cmd)
    }
}

const chrootDir = process.argv[2]
const target = process.argv[3]
const restartExecutor = new RestartExecutor(chrootDir, target)

Promise.resolve()
.then(restartExecutor.execute)
.catch((err) => {
    console.error(err)

    restartExecutor.reportError(err.message)
})
