const NATS = require('nats')

function createMsgCli(servers) {
    const msgCli = NATS.connect({
        maxReconnectAttempts: -1,
        reconnectTimeWait: 250,
        servers,
        json: true,
    })

    msgCli.on('error', (err) => {
        console.error(err)
    })
    msgCli.on('connect', (nc) => {
        console.log('connected')
    })
    msgCli.on('disconnect', () => {
        console.log('disconnected')
    })
    msgCli.on('reconnecting', () => {
        console.log('reconnecting')
    })
    msgCli.on('close', () => {
        console.log('connection closed')
    })

    return msgCli
}

module.exports = {
    createMsgCli,
}
