const process = require('process')
const ApiRequestHandler = require('./ApiRequestHandler')
const { channels: CHANNELS, } = require('../../shared/messaging.json')

// Hosts of messaging server.
const MESSAGING_HOST = process.env.MESSAGING_HOST || 'messaging'
const MESSAGING_SERVERS = [`nats://${MESSAGING_HOST}:4222`]

// For prometheus probe
const http = require('http')
const express = require('express')

process.on('unhandledRejection', (reason, promise) => {
    console.error(
        `Unhandle rejection, reason: ${reason}, promise: ${JSON.stringify(promise)}`
    )
}).on('uncaughtException', (err) => {
    console.error(`Uncaught exception: \n${err.stack}`)
    process.exit(1)
})

var app = express()

// The directory for chroot.
const CHROOT_DIR = process.env.CHROOT_DIR || '/host-root'

new ApiRequestHandler(
    MESSAGING_SERVERS,
    CHANNELS.API_TO_HOST,
    CHANNELS.HOST_TO_API,
    CHROOT_DIR,
).start()

app.use(function(req, res, next) {
    // prometheus blackbox test, return 200 if server is running
    res.status(200).send()
});

const httpServer = http.createServer(app)
const port = 5000
httpServer.listen(port)
httpServer.on('error', error => {
    if (error.syscall !== 'listen') {
        throw error;
      }
        
      // handle specific listen errors with friendly messages
      switch (error.code) {
        case 'EACCES':
          console.error('Port ' + port + ' requires elevated privileges');
          process.exit(1);
          break;
        case 'EADDRINUSE':
          console.error('Port ' + port + ' is already in use');
          process.exit(1);
          break;
        default:
          throw error;
      }
})
