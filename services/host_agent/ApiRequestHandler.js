const { fork } = require('child_process')

const merge = require('lodash/merge')

const { createMsgCli } = require('./messaging')
const {
    ch_api_host: COMMAND_TYPE,
    ch_host_api_report: REPORT_TYPE,
} = require('../../shared/messaging.json')

class ApiRequestHandler {
    constructor(msgServers, chApiHost, chHostApi, chrootDir) {
        this.msgCli = createMsgCli(msgServers)
        this.chApiHost = chApiHost
        this.chHostApi = chHostApi
        this.chrootDir = chrootDir

        this.reportToApi = this.reportToApi.bind(this)
        this.handleApiRequest = this.handleApiRequest.bind(this)
    }

    async reportToApi(command, report, replyTo) {
        const fullReport = merge(report, {
            from: command,
        })
        await this.msgCli.publish(replyTo, fullReport)

        console.log(
            `Report to api: ${JSON.stringify(fullReport)} on ${replyTo}`
        )
    }

    async handleApiRequest(request, replyTo) {
        console.log(`Request from api: ${JSON.stringify(request)}`)

        let replied = false

        // Parse the request.
        const { command, params } = request

        try {
            switch (command) {
                // Handle upgrading request.
                case COMMAND_TYPE.UPGRADE:
                    // Parse the request parameters, to get the path to the
                    // upgrade file.
                    const { filePath } = params

                    // Report to API that the host upgrading is about to start.
                    await this.reportToApi(
                        command,
                        { type: REPORT_TYPE.START },
                        replyTo,
                    )

                    replied = true

                    // Create a deploy executor process.
                    // When the deploy has any new message, the new message
                    // will be reported to API.
                    const args = [this.chrootDir, filePath]
                    const task = fork('deploy.js', args)
                    task.on('message', async (report) => {
                        await this.reportToApi(command, report, this.chHostApi)
                    })

                    break
                // Handle restarting request.
                case COMMAND_TYPE.RESTART:
                    // Parse the request parameters, to get the target to
                    // restart.
                    const { target } = params

                    // Report to API that the host upgrading is about to start.
                    await this.reportToApi(
                        command,
                        { type: REPORT_TYPE.START },
                        replyTo,
                    )

                    replied = true

                    // Create a restart executor process.
                    fork('restart.js', [this.chrootDir, target])

                    break
                default:
                    throw new Error(`Non-supported command type: ${command}`)
            }
        } catch (err) {
            console.error(err)

            // Report to API when error occurs.
            const report = {
                type: REPORT_TYPE.ERROR,
                message: err.message,
            }
            this.reportToApi(
                command,
                report,
                !replied ? replyTo : this.chHostApi,
            )
        }
    }

    start() {
        this.msgCli.subscribe(this.chApiHost, this.handleApiRequest)
    }
}

module.exports = ApiRequestHandler
