ARG VERSION
FROM jagereye/nodejs:${VERSION} as builder

LABEL maintainer="feabries@gmail.com"

WORKDIR ${HOME}

# Create services structure
RUN mkdir -p jagereye_ng/services/host_agent && \
    mkdir -p jagereye_ng/shared


# Copy shared files
# [NOTE] Shared files should be copied in to the build context before
#        running docker build
WORKDIR ${HOME}/jagereye_ng/shared
COPY --chown=jager:jager messaging.json .

WORKDIR ${HOME}/jagereye_ng/services/host_agent
# Copy service files
COPY --chown=jager:jager . .
RUN npm install --no-cache && npm install -g pkg && pkg package.json --output host_agent

FROM jagereye/nodejs:${VERSION}

ENV JAGERENV production

# Create services structure
RUN mkdir -p jagereye_ng/services/host_agent && \
    mkdir -p jagereye_ng/shared

# Copy shared files
# [NOTE] Shared files should be copied in to the build context before
#        running docker build
WORKDIR ${HOME}/jagereye_ng/shared
COPY --chown=jager:jager messaging.json .

WORKDIR ${HOME}/jagereye_ng/services/host_agent
COPY --from=builder ${HOME}/jagereye_ng/services/host_agent/host_agent .

USER root

ENTRYPOINT ["/home/jager/jagereye_ng/services/host_agent/host_agent"]
