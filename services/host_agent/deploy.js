const fs = require('fs')
const path = require('path')
const shell = require('shelljs')
const tar = require('tar')
const Promise = require('bluebird')

const endsWith = require('lodash/endsWith')

const Executor = require('./Executor')

function isExecutable(execPath) {
    if (!fs.existsSync(execPath)) {
        return false
    }

    const stats = fs.statSync(execPath)
    const { mode, gid, uid } = stats

    if (!stats.isFile()) {
        return false
    }

    const isGroup = gid ? process.getgid && gid === process.getgid() : true;
    const isUser = uid ? process.getuid && uid === process.getuid() : true;

    return Boolean((mode & 0o0001) ||
        ((mode & 0o0010) && isGroup) ||
        ((mode & 0o0100) && isUser));
}

class DeployExecutor extends Executor {
    constructor(chrootDir, filePath) {
        super(chrootDir)

        this.filePath = filePath

        this.execute = this.execute.bind(this)
    }

    createError(msg, detailed) {
        const message = detailed ? `${msg} (${detailed})` : msg

        return new Error(message)
    }

    runDeployment(execPath, command) {
        const result = this.runCmdWithChroot(`${execPath} ${command}`)

        result.failed = result.code !== 0

        if (result.failed) {
            result.errMsg = result.stderr
        }

        return result
    }

    async execute() {
        // Go to new step: Unpack upgrade file.
        this.reportNewStep('Unpack upgrade file')

        // Current working directory.
        const cwd = path.dirname(this.filePath)
        // Get the root directory of deploy files.
        // Assumptions:
        // 1. The tarball file name ends with ".tar.gz" or ".tgz".
        // 2. After extracting the tarball, its has a root directory that has
        //    the same name of the tarball (without extension). For example,
        //    if the tarball file name is "jagereye-1.0.3.tar.gz", then the
        //    name of its root directory will be "jagereye-1.0.3".
        const fileExtension = endsWith(this.filePath, '.tar.gz') ? '.tar.gz' : '.tgz'
        const rootDirBasename = path.basename(this.filePath, fileExtension)
        const rootDirPath = path.resolve(cwd, rootDirBasename)

        try {
            // Try to unpack the upgrade file.
            await tar.extract({
                cwd,
                file: this.filePath,
                strict: true,
            })
        } catch (err) {
            throw this.createError('Fail to unpack upgrade file', err.message)
        }

        // Go to new step: Check format of upgrade file.
        this.reportNewStep('Check format of upgrade file')

        // We assume the path to the deployment file is "bin/jager-deploy" under
        // the root directory. For exmaple, if the root directory is
        // "jagereye-1.0.3", then the deployment file path is
        // "jagereye-1.0.3/bin/jager-deploy".
        const deployExecPath = path.resolve(rootDirPath, 'bin/jager-deploy')

        // Check whether the deployment executable exists or not.
        if (!isExecutable(deployExecPath)) {
            throw this.createError(
                'Invalid format of upgrade file',
                `Non-executible deploy file: ${deployExecPath}`,
            )
        }

        // Run inspect command of deployment executable to make sure all
        // deployment files are correct.
        const inspectResult = this.runDeployment(deployExecPath, 'inspect')
        if (inspectResult.failed) {
            throw this.createError(
                'Invalid format of upgrade file',
                inspectResult.errMsg
            )
        }

        // Go to new step: Install to system.
        this.reportNewStep('Install to system')

        // Run install command of deployment executable.
        const installResult = this.runDeployment(deployExecPath, 'install')
        if (installResult.failed) {
            throw this.createError(
                'Failed while installation',
                installResult.errMsg
            )
        }

        // Go to new step: Verify installation.
        this.reportNewStep('Verify installation')

        // Run verify command of deployment executable.
        const verifyResult = this.runDeployment(deployExecPath, 'verify')
        if (verifyResult.failed) {
            throw this.createError(
                'Installation verification failed',
                verifyResult.errMsg
            )
        }

        // Finish.
        this.reportFinish()
    }

    deleteFileDir() {
        const fileDir = path.dirname(this.filePath)

        shell.rm('-rf', fileDir)
    }
}

const chrootDir = process.argv[2]
const filePath = process.argv[3]
const deployExecutor = new DeployExecutor(chrootDir, filePath)

Promise.resolve()
.then(deployExecutor.execute)
.catch((err) => {
    console.error(err)

    deployExecutor.reportError(err.message)
})
.finally(() => {
    deployExecutor.deleteFileDir()
})
