#!/bin/bash

/mongodb_exporter -mongodb.uri "mongodb://$MONGO_INITDB_ROOT_USERNAME:$MONGO_INITDB_ROOT_PASSWORD@localhost:27017/" -mongodb.collect.replset=false -mongodb.collect.oplog=false -mongodb.keep-connection=true &
docker-entrypoint.sh mongod --bind_ip_all
