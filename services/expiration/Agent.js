const fs = require('fs');

const chunk = require('lodash/chunk')
const cloneDeep = require('lodash/cloneDeep');
const concat = require('lodash/concat')
const forEach = require('lodash/forEach');
const isArray = require('lodash/isArray');
const isPlainObject = require('lodash/isPlainObject');
const isString = require('lodash/isString');
const map = require('lodash/map');
const startsWith = require('lodash/startsWith');
const toLower = require('lodash/toLower');

const { deleteObjects } = require('./utils');

mongoose.Promise = require('bluebird');

const { Schema } = mongoose;

const OBJS_DELETION_CHUNK_SIZE = 1000
const DB_DELETION_CHUNK_SIZE = 10000

class Agent {
    constructor() {

    }

    findObjectKeys(content) {
        let objKeys = [];

        if (isPlainObject(content) || isArray(content)) {
            forEach(content, (value) => {
                objKeys = concat(objKeys, this.findObjectKeys(value))
            })
        } else if (isString(content)) {
            // To generalize, we assume object key is stored in string type.
            objKeys.push(content);
        }

        return objKeys;
    }

    async deleteDocuments(model, docs, withObjects = true) {
        if (withObjects) {
            const objChunks = chunk(docs, OBJS_DELETION_CHUNK_SIZE)
            for(const objChunk of objChunks) {
                let objKeys = []
                // Collect of the object keys stored in the contents.
                forEach(objChunk, (doc) => {
                    objKeys = concat(objKeys, this.findObjectKeys(doc.content))
                })
                // Delete all objects by keys.
                if (objKeys.length > 0) {
                    await deleteObjects(objKeys);
                    console.log(`Delete ${objKeys.length} objects for ${model.collection.name}: ${objKeys}`);
                }
            }
        }

        const docIds = map(docs, doc => doc._id);
        const docIdsChunks = chunk(docIds, DB_DELETION_CHUNK_SIZE);

        // Delete all documents.
        for (const docIdsChunk of docIdsChunks) {
            await model.deleteMany({
                _id: {
                    $in: docIdsChunk,
                },
            })
            console.log(`Delete ${docIdsChunk.length} documents for ${model.collection.name}: ${docIdsChunk}`)
        }
    }
}

module.exports = Agent;
