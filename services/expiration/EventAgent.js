const map = require('lodash/map');
const config = require('jagereye_ng/config')
const models = require('jagereye_ng/database')
const request = require('request-promise-native');

const Agent = require('./Agent');

const PROMETHEUS_URL = config.services.prometheus.params.endpoint_url + '/' +
                       config.services.prometheus.params.api_version
const PROMETHEUS_PREFIX = PROMETHEUS_URL + '/query?query='

async function getDataDiskUsage() {
    let dataDiskSize = 0, dataDiskFree = 0
    try {
        dataDiskSize = await request(PROMETHEUS_PREFIX + '(node_filesystem_size{ \
            fstype="ext4", mountpoint="/data"}) / 1024 / 1024 / 1024', { json: true })
        if (dataDiskSize['status'] === 'success') {
            dataDiskSize = getPrometheusReturnValue(dataDiskSize)
        }
        dataDiskFree = await request(PROMETHEUS_PREFIX + '(node_filesystem_free{ \
            fstype="ext4", mountpoint="/data"}) / 1024 / 1024 / 1024', { json: true })
        if (dataDiskFree['status'] === 'success') {
            dataDiskFree = getPrometheusReturnValue(dataDiskFree)
        }
    } catch(err) {
        console.error(err)
    }
    return 1 - (dataDiskFree/dataDiskSize)
}

function getPrometheusReturnValue(ret) {
    let result = 0
    ret.data.result.forEach(disk => {
        result += parseFloat(disk.value[1])
    });
    return result
}

class EventAgent extends Agent {
    constructor() {
        super()

        this.eventsModel = models['events']
        this.analyzersModel = models['events']
    }

    // Delete events before a given day(s) ago.
    async deleteBefore(days) {
        // Convert days into seconds.
        const seconds = days * 24 * 60 * 60;
        // Calculate the maximum timestamp (in seconds) that allows events to
        // live.
        const maxTimestamp = Date.now() / 1000 - seconds;

        await this.delete([{
            $match: {
                timestamp: {
                    $lte: maxTimestamp,
                },
            },
        }], `over ${days} day(s) ago`)
    }

    // Delete events if the number of stored records is more than a given
    // maximum number. If exceeds, the oldest records will be deleted first.
    async deleteIfMoreThan(maxNum) {
        const count = await this.eventsModel.count();

        if (count > maxNum) {
            await this.delete([{
                $sort: {
                    timestamp: 1
                },
            }, {
                $limit: count - maxNum
            }], `over ${maxNum} records`)
        }
    }

    // Delete oldest events if the disk usage over threshold.
    async deleteOldest(threshold) {
        let usage = await getDataDiskUsage();
        if(usage > threshold) {
            // if usage is larger than threshold, say over 1%, we will delete the oldest 1% events
            const count = await this.eventsModel.count();
            if (count > 0) {
                await this.delete([{
                    $sort: {
                        timestamp: 1
                    },
                }, {
                    $limit: Math.floor(count * (usage - threshold))
                }], 'over disk usage')
            }
        }
    }

    // Delete events that are expired. We categorize a event as expired in one
    // of following conditions:
    // 1. If the event is denoted as expired, i.e., its "expired" field in
    //    database is "true".
    // 2. If the analyzer the event belong to is deleted, i.e., the value of
    //    its "analyzerId" field does not exist in "analyzers" collection of
    //    database.
    async deleteExpired() {
        const analyzers = await this.analyzersModel.find({})
        const analyzerIds = map(analyzers, (x) => x._id.toString())

        await this.delete([{
            $match: {
                $or: [{
                    expired: true,
                }, {
                    analyzerId: {
                        $nin: analyzerIds,
                    },
                }],
            },
        }], 'expired')
    }

    // Generic function for events deletion.
    async delete(filters, reason) {
        // Find all matched events.
        const events = await this.eventsModel.aggregate(filters);

        if (events.length > 0) {
            console.log(`=== Deleting ${reason} events ===`)
            await this.deleteDocuments(this.eventsModel, events)
            console.log(`=== Done deleting ${reason} events ===`)
        }
    }
}

module.exports = EventAgent;
