const fs = require('fs');

const assign = require('lodash/assign')
const forEach = require('lodash/forEach');
const isEqual = require('lodash/isEqual');
const map = require('lodash/map');
const mapValues = require('lodash/mapValues');
const pick = require('lodash/pick');
const uniq = require('lodash/uniq');

const Agent = require('./Agent');
const models = require('jagereye_ng/database')

function getExternalContentProjections(externalsSchema) {
    const projections = mapValues(externalsSchema, (schema) => (
        schema.PROJECTIONS
    ));

    return projections;
}

function projectExternalContent(external, externalContentProjections) {
    const projection = externalContentProjections[external.type]
    
    if (!projection) {
        return external;
    } else {
        return assign(external, {
            content: pick(external.content, projection),
        });
    }
}

class ReferenceMatcher {
    constructor(externalsModel, externalContentProjections) {
        this.externalsModel = externalsModel
        this.externalContentProjections = externalContentProjections
        this.references = {}
    }

    async setReferences(ids) {
        this.references =
            ids.length > 0 ?
            await this.findReferenceExternals(ids) :
            []
    }

    async findReferenceExternals(referenceIds) {
        const referenceExternalsObj = {}
        const referenceExternals = await this.externalsModel.find({
            _id: {
                $in: referenceIds,
            },
        });

        forEach(referenceExternals, (ext) => {
            referenceExternalsObj[ext._id] = projectExternalContent(
                ext,
                this.externalContentProjections,
            );
        });

        return referenceExternalsObj;
    }

    match(external) {
        const { reference: id } = external
        const projectedExternal = projectExternalContent(
            external,
            this.externalContentProjections,
        )
        return (
            this.references[id] &&
            isEqual(this.references[id].content, projectedExternal.content)
        )
    }
}

class ExternalAgent extends Agent {
    constructor(externalsSchemaPath) {
        super()

        this.externalsModel = models['externals']
        this.externalsSchema = JSON.parse(fs.readFileSync(externalsSchemaPath, 'utf8'));
        this.externalContentProjections = getExternalContentProjections(
            this.externalsSchema,
        );
    }

    // Delete externals that are expired. We categorize an external an expired
    // if its "expired" field is true and the value of its "expiredAt" field is
    // less than current timestamp.
    // An expired external may contain a "reference" field. The field is an
    // object ID that references to another external. If the external content
    // equals to the content of the reference external, then it means we cannot
    // delete objects related to the external because the objects are still used
    // by the reference external. We delete such externals "softly". For other
    // externals that do not match above situations, we delete them "hardly",
    // i.e., delete their records and related objects.
    async deleteExpired() {
        const hardDeletions = [];
        const softDeletions = [];
        
        // Find all expired externals.
        const externals = await this.externalsModel.find({
            expired: true,
            expiredAt: {
                $lt: Date.now() / 1000,
            },
        });
        if (externals.length <= 0) {
            return
        }
        console.log('=== Deleting expired externals ===')
        // Setup reference matcher.
        const referenceMatcher = new ReferenceMatcher(
            this.externalsModel,
            this.externalContentProjections,
        );
        const referenceIds = uniq(map(externals, (ext) => ext.reference));

        await referenceMatcher.setReferences(referenceIds)

        // Categorize externals as soft or hard by using the reference matcher.
        forEach(externals, (external) => {
            if (!external.reference || !referenceMatcher.match(external)) {
                hardDeletions.push(external);
            } else {
                softDeletions.push(external);
            }
        })

        if (hardDeletions.length > 0) {
            // Hard deletion: delete records with objects.
            await this.deleteDocuments(this.externalsModel, hardDeletions);
        }

        if (softDeletions.length > 0) {
            // Soft deletion: delete records without objects.
            await this.deleteDocuments(this.externalsModel, softDeletions, false);
        }
        console.log('=== Done deleting expired externals ===')
    }
}

module.exports = ExternalAgent
