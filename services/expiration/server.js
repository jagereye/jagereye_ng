const process = require('process')
const { CronJob } = require('cron');
const EventAgent = require('./EventAgent');
const ExternalAgent = require('./ExternalAgent');
const config = require('jagereye_ng/config')

// for prometheus probe
const http = require('http')
const express = require('express')

// Path to the database schema.
const DATABASE_SCHEMA_PATH = '../../shared/database.json';
const EXTERNALS_SCHEMA_PATH = '../../shared/externals.json';

process.on('unhandledRejection', (reason, promise) => {
    console.error(
        `Unhandle rejection, reason: ${reason}, promise: ${JSON.stringify(promise)}`
    )
}).on('uncaughtException', (err) => {
    console.error(`Uncaught exception: \n${err.stack}`)
    process.exit(1)
})

var app = express()
var job

try {
    const {
        disk_usage_threshold: diskUsageThreshold,
        repeat_period_mins: repeatPeriodMins,
    } = config.services.expiration.params;

    job = new CronJob(`00 */${repeatPeriodMins} * * * *`, async () => {
        const eventAgent = new EventAgent()
        const externalAgent = new ExternalAgent(EXTERNALS_SCHEMA_PATH)
        try {
            console.log('== Commencing data cleaning ==')
            await eventAgent.deleteExpired();
            await eventAgent.deleteOldest(diskUsageThreshold);

            await externalAgent.deleteExpired();
        } catch (err) {
            console.error(err);
        } finally {
            console.log('== Done data cleaning ==')
        }
    }, null, true);

    console.log(`Start a expiration cron job every ${repeatPeriodMins} minute(s)`)
} catch (e) {
    console.error(e)
}

app.use(function(req, res, next) {
    // prometheus blackbox test, return 200 if cronjob is still running
    if(job.running) {
        res.status(200).send()
    } else {
        console.error('CronJob is not running')
        res.status(500).send()
    }
});

const httpServer = http.createServer(app)
const port = 5000
httpServer.listen(port)
httpServer.on('error', error => {
    if (error.syscall !== 'listen') {
        throw error;
      }
      // handle specific listen errors with friendly messages
      switch (error.code) {
        case 'EACCES':
          console.error('Port ' + port + ' requires elevated privileges');
          process.exit(1);
          break;
        case 'EADDRINUSE':
          console.error('Port ' + port + ' is already in use');
          process.exit(1);
          break;
        default:
          throw error;
      }
})
