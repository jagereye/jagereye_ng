global:
  scrape_interval: 15s

scrape_configs:
  - job_name: 'prometheus'
    scrape_interval: 5s
    static_configs:
      - targets: ['prometheus:9090']
  - job_name: 'node'
    scrape_interval: 5s
    static_configs:
      - targets: ['172.66.66.1:9100']
  - job_name: 'database'
    static_configs:
      - targets: ['database:9001']
  - job_name: 'obj_storage'
    metrics_path: /minio/prometheus/metrics
    static_configs:
      - targets: ['172.66.66.1:9000']
  - job_name: 'messaging'
    static_configs:
      - targets: ['messaging:7777']
  - job_name: 'analyzers'
    metrics_path: /api/v1/metrics
    static_configs:
      - targets: ['172.66.66.1:5000']
  - job_name: 'blackbox'
    metrics_path: /probe
    params:
      module: [http_2xx]  # Look for a HTTP 200 response.
    static_configs:
      - targets: ['expiration:5000', 'host_agent:5000'] # expiration
    relabel_configs:
      - source_labels: [__address__]
        target_label: __param_target
      - source_labels: [__param_target]
        target_label: instance
      - target_label: __address__
        replacement: 172.66.66.70:9115  # The blackbox exporter's real hostname:port.