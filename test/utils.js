const crypto = require('crypto');

const httpRequest = require('request-promise');
const { MongoClient } = require('mongodb');
const isNumber = require('lodash/isNumber');

const config = require('./config');
const licSvrConfig = require('./licSvrConfig');
const { API_HOST, LIC_SVR_HOST, LIC_SVR_ROLES, ROLES } = require('./constants');

const {
    db_name: dbName,
    ports: dbPorts,
} = config.services.database;
const {
    ports: apiPorts,
    base_url: apiBaseUrl,
} = config.services.api;
const {
    ports: objPorts,
    params: {
        bucket_name: objBaseUrl,
    },
} = config.services.obj_storage;
const {
    database: licSvrDb,
} = licSvrConfig;
const {
    port: licSvrCliPort,
    base_url: licSvrBaseUrl,
} = licSvrConfig;

const dbCliPort = parseClientPort(dbPorts);
const apiCliPort = parseClientPort(apiPorts);
const objCliPort = parseClientPort(objPorts);

const DB_URL = `mongodb://${API_HOST}:${dbCliPort}`;
const API_URI_PREFIX = `http://${API_HOST}:${apiCliPort}/${apiBaseUrl}`;
const OBJ_URI_PREFIX = `http://${API_HOST}:${objCliPort}/${objBaseUrl}`;
const LIC_SVR_DB_URL = `mongodb://${licSvrDb.ip}:${licSvrDb.port}`
const LIC_SVR_URI_PREFIX =
    `http://${LIC_SVR_HOST}:${licSvrCliPort}/${licSvrBaseUrl}`;

const dbOptions = {
    auth: {
        user: config.services.database.env.username,
        password: config.services.database.env.password,
    },
};

const licSvrDbOptions = {
    auth: {
        user: licSvrDb.username,
        password: licSvrDb.password,
    },
};

function parseClientPort(ports) {
    return ports.client.split(':')[0];
}

async function removeCollection(db, name) {
    const coll = db.collection(name);

    await coll.deleteMany({});
}

async function resetDatabse(
    url = DB_URL,
    name = dbName,
    options = dbOptions,
    admin = {
        username: config.services.api.admin.username,
        password: config.services.api.admin.default_password,
        role: ROLES.ADMIN,
    },
) {
    // Connect the database.
    const conn = await MongoClient.connect(url, options);
    const db = conn.db(name);

    // Remove collections of database.
    const colls = await db.listCollections().toArray();
    for (const coll of colls) {
        await removeCollection(db, coll.name);
    }

    // Create the admin user.
    const userColl = db.collection('users');

    // Create the admin user.
    const salt = crypto.randomBytes(128).toString('hex');
    const hashedPassword =
        crypto.pbkdf2Sync(admin.password, salt, 10000, 512, 'sha512').toString('hex');

    userColl.insertOne({
        username: admin.username,
        salt,
        hashedPassword,
        role: admin.role,
    });

    // Close the database connection.
    await conn.close();
}

async function resetLicSvr() {
    const admin = {
        username: licSvrConfig.admin.username,
        password: licSvrConfig.admin.default_password,
        role: LIC_SVR_ROLES.ADMIN,
    };

    await resetDatabse(
        LIC_SVR_DB_URL,
        licSvrDb.name,
        licSvrDbOptions,
        admin,
    );
}

async function request({
    url,
    method,
    body,
    formData,
    token,
    contentType = 'application/json',
    json = true,
}, uriPrefix = API_URI_PREFIX) {
    try {
        const headers = {
            'Content-Type': contentType,
        };

        if (token) {
            headers.Authorization = `Bearer ${token}`;
        }

        const response = await httpRequest({
            method,
            uri: `${uriPrefix}/${url}`,
            headers,
            body,
            formData,
            json,
            resolveWithFullResponse: true,
        });

        return response;
    } catch (err) {
        return err;
    }
}

async function requestLicSvr(options) {
    return await request(options, LIC_SVR_URI_PREFIX)
}

async function getObj(url) {
    return await httpRequest(`${OBJ_URI_PREFIX}/${url}`);
}

async function getAuthorization(username, password) {
    const result = await request({
        url: 'login',
        method: 'POST',
        body: {
            username,
            password,
        },
    });

    return result.body.token;
}

module.exports = {
    getAuthorization,
    getObj,
    resetDatabse,
    resetLicSvr,
    request,
    requestLicSvr,
}
