# JagerEye Integration Testing

The directory contains integration testing for JagerEye. In the testing, we regard JagerEye service as a blackbox and interact it with public API calls.

## Prerequisites

* The whole JagerEye service is running on localhost (including analyzer manager)

* The whole [license server](https://gitlab.com/jagereye/license-server) is running on localhost

* Clone the source code of [license server](https://gitlab.com/jagereye/license-server) in the directory (You may be prompt with gitlab username/password)

```bash
git clone https://gitlab.com/jagereye/license-server
```

* Make sure you have a network interface whose name is `jager1` on local

```bash
# Run following command if you do not have the network interface, the command will create a dummy one.
sudo ip link add jager1 type dummy
```

* Node.js (>=8.x) is installed

* Install dependecies through npm

```bash
npm install
```

## Usage

Run the tesing:

```bash
npm test
```
