const fs = require('fs');
const yaml = require('js-yaml');
const { LIC_SVR_FOLDER } = require('./constants');

// Path to the config file.
const CONFIG_PATH = `${LIC_SVR_FOLDER}/config.yml`;
// The read configuration.
const config = yaml.safeLoad(fs.readFileSync(CONFIG_PATH))

module.exports = config
