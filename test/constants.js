const API_HOST = 'localhost';

const LIC_SVR_FOLDER = 'license-server';

const LIC_SVR_HOST = 'localhost';

const LIC_SVR_ROLES = {
    ADMIN: 'admin',
    USER: 'user',
};

const PIPELINES = {
    INTRUSION_DETECTION: 'IntrusionDetection',
    SINGLE_CAMERA_LICENSE_PLATE_RECOGNITION: 'SingleCameraLicensePlateRecognition',
    CROSS_CAMERA_LICENSE_PLATE_RECOGNITION: 'CrossCameraLicensePlateRecognition',
    SINGLE_CAMERA_FACE_RECOGNITION: 'SingleCameraFaceRecognition',
    CROSS_CAMERA_FACE_RECOGNITION: 'CrossCameraFaceRecognition',
    CAR_COUNTING: 'CarCounting',
    DIRECTIONAL_CAR_COUNTING: 'DirectionalCarCounting',
};

const ROLES = {
    ADMIN: 'admin',
    WRITER: 'writer',
    READER: 'reader',
}

module.exports = {
    API_HOST,
    LIC_SVR_FOLDER,
    LIC_SVR_HOST,
    LIC_SVR_ROLES,
    PIPELINES,
    ROLES,
}
