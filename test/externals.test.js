const fs = require('fs')
const path = require('path')
const mime = require('mime-types')

const HttpStatus = require('http-status-codes')
const ObjectId = require('mongodb').ObjectID

const config = require('./config');
const {
    getAuthorization,
    getObj,
    resetDatabse,
    request,
} = require('./utils')

async function getExternal(id, token) {
    const options = {
        url: `external/${id}`,
        method: 'GET',
        token,
    }
    const result = await request(options)

    return result
}

describe('External Operations: ', () => {
    const {
        username: adminUsername,
        default_password: adminPassword,
    } = config.services.api.admin;

    let adminToken;

    beforeAll(async () => {
        await resetDatabse()

        adminToken = await getAuthorization(adminUsername, adminPassword)
    })
    afterAll(() => {
    })
    describe('face recognition', () => {
        let externalID = ''

        const type = 'cross_camera_face_recognition.face'
        const group = 'test face group'
        const facename = 'test face name'
        const filepath = './src/face'
        const files = fs.readdirSync(filepath)
        const file = files[0]
        /* face data in external db
            {
                "_id" : ObjectId("5c04b776ee98dd60e23d40ea"),
                "group" : "test face",
                "type" : "cross_camera_face_recognition.face",
                "task" : {
                    "createdAt" : 1543812982.089,
                    "status" : "pending",
                    "name" : "FaceFeaturization"
                },
                "expired" : false,
                "content" : {
                    "image" : "cross_camera_face_recognition.face/6e820b2e-1c3b-4336-a358-2cc38074ba81.jpeg",
                    "name" : "test facename"
                    "feature": []
                }
            }
        */
        test(`create ${type}`, async () => {
            const options = {
                url: 'externals',
                method: 'POST',
                formData: {
                    type,
                    group,
                    'content[name]': facename,
                    'content[image]': {
                        value: fs.createReadStream(`${filepath}/${file}`),
                        options: {
                            filename: file,
                            contentType:
                                mime.lookup(path.extname(`${filepath}/${file}`))
                        }
                    }
                },
                token: adminToken,
            }
            let result = await request(options)
            // check response
            expect(result.statusCode).toBe(HttpStatus.CREATED)
            expect(result).toHaveProperty('body')
            expect(result.body).toHaveProperty('_id')
            externalID = result.body._id

            // check data
            result = await getExternal(externalID, adminToken)
            expect(result).toHaveProperty('body')
            const external = result.body
            expect(external).toHaveProperty('group')
            expect(external.group).toBe(group)
            expect(external).toHaveProperty('type')
            expect(external.type).toBe(type)
            expect(external).toHaveProperty('task')
            expect(external.task).toHaveProperty('status')
            expect(external).toHaveProperty('task')
            expect(external).toHaveProperty('content')
            expect(external.content).toHaveProperty('image')
            expect(external.content).toHaveProperty('name')

            // check obj store
            r = await getObj(external.content.image)
            expect(r.length).toBeGreaterThan(0)
        })
        test.skip(`update ${type}`, async () => {
            // TODO
        })
        test(`delete ${type}`, async () => {
            /* TODO remove data for test, currently delete testing data by hand
             * or expiration service
             */
            const options = {
                url: `external/${externalID}`,
                method: 'DELETE',
                token: adminToken,
            }
            let result = await request(options)
            // check response
            expect(result.statusCode).toBe(HttpStatus.NO_CONTENT)
            // check data
            result = await getExternal(externalID, adminToken)
            expect(result.statusCode).toBe(HttpStatus.NOT_FOUND)
        })
    })

    // ----------------------------------------------

    describe('calibration', () => {
        let externalID = ''

        const type = 'stitching.calibration'
        const group = 'test calibration group'
        const filepath = './src/calibration'
        const files = fs.readdirSync(filepath)
        /* calibration data in external db
            {
                "_id" : ObjectId("5c4596a5f76cd3203f44e831"),
                "group" : "test calibration group",
                "type" : "stitching.calibration",
                "task" : {
                    "createdAt" : 1548064420.886,
                    "status" : "pending",
                    "name" : "StitchingCalibration"
                },
                "expired" : false,
                "content" : {
                    "point_extraction" : {
                        "y_num_squares" : 24,
                        "x_num_squares" : 14,
                        "chess_square_side_length_in_px" : 240,
                        "chess_square_side_length_in_mm" : 40,
                        "aruco_marker_side_length_in_px" : 160,
                        "aruco_marker_side_length_in_mm" : 2.666666666666,
                        "aruco_marker_dictionary_id" : 10,
                        "aruco_marker_border_bits" : 1,
                        "method" : "charuco_rig",
                        "type" : "pattern_rig_point_finder"
                    },
                    "source" : [
                        {
                            "name" : "subcam 0",
                            "url" : "rtsp://10.10.0.87/stream1",
                            "width" : 1920,
                            "height" : 1080,
                            "images" : [
                                "stitching.calibration/8e900d36-da9d-4109-beac-1b9d5762addc.png"
                            ],
                            "intrinsic_model" : "pinhole_brown",
                            "parameter_prior" : {}
                        },
                        {
                            "name" : "subcam 1",
                            "url" : "rtsp://10.10.0.88/stream1",
                            "width" : 1920,
                            "height" : 1080,
                            "images" : [
                                "stitching.calibration/85b33164-6026-4276-85b5-41097395dbd0.png"
                            ],
                            "intrinsic_model" : "pinhole_brown",
                            "parameter_prior" : {}
                        }
                    ],
                    "name" : "testa"
                },
                "__v" : 1
            }
        */

        test(`create ${type}`, async () => {
            const source = JSON.stringify([
                            {
                                "name" : "subcam 0",
                                "url" : "rtsp://10.10.0.87/stream1",
                                "width" : 1920,
                                "height" : 1080,
                                "images" : [],
                                "intrinsic_model" : "pinhole_brown",
                                "parameter_prior" : {}
                            }, {
                                "name" : "subcam 1",
                                "url" : "rtsp://10.10.0.88/stream1",
                                "width" : 1920,
                                "height" : 1080,
                                "images" : [],
                                "intrinsic_model" : "pinhole_brown",
                                "parameter_prior" : {}
                            }])
            const point_extraction = JSON.stringify({
                                    "type" : "pattern_rig_point_finder",
                                    "method" : "charuco_rig",
                                    "aruco_marker_border_bits" : 1,
                                    "aruco_marker_dictionary_id" : 10,
                                    "aruco_marker_side_length_in_mm" : 2.666666666666,
                                    "aruco_marker_side_length_in_px" : 160,
                                    "chess_square_side_length_in_mm" : 40,
                                    "chess_square_side_length_in_px" : 240,
                                    "x_num_squares" : 14,
                                    "y_num_squares" : 24
                                })
            const optimization_config = JSON.stringify({})

            const options = {
                url: 'externals',
                method: 'POST',
                formData: {
                    type,
                    group,
                    'content[name]': "testa",
                    'content[source]': source,
                    'content[source][0][images][0]': {
                        value: fs.createReadStream(`${filepath}/${files[0]}`),
                        options: {
                            filename: files[0],
                            contentType:
                                mime.lookup(path.extname(`${filepath}/${files[0]}`))
                        }
                    },
                    'content[source][1][images][0]': {
                        value: fs.createReadStream(`${filepath}/${files[1]}`),
                        options: {
                            filename: files[1],
                            contentType:
                                mime.lookup(path.extname(`${filepath}/${files[1]}`))
                        }
                    },
                    'content[point_extraction]': point_extraction,
                    'content[optimization_config]': optimization_config
                },
                token: adminToken,
            }
            let result = await request(options)
            // check response
            expect(result.statusCode).toBe(HttpStatus.CREATED)
            expect(result).toHaveProperty('body')
            expect(result.body).toHaveProperty('_id')
            externalID = result.body._id
            // check data
            result = await getExternal(externalID, adminToken)
            expect(result).toHaveProperty('body')
            const external = result.body
            expect(external).toHaveProperty('group')
            expect(external.group).toBe(group)
            expect(external).toHaveProperty('type')
            expect(external.type).toBe(type)
            expect(external).toHaveProperty('task')
            expect(external).toHaveProperty('content')
            expect(external.content).toHaveProperty('source')
            expect(external.content.source.length).toEqual(2)
            expect(external.content).toHaveProperty('name')
            // check obj store
            for (let i = 0; i < external.content.source.length; i++) {
                for (let j = 0; j < external.content.source[i].images.length; j++) {
                    r = await getObj(external.content.source[i].images[j])
                    expect(r.length).toBeGreaterThan(0)
                }
            }
        })
        test.skip(`update ${type}`, async () => {
            // TODO
        })
        test(`delete ${type}`, async () => {
            /* TODO remove data for test, currently delete testing data by hand
             * or expiration service
             */
            const options = {
                url: `external/${externalID}`,
                method: 'DELETE',
                token: adminToken,
            }
            let result = await request(options)
            // check response
            expect(result.statusCode).toBe(HttpStatus.NO_CONTENT)
            // check data
            result = await getExternal(externalID, adminToken)
            expect(result.statusCode).toBe(HttpStatus.NOT_FOUND)
        })
    })
})

