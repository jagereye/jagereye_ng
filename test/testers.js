const toBeType = require('jest-tobetype');
const HttpStatus = require('http-status-codes');

expect.extend(toBeType);

function testCreateAnalyzerResult(result) {
    // Test the status code, must be 201 CREATED.
    expect(result.statusCode).toBe(HttpStatus.CREATED);
}

function testCreateAnalyzerQuotaExceeded(result) {
    // Test the status code, must be 403 FORBIDDEN.
    expect(result.statusCode).toBe(HttpStatus.FORBIDDEN);
    // Test the error message.
    expect(result.error.error.message).toBe(
        'Quota exceeded.',
    );
}

function testDeleteAnalyzerResult(result) {
    // Test the status code, must be 204 NO_CONTENT.
    expect(result.statusCode).toBe(HttpStatus.NO_CONTENT);
}

function testUploadPublicKeyResult(result) {
    // Test the status code, must be 204 NO_CONTENT.
    expect(result.statusCode).toBe(HttpStatus.NO_CONTENT);
}

function testGenerateLicenseRequestResult(result) {
    // Test the status code, must be 200 OK.
    expect(result.statusCode).toBe(HttpStatus.OK);
}

function testSendLicenseRequestResult(result) {
    // Test the status code, must be 200 OK.
    expect(result.statusCode).toBe(HttpStatus.OK);

    // Test the return body, must have "license".
    expect(result).toHaveProperty('body');
    expect(result.body).toHaveProperty('license');

    // Test the type of "license".
    expect(typeof(result.body.license)).toBe('string');
}

function testUploadLicenseResult(result) {
    // Test the status code, must be 201 CREATED.
    expect(result.statusCode).toBe(HttpStatus.CREATED);
}

function testLicenseInfo(licenseInfo, expectedLicenseInfo) {
    // Test the type and value of "_id" field.
    expect(licenseInfo).toHaveProperty('_id');
    expect(licenseInfo._id).toBe(expectedLicenseInfo._id);

    // Test the type and value of "vendor" field;
    expect(licenseInfo).toHaveProperty('vendor');
    expect(licenseInfo.vendor).toBe(expectedLicenseInfo.vendor);

    // Test the type of "date" field.
    expect(licenseInfo).toHaveProperty('date');
    expect(licenseInfo.date).toBeType('string');

    // Test the value of "application" field.
    expect(licenseInfo).toHaveProperty('application');
    expect(licenseInfo.application).toEqual(expectedLicenseInfo.application);
}

function testGetLicenseResult(result, expectedLicenseInfo) {
    // Test the status code, must be 200 OK.
    expect(result.statusCode).toBe(HttpStatus.OK);

    expect(result).toHaveProperty('body');

    // Test the license information.
    testLicenseInfo(result.body, expectedLicenseInfo);
}

function testGetLicensesResult(result, expectedLicenseInfoList) {
    // Test the status code, must be 200 OK.
    expect(result.statusCode).toBe(HttpStatus.OK);

    // Test the type of return body, must be an array.
    expect(result).toHaveProperty('body');
    expect(result.body).toBeType('array');

    // Test the length of return body.
    expect(result.body).toHaveLength(expectedLicenseInfoList.length);

    // Test each license instance of return body.
    for (let idx = 0; idx < result.body.length; idx++) {
        const licenseInfo = result.body[idx];
        const expectedLicenseInfo = expectedLicenseInfoList[idx];

        testLicenseInfo(licenseInfo, expectedLicenseInfo);
    }
}

function testDeleteLicenseResult(result) {
    // Test the status code, must be 204 NO_CONTENT.
    expect(result.statusCode).toBe(HttpStatus.NO_CONTENT);
}

module.exports = {
    /* Testers about analyzers. */
    testCreateAnalyzerResult,
    testCreateAnalyzerQuotaExceeded,
    testDeleteAnalyzerResult,
    /* Testers about licensing. */
    testGenerateLicenseRequestResult,
    testUploadPublicKeyResult,
    testSendLicenseRequestResult,
    testUploadLicenseResult,
    testGetLicenseResult,
    testGetLicensesResult,
    testDeleteLicenseResult,
}
