const HttpStatus = require('http-status-codes');
const WebSocket = require('ws');

const video = require('./video/app.js');

const videoAppPort = 8081;

const config = require('./config');
const {
    getAuthorization,
    resetDatabse,
    request,
} = require('./utils')

describe.skip('Analyzer Integration Test: ', () => {
    describe.skip('green path(create => start => get status => delete): ', () => {
        // skip because roi format is not correct
        let analyzerId = null;
        let videoApp = null;
        const testStartTime = (new Date().getTime() / 1000)

        const analyzerInfo = {
            "name": "Front Gate 1",
            "source": {
                "mode": "streaming",
                "url": "http://localhost:"+videoAppPort+"/video.mp4"
            },
            "pipelines": [
                {
                    "type": "IntrusionDetection",
                    "params": {
                        "roi": [
                            {"x": 200.1,"y": 100.01},
                            {"x": 400.1,"y": 100.01},
                            {"x": 400.1, "y": 600.01},
                            {"x": 200.1, "y": 600.01}
                        ],
                        "triggers": ["person"]
                    }
                }
            ]
        };

        beforeAll(async () => {
            /*
            videoApp = new video.VideoApp(videoAppPort);
            videoApp.start();

            const mongoConn = await MongoClient.connect('mongodb://database:27017');
            const mongoDB = mongoConn.db('jager_test');
            const analColl = mongoDB.collection('analyzers');
            const eventColl = mongoDB.collection('events');
            await analColl.remove({});
            await eventColl.remove({});
            mongoConn.close();
            */
            return;
        });

        afterAll(() => {
            /*
            videoApp.stop();
            */
        });

        test('create an analyzer', async (done) => {
            let postData = analyzerInfo;
            let options =  {
                method: 'POST',
                uri: 'http://localhost:5000/api/v1/analyzers',
                body: postData,
                json: true,
                resolveWithFullResponse: true
            };
            const result = await request(options);
            expect(result.statusCode).toBe(HttpStatus.CREATED);
            expect(result).toHaveProperty('body');
            expect(result.body).toHaveProperty('_id');
            analyzerId = result.body._id;
            done();
        });
        // ----- test('create analyzer')

        test('start the analyzer', async (done) => {
            let options =  {
                method: 'POST',
                uri: 'http://localhost:5000/api/v1/analyzer/' + analyzerId + '/start',
                json: true,
                resolveWithFullResponse: true
            };
            const result = await request(options);

            expect(result.statusCode).toBe(HttpStatus.NO_CONTENT);
            done();
        });
        // ----- test('start the analyzer')

        test('get status of the analyzer', async (done) => {
            let options =  {
                method: 'GET',
                uri: 'http://localhost:5000/api/v1/analyzer/' + analyzerId,
                json: true,
                resolveWithFullResponse: true
            };
            const result = await request(options);
            expect(result.statusCode).toBe(HttpStatus.OK);
            expect(result).toHaveProperty('body');
            expect(result.body).toHaveProperty('_id');
            expect(result.body._id).toBe(analyzerId);
            expect(result.body).toHaveProperty('name');
            expect(result.body.name).toBe(analyzerInfo.name);
            expect(result.body).toHaveProperty('source');
            expect(result.body.source).toEqual(analyzerInfo.source);
            expect(result.body).toHaveProperty('pipelines');
            expect(result.body.pipelines).toEqual(analyzerInfo.pipelines);
            expect(result.body).toHaveProperty('status');
            expect(result.body.status).toBe('running');

            done();
        });
        // ----- test('get status of the analyzer')

        test('wait for notification', async (done) => {
            const ws = new WebSocket('ws://localhost:5000/notification');

            ws.on('message', function incoming(data) {
                data = data.replace(/'/g, '"');
                notifiedInfo = JSON.parse(data);

                expect(notifiedInfo).toHaveProperty('category');
                expect(notifiedInfo.category).toBe('Analyzer');
                expect(notifiedInfo).toHaveProperty('message');
                const msg = notifiedInfo.message;
                expect(msg).toHaveProperty('date');
                expect(msg).toHaveProperty('content');
                expect(msg).toHaveProperty('type');
                expect(msg.type).toBe('intrusion_detection.alert');
                expect(msg).toHaveProperty('analyzerId');
                expect(msg.analyzerId).toBe(analyzerId);
                expect(msg).toHaveProperty('timestamp');
                expect(typeof(msg.timestamp)).toBe('number');

                const content = msg.content;
                expect(content).toHaveProperty('video');
                expect(content).toHaveProperty('metadata');
                expect(content).toHaveProperty('thumbnail');
                expect(content).toHaveProperty('triggered');
                ws.close();
                done();
            });

        });
        // ----- test('wait for events')

        test('query events', async (done) => {
            let now = (new Date().getTime() / 1000)
            let postData = {
                timestamp: {
                    start: testStartTime,
                    end: now
                },
                analyzers: [analyzerId]
            };

            let options =  {
                method: 'POST',
                uri: 'http://localhost:5000/api/v1/events',
                body: postData,
                json: true,
                resolveWithFullResponse: true
            };
            const result = await request(options);
            expect(result.statusCode).toBe(HttpStatus.OK);
            expect(result).toHaveProperty('body');

            const eventInfo = result.body[0];
            expect(eventInfo).toHaveProperty('timestamp');
            expect(eventInfo).toHaveProperty('date');
            expect(eventInfo).toHaveProperty('_id');
            expect(typeof(eventInfo.timestamp)).toBe('number');
            expect(eventInfo).toHaveProperty('analyzerId');
            expect(eventInfo.analyzerId).toBe(analyzerId);
            expect(eventInfo).toHaveProperty('type');
            expect(eventInfo.type).toBe('intrusion_detection.alert');
            expect(eventInfo).toHaveProperty('content');
            const content = eventInfo.content;
            expect(content).toHaveProperty('video');
            expect(content).toHaveProperty('metadata');
            expect(content).toHaveProperty('thumbnail');
            expect(content).toHaveProperty('triggered');

            done();
        });
        // ----- test('query events')

        test('delete the analyzer', async (done) => {
            let options =  {
                method: 'DELETE',
                uri: 'http://localhost:5000/api/v1/analyzer/' + analyzerId,
                json: true,
                resolveWithFullResponse: true
            };
            const result = await request(options);
            expect(result.statusCode).toBe(HttpStatus.NO_CONTENT);
            done();
        });
        // ----- test('delete the analyzer')
    });
});
describe('Analyzer CRUD Operations: ', () => {
    const {
        username: adminUsername,
        default_password: adminPassword,
    } = config.services.api.admin;

    let adminToken
    let mongoDB, analyzerCollection

    beforeAll(async () => {
        await resetDatabse()

        adminToken = await getAuthorization(adminUsername, adminPassword);
    })
    afterAll(() => {
    })
    describe('intrusion detection', () => {
        let externalID = ''
        test('create', async () => {
            const options = {
                url: 'analyzers',
                method: 'POST',
                body: {
                    "name": "Front Gate 1",
                    "source": {
                        "url": "rtsp://10.10.0.86",
                        "mode": "streaming"
                    },
                    "pipelines": [
                        {
                            "type": "IntrusionDetection",
                            "params": {
                                "roi": [
                                    {"x": 0, "y": 0},
                                    { "x": 1, "y": 0},
                                    { "x": 0.5, "y": 0.5}
                                ],
                                "triggers": ["person"]
                            }
                        }
                    ]
                },
                token: adminToken,
            }
            const result = await request(options)
            // check response
            expect(result.statusCode).toBe(HttpStatus.CREATED)
            expect(result).toHaveProperty('body')
            expect(result.body).toHaveProperty('_id')
        })
    })
    describe('car counting', () => {
        let externalID = ''
        test('create', async () => {
            const options = {
                url: 'analyzers',
                method: 'POST',
                body: {
                    "name": "Freeway",
                    "source": {
                        "url": "rtsp://10.10.0.85",
                        "mode": "streaming"
                    },
                    "pipelines": [
                        {
                            "type": "CarCounting",
                            "params": {
                                "lines": [
                                    {
                                        "x1": 0.27421875, "y1": 0.584259259259259,
                                        "x2": 0.329166666666667, "y2": 0.587962962962963
                                    },
                                    {
                                        "x1": 0.330208333333333, "y1": 0.589814814814815,
                                        "x2": 0.38984375, "y2": 0.597685185185185
                                    },
                                    {
                                        "x1": 0.391927083333333, "y1": 0.595833333333333,
                                        "x2": 0.452604166666667, "y2": 0.603703703703704
                                    }
                                ],
                                "triggers": ["car","truck","bus","motorcycle"]
                            }
                        }
                    ]
                },
                token: adminToken,
            }
            const result = await request(options)
            // check response
            expect(result.statusCode).toBe(HttpStatus.CREATED)
            expect(result).toHaveProperty('body')
            expect(result.body).toHaveProperty('_id')
        })
    })
    describe('stitching', () => {
        let externalID = ''
        test('create', async () => {
            const options = {
                url: 'analyzers',
                method: 'POST',
                body:{
                    "name" : "Stitching",
                    "enabled" : false,
                    "source" : {
                        "urls" : [
                            "rtsp://10.10.0.87/stream1",
                            "rtsp://10.10.0.88/stream1"
                        ],
                        "mode" : "multistreaming"
                    },
                    "pipelines" : [
                        {
                            "type" : "Stitching",
                            "params" : {
                                "crop_enable" : true,
                                "first_cam_angle" : 0.0,
                                "panorama_width" : 3840,
                                "panorama_height" : 2160,
                                "panorama_fov" : 180.0,
                                "projection_surface" : "cylindrical",
                                "blending_method" : "alpha_blending",
                                "blend_slope" : 4,
                                "calibration_id" : ""
                            }
                        }
                    ]
                },
                token: adminToken,
            }
            const result = await request(options)
            // check response
            expect(result.statusCode).toBe(HttpStatus.CREATED)
            expect(result).toHaveProperty('body')
            expect(result.body).toHaveProperty('_id')
        })
    })
})
