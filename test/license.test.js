const HttpStatus = require('http-status-codes');

const apiConfig = require('./config');
const licSvrConfig = require('./licSvrConfig');
const {
    getAuthorization,
    resetDatabse,
    resetLicSvr,
    request,
    requestLicSvr,
} = require('./utils');
const { LIC_SVR_ROLES, PIPELINES, ROLES } = require('./constants');
const { LICENSE_FIXTURE } = require('./fixtures');
const {
    testCreateAnalyzerResult,
    testCreateAnalyzerQuotaExceeded,
    testDeleteAnalyzerResult,
    testGenerateLicenseRequestResult,
    testUploadPublicKeyResult,
    testSendLicenseRequestResult,
    testUploadLicenseResult,
    testGetLicenseResult,
    testGetLicensesResult,
    testDeleteLicenseResult,
} = require('./testers');

const ADMIN_DEFAULT = {
    username: apiConfig.services.api.admin.username,
    password: apiConfig.services.api.admin.default_password,
};

const ANALYZERS = {
    [PIPELINES.INTRUSION_DETECTION]: {
            name: PIPELINES.INTRUSION_DETECTION,
            source: {
                mode: 'streaming',
                url: `rtsp://${PIPELINES.INTRUSION_DETECTION}`,
            },
            pipelines: [{
                type: PIPELINES.INTRUSION_DETECTION,
                params: {
                    roi: [{
                        x: 0.156257813,
                        y: 0.138902778,
                    }, {
                        x: 0.312507813,
                        y: 0.138902778,
                    }, {
                        x: 0.312507813,
                        y: 0.312507813,
                    }, {
                        x: 0.156257813,
                        y: 0.833347222,
                    }],
                    triggers: [
                        'person',
                    ],
                },
            }],
    },
    [PIPELINES.SINGLE_CAMERA_LICENSE_PLATE_RECOGNITION]: {
        name: PIPELINES.SINGLE_CAMERA_LICENSE_PLATE_RECOGNITION,
        source: {
            mode: 'streaming',
            url: `rtsp://${PIPELINES.SINGLE_CAMERA_LICENSE_PLATE_RECOGNITION}`,
        },
        pipelines: [{
            type: PIPELINES.SINGLE_CAMERA_LICENSE_PLATE_RECOGNITION,
            params: {
                triggers: [{
                    owner: 'Hawk Lin',
                    number: 'AGT-5566',
                }],
            },
        }],
    },
    [PIPELINES.CROSS_CAMERA_LICENSE_PLATE_RECOGNITION]: {
        name: PIPELINES.CROSS_CAMERA_LICENSE_PLATE_RECOGNITION,
        source: {
            mode: 'streaming',
            url: `rtsp://${PIPELINES.CROSS_CAMERA_LICENSE_PLATE_RECOGNITION}`,
        },
        pipelines: [{
            type: PIPELINES.CROSS_CAMERA_LICENSE_PLATE_RECOGNITION,
            params: {
                triggers: [
                    'black list',
                ],
            },
        }],
    },
    [PIPELINES.SINGLE_CAMERA_FACE_RECOGNITION]: {
        name: PIPELINES.SINGLE_CAMERA_FACE_RECOGNITION,
        source: {
            mode: 'streaming',
            url: `rtsp://${PIPELINES.SINGLE_CAMERA_FACE_RECOGNITION}`,
        },
        pipelines: [{
            type: PIPELINES.SINGLE_CAMERA_FACE_RECOGNITION,
            params: {
                triggers: [
                    'black list',
                ],
            },
        }],
    },
    [PIPELINES.CROSS_CAMERA_FACE_RECOGNITION]: {
        name: PIPELINES.CROSS_CAMERA_FACE_RECOGNITION,
        source: {
            mode: 'streaming',
            url: `rtsp://${PIPELINES.CROSS_CAMERA_FACE_RECOGNITION}`,
        },
        pipelines: [{
            type: PIPELINES.CROSS_CAMERA_FACE_RECOGNITION,
            params: {
                triggers: [
                    'black list',
                ],
            },
        }],
    },
    [PIPELINES.CAR_COUNTING]: {
        name: PIPELINES.CAR_COUNTING,
        source: {
            mode: 'streaming',
            url: `rtsp://${PIPELINES.CAR_COUNTING}`,
        },
        pipelines: [{
            type: PIPELINES.CAR_COUNTING,
            params: {
                triggers: [
                    'car',
                ],
                lines: [{
                    x1: 0.123,
                    y1: 0.123,
                    x2: 0.456,
                    y2: 0.456,
                }],
            },
        }],
    },
    [PIPELINES.DIRECTIONAL_CAR_COUNTING]: {
        name: PIPELINES.DIRECTIONAL_CAR_COUNTING,
        source: {
            mode: 'streaming',
            url: `rtsp://${PIPELINES.DIRECTIONAL_CAR_COUNTING}`,
        },
        pipelines: [{
            type: PIPELINES.DIRECTIONAL_CAR_COUNTING,
            params: {
                triggers: [
                    'car',
                ],
                paths: [{
                    name: "Path 1",
                    start: {
                        x1: 0.28359375,
                        y1: 0.79027,
                        x2: 0.4328125,
                        y2: 0.8056,
                    },
                    end: {
                        x1: 0.365625,
                        y1: 0.63194,
                        x2: 0.4734375,
                        y2: 0.63,
                    },
                }],
            },
        }],
    },
};

describe('License Operations', () => {
    const licSvr = {
        admin: {
            username: licSvrConfig.admin.username,
            password: licSvrConfig.admin.default_password,
        },
        user: {
            username: 'user',
            password: 'user',
        },
    };

    beforeAll(async () => {
        // Reset the license server to initial state.
        await resetLicSvr();

        // Get the admin token on license server.
        const loginAdminResult = await requestLicSvr({
            url: 'login',
            method: 'POST',
            body: {
                username: licSvr.admin.username,
                password: licSvr.admin.password,
            },
        });

        licSvr.admin.token = loginAdminResult.body.token;

        // Create a user on license server.
        await requestLicSvr({
            url: 'users',
            method: 'POST',
            body: {
                username: licSvr.user.username,
                password: licSvr.user.password,
                role: LIC_SVR_ROLES.USER,
            },
            token: licSvr.admin.token,
        });

        // Get the user token on license server.
        const loginUserResult = await requestLicSvr({
            url: 'login',
            method: 'POST',
            body: {
                username: licSvr.user.username,
                password: licSvr.user.password,
            },
        });

        licSvr.user.token = loginUserResult.body.token;

        // Get public key from license server.
        const getPublicKeyResult = await requestLicSvr({
            url: 'serverKey',
            method: 'GET',
            token: licSvr.user.token,
        });

        licSvr.publicKey = getPublicKeyResult.body.publicKey;
    });

    describe('General flow', () => {
        const api = {
            admin: ADMIN_DEFAULT,
            token: undefined,
        };

        const invalidApplicationRequest = {
            invalidApplicationName: [{
                type: `${PIPELINES.INTRUSION_DETECTION} Invalid`,
                channel: 1,
            }],
            channelNotProvided: [{
                type: PIPELINES.INTRUSION_DETECTION,
            }],
            insufficientResource: [{
                type: PIPELINES.INTRUSION_DETECTION,
                channel: 2147482641,
            }],
            zeroChannel: [{
                type: PIPELINES.INTRUSION_DETECTION,
                channel: 0,
            }],
        };

        const intrusionDetection = {
            application: [{
                type: PIPELINES.INTRUSION_DETECTION,
                channel: 1,
            }],
            analyzer: ANALYZERS[PIPELINES.INTRUSION_DETECTION],
            analyzerId: undefined,
            licenseRequest: undefined,
            license: undefined,
            licenseId: undefined,
        };

        const faceRecognition = {
            application: [{
                type: PIPELINES.CROSS_CAMERA_FACE_RECOGNITION,
                channel: 1,
            }],
            analyzer: ANALYZERS[PIPELINES.CROSS_CAMERA_FACE_RECOGNITION],
            analyzerId: undefined,
            licenseRequest: undefined,
            license: undefined,
            licenseId: undefined,
        }

        beforeAll(async () => {
            // Reset the API database to initial state.
            await resetDatabse();

            // Get the admin token on API.
            api.admin.token = await getAuthorization(
                api.admin.username,
                api.admin.password,
            );
        });

        test('Get the information of uploaded licenses from API, which should be empty', async () => {
            const result = await request({
                url: 'licenses',
                method: 'GET',
                token: api.admin.token,
            });

            testGetLicensesResult(result, []);
        });

        test('Create an analyzer that is not allowed in uploaded licenses, which will return error', async () => {
            const result = await request({
                url: 'analyzers',
                method: 'POST',
                body: intrusionDetection.analyzer,
                token: api.admin.token,
            });

            testCreateAnalyzerQuotaExceeded(result);
        });

        test('Generate a invalid license request (invalid application name) from API, which will return error', async () => {
            const result = await request({
                url: 'license/request',
                method: 'POST',
                body: {
                    application: invalidApplicationRequest.invalidApplicationName,
                },
                token: api.admin.token,
            });

            // Test the status code, must be 400 BAD_REQUEST.
            expect(result.statusCode).toBe(HttpStatus.BAD_REQUEST);
            // Test the error message.
            expect(result.error.error.message).toBe(
                'Application type is not valid',
            );
        });

        test('Generate a invalid license request (channel not provided) from API, which will return error', async () => {
            const result = await request({
                url: 'license/request',
                method: 'POST',
                body: {
                    application: invalidApplicationRequest.channelNotProvided,
                },
                token: api.admin.token,
            });

            // Test the status code, must be 400 BAD_REQUEST.
            expect(result.statusCode).toBe(HttpStatus.BAD_REQUEST);
            // Test the error message.
            expect(result.error.error.message).toBe(
                'Channel is required',
            );
        });

        test('Generate a invalid license request (too many # of channels) from API, which will return error', async () => {
            const result = await request({
                url: 'license/request',
                method: 'POST',
                body: {
                    application: invalidApplicationRequest.insufficientResource,
                },
                token: api.admin.token,
            });

            // Test the status code, must be 400 BAD_REQUEST.
            expect(result.statusCode).toBe(HttpStatus.BAD_REQUEST);
            // Test the error message.
            expect(result.error.error.message).toBe(
                'Insufficient GPU memory',
            );
        });

        test('Generate a invalid license request (zero channel) from API, which will return error', async () => {
            const result = await request({
                url: 'license/request',
                method: 'POST',
                body: {
                    application: invalidApplicationRequest.zeroChannel,
                },
                token: api.admin.token,
            });

            // Test the status code, must be 400 BAD_REQUEST.
            expect(result.statusCode).toBe(HttpStatus.BAD_REQUEST);
            // Test the error message.
            expect(result.error.error.message).toBe(
                'Channel must greater than 0',
            );
        });

        test('Generate a valid license request from API, but before sending public key, which will return error', async () => {
            const result = await request({
                url: 'license/request',
                method: 'POST',
                body: {
                    application: intrusionDetection.application,
                },
                token: api.admin.token,
            });

            // Test the status code, must be 400 BAD_REQUEST.
            expect(result.statusCode).toBe(HttpStatus.BAD_REQUEST);
            // Test the error message.
            expect(result.error.error.message).toBe(
                'No server public key found, please upload a server public key.',
            );
        });

        test('Send public key to API', async () => {
            const result = await request({
                url: 'license/upload_server_key',
                method: 'POST',
                body: {
                    publicKey: licSvr.publicKey,
                },
                token: api.admin.token,
            });

            testUploadPublicKeyResult(result);
        });

        test('Generate a valid license request from API after sending public key', async () => {
            const result = await request({
                url: 'license/request',
                method: 'POST',
                body: {
                    application: intrusionDetection.application,
                },
                token: api.admin.token,
            });

            testGenerateLicenseRequestResult(result);

            intrusionDetection.licenseRequest = result.body.licenseRequest;
        });

        test('Send the license request to license server and get a valid license', async () => {
            const result = await requestLicSvr({
                url: 'getLicense',
                method: 'POST',
                body: {
                    licenseRequest: intrusionDetection.licenseRequest,
                },
                token: licSvr.user.token,
            });

            testSendLicenseRequestResult(result);

            intrusionDetection.license = result.body.license;
        });

        test('Upload the valid license to API', async () => {
            const result = await request({
                url: 'license/upload',
                method: 'POST',
                body: {
                    license: intrusionDetection.license,
                },
                token: api.admin.token,
            });

            testUploadLicenseResult(result);

            intrusionDetection.licenseId = result.body._id;
        });

        test('Upload the same license to API again, which should return error', async () => {
            const result = await request({
                url: 'license/upload',
                method: 'POST',
                body: {
                    license: intrusionDetection.license,
                },
                token: api.admin.token,
            });

            // Test the status code, must be 400 BAD_REQUEST.
            expect(result.statusCode).toBe(HttpStatus.BAD_REQUEST);
            // Test the error message.
            expect(result.error.error.message).toBe(
                'License already existed',
            );
        });

        test('Upload the same license (appended with some string) to API again, which should return error', async () => {
            const result = await request({
                url: 'license/upload',
                method: 'POST',
                body: {
                    license: `${intrusionDetection.license} attack`,
                },
                token: api.admin.token,
            });

            // Test the status code, must be 400 BAD_REQUEST.
            expect(result.statusCode).toBe(HttpStatus.BAD_REQUEST);
            // Test the error message.
            expect(result.error.error.message).toBe(
                'License already existed',
            );
        });

        test('Get the information of uploaded license from API', async () => {
            const result = await request({
                url: `license/${intrusionDetection.licenseId}`,
                method: 'GET',
                token: api.admin.token,
            });

            testGetLicenseResult(result, {
                _id: intrusionDetection.licenseId,
                vendor: licSvr.user.username,
                application: intrusionDetection.application,
            });
        });

        test('Get the information of uploaded licenses from API, which should contain 1 valid license', async () => {
            const result = await request({
                url: 'licenses',
                method: 'GET',
                token: api.admin.token,
            });

            testGetLicensesResult(result, [{
                _id: intrusionDetection.licenseId,
                vendor: licSvr.user.username,
                application: intrusionDetection.application,
            }]);
        });

        test('Create an analyzer that is allowed in uploaded licenses', async () => {
            const result = await request({
                url: 'analyzers',
                method: 'POST',
                body: intrusionDetection.analyzer,
                token: api.admin.token,
            });

            testCreateAnalyzerResult(result)

            intrusionDetection.analyzerId = result.body._id;
        });

        test('Create the same analyzer (exceeds # of allowed applications), which will return an error', async () => {
            const result = await request({
                url: 'analyzers',
                method: 'POST',
                body: intrusionDetection.analyzer,
                token: api.admin.token,
            });

            testCreateAnalyzerQuotaExceeded(result);
        });

        test('Create another analyzer that is not allowed in uploaded licenses, which will return error', async () => {
            const result = await request({
                url: 'analyzers',
                method: 'POST',
                body: faceRecognition.analyzer,
                token: api.admin.token,
            });

            testCreateAnalyzerQuotaExceeded(result);
        });

        test('Generate second valid license request from API', async () => {
            const result = await request({
                url: 'license/request',
                method: 'POST',
                body: {
                    application: faceRecognition.application,
                },
                token: api.admin.token,
            });

            testGenerateLicenseRequestResult(result);

            faceRecognition.licenseRequest = result.body.licenseRequest;
        });

        test('Send second license request to license server and get second valid license', async () => {
            const result = await requestLicSvr({
                url: 'getLicense',
                method: 'POST',
                body: {
                    licenseRequest: faceRecognition.licenseRequest,
                },
                token: licSvr.user.token,
            });

            testSendLicenseRequestResult(result);

            faceRecognition.license = result.body.license;
        });

        test('Upload second valid license to API', async () => {
            const result = await request({
                url: 'license/upload',
                method: 'POST',
                body: {
                    license: faceRecognition.license,
                },
                token: api.admin.token,
            });

            testUploadLicenseResult(result);

            faceRecognition.licenseId = result.body._id;
        });

        test('Get the information of second uploaded license from API', async () => {
            const result = await request({
                url: `license/${faceRecognition.licenseId}`,
                method: 'GET',
                token: api.admin.token,
            });

            testGetLicenseResult(result, {
                _id: faceRecognition.licenseId,
                vendor: licSvr.user.username,
                application: faceRecognition.application,
            });
        });

        test('Get the information of uploaded licenses from API, which should contain 2 valid licenses', async () => {
            const result = await request({
                url: 'licenses',
                method: 'GET',
                token: api.admin.token,
            });

            testGetLicensesResult(result, [{
                _id: intrusionDetection.licenseId,
                vendor: licSvr.user.username,
                application: intrusionDetection.application,
            }, {
                _id: faceRecognition.licenseId,
                vendor: licSvr.user.username,
                application: faceRecognition.application,
            }]);
        });

        test('Create second analyzer that is allowed in uploaded licenses', async () => {
            const result = await request({
                url: 'analyzers',
                method: 'POST',
                body: faceRecognition.analyzer,
                token: api.admin.token,
            });

            testCreateAnalyzerResult(result)

            faceRecognition.analyzerId = result.body._id;
        });

        test('Delete the first uploaded license in API before its allowed analyzer is deleted, which will return error', async () => {
            const result = await request({
                url: `license/${intrusionDetection.licenseId}`,
                method: 'DELETE',
                token: api.admin.token,
            });

            // Test the status code, must be 400 BAD_REQUEST.
            expect(result.statusCode).toBe(HttpStatus.BAD_REQUEST);
            // Test the error message.
            expect(result.error.error.message).toBe(
                `License of ${PIPELINES.INTRUSION_DETECTION} will be insufficient.`,
            );
        });

        test('Delete the analyzer that is allowed in first upload license', async () => {
            const result = await request({
                url: `analyzer/${intrusionDetection.analyzerId}`,
                method: 'DELETE',
                token: api.admin.token,
            });

            testDeleteAnalyzerResult(result);
        });

        test('Delete the first uploaded license in API after its allowed analyzer is deleted', async () => {
            const result = await request({
                url: `license/${intrusionDetection.licenseId}`,
                method: 'DELETE',
                token: api.admin.token,
            });

            testDeleteLicenseResult(result)
        });

        test('Get the information of uploaded licenses from API, which should contain 1 valid license', async () => {
            const result = await request({
                url: 'licenses',
                method: 'GET',
                token: api.admin.token,
            });

            testGetLicensesResult(result, [{
                _id: faceRecognition.licenseId,
                vendor: licSvr.user.username,
                application: faceRecognition.application,
            }]);
        });

        test('Delete the second uploaded license in API before its allowed analyzer is deleted, which will return error', async () => {
            const result = await request({
                url: `license/${faceRecognition.licenseId}`,
                method: 'DELETE',
                token: api.admin.token,
            });

            // Test the status code, must be 400 BAD_REQUEST.
            expect(result.statusCode).toBe(HttpStatus.BAD_REQUEST);
            // Test the error message.
            expect(result.error.error.message).toBe(
                `License of ${PIPELINES.CROSS_CAMERA_FACE_RECOGNITION} will be insufficient.`,
            );
        });

        test('Delete the analyzer that is allowed in second upload license', async () => {
            const result = await request({
                url: `analyzer/${faceRecognition.analyzerId}`,
                method: 'DELETE',
                token: api.admin.token,
            });

            testDeleteAnalyzerResult(result);
        });

        test('Delete the second uploaded license in API after its allowed analyzer is deleted', async () => {
            const result = await request({
                url: `license/${faceRecognition.licenseId}`,
                method: 'DELETE',
                token: api.admin.token,
            });

            testDeleteLicenseResult(result)
        });

        test('Get the information of uploaded licenses from API, which should return an empty list', async () => {
            const result = await request({
                url: 'licenses',
                method: 'GET',
                token: api.admin.token,
            });

            testGetLicensesResult(result, []);
        });
    });

    describe.each(Object.values(PIPELINES))('License flow of %p', (pipelineType) => {
        const api = {
            admin: ADMIN_DEFAULT,
            token: undefined,
        };

        const pipeline = {
            application: [{
                type: pipelineType,
                channel: 1,
            }],
            analyzer: ANALYZERS[pipelineType],
            analyzerId: undefined,
            licenseRequest: undefined,
            license: undefined,
            licenseId: undefined,
        };

        beforeAll(async() => {
            // Reset the API database to initial state.
            await resetDatabse();

            // Get the admin token on API.
            api.admin.token = await getAuthorization(
                api.admin.username,
                api.admin.password,
            );
        });

        test('Send public key to API', async () => {
            const result = await request({
                url: 'license/upload_server_key',
                method: 'POST',
                body: {
                    publicKey: licSvr.publicKey,
                },
                token: api.admin.token,
            });

            testUploadPublicKeyResult(result);
        });

        test('Generate a valid license request from API after sending public key', async () => {
            const result = await request({
                url: 'license/request',
                method: 'POST',
                body: {
                    application: pipeline.application,
                },
                token: api.admin.token,
            });

            testGenerateLicenseRequestResult(result);

            pipeline.licenseRequest = result.body.licenseRequest;
        });

        test('Send the license request to license server and get a valid license', async () => {
            const result = await requestLicSvr({
                url: 'getLicense',
                method: 'POST',
                body: {
                    licenseRequest: pipeline.licenseRequest,
                },
                token: licSvr.user.token,
            });

            testSendLicenseRequestResult(result);

            pipeline.license = result.body.license;
        });

        test('Upload the valid license to API', async () => {
            const result = await request({
                url: 'license/upload',
                method: 'POST',
                body: {
                    license: pipeline.license,
                },
                token: api.admin.token,
            });

            testUploadLicenseResult(result);

            pipeline.licenseId = result.body._id;
        });

        test('Get the information of uploaded license from API', async () => {
            const result = await request({
                url: `license/${pipeline.licenseId}`,
                method: 'GET',
                token: api.admin.token,
            });

            testGetLicenseResult(result, {
                _id: pipeline.licenseId,
                vendor: licSvr.user.username,
                application: pipeline.application,
            });
        });

        test('Get the information of uploaded licenses from API, which should contain 1 valid license', async () => {
            const result = await request({
                url: 'licenses',
                method: 'GET',
                token: api.admin.token,
            });

            testGetLicensesResult(result, [{
                _id: pipeline.licenseId,
                vendor: licSvr.user.username,
                application: pipeline.application,
            }]);
        });

        test('Create an analyzer that is allowed in uploaded licenses', async () => {
            const result = await request({
                url: 'analyzers',
                method: 'POST',
                body: pipeline.analyzer,
                token: api.admin.token,
            });

            testCreateAnalyzerResult(result)

            pipeline.analyzerId = result.body._id;
        });

        test('Delete the uploaded license in API before its allowed analyzer is deleted, which will return error', async () => {
            const result = await request({
                url: `license/${pipeline.licenseId}`,
                method: 'DELETE',
                token: api.admin.token,
            });

            // Test the status code, must be 400 BAD_REQUEST.
            expect(result.statusCode).toBe(HttpStatus.BAD_REQUEST);
            // Test the error message.
            expect(result.error.error.message).toBe(
                `License of ${pipelineType} will be insufficient.`,
            );
        });

        test('Delete the analyzer that is allowed in upload license', async () => {
            const result = await request({
                url: `analyzer/${pipeline.analyzerId}`,
                method: 'DELETE',
                token: api.admin.token,
            });

            testDeleteAnalyzerResult(result);
        });

        test('Delete the uploaded license in API', async () => {
            const result = await request({
                url: `license/${pipeline.licenseId}`,
                method: 'DELETE',
                token: api.admin.token,
            });

            testDeleteLicenseResult(result)
        });

        test('Get the information of uploaded licenses from API, which should return an empty list', async () => {
            const result = await request({
                url: 'licenses',
                method: 'GET',
                token: api.admin.token,
            });

            testGetLicensesResult(result, []);
        });
    });

    describe.each([
        ROLES.WRITER,
        ROLES.READER,
    ])('Unauthorized requests by %p users', (role) => {
        const api = {
            admin: {
                username: ADMIN_DEFAULT.username,
                password: ADMIN_DEFAULT.password,
                token: undefined,
            },
            user: {
                username: role,
                password: role,
                token: undefined,
            },
        };

        const pipeline = {
            application: [{
                type: PIPELINES.INTRUSION_DETECTION,
                channel: 1,
            }],
            analyzer: ANALYZERS[PIPELINES.INTRUSION_DETECTION],
            license: LICENSE_FIXTURE.LICENSE,
            licenseId: LICENSE_FIXTURE.LICENSE_ID,
        };

        beforeAll(async() => {
            // Reset the API database to initial state.
            await resetDatabse();

            // Get the admin token on API.
            api.admin.token = await getAuthorization(
                api.admin.username,
                api.admin.password,
            );

            // Create a user on API.
            await request({
                url: 'users',
                method: 'POST',
                body: {
                    username: api.user.username,
                    password: api.user.password,
                    role,
                },
                token: api.admin.token,
            });

            // Get the user token on API.
            api.user.token = await getAuthorization(
                api.user.username,
                api.user.password,
            );
        });

        test('Unauthorized to send public key to API, which will return an error', async () => {
            const result = await request({
                url: 'license/upload_server_key',
                method: 'POST',
                body: {
                    publicKey: licSvr.publicKey,
                },
                token: api.user.token,
            });

            // Test the status code, must be 403 FORBIDDEN.
            expect(result.statusCode).toBe(HttpStatus.FORBIDDEN);
        });

        test('Unauthorized request to generate a license request to API, which will return an error', async () => {
            const result = await request({
                url: 'license/request',
                method: 'POST',
                body: {
                    application: pipeline.application,
                },
                token: api.user.token,
            });

            // Test the status code, must be 403 FORBIDDEN.
            expect(result.statusCode).toBe(HttpStatus.FORBIDDEN);
        });

        test('Unauthorized request to upload a license to API, which will return an error', async () => {
            const result = await request({
                url: 'license/upload',
                method: 'POST',
                body: {
                    license: pipeline.license,
                },
                token: api.user.token,
            });

            // Test the status code, must be 403 FORBIDDEN.
            expect(result.statusCode).toBe(HttpStatus.FORBIDDEN);
        });

        test('Unauthorized request to get uploaded licenses from API, which will return an error', async () => {
            const result = await request({
                url: 'licenses',
                method: 'GET',
                token: api.user.token,
            });

            // Test the status code, must be 403 FORBIDDEN.
            expect(result.statusCode).toBe(HttpStatus.FORBIDDEN);
        });

        test('Unauthorized request to get a uploaded license from API, which will return an error', async () => {
            const result = await request({
                url: `license/${pipeline.licenseId}`,
                method: 'GET',
                token: api.user.token,
            });

            // Test the status code, must be 403 FORBIDDEN.
            expect(result.statusCode).toBe(HttpStatus.FORBIDDEN);
        });

        test('Unauthorized request to delete a uploaded license from API, which will return an error', async () => {
            const result = await request({
                url: `license/${pipeline.licenseId}`,
                method: 'DELETE',
                token: api.user.token,
            });

            // Test the status code, must be 403 FORBIDDEN.
            expect(result.statusCode).toBe(HttpStatus.FORBIDDEN);
        });
    });
});
