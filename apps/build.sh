#!/bin/sh

# Generate analyzer.spec for packaging.
.local/bin/pyinstaller --clean -y \
	--exclude-module torch \
	--hidden-import PyQt5._QOpenGLFunctions_4_1_Core \
	--path /usr/local/lib/python3.6/dist-packages \
	--add-data /usr/local/lib/python3.6/dist-packages/distributed/distributed.yaml:./distributed/ \
	--add-data /usr/local/lib/python3.6/dist-packages/jagereye_ng/static/database.json:./jagereye_ng/static/ \
	--add-data /usr/local/lib/python3.6/dist-packages/jagereye_ng/static/externals.json:./jagereye_ng/static/ \
	--add-data /usr/local/lib/python3.6/dist-packages/jagereye_ng/static/messaging.json:./jagereye_ng/static/ \
	--add-data /usr/local/lib/python3.6/dist-packages/jagereye_ng/static/models.yml:./jagereye_ng/static/ \
	--add-data /usr/local/lib/python3.6/dist-packages/jagereye_ng/static/logging.template.yml:./jagereye_ng/static/ \
	--add-data /usr/local/lib/python3.6/dist-packages/jagereye_ng/static/models:./jagereye_ng/static/models/ \
	analyzer.py
