from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os
from dask.distributed import get_client
from events import EventVideoFrame

from jagereye_ng import image as im
from jagereye_ng import gpu_worker
from jagereye_ng.io.notification import Notification
from jagereye_ng.io.database import Database
from jagereye_ng import logging
from jagereye_ng.util.generic import get_config

import cv2
from PIL import Image
import threading
import numpy as np

config = get_config()["apps"]["dask"]
address = config["cluster_ip"] + ":" + config["scheduler_port"]

def transform_lines_format(lines, frame_size):
    """Transform lines format

    Transforms rules:
        absolution point value = relative point value * corresponding frame size

    Examples:
        Assume frame size is (100, 100),
        Expect source format (list of dict):
            [{"x1": 0.21, "y1": 0.33, "x2": 0.32, "y2": 0.43}, ...]. each
            value means scaled x and y position, 0 <= x1, y1, x2, y2 < 1.

        Transformed lines format (list of tuple):
            np.array([21, 33, 32, 43])

    Args:
        lines (list of dict): Input line to be transformed.
        frame_size (tuple): The frame size of input image frames with
            format (width, height).

    Returns:
        (`numpy.ndarray`)
    """
    transformed_lines = []
    line_target = ["x1", "y1", "x2", "y2"]
    for i in range(len(lines)):
        for lt in line_target:
            if lines[i].get(lt) is not None:
                if lines[i][lt] < 0 or lines[i][lt] > 1:
                    raise ValueError("Invalid line format, should be a float with "
                                        "value between 0 and 1.")
            else:
                raise ValueError("Invalid line format, {} not found".format(lt))
        transformed_lines.append([
                float(lines[i]["x1"]) * float(frame_size[0]), \
                float(lines[i]["y1"]) * float(frame_size[1]), \
                float(lines[i]["x2"]) * float(frame_size[0]), \
                float(lines[i]["y2"]) * float(frame_size[1]) ])

    return np.array(transformed_lines)

class CarCounting(object):
    """A class used to detect intrusion event.

    Attributes:
        lines (`numpy.ndarray`): The lines for counting target, for example:
            numpy.array([[x1, y1, x2, y2]]). x1, y1, x2, y2 are in pixel
        triggers (list of string): The target of interest object.
        frame_size (tuple): The frame size of input image frames with
            format (width, height).
    """

    def __init__(self, lines, triggers, frame_size):
        try:
            # Get Dask client
            self._client = get_client(address=address)
        except ValueError:
            assert False, ("Should connect to Dask scheduler before"
                           " initializing this object.")
        self._lines = lines
        self.frame_size = frame_size
        self.triggers = triggers
        self._length_of_lines = self._get_length_of_lines(self._lines)
        self._line_function_coeff, self._ortho_function_coeff = \
            self._get_linear_func_coeff(self._lines)
        # object list keep tracking
        self._object_list = []
        # object check parameters based on 1080p resolution
        self._config_ratio = frame_size[1] / 1080.0
        self._object_check_dist = int(100 * self._config_ratio)
        self._counting_check_dist = int(40 * self._config_ratio)
        self._dist_thr = int(60 * self._config_ratio)
        self._dist = int(1000 * self._config_ratio)
        logging.info("Created an CarCounting (lines: {}, triggers: {})"
                     .format(self._lines, self.triggers))

    def __del__(self):
        self._client.close()

    @property
    def lines(self):
        return self._lines

    @lines.setter
    def lines(self, value):
        self._lines = value
        self._length_of_lines = self._get_length_of_lines(self._lines)
        self._line_function_coeff, self._ortho_function_coeff = \
            self._get_linear_func_coeff(self._lines)

    def _get_length_of_lines(self, lines):
        """Get length of lines

        Args:
            lines (`numpy.ndarray`): The lines for counting target, for example:
                numpy.array([[x1, y1, x2, y2]]). x1, y1, x2, y2 are in pixel
        Returns:
            (list of float): each row length of line in pixel
        """
        length_of_lines = []
        for line in lines:
            x1, y1, x2, y2 = line
            length_of_lines.append(np.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2))
        return length_of_lines

    def _get_linear_func_coeff(self, lines):
        """Get line and ortho math function coeff vector

        Args:
            lines (`numpy.ndarray`): The lines for counting target, for example:
                numpy.array([[x1, y1, x2, y2]]). x1, y1, x2, y2 are in pixel
        Returns:
            (`numpy.ndarray`, `numpy.ndarray`):
                normalized linear function, line and ortho. each row corresponds
                line in lines, contains [ coeff_x, coeff_y, const ]
        """

        line_function_coeff = []
        ortho_function_coeff = []
        for line in lines:
            x1, y1, x2, y2 = line
            x_, y_ = (x1 + x2) / 2, (y1 + y2) / 2
            coeff_x, coeff_y, const = \
                -(y2 - y1), x2 - x1, (y2 - y1) * x1 - (x2 - x1) * y1
            ortho_coeff_x, ortho_coeff_y, ortho_const = \
                x2 - x1, y2 - y1, - (y2 - y1) * y_ - (x2 - x1) * x_
            # normalize
            normalizer1 = np.sqrt(coeff_x**2 + coeff_y**2)
            normalizer2 = np.sqrt(ortho_coeff_x ** 2 + ortho_coeff_y ** 2)

            line_function_coeff.append(
                    np.array([coeff_x, coeff_y, const]) / normalizer1)
            ortho_function_coeff.append(
                    np.array([ortho_coeff_x, ortho_coeff_y, ortho_const]) / normalizer2)
        return np.array(line_function_coeff), np.array(ortho_function_coeff)

    def _dist_between_line_and_box(self, coeffs, boxs):
        """Distance between box center and lines

        Args:
            coeffs (`numpy.ndarray`): normalized linear function
            boxs (list of dict): bounding boxs [{"xmin": 100, "ymin": 100,
                "xmax": 150, "ymax": 150}]
        Returns:
            (numpy.ndarray): each row corresponds each line distance in pixel
        """
        box_array = []
        dist = np.array([self._dist])
        for box in boxs:
            box_c = self._convert_center_box(box)
            b = [box_c["x"], box_c["y"], 1]
            box_array.append(b)

        if len(box_array) > 0:
            dist = abs(np.dot(np.array(box_array), coeffs.T))
        return dist

    def _box_dist(self, box1, box2):
        """Get two boxes distance

        Args:
            box1 (dict): bounding center box {"x": 75,"y": 75, "xmin": 50,
                "ymin": 50, "xmax": 100, "ymax": 100}
            box2 (dict): bounding center box {"x": 125,"y": 125, "xmin": 100,
                "ymin": 150, "xmax": 100, "ymax": 150}

        Returns:
            (float): distance in pixel
        """
        dist = ((box1["x"]-box2["x"])**2 +
                (box1["y"]-box2["y"])**2)**0.5
        return dist

    def _convert_center_box(self, box):
        """Convert bounding box to center x, y object

        Args:
            box (dict): bounding box {"xmin": 50, "ymin": 50,
                "xmax": 100, "ymax": 100}
        Returns:
            (dict): {"x": 75,"y": 75, "xmin": 50, "ymin": 50,
                "xmax": 100, "ymax": 100}
        """
        box["x"] = int((box["xmax"] + box["xmin"]) / 2)
        box["y"] = int((box["ymax"] + box["ymin"]) / 2)
        return box

    def _check_object_list(self, boxes):
        """Get new object list index of boxes

        check incoming boxes existed in current self._object_list,
        check if box close to lines enough.

        Args:
            boxs (list of dict): bounding boxs [{"xmin": 100, "ymin": 100,
                "xmax": 150, "ymax": 150}]

        Returns:
            (list of int): list of index of boxes is new object list
        """
        new_obj_idx = []
        dist_1 = self._dist_between_line_and_box(self._line_function_coeff, boxes)
        dist_2 = self._dist_between_line_and_box(self._ortho_function_coeff, boxes)

        for box_idx, box in list(enumerate(boxes)):
            box_c = self._convert_center_box(box)

            # check the object and line if distance close enough
            count, lane_number = self._check_lines(dist_1[box_idx],
                                                    dist_2[box_idx],
                                                    self._length_of_lines)
            # check the object list and box if distance close enough
            box_counted = False
            if min(dist_1[box_idx]) < self._object_check_dist:
                self._object_list, box_aa = \
                    self._check_object_box(self._object_list, box_c)
                box_counted = box_aa

            if count and lane_number != -1:
                if not box_counted:
                    # add to new object index list
                    new_obj_idx.append(box_idx)
                self._object_list.append(box_c)
        return new_obj_idx

    def _check_object_box(self, object_list, box_c):
        """Mantaining the tracking list object_list

        note that replace object_list with return object_list, and recover
            self._object_list box

        Args:
            object_list (list of dict): list of bounding box as `box`
            box_c (dict): bounding center box {"x": 75,"y": 75, "xmin": 50,
                "ymin": 50, "xmax": 100, "ymax": 100}

        Returns
            (list of dict, bool): list of bounding box as `box`, if box count
        """
        if len(object_list) == 0:
            return object_list, False

        for idx, obox in list(enumerate(object_list)):
            dist = self._box_dist(obox, box_c)
            if dist < self._dist_thr:
                del object_list[idx]
                return object_list, True

        return object_list, False

    def _check_lines(self, dist_1, dist_2, length_of_lane):
        """ Check if object to be counted

        check distance between lane and object is close enough

        Args:
            dist_1, dist_2 (numpy.ndarray): each row corresponds each line
                distance in pixel
            length_of_lane (list of float): each row length of line in pixel
        Returns:
            (bool, int): if count, land index
        """
        count = False
        lane_idx = -1
        for i, (d1, d2, length) in \
                list(enumerate(zip(dist_1, dist_2, length_of_lane))):
            if d1 < self._counting_check_dist and d2 < (length / 2):
                count = True
                lane_idx = i
                break
        return count, lane_idx

    def _check_triggers(self, detections):
        """Check if the detected objects is an new car object event.

        Args:
            detections: A list of object detection result objects, each a
                object of format (bboxes, scores, classes, num_detctions).

        Returns:
            (list of dict): A list of dictionary that specifies the catched
                vehicle, format of each dictonary is:
                {
                    "is_triggered": (boolean),
                    "bboxes": [...],
                    "scores": [...],
                    "labels": [...],
                }
        """
        width, height = self.frame_size

        results = []
        for i in range(len(detections)):
            (bboxes, scores, labels) = detections[i]
            cands = {"bboxes_detect": []}
            for j in range(len(bboxes)):
                # Check whether the object's bbox is in roi or not.
                bbox = bboxes[j]
                cands["bboxes_detect"].append({
                    "xmin": bbox.xmin,
                    "ymin": bbox.ymin,
                    "xmax": bbox.xmax,
                    "ymax": bbox.ymax})

            new_obj_idx = []
            w_cands = {"is_triggered": False, "bboxes": [],
                        "scores": [], "labels": []}
            if len(cands["bboxes_detect"]) > 0:
                # Check whether object is the same, line, ...etc
                new_obj_idx = self._check_object_list(cands["bboxes_detect"])
            for j in new_obj_idx:
                bbox = bboxes[j]
                label = labels[j]

                if label in self.triggers:
                    w_cands["is_triggered"] = True
                    normalized_bbox = [
                        float(bbox.ymin) / height,
                        float(bbox.xmin) / width,
                        float(bbox.ymax) / height,
                        float(bbox.xmax) / width,
                    ]
                    w_cands["bboxes"].append(normalized_bbox)
                    w_cands["scores"].append(scores[j])
                    w_cands["labels"].append(labels[j])
            results.append(w_cands)
        return results

    def run(self, frames):
        images = [frame.image for frame in frames]
        f_images = self._client.scatter(images)
        f_detect = gpu_worker.run_model(self._client,
                                        "vehicle_detection",
                                        f_images,
                                        iou_threshold=0.3)
        catched = self._check_triggers(f_detect.result())
        output_frames = []

        for i in range(len(catched)):
            current_metadata = catched[i].copy()
            output_frames.append(EventVideoFrame(frames[i], current_metadata))
        return output_frames

    def release(self):
        pass

class CarCountingPipeline(object):
    """A class used to run the Intrusion Detection pipeline.

    Attributes:
        anal_id (lib.ObjectID): The ObjectID of the analyzer that the pipeline
            was attached with.
        lines (list of dict): The lines object pass through, for example:
            [{"x1": 0.21, "y1": 0.33, "x2": 0.32, "y2": 0.43}, ...]. each
            value means scaled x and y position, 0 <= x1, y1, x2, y2 < 1.
        triggers (list of string): The target of interest.
        frame_size (tuple): The size of the input image frame with format
            of (width, height).
    """
    def __init__(self, anal_id, lines, triggers, frame_size):
        self._anal_id = anal_id
        self._obj_key_prefix = os.path.join("car_counting", anal_id)
        transformed_lines = transform_lines_format(lines, frame_size)

        # Create an car counting
        self._detector = CarCounting(
            transformed_lines,
            triggers,
            frame_size)

        # Connect to Notification service
        self._notification = Notification()

        # Connect to Database service
        self._database = Database()

    def _output_event(self, timestamp, triggered):
        """Output event to notification center and database.

        Args:
            event: The event object to be outputted.
            timestamp: event timestamp
            triggerd: The triggerd objects of the event.
        """
        event_type = "car_counting.alert"
        content = {
            "triggered": triggered,
        }

        # Save event to database
        event_id = self._database.save_event(self._anal_id,
                                             timestamp,
                                             event_type,
                                             content).result()
        logging.info("Success to save event in database: _id: {}, analyzerId: "
                     "{}, timestamp: {}, type: {}, content: {}"
                     .format(event_id,
                             self._anal_id,
                             timestamp,
                             event_type,
                             content))

        # Push notification
        # Check whether the object near lines or not.
        self._notification.push_anal(event_id,
                                     self._anal_id,
                                     timestamp,
                                     event_type,
                                     content)

    def run(self, frames, motions, is_src_reader_busy):
        """Run Car Counting pipeline.

        Args:
            frames: A list of raw video frames to be detected.
            motions, is_src_reader_busy: not in use
        """
        detected = self._detector.run(frames)
        for frame in detected:
            # check if frame contains cars
            if frame.metadata["is_triggered"]:
                for label in frame.metadata["labels"]:
                    # triggered by label
                    self._output_event(frame.timestamp, label)

    def release(self):
        pass

