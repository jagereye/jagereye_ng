ARG VERSION

FROM jagereye/framework:${VERSION} as builder

LABEL maintainer="feabries@gmail.com"

COPY --chown=jager:jager . .

ARG GITLAB_DEPLOY_ACCOUNT
ARG GITLAB_DEPLOY_TOKEN

# Install the dependencies.
RUN pip3 install --no-cache-dir -r ./requirements.txt --user && \
    pip3 install pyinstaller==3.5 --user && \
    pip3 install git+https://${GITLAB_DEPLOY_ACCOUNT}:${GITLAB_DEPLOY_TOKEN}@gitlab.com/toppano-mirror/Nopa@v2.1.0 --user && \
    ./build.sh

FROM jagereye/framework_production:${VERSION}
COPY --from=builder /home/jager/dist/analyzer .
ENV PYTHONIOENCODING=utf-8
CMD ["./analyzer"]