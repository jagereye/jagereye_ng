from shapely import geometry

from jagereye_ng import image as im


class ROIList(object):
    """A class to represent a list of regions of interest (roi).

    Attributes:
        roi_list (list of list of object): A list that contains zero, one or
            more region(s) of interest (roi). The format of each roi is a list
            of object points, such as [{"x": 0.21, "y": 0.33},
            {"x": 0.32, "y": 0.43}, ...]
        frame_size (tuple): The frame size of input image frames with
            format (width, height).
    """

    def __init__(self, roi_list, frame_size):
        self._roi_list = roi_list
        self._frame_size = frame_size

        self._normalized_roi_list = [self._transform_roi_format(roi, frame_size)
                                     for roi in roi_list]
        self._roi_polygons = [geometry.Polygon(normalized_roi)
                              for normalized_roi in self._normalized_roi_list]

    def _transform_roi_format(self, roi, frame_size):
        """Transforms roi format.

        Transforms rules:
            absolution point value = relative point value * corresponding frame size

        Examples:
            Assume frame size is (100, 100),
            Expect source roi format (list of dict):
                [{"x": 0.21, "y": 0.33}, {"x": 0.32, "y": 0.43}, ...]

            Transformed roi format (list of tuple):
                [(21, 33), (32, 43), ...]

        Args:
            roi (list of objects): Input roi to be transformed.
            frame_size (tuple): The frame size of input image frames with
                format (width, height).

        Returns:
            Transformed roi.
        """
        result = []
        for r in roi:
            if ((r["x"] < 0 or r["x"] > 1) or (r["y"] < 0 or r["y"] > 1)):
                raise ValueError("Invalid roi point format, should be a float "
                                 "with value between 0 and 1.")
            result.append((float(r["x"]) * float(frame_size[0]),
                           float(r["y"]) * float(frame_size[1])))

        return tuple(result)

    def _is_in_roi(self, bbox, roi_polygon, threshold):
        """Check whether a bounding box  is in the roi or not.

        Args:
            bbox (`jagereye_ng.shape.BoundingBox`): The bounding box.
            roi_polygon (): Region of interest.
            threshold: The overlap threshold, range from 0 to 1.

        Returns:
            True if bbox is in the roi and false otherwise.
        """
        xmin = bbox.xmin
        ymin = bbox.ymin
        xmax = bbox.xmax
        ymax = bbox.ymax
        bbox_polygon = geometry.Polygon([[xmin, ymin], [xmax, ymin],
                                        [xmax, ymax], [xmin, ymax]])
        overlap_area = roi_polygon.intersection(bbox_polygon).area

        return overlap_area > threshold

    def is_in(self, bbox, threshold=0.0):
        """Check whether a bounding box is in any one of the roi list.

        Args:
            bbox (`jagereye_ng.shape.BoundingBox`): The bounding box.
            threshold: The overlap threshold, range from 0 to 1.

        Returns:
            True if bbox is in any of of the roi list and false otherwise.
        """
        for roi_polygon in self._roi_polygons:
            if self._is_in_roi(bbox, roi_polygon, threshold):
                return True

        return False

    def draw_on(self, image, color, alpha):
        """Draw roi list on a image.

        Args:
            image (ndarray): The image to draw.
            color (tuple): The region color of format (B, G, R).
            alpha (float): The level for alpha blending. Default to 0.5.

        Returns:
            The drawn image.
        """
        drawn_image = image.copy()

        for roi in self._normalized_roi_list:
            drawn_image = im.draw_region(drawn_image, roi, color, alpha)

        return drawn_image

    def is_empty(self):
        return len(self._roi_list) == 0
