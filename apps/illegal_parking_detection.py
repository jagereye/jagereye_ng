from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os
import sys
import copy
import numpy as np
import math
import uuid
import cv2
from dask.distributed import get_client
from shapely import geometry

from events import SharedPersistentEventVideoAgent
from events import SharedEventVideoFrame
from events import SharedEventVideoPolicy

from jagereye_ng import image as im
from jagereye_ng import gpu_worker
from jagereye_ng.io.obj_storage import ObjectStorageClient
from jagereye_ng.io.notification import Notification
from jagereye_ng.io.database import Database
from jagereye_ng.io.streaming import VideoFrame
from jagereye_ng import logging
from jagereye_ng.util.generic import get_config
from jagereye_ng.util.shape import BoundingBox
from jagereye_ng.util.math import clamp

VALID_TRIGGER_TYPES = ['car', 'motorcycle', 'bus', 'truck']
REFRESH_TIME = 2
DAY_TIME = 86400
EXPIRATION_TIME = 30 * DAY_TIME
COLOR_GREEN = (0, 255, 0)
COLOR_RED = (0, 0, 255)
COLOR_WHITE = (255, 255, 255)
COLOR_GREY = (105, 105, 105)
COLOR_ROI = (40, 37, 219)

config = get_config()["apps"]["dask"]
address = config["cluster_ip"] + ":" + config["scheduler_port"]

def _transform_roi_list_format(roi_list, frame_size):
    """Transforms roi list format.

    Transforms rules:
        absolution point value = relative point value * corresponding frame
        size

    Examples:
        Assume frame size is (100, 100),
        Expect source roi format (list of dict):
            [[{"x": 0.21, "y": 0.33}, {"x": 0.32, "y": 0.43}, ...]]

        Transformed roi format (list of tuple):
            [[(21, 33), (32, 43), ...]]

    Args:
        roi_list (list of list of objects): Input roi to be transformed.
        frame_size (tuple): The frame size of input image frames with
            format (width, height).

    Returns:
        Transformed roi list.
    """
    result = []
    for roi in roi_list:
        transformed_roi = []
        for r in roi:
            if ((r["x"] < 0 or r["x"] > 1) or
                    (r["y"] < 0 or r["y"] > 1)):
                raise ValueError("Invalid roi point format, should be a float "
                                 "with value between 0 and 1.")
            transformed_roi.append((int(r["x"] * frame_size[0]),
                                    int(r["y"] * frame_size[1])))
        result.append(transformed_roi)
    return tuple(result)


def _draw_object_bbox(img, obj):
    """Draw bounding box and text on image

    Args:
        img (`np.ndarray`): image
        obj (`_Vehicle`): detected object
        info (string): text to rendering
        plate (string): plate to rendering
        color (`np.ndarray`): color for OpenCV
    """
    if obj is not None:
        info = '{} sec'.format(obj.time_diff)
        # vehicle bbox
        cv2.putText(img,
                    info,
                    (obj.bbox.xmin, obj.bbox.ymax + 40),
                    cv2.FONT_HERSHEY_COMPLEX,
                    2,
                    obj.color,
                    3)
        cv2.rectangle(img,
                    (obj.bbox.xmin, obj.bbox.ymin),
                    (obj.bbox.xmax, obj.bbox.ymax),
                    obj.color, 2)
        if obj.plate != '':
            # license plate
            lift = 200
            text_box = [obj.bbox.xmin, obj.bbox.ymin - 35, obj.bbox.xmin + lift, obj.bbox.ymin ]
            cv2.rectangle(img, (text_box[0], text_box[1]), (text_box[2], text_box[3]), (255, 255, 0), -1)
            cv2.putText(img,
                        ' ' + obj.plate,
                        (text_box[0] + 5, text_box[3] - 10),
                        cv2.FONT_HERSHEY_SIMPLEX,
                        1e-3 * img.shape[0],
                        (0, 0, 0), 2)


class _Vehicle(object):

    STATE_NORMAL = 0
    STATE_RECORDING_START = 1
    STATE_RECORDING_END = 2
    STATE_EXPIRE = 3

    def __init__(self, xmin, ymin, xmax, ymax, label, score):
        self.bbox = BoundingBox(xmin, ymin, xmax, ymax)
        self.bbox.x_center = (xmax + xmin) // 2
        self.bbox.y_center = (ymax + ymin) // 2
        self.label = label
        self.score = score
        self.vehicle_id = None
        self.plate = ''
        self.plate_score = 0
        self.plate_bbox = None
        self.roi_idx = -1 # -1: None; 0, 1, 2, ... index of roi_list
        self.timestamp = 0
        self.timestamp_start = 0
        self.time_diff = 0
        self.state = _Vehicle.STATE_NORMAL
        self.color = COLOR_GREEN
        # for snapshot when object entered roi
        self.frame_entered = None  # (`VideoFrame`)
        self.bbox_entered = None  # (`BoundingBox`)

    def is_in_roi(self, roi_polygon):
        """Check whether a bbox is in the roi or not.

        Args:
            roi_polygon (list of `geometry.Polygon`): RoI as polygon format

        Returns:
            True if bbox is in the roi and false otherwise.
        """
        p = geometry.Point(self.bbox.x_center, self.bbox.y_center)
        return p.within(roi_polygon)

    def in_roi_area_with_ratio(self, roi_polygon, threshold=0.75):
        """Check whether a obj is in the roi area with ratio.

        Args:
            roi_polygon (list of `geometry.Polygon`): RoI as polygon format
            threshold (float): The overlap threshold.
        Returns:
            True if bbox is in the roi and false otherwise.
        """
        xmin = self.bbox.xmin
        ymin = self.bbox.ymin
        xmax = self.bbox.xmax
        ymax = self.bbox.ymax
        obj_polygon = geometry.Polygon([[xmin, ymin], [xmax, ymin],
                                        [xmax, ymax], [xmin, ymax]])
        area = self.bbox.area()
        overlap_area = roi_polygon.intersection(obj_polygon).area
        ratio = (overlap_area / area)
        return ratio > threshold

    def distance_between_obj(self, obj, distance_thr):
        """Check the distance of two objects.

        Args:
            obj (`_Vehicle`): detected object
            distance_thr (int): distance in pixel to determine if close enough
        Return:
            (tuple):
                (bool): True if object is close enough
                (float): distance between two objects
        """

        distance = math.sqrt((self.bbox.x_center - obj.bbox.x_center) ** 2 +
                             (self.bbox.y_center - obj.bbox.y_center) ** 2)
        return distance < distance_thr, distance


class _IllegalParkingDetector(object):
    """A class used to detect intrusion event.

    Attributes:
        roi_list (list of list of object): The list of region of interest with
            format of a list of tuple, such as [[(21, 33), (32, 43), ...]]
        triggers (list of string): The target of interest.
        alert_time (int): Vehicle stay time in second that trigger event
        record_time_after_alert (int): event video recording time in second
            before trigger
        frame_size (tuple): The frame size of input image frames with
            format (width, height).
    """

    def __init__(self, roi_list, triggers, alert_time,
                 record_time_after_alert,
                 frame_size):
        try:
            # Get Dask client
            self._client = get_client(address=address)
        except ValueError:
            assert False, ("Should connect to Dask scheduler before"
                           " initializing this object.")

        self._roi_list = roi_list
        self._roi_list_polygon = [geometry.Polygon(roi)
                                  for roi in self._roi_list]
        self._frame_size = frame_size
        for trigger in triggers:
            assert trigger in VALID_TRIGGER_TYPES
        self._triggers = triggers
        self._config_ratio = frame_size[1] / 1080.0
        self._distance_thr = int(100 * self._config_ratio)
        self._iou_overlap_thr = 0.2
        self.object_list = {}
        self._alert_time = alert_time
        self._record_time_after_alert = record_time_after_alert

        logging.info("Created an IllegalParkingDetector "
                     "(roi_list: {}, triggers: {})"
                     .format(self._roi_list, self._triggers))

    def __del__(self):
        self._client.close()

    @property
    def roi_list(self):
        return self._roi_list

    @roi_list.setter
    def roi_list(self, value):
        self._roi_list = value
        self._roi_list_polygon = [geometry.Polygon(roi)
                                  for roi in self._roi_list]

    def add_and_modify_object_list(self, obj, detection_plate, prediction_plate, frame):
        """Add and modify into object list

        Args:
            obj (`_Vehicle`): the object to check if existing current
                object list
            detection_plate (list of list): A list of license plates detection
                result, format of each result is [bboxes, bbox_scores].
                bbox i is stored in bboxes[i], bbox_scores i is stored in
                bbox_scores[i].
            prediction_plate (list of list): A list of license plates
                recognition result, format of each result is
                [plate_numbers, plate_scores]. plate_number i is stored in
                plate_numbers[i], plate_score i is stored in plate_scores[i].
            frame (`VideoFrame`): A raw video frame to be detected.
        """
        vehicle_id_closet, min_distance = None, sys.maxsize
        obj.timestamp = frame.timestamp  # refresh timestamp
        if len(detection_plate) > 0:
            num_bboxes = len(detection_plate[0])
            for i in range(num_bboxes):
                bbox = detection_plate[0][i]
                # Brute-force search
                if obj.bbox.iou(bbox) > 0:
                    # if bbox is in obj, assign plate
                    plate = prediction_plate[0][i]
                    plate_score = prediction_plate[1][i]
                    obj.plate = plate
                    obj.plate_score = plate_score
                    obj.plate_bbox = bbox
                    break
        for vehicle_id, obj_ol in self.object_list.items():
            is_close_enough, distance = obj.distance_between_obj(
                                            obj_ol, self._distance_thr)
            if is_close_enough:
                if obj.bbox.iou(obj_ol.bbox) > self._iou_overlap_thr:
                    if distance < min_distance:
                        min_distance = distance
                        vehicle_id_closet = vehicle_id
        if vehicle_id_closet is None:
            # assign an uuid
            vehicle_id = str(uuid.uuid4())
            obj.vehicle_id = vehicle_id
            # add obj
            self.object_list[vehicle_id] = obj
            self.object_list[vehicle_id].frame_entered = copy.deepcopy(frame)
            self.object_list[vehicle_id].bbox_entered = copy.deepcopy(obj.bbox)
        else:
            # modify obj
            vehicle_id = vehicle_id_closet
            self.object_list[vehicle_id].bbox = obj.bbox
            self.object_list[vehicle_id].label = obj.label
            self.object_list[vehicle_id].score = obj.score
            self.object_list[vehicle_id].timestamp = obj.timestamp

            if obj.plate_score > self.object_list[vehicle_id].plate_score:
                # update plate with highest score, this 
                self.object_list[vehicle_id].plate = obj.plate
                self.object_list[vehicle_id].plate_score = obj.plate_score
                self.object_list[vehicle_id].plate_bbox = obj.plate_bbox

            if self.object_list[vehicle_id].roi_idx != obj.roi_idx:
                self.object_list[vehicle_id].roi_idx = obj.roi_idx
                self.object_list[vehicle_id].timestamp_start = \
                    obj.timestamp_start

    def update_object_list(self, new_object_list, detection_plate, prediction_plate, frame):
        """ Merge new object list into object list if it is in roi list

        Args:
            new_object_list (list of `_Vehicle`): detected object in current
                frame.
            detection_plate (list of list): A list of license plates detection
                result, format of each result is [bboxes, bbox_scores].
                bbox i is stored in bboxes[i], bbox_scores i is stored in
                bbox_scores[i].
            prediction_plate (list of list): A list of license plates
                recognition result, format of each result is
                [plate_numbers, plate_scores]. plate_number i is stored in
                plate_numbers[i], plate_score i is stored in plate_scores[i].
            frame (`VideoFrame`): A raw video frame to be detected.
        """
        for _, obj in list(enumerate(new_object_list)):
            for i in range(len(self._roi_list_polygon)):
                if obj.is_in_roi(self._roi_list_polygon[i]) and \
                   obj.in_roi_area_with_ratio(self._roi_list_polygon[i]):
                    obj.roi_idx = i
                    obj.timestamp_start = frame.timestamp
                    self.add_and_modify_object_list(obj, detection_plate, prediction_plate, frame)
                    break

    def _check_trigger(self, detections, detections_plate, predictions_plate, frames):
        """Check if the detected objects is an illegal parking event.

        Args:
            detections: A list of object detection result objects, each a
                object of format [bboxes, scores, classes].
            detections_plate (list of list of list): The numbers of first dimension is
                equals to numbers of images. A list of license plates detection
                result, format of each result is [bboxes, bbox_scores].
                The i-th bbox is stored in bboxes[i], i-th bbox_score is stored in
                bbox_scores[i].
            predictions_plate (list of list of list): The numbers of first dimension is
                equals to numbers of images. A list of license plates
                recognition result, format of each result is
                [plate_numbers, plate_scores]. The i-th plate_number is stored in
                plate_numbers[i], i-th plate_score is stored in plate_scores[i].
            frames: original frames from video streaming

        Returns:
            results (list of dict): A list of dictionary that specifies the
                catched vehicle, format of each dictonary is:
                {
                    "is_triggered": True,
                    "vehicle_id": [],
                    "bboxes": [...],
                    "scores": [...],
                    "labels": [...],
                }

            rendered_frames (list of `VideoFrame`): A list of application
                rendered frames
        """
        width, height = self._frame_size

        results = []
        rendered_frames = [copy.deepcopy(frame)
                           for frame in frames]
        for i in range(len(detections)):
            (bboxes, scores, labels) = detections[i]
            new_object_list = []
            for j in range(len(bboxes)):
                bbox = bboxes[j]
                score = scores[j]
                label = labels[j]
                # Check if the object in in the trigger list.
                if label not in self._triggers:
                    continue
                obj = _Vehicle(bbox.xmin, bbox.ymin,
                               bbox.xmax, bbox.ymax, label, score)
                new_object_list.append(obj)

            self.update_object_list(new_object_list,
                                    detections_plate[i], predictions_plate[i],
                                    frames[i])
            w_cands = {
                'is_triggered': False,
                'vehicle_id': [],
                'bboxes': [],
                'scores': [],
                'labels': []
            }
            for _, obj in self.object_list.items():
                state = obj.state
                if state == _Vehicle.STATE_RECORDING_START or \
                   state == _Vehicle.STATE_RECORDING_END:
                    w_cands['is_triggered'] = True
                    normalized_bbox = [
                        float(obj.bbox.ymin) / height,
                        float(obj.bbox.xmin) / width,
                        float(obj.bbox.ymax) / height,
                        float(obj.bbox.xmax) / width
                    ]
                    w_cands['bboxes'].append(normalized_bbox)
                    w_cands['scores'].append(obj.score)
                    w_cands['labels'].append(obj.label)
                    w_cands['vehicle_id'].append(obj.vehicle_id)

                # draw objects
                _draw_object_bbox(rendered_frames[i].image, obj)
            # draw roi
            for roi in self._roi_list:
                rendered_frames[i].image = im.draw_region(
                        rendered_frames[i].image, roi, COLOR_ROI, 0.3)
            results.append(w_cands)
        return results, rendered_frames

    def _post_process_state(self, timestamp):
        """Update detected object list's state

        Args:
            timestamp: timestamp from original frame from video streaming
        """
        for vehicle_id in list(self.object_list):
            obj = self.object_list[vehicle_id]
            state = obj.state
            time_diff_start = timestamp - obj.timestamp_start
            time_diff_renew = timestamp - obj.timestamp
            is_expire = False
            if time_diff_renew > REFRESH_TIME:
                is_expire = True
            self.object_list[vehicle_id].time_diff = int(time_diff_start)
            if state == _Vehicle.STATE_NORMAL:
                if is_expire:
                    self.object_list[vehicle_id].state = \
                        _Vehicle.STATE_EXPIRE
                elif time_diff_start > self._alert_time:
                    self.object_list[vehicle_id].state = \
                        _Vehicle.STATE_RECORDING_START
            elif state == _Vehicle.STATE_RECORDING_START:
                self.object_list[vehicle_id].color = COLOR_RED
                if is_expire:
                    self.object_list[vehicle_id].state = \
                        _Vehicle.STATE_RECORDING_END

                if time_diff_start > self._alert_time + self._record_time_after_alert:
                    self.object_list[vehicle_id].state = \
                        _Vehicle.STATE_RECORDING_END
            elif state == _Vehicle.STATE_RECORDING_END:
                if time_diff_start - self._record_time_after_alert > EXPIRATION_TIME:
                    is_expire = True
                if is_expire:
                    self.object_list[vehicle_id].state = \
                        _Vehicle.STATE_EXPIRE
            elif state == _Vehicle.STATE_EXPIRE:
                self.object_list.pop(vehicle_id, None)

    def check_image_plates(self, images, f_images):
        """Catch recognized license plates.

        Args:
            images: (list of `np.ndarray`) the images to be detected.
            f_images (distributed.client.Future): The dask client future images

        Returns:
            detections (list of list of list): The numbers of first dimension
                is equals to numbers of images. A list of license plates
                detection result, format of each result is
                [bboxes, bbox_scores]. The i-th bbox is stored in bboxes[i],
                the i-th bbox_score is stored in bbox_scores[i].
                
            predictions (list of list of list): The numbers of first dimension
                is equals to numbers of images. A list of license plates
                recognition result, format of each result is
                [plate_numbers, plate_scores]. plate_number i is stored in
                plate_numbers[i], plate_score i is stored in plate_scores[i].
        """
        # Run license plate detection.
        f_detect = gpu_worker.run_model(self._client,
                                        "license_plate_detection",
                                        f_images)
        detections = f_detect.result()

        # Crop the detected license plates.
        plates_list = []
        for image, detection in zip(images, detections):
            plates = []
            bboxes = detection[0]
            for bbox in bboxes:
                w = bbox.xmax - bbox.xmin
                h = bbox.ymax - bbox.ymin
                xmin = clamp(bbox.xmin - int(0.15*w), 0, image.shape[1] - 1)
                ymin = clamp(bbox.ymin - int(0.15*h), 0, image.shape[0] - 1)
                xmax = clamp(bbox.xmax + int(0.15*w), 0, image.shape[1] - 1)
                ymax = clamp(bbox.ymax + int(0.15*h), 0, image.shape[0] - 1)
                plate = image[ymin:ymax, xmin:xmax]
                if plate is not None:
                    plates.append(plate)
            plates_list.append(plates)

        # Run license plate recognition.
        f_plates_list = self._client.scatter(plates_list)
        f_recognize = gpu_worker.run_model(self._client,
                                           "license_plate_recognition",
                                           f_plates_list,
                                           pred_threshold=0.3)
        predictions = f_recognize.result()
        return detections, predictions

    def run(self, frames, motions):
        images = [frame.image for frame in frames]
        f_images = self._client.scatter(images)
        f_detect = gpu_worker.run_model(self._client,
                                        "vehicle_detection",
                                        f_images)

        detections_plate, predictions_plate = self.check_image_plates(images, f_images)
        results = f_detect.result()
        
        catched, rendered_frames = self._check_trigger(
                results,
                detections_plate, predictions_plate,
                frames)

        event_video_frames = []
        for i in range(len(catched)):
            metadatas = {}
            base_metadata = {'mode': _Vehicle.STATE_NORMAL}
            for vehicle_id in catched[i]['vehicle_id']:
                state = self.object_list[vehicle_id].state
                metadatas[vehicle_id] = {'mode': state}
            event_video_frames.append(SharedEventVideoFrame(
                copy.deepcopy(frames[i]),
                metadatas=metadatas,
                base_metadata=base_metadata,
            ))
        # the post process state order matters
        self._post_process_state(frames[0].timestamp)
        return rendered_frames, event_video_frames

    def release(self):
        pass


class _OutputPolicy(SharedEventVideoPolicy):
    def compute(self, frame):
        actions = {}

        for key, metadata in frame.metadatas.items():
            if metadata["mode"] == _Vehicle.STATE_RECORDING_START:
                action = SharedEventVideoPolicy.START_RECORDING
            elif metadata["mode"] == _Vehicle.STATE_RECORDING_END:
                action = SharedEventVideoPolicy.STOP_RECORDING
            else:
                action = None
            actions[key] = action

        return actions


class IllegalParkingDetectionPipeline(object):
    """A class used to run the Illegal Parking Detection pipeline.

    Attributes:
        anal_id (lib.ObjectID): The ObjectID of the analyzer that the pipeline
            was attached with.
        roi_list (list of list of object): The list of region of interest with
            format of a list of tuple, such as [[{"x": 0.21, "y": 0.33},
            {"x": 0.32, "y": 0.43}, ...]]
        triggers (list of string): The target of interest.
        alert_time (int): Vehicle stay time in second that trigger event
        record_time_after_alert (int): event video recording time in second
            before trigger
        frame_size (tuple): The size of the input image frame with format
            of (width, height).
        video_format (str): The output video format.
        fps (int): The output video fps.
    """
    def __init__(self, anal_id, roi_list, triggers,
                 alert_time,
                 record_time_before_alert,
                 record_time_after_alert,
                 frame_size,
                 video_format="mp4",
                 fps_recording=15):
        self._name = 'illegal_parking_detection'
        self._anal_id = anal_id
        self._transformed_roi_list = _transform_roi_list_format(roi_list, frame_size)

        # Create an intruson detector
        self._detector = _IllegalParkingDetector(
            self._transformed_roi_list,
            triggers,
            alert_time,
            record_time_after_alert,
            frame_size)

        self._anal_id = anal_id
        self._obj_key_prefix = os.path.join(self._name, anal_id)

        # Create output video agent.
        event_video_metadata = {
            "event_name": self._name,
            "event_custom": {'roi_list': self._transformed_roi_list},
        }

        record_time = record_time_before_alert + record_time_after_alert
        q_size = int(fps_recording * record_time)
        self._output_agent = SharedPersistentEventVideoAgent(
            anal_id,
            _OutputPolicy(),
            event_video_metadata,
            self._obj_key_prefix,
            q_size,
            frame_size,
            fps_recording,
            video_format)

        # Connect to Object Storage service.
        self._obj_storage = ObjectStorageClient()
        self._obj_storage.connect()

        # Connect to Notification service.
        self._notification = Notification()

        # Connect to Database service.
        self._database = Database()

    def _take_snapshot(self, filename, frame):
        """Save a frame to an image file and push it to the object storage.

        Args:
            filename (string): The name of the snapshot.
            frame (`VideoFrame`): The frame object to be saved.

        Returns:
            string: The key of the snapshot in the object storage.
        """
        thumbnail_key = os.path.join(self._obj_key_prefix, "{}.jpg"
                                     .format(filename))
        shrunk_image = im.shrink_image(frame.image)
        self._obj_storage.save_image_obj(thumbnail_key, shrunk_image)

        return thumbnail_key

    def _output_event(self, event, vehicle_id):
        """Output event to notification center and database.

        Args:
            event (`AgentEvent`): The event object to be outputted.
            vehicle_id (string): The object ID of vehicle
        """
        frame_entered = self._detector.object_list[vehicle_id].frame_entered

        # Metadata for snapshot
        label = self._detector.object_list[vehicle_id].label
        score = self._detector.object_list[vehicle_id].score
        plate = self._detector.object_list[vehicle_id].plate
        plate_score = self._detector.object_list[vehicle_id].plate_score
        triggered = {'type': label, 'plate': plate}
        bbox = self._detector.object_list[vehicle_id].bbox_entered.tolist()
        metadata_thumbnail = {
            "illegal_parking_detection": {
                "timestamp": frame_entered.timestamp,
                "frame": {
                    "bbox": bbox,
                    "bbox_label": label,
                    "bbox_score": score,
                    "plate_number": plate,
                    "plate_score": plate_score
                },
                "custom": {
                    "roi_list": self._transformed_roi_list
                }
            }
        }
        metadata_thumbnail_key = self._save_meta_data(
            os.path.join(vehicle_id, str(frame_entered.timestamp + 1)),
            metadata_thumbnail)
        # TODO prevent metadata filename conflict

        # Image for snapshot
        thumbnail_filename_entered = os.path.join(
            vehicle_id,
            str(frame_entered.timestamp))
        thumbnail_key_entered = self._take_snapshot(
            thumbnail_filename_entered,
            frame_entered)
        thumbnail = {'entered': thumbnail_key_entered}

        # Event content
        timestamp = event.content["timestamp"]
        event_type = "{}.alert".format(self._name)
        content = {
            "video": event.content["video_key"],
            "metadata_video": event.content["metadata_key"],
            "metadata_thumbnail": metadata_thumbnail_key,
            "thumbnail": thumbnail,
            "triggered": triggered,
        }

        # Save event to database.
        event_id = self._database.save_event(self._anal_id,
                                             timestamp,
                                             event_type,
                                             content)
        logging.info("Success to save event in database: _id: {}, analyzerId: "
                     "{}, timestamp: {}, type: {}, content: {}"
                     .format(event_id,
                             self._anal_id,
                             timestamp,
                             event_type,
                             content))

        # Push notification.
        self._notification.push_anal(event_id,
                                     self._anal_id,
                                     timestamp,
                                     event_type,
                                     content)

    def _save_meta_data(self, filename, meta_data):
        metadata_key = os.path.join(self._obj_key_prefix, "{}.json"
                                    .format(filename))
        self._obj_storage.save_json_obj(metadata_key, meta_data)
        return metadata_key

    def run(self, frames, motions, is_src_reader_busy):
        """Run Illegal Parking Detection pipeline.

        Args:
            frames: A list of raw video frames to be detected.
            motions: The motion of the input frames. It should be the output of
                video_proc.detect_motion().
        """
        rendered_frames, event_video_frames = self._detector.run(
                frames, motions)
        for event_video_frame in event_video_frames:
            out_event = self._output_agent.process(event_video_frame)
            for vehicle_id, agent_event in out_event.agent_events.items():
                if agent_event.action == \
                        SharedEventVideoPolicy.START_RECORDING:
                    self._output_event(agent_event, vehicle_id)
                elif agent_event.action == \
                        SharedEventVideoPolicy.STOP_RECORDING:
                    logging.info("End of event video for {}".format(
                        vehicle_id))
        return rendered_frames

    def release(self):
        self._detector.release()
        self._output_agent.release()
