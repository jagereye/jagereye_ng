from license_plate_recognition import LicensePlateRecognitionPipeline
from license_plate_recognition import LICENSE_PLATE_FORMAT


class SingleCameraLicensePlateRecognitionPipeline(LicensePlateRecognitionPipeline):
    """A class used to run the Single Camera Licnse Plate Recognition pipeline.

    Attributes:
        anal_id (lib.ObjectID): The ObjectID of the analyzer that the pipeline
            was attached with.
        triggers (list of dict): The target of interest. Each item contains
            following key and value:
                "group" (lib.ObjectID): The group ID that the license plate belongs to.
                    Defaults to "".
                "owner" (string): The owner of the licese plate.
                "number" (string): The licese plate number.
        frame_size (tuple): The size of the input image frame with format
            of (width, height).
        video_format (string): The output video format.
        fps (int): The output video fps.
        history_len (int): The length, in seconds, of frame history queue. This
            determines when the agent starts to record before policy returning
            action "START_RECORDING".
    """
    def __init__(self,
                 anal_id,
                 triggers,
                 frame_size,
                 video_format="mp4",
                 fps=15,
                 history_len=3):
        super().__init__(LICENSE_PLATE_FORMAT.PLAIN,
                         "single_camera_license_plate_recognition",
                         anal_id,
                         triggers,
                         frame_size,
                         video_format=video_format,
                         fps=15,
                         history_len=3)
