import os
from enum import Enum
from math import modf
from concurrent.futures import ThreadPoolExecutor
from threading import Timer

import cv2
import numpy as np
from dask.distributed import get_client
from jagereye_ng import image as im
from jagereye_ng import gpu_worker
from jagereye_ng.gpu_worker.face import crop_align_faces
from jagereye_ng.gpu_worker.face import match_faces
from jagereye_ng.io.database import Database
from jagereye_ng.io.const import ONE_SHOT_STATUS
from jagereye_ng.io.notification import Notification
from jagereye_ng.io.obj_storage import ObjectStorageClient
from jagereye_ng.io.streaming import VideoFrame
from jagereye_ng import logging
from jagereye_ng.util.generic import get_config
from jagereye_ng.util.generic import uuid

from const import NORMAL_BBOX_DRAW_COLOR
from const import TRIGGERED_BBOX_DRAW_COLOR
from const import BBOX_DRAW_THICKNESS
from const import ROI_DRAW_ALPHA
from const import ROI_DRAW_COLOR
from events import EventVideoFrame
from roi import ROIList


config = get_config()["apps"]["dask"]
address = config["cluster_ip"] + ":" + config["scheduler_port"]


NAME_TEXT_FONT = cv2.FONT_HERSHEY_COMPLEX
NAME_TEXT_SCALE = 0.7
NAME_TEXT_THICKNESS = 2
NAME_TEXT_COLOR = (255, 255, 255)


class _AnalysisSampler(object):
    """Sampling helper for analysis.

    Attributes:
        fps (int): FPS (frame per second) to sample frames.
    """

    def __init__(self, fps):
        self._fps = fps

        self._interval = 1.0 / fps
        self._slots = [self._create_slot() for _ in range(self._fps)]

    def _create_slot(self, ts_int=-1, frames=[], raw=[]):
        return {
            "ts_int": ts_int,
            "frames": frames,
            "raw": raw,
        }

    def _get_slot_idx(self, ts_frac):
        return int(ts_frac / self._interval)

    def get_raw(self, slot_idx):
        return self._slots[slot_idx]["raw"]

    def update_raw(self, slot_idx, raw):
        self._slots[slot_idx]["raw"] = raw

    def sample(self, frames):
        """Sample candidates from given frames.

        Args:
            frames (list of `VideoFrame`): Frames to be sampled.

        Returns:
            list of dict: A list that indicates each frame is a sampled
                candidate or not. Each item in the list is an dictionary object
                correspoding to a frame that has such following format:
                "slot_idx": The index of timestamp slot that the frame
                            belongs to.
                "frame_idx": The index of frame index in the list.
                "is_sampled": The frame is sampled or not.
        """
        samples = []

        for frame_idx, frame in enumerate(frames):
            # Split the timstamp into two parts: (fractional, integer).
            # For exmaple, if timestamp = 3.41, then it will become (0.41, 3).
            ts_frac, ts_int = modf(frame.timestamp)
            slot_idx = self._get_slot_idx(ts_frac)
            is_sampled = ts_int > self._slots[slot_idx]["ts_int"]

            if is_sampled:
                self._slots[slot_idx] = self._create_slot(
                    ts_int=ts_int,
                    frames=[frame],
                )
            else:
                self._slots[slot_idx]["frames"].append(frame)

            samples.append({
                "slot_idx": slot_idx,
                "frame_idx": frame_idx,
                "is_sampled": is_sampled,
            })

        return samples


class _ApperanceTraker(object):
    """Tracker of objects appearances.

    Attributes:
        outdated_period (int): The period to be outdated (in seconds).
    """

    def __init__(self, outdated_period):
        self._outdated_period = outdated_period

        self._appearances = {}

    def appear_recently(self, obj_id):
        return obj_id in self._appearances

    def update_appearance(self, obj_id, timestamp):
        self._appearances[obj_id] = {
            "timestamp": timestamp,
        }

    def clear_outdated(self, cur_timestamp):
        for obj_id, appearance in list(self._appearances.items()):
            elapsed = cur_timestamp - appearance["timestamp"]
            if elapsed > self._outdated_period:
                del self._appearances[obj_id]


class _StrangerApperanceTraker(_ApperanceTraker):
    """Tracker of strangers appearances.

    Attributes:
        outdated_period (int): The period to be outdated (in seconds).
        recog_thold (float): Face recognition threshold, range from 0 to 1.
        max_per_features (int): The maximum number of recent features that a
            stranger will record. Defaults to 5.
    """

    def __init__(self, outdated_period, recog_thold, max_per_features=5):
        super().__init__(outdated_period)

        self._recog_thold = recog_thold
        self._max_per_features = max_per_features

    def match(self, feature):
        matched_id = None
        max_score = 0.0

        for stranger_id, appearance in self._appearances.items():
            scores = np.dot(feature, np.array(appearance["features"]).T)
            score = max(scores)
            if score > self._recog_thold and score > max_score:
                matched_id = stranger_id
                max_score = score

        return matched_id

    def update_appearance(self, stranger_id, timestamp, feature):
        if stranger_id not in self._appearances:
            features = [feature]
        else:
            features = self._appearances[stranger_id]["features"]
            if len(features) >= self._max_per_features:
                features = features[1:]
            features.append(feature)

        self._appearances[stranger_id] = {
            "timestamp": timestamp,
            "features": features,
        }


class _FaceRecognizer(object):
    """A class used to recognize faces events for given list.

    Attributes:
        external_type (string): The type name of face externals.
        triggers (string, list of string, list of `bson.objectid.ObjectId`): The
            target of interest.
        triggers_field (string): The field name to get triggers from externals.
        analysis_fps (int): FPS (frame per second) to analyze the input video
            frames. Note that this is different from source FPS and analysis FPS
            is usually lower than source FPS (e.g., source FPS = 15, analysis
            FPS = 5).
        roiList (`roi.ROIList`): Object to represent a list of regions of
            interest (roi).
        face_min_width(int): The minimum face width for detection and
            recognition (in pixel).
        face_min_height(int): The minimum face height for detection and
            recognition (in pixel).
        detn_thold (float): Face detection threshold, range from 0 to 1.
        recog_thold (float): Face recognition threshold, range from 0 to 1.
        next period (int): The period (in seconds) to recognize the same person
            (who was recognized for a while) again.
        output_non_matched (bool): Whether to output faces that are not in the
            database or not.
    """
    SYNC_INTERVAL = 3

    def __init__(self,
                 external_type,
                 triggers,
                 triggers_field,
                 analysis_fps,
                 roi_list,
                 face_min_width,
                 face_min_height,
                 detn_thold,
                 recog_thold,
                 next_period,
                 output_non_matched):
        try:
            # Get Dask client
            self._client = get_client(address=address)
        except ValueError:
            assert False, ("Should connect to Dask scheduler before"
                           " initializing this object.")

        # Connect to Database service.
        self._database = Database()

        self._roi_list = roi_list
        self._face_min_width = face_min_width
        self._face_min_height = face_min_height
        self._detn_thold = detn_thold
        self._recog_thold = recog_thold
        self._next_period = next_period
        self._output_non_matched = output_non_matched

        self._recog_scale = 0.9 / (0.6 ** 0.5)
        # Rescale the input threholds.
        self._input_detn_thold = \
            self._rescale_input_detn_thold(self._detn_thold)
        self._input_recog_thold = \
            self._rescale_input_recog_thold(self._recog_thold)

        self._analysis_sampler = _AnalysisSampler(analysis_fps)

        # Create a appearance tracker for faces.
        self._appearance_tracker = _ApperanceTraker(self._next_period)

        # Create a appearance tracker for strangers if non-matched outputs are
        # needed.
        if self._output_non_matched:
            self._stranger_apperance_traker = _StrangerApperanceTraker(
                self._next_period,
                self._input_recog_thold,
            )

        # Synchronize with the face externals repeatly.
        self._sync_timer = None
        self._triggers = None
        self._update_triggers(external_type, triggers, triggers_field)

    def __del__(self):
        self._client.close()

    def _rescale_input_detn_thold(self, detn_thold):
        if detn_thold <= 0.9:
            return detn_thold / (0.9 / 0.5)
        else:
            return 0.5 + (detn_thold - 0.9) * 5

    def _rescale_input_recog_thold(self, recog_thold):
        return (recog_thold / self._recog_scale) ** 2

    def _rescale_output_detn_score(self, detn_score):
        if detn_score <= 0.5:
            return detn_score * (0.9 / 0.5)
        else:
            return 0.9 + (detn_score - 0.5) / 5

    def _rescale_output_recog_score(self, recog_score):
        return min(0.999999, (recog_score ** 0.5) * self._recog_scale)

    def _update_triggers(self, external_type, triggers, triggers_field):
        self._triggers = self._transform_triggers(external_type,
                                                  triggers,
                                                  triggers_field)
        self._sync_timer = Timer(_FaceRecognizer.SYNC_INTERVAL,
                                 self._update_triggers,
                                 [external_type, triggers, triggers_field])
        self._sync_timer.start()

    def _transform_triggers(self, external_type, triggers, triggers_field):
        query = {
            "type": external_type,
            "task_status": ONE_SHOT_STATUS.DONE,
        }
        query[triggers_field] = triggers

        details = self._database.query_externals(query).result()

        # Get id and feature for each face.
        ids = []
        features = []
        names = []
        for detail in details:
            ids.append(detail["_id"])
            features.append(detail["content"]["feature"])
            names.append(detail["content"]["name"])

        return {
            "ids": ids,
            "features": np.array(features),
            "names": names,
        }

    def _match_triggers(self, features):
        if not self._triggers["ids"] or not features:
            return np.array([]), []

        scores_matrix, matches_matrix = match_faces(
            features,
            self._triggers["features"],
            threshold=self._input_recog_thold,
        )

        return scores_matrix, matches_matrix

    def _find_triggers_indexes(self,
                               scores_matrix,
                               matches_matrix,
                               max_only=True):
        matched_indexes = np.argwhere(matches_matrix == True).tolist()

        if not max_only:
            return matched_indexes
        else:
            max_matched_indexes = {}
            for matched_index in matched_indexes:
                detect_idx = matched_index[0]
                trigger_idx = matched_index[1]
                score = scores_matrix[detect_idx][trigger_idx]
                if (detect_idx not in max_matched_indexes or
                    score > max_matched_indexes[detect_idx]["score"]):
                    max_matched_indexes[detect_idx] = {
                        "trigger_idx": trigger_idx,
                        "score": score,
                    }
            max_matched_indexes = [[k, v["trigger_idx"]]
                                   for k, v in max_matched_indexes.items()]

            return max_matched_indexes

    def _find_non_triggers_indexes(self, scores_matrix, matches_matrix):
        non_matched_indexes = []

        for idx in range(scores_matrix.shape[0]):
            if not np.any(matches_matrix[idx]):
                non_matched_indexes.append([idx, np.argmax(scores_matrix[idx])])

        return non_matched_indexes

    def _catch_triggers(self, detections, features_list, timestamps):
        """Catch recognized faces that are in the target of interest.

        Args:
            detections (list of list): A list of faces detection result, format
                of each result is (bboxes, landmarks, bbox_scores).
            features_list (list of list of `numpy.ndarray`): A list of features of
                detected faces.
            timestamps (list of float): A list of timestamp for each faces
                detection result.

        Returns:
            list of dict, list of list of dict: The first is a list of
                dictionary that specifies the catched faces, format of each
                dictonary is:
                {
                    "captures": [{
                        "external": ...,
                        "bbox": ...,
                        "bbox_score": ...,
                        "recog_score": ...,
                        "matched": ...,
                    }, ...],
                }
                The second is a list of detected faces list, format of each
                detected face is:
                {
                    "bbox": ...,
                    "name": ...,
                }
        """
        results = []
        raws = []
        for detection, features, timestamp in zip(detections, features_list, timestamps):
            bboxes, _, bbox_scores = detection
            recog_scores_matrix , matches_matrix = self._match_triggers(features)
            matched_indexes = self._find_triggers_indexes(
                recog_scores_matrix,
                matches_matrix,
            )

            raw = [{ "bbox": bbox, "name": None } for bbox in bboxes]

            result = { "captures": [] }

            for matched_index in matched_indexes:
                detect_idx = matched_index[0]
                trigger_idx = matched_index[1]
                external = self._triggers["ids"][trigger_idx]
                name = self._triggers["names"][trigger_idx]

                if not self._appearance_tracker.appear_recently(external):
                    bbox = bboxes[detect_idx]
                    bbox_score = bbox_scores[detect_idx]
                    bbox_score = self._rescale_output_detn_score(bbox_score)
                    recog_score = recog_scores_matrix[detect_idx][trigger_idx]
                    recog_score = self._rescale_output_recog_score(recog_score)

                    result["captures"].append({
                        "external": external,
                        "bbox": bbox,
                        "bbox_score": bbox_score,
                        "recog_score": recog_score,
                        "matched": True,
                    })

                raw[detect_idx]["name"] = name

                self._appearance_tracker.update_appearance(external, timestamp)

            if self._output_non_matched:
                non_matched_indexes = self._find_non_triggers_indexes(
                    recog_scores_matrix,
                    matches_matrix,
                )

                for non_matched_index in non_matched_indexes:
                    detect_idx = non_matched_index[0]
                    trigger_idx = non_matched_index[1]
                    external = self._triggers["ids"][trigger_idx]
                    feature = features[detect_idx]
                    stranger_id = self._stranger_apperance_traker.match(feature)
                    stranger_id = uuid() if stranger_id is None else stranger_id

                    if not self._stranger_apperance_traker.appear_recently(stranger_id):
                        bbox = bboxes[detect_idx]
                        bbox_score = bbox_scores[detect_idx]
                        bbox_score = self._rescale_output_detn_score(bbox_score)
                        recog_score = recog_scores_matrix[detect_idx][trigger_idx]
                        recog_score = self._rescale_output_recog_score(recog_score)

                        result["captures"].append({
                            "external": external,
                            "bbox": bbox,
                            "bbox_score": bbox_score,
                            "recog_score": recog_score,
                            "matched": False,
                        })

                    self._stranger_apperance_traker.update_appearance(
                        stranger_id,
                        timestamp,
                        feature,
                    )

            results.append(result)

            raws.append(raw)

            self._appearance_tracker.clear_outdated(timestamp)

            if self._output_non_matched:
                self._stranger_apperance_traker.clear_outdated(timestamp)

        return results, raws

    def _render_frames(self, frames, raws):
        rendered_frames = []

        # Draw detected faces on the frame.
        for frame, raw in zip(frames, raws):
            drawn_image = frame.image.copy()
            for detected_face in raw:
                bbox = detected_face["bbox"]
                name = detected_face["name"]
                is_triggered = name is not None
                color = TRIGGERED_BBOX_DRAW_COLOR \
                        if is_triggered \
                        else NORMAL_BBOX_DRAW_COLOR

                drawn_image = im.draw_bbox(drawn_image,
                                           bbox,
                                           color,
                                           BBOX_DRAW_THICKNESS)

                if is_triggered:
                    (text_width, text_height) = cv2.getTextSize(
                        name,
                        NAME_TEXT_FONT,
                        fontScale=NAME_TEXT_SCALE,
                        thickness=NAME_TEXT_THICKNESS,
                    )[0]
                    text_x = bbox.xmin
                    text_y = bbox.ymax - 4
                    cv2.rectangle(
                        drawn_image,
                        (text_x, text_y + 2),
                        (text_x + text_width - 2, text_y - text_height - 2),
                        color,
                        cv2.FILLED,
                    )
                    drawn_image = cv2.putText(
                        drawn_image,
                        name,
                        (text_x, text_y),
                        NAME_TEXT_FONT,
                        NAME_TEXT_SCALE,
                        NAME_TEXT_COLOR,
                        NAME_TEXT_THICKNESS,
                    )

            rendered_frames.append(VideoFrame(drawn_image, frame.timestamp))

        return rendered_frames

    def run(self, frames, motions):
        catches = [{ "captures": [] }] * len(frames)
        raws = [[]] * len(frames)
        samples = self._analysis_sampler.sample(frames)
        sampled_indexes = [s["frame_idx"] for s in samples if s["is_sampled"]]

        if motions["frames"] and sampled_indexes:
            sampled_frames = [frames[idx] for idx in sampled_indexes]
            images = [frame.image for frame in sampled_frames]
            timestamps = [frame.timestamp for frame in sampled_frames]

            # Run face detection.
            f_images = self._client.scatter(images)
            f_detected = gpu_worker.run_model(
                self._client,
                "face_detection",
                f_images,
                min_width=self._face_min_width,
                min_height=self._face_min_height,
                score_threshold=self._input_detn_thold,
            )
            detections = f_detected.result()

            # Crop faces for each image.
            faces_list = []
            face_detected = False
            for image, detection in zip(images, detections):
                bboxes, landmarks, _ = detection

                face_detected = face_detected or bool(bboxes)

                if self._roi_list.is_empty():
                    bboxes_in = bboxes
                    landmarks_in = landmarks
                else:
                    bboxes_in = []
                    landmarks_in = []
                    for bbox, landmark in zip(bboxes, landmarks):
                        if self._roi_list.is_in(bbox):
                            bboxes_in.append(bbox)
                            landmarks_in.append(landmark)

                faces = crop_align_faces(image, bboxes_in, landmarks_in)
                faces_list.append(faces)

            if not face_detected:
                features_list = [[]] * len(images)
            else:
                # Calculate feature for each face.
                f_faces_list = self._client.scatter(faces_list)
                f_recognized = gpu_worker.run_model(self._client,
                                                    "face_recognition",
                                                    f_faces_list)
                features_list = f_recognized.result()

            # Catches faces that are in triggers list.
            sampled_catches, sampled_raws = self._catch_triggers(
                detections,
                features_list,
                timestamps,
            )

            # Update the catched list.
            for catch, idx in zip(sampled_catches, sampled_indexes):
                catches[idx] = catch

            # Update the raw list for drawing.
            for raw, idx in zip(sampled_raws, sampled_indexes):
                sample = samples[idx]
                self._analysis_sampler.update_raw(sample["slot_idx"], raw)
            for sample in samples:
                raw = self._analysis_sampler.get_raw(sample["slot_idx"])
                raws[sample["frame_idx"]] = raw

        output_frames = []
        for frame, catch in zip(frames, catches):
            output_frames.append(EventVideoFrame(frame, catch))

        rendered_frames = self._render_frames(frames, raws)

        return output_frames, rendered_frames

    def release(self):
        if self._sync_timer is not None:
            self._sync_timer.cancel()


class FaceRecognitionPipeline(object):
    """A class used to run the Single Camera Face Recognition pipeline.

    Attributes:
        name (string): The pipeline name.
        anal_id (lib.ObjectID): The ObjectID of the analyzer that the pipeline
            was attached with.
        triggers (string, `bson.objectid.ObjectId`, list of string, list of
            `bson.objectid.ObjectId`): The target of interest.
        triggers_field (string): The field name of triggers in externals.
        analysis_fps (int): FPS (frame per second) to analyze the input video
            frames. Note that this is different from source FPS and analysis FPS
            is usually lower than source FPS (e.g., source FPS = 15, analysis
            FPS = 5).
        roi_list (list of list of object): A list that contains zero, one or
            more region(s) of interest (roi). The format of each roi is a list
            of object points, such as [{"x": 0.21, "y": 0.33},
            {"x": 0.32, "y": 0.43}, ...]
        face_min_width(int): The minimum face width for detection and
            recognition (in pixel).
        face_min_height(int): The minimum face height for detection and
            recognition (in pixel).
        detn_thold (float): Face detection threshold, range from 0 to 1.
        recog_thold (float): Face recognition threshold, range from 0 to 1.
        next period (int): The period (in seconds) to recognize the same person
            (who was recognized for a while) again.
        output_non_matched (bool): Whether to output faces that are not in the
            database or not.
        frame_size (tuple): The size of the input image frame with format
            of (width, height).
    """
    def __init__(self,
                 name,
                 anal_id,
                 triggers,
                 triggers_field,
                 analysis_fps,
                 roi_list,
                 face_min_width,
                 face_min_height,
                 detn_thold,
                 recog_thold,
                 next_period,
                 output_non_matched,
                 frame_size):

        self._name = name
        self._anal_id = anal_id
        self._obj_key_prefix = os.path.join(name, anal_id)
        self._analysis_fps = analysis_fps
        self._roi_list = ROIList(roi_list, frame_size)

        # Create a face recognizer.
        self._recognizer = _FaceRecognizer("{}.face".format(name),
                                           triggers,
                                           triggers_field,
                                           self._analysis_fps,
                                           self._roi_list,
                                           face_min_width,
                                           face_min_height,
                                           detn_thold,
                                           recog_thold,
                                           next_period,
                                           output_non_matched)

        # Create a thread pool executor to handle captured events.
        self._capture_executor = ThreadPoolExecutor(max_workers=1)

        # Connect to Object Storage service.
        self._obj_storage = ObjectStorageClient()
        self._obj_storage.connect()

        # Connect to Notification service.
        self._notification = Notification()

        # Connect to Database service.
        self._database = Database()

    def _take_snapshot(self, filename, frame, bbox):
        """Save a frame to an image file and push it to the object storage.

        Args:
            filename (string): The name of the snapshot.
            frame (`EventVideoFrame`): The frame object to be saved.
            bbox (`jagereye_ng.shape.BoundingBox`): The boudning box of
                recognized face.

        Returns:
            string: The key of the snapshot in the object storage.
        """
        snapshot_key = os.path.join(self._obj_key_prefix, "{}.jpg"
                                    .format(filename))
        drawn_image = self._roi_list.draw_on(frame.image,
                                             ROI_DRAW_COLOR,
                                             ROI_DRAW_ALPHA)
        drawn_image = im.draw_bbox(drawn_image,
                                   bbox,
                                   TRIGGERED_BBOX_DRAW_COLOR,
                                   BBOX_DRAW_THICKNESS)
        shrunk_image = im.shrink_image(drawn_image)
        self._obj_storage.save_image_obj(snapshot_key, shrunk_image)

        return snapshot_key

    def _crop_face(self, filename, frame, bbox):
        """Crop a face from a frame and save it to the object storage.

        Args:
            filename (string): The name of the cropped face.
            frame (`EventVideoFrame`): The frame object to be saved.
            bbox (`jagereye_ng.shape.BoundingBox`): The boudning box of cropped
                region.

        Returns:
            string: The key of the cropped face in the object storage.
        """
        face_key = os.path.join(self._obj_key_prefix, "{}.jpg".format(filename))
        face_image = im.crop(frame.image, bbox)
        self._obj_storage.save_image_obj(face_key, face_image)

        return face_key

    def _output_event(self,
                      capture,
                      capture_scene_key,
                      capture_face_key,
                      timestamp):
        """Output event to notification center and database.

        Args:
            capture (dict): Details about the event.
            capture_scene_key (string): The key of the captured scene image in
                the object storage.
            capture_face_key (string): The key of the captured face image in the
                object storage.
            timestamp (float): The timestamp of the event.
        """
        event_type = "{}.alert".format(self._name)
        content = {
            "capture": {
                "scene": capture_scene_key,
                "face": capture_face_key,
            },
            "detection": {
                "bbox": capture["bbox"].tolist(),
                "confidence": capture["bbox_score"],
            },
            "recognition": {
                "confidence": capture["recog_score"],
            },
            "triggered": capture["external"],
            "matched": capture["matched"],
        }

        # Save event to database.
        event_id = self._database.save_event(self._anal_id,
                                             timestamp,
                                             event_type,
                                             content).result()
        logging.info("Success to save event in database: _id: {}, analyzerId: "
                     "{}, timestamp: {}, type: {}, content: {}"
                     .format(event_id,
                             self._anal_id,
                             timestamp,
                             event_type,
                             content))
        # Check group settings for notification                    
        external = self._database.get_external(capture['external']).result()
        group = self._database.get_external_group(external['group']).result()
        analyzer = self._database.get_analyzer(self._anal_id).result()

        notification = {}

        for notify, enabled in group['notification'].items():
            if notify == 'line' and enabled == True and capture["matched"]:
                notification['line'] = {
                    "name": external['content']['name'],
                    "camera": analyzer['name'],
                    "face": capture_face_key,
                    "scene": capture_scene_key
                }
        
        # Push notification.
        self._notification.push_anal(event_id,
                                     self._anal_id,
                                     timestamp,
                                     event_type,
                                     content,
                                     notification)

    def _handle_capture(self, timestamp, capture, recognition):
        try:
            capture_scene_filename = os.path.join(
                capture["external"],
                "{}_scene".format(timestamp),
            )
            capture_scene_key = self._take_snapshot(
                capture_scene_filename,
                recognition,
                capture["bbox"],
            )
            capture_face_filename = os.path.join(
                capture["external"],
                "{}_face".format(timestamp),
            )
            capture_face_key = self._crop_face(
                capture_face_filename,
                recognition,
                capture["bbox"],
            )
            self._output_event(capture,
                               capture_scene_key,
                               capture_face_key,
                               timestamp)
        except Exception as e:
            logging.exception(e)
            raise e

    def run(self, frames, motions, is_src_reader_busy):
        recognitions, rendered_frames = self._recognizer.run(frames, motions)

        for recognition in recognitions:
            timestamp = recognition.timestamp
            for capture in recognition.metadata["captures"]:
                # The function "_handle_capture" many blocking IO operations, so
                # we use a thread pool executor to handle it.
                self._capture_executor.submit(self._handle_capture,
                                              timestamp,
                                              capture,
                                              recognition)

        return rendered_frames

    def release(self):
        self._recognizer.release()
