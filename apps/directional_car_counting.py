import os

from dask.distributed import get_client
import numpy as np

from jagereye_ng import gpu_worker
from jagereye_ng import logging
from jagereye_ng.io.notification import Notification
from jagereye_ng.io.database import Database
from jagereye_ng.util.generic import get_config
from jagereye_ng.util.generic import uuid

from events import EventVideoFrame


config = get_config()["apps"]["dask"]
address = config["cluster_ip"] + ":" + config["scheduler_port"]


def transform_lines_format(lines, frame_size):
    """Transform lines format

    Transforms rules:
        absolution point value = relative point value * corresponding frame size

    Examples:
        Assume frame size is (100, 100),
        Expect source format (list of dict):
            [{"x1": 0.21, "y1": 0.33, "x2": 0.32, "y2": 0.43}, ...]. each
            value means scaled x and y position, 0 <= x1, y1, x2, y2 < 1.

        Transformed lines format (list of tuple):
            np.array([21, 33, 32, 43])

    Args:
        lines (list of dict): Input line to be transformed.
        frame_size (tuple): The frame size of input image frames with
            format (width, height).

    Returns:
        (`numpy.ndarray`)
    """
    transformed_lines = []
    line_target = ["x1", "y1", "x2", "y2"]
    for i in range(len(lines)):
        for lt in line_target:
            if lines[i].get(lt) is not None:
                if lines[i][lt] < 0 or lines[i][lt] > 1:
                    raise ValueError("Invalid line format, should be a float with "
                                        "value between 0 and 1.")
            else:
                raise ValueError("Invalid line format, {} not found".format(lt))
        transformed_lines.append([
                float(lines[i]["x1"]) * float(frame_size[0]), \
                float(lines[i]["y1"]) * float(frame_size[1]), \
                float(lines[i]["x2"]) * float(frame_size[0]), \
                float(lines[i]["y2"]) * float(frame_size[1]) ])

    return np.array(transformed_lines)


class DirectionalCarCounting(object):
    """A class used to detect directional car counting event.

    Attributes:
        paths (list of dict): The paths for counting target. Each path is a
            dictionary must has following fields:
            "name" (string): The path name.
            "start": (`numpy.ndarray`): The starting line. An one-dimensional
                array that contains 4 floating values (x1, y1, x2, y2).
            "end": (`numpy.ndarray`): The ending line. An one-dimensional array
                that contains 4 floating values (x1, y1, x2, y2).
        triggers (list of string): The target of interest object.
        frame_size (tuple): The frame size of input image frames with
            format (width, height).
    """

    def __init__(self, paths, triggers, frame_size):
        try:
            # Get Dask client
            self._client = get_client(address=address)
        except ValueError:
            assert False, ("Should connect to Dask scheduler before"
                           " initializing this object.")

        self._paths = paths
        self._triggers = triggers
        self._frame_size = frame_size

        # Detailed information for each lane.
        self._lane_info_list = []
        for idx, path in enumerate(paths):
            self._lane_info_list.append({
                "is_start": True,
                "peer_idx": 2 * idx + 1,
                "path_name": path["name"],
            })
            self._lane_info_list.append({
                "is_start": False,
                "peer_idx": 2 * idx,
                "path_name": path["name"],
            })

        # Gather lines from paths.
        lines = []
        for path in paths:
            lines.append(path["start"])
            lines.append(path["end"])
        lines = np.array(lines)

        # Coefficients for each line.
        self._length_of_lines = self._get_length_of_lines(lines)
        self._line_function_coeff, self._ortho_function_coeff = \
            self._get_linear_func_coeff(lines)

        # Tracking object list.
        self._object_list = {}

        # Period (in seconds) to expire old objects in the tracking object
        # list.
        self._object_expired_period = 0.3

        # Object checking parameters based on 720p resolution.
        self._config_ratio = frame_size[1] / 720.0
        self._counting_check_dist = int(40 * self._config_ratio)
        self._dist_thr = int(60 * self._config_ratio)
        self._dist = int(1000 * self._config_ratio)

    def __del__(self):
        if self._client:
            self._client.close()

    def _get_length_of_lines(self, lines):
        """Get length of lines.

        Args:
            lines (`numpy.ndarray`): The lines for counting target, for example:
                numpy.array([[x1, y1, x2, y2]]). x1, y1, x2, y2 are in pixel
        Returns:
            (list of float): each row length of line in pixel
        """
        length_of_lines = []

        for line in lines:
            x1, y1, x2, y2 = line
            length_of_lines.append(np.sqrt((x2 - x1) ** 2 + (y2 - y1) ** 2))

        return length_of_lines

    def _get_linear_func_coeff(self, lines):
        """Get line and ortho math function coeff vector.

        Args:
            lines (`numpy.ndarray`): The lines for counting target, for example:
                numpy.array([[x1, y1, x2, y2]]). x1, y1, x2, y2 are in pixel
        Returns:
            (`numpy.ndarray`, `numpy.ndarray`):
                normalized linear function, line and ortho. each row corresponds
                line in lines, contains [ coeff_x, coeff_y, const ]
        """
        line_function_coeff = []
        ortho_function_coeff = []

        for line in lines:
            x1, y1, x2, y2 = line
            x_, y_ = (x1 + x2) / 2, (y1 + y2) / 2
            coeff_x, coeff_y, const = \
                -(y2 - y1), x2 - x1, (y2 - y1) * x1 - (x2 - x1) * y1
            ortho_coeff_x, ortho_coeff_y, ortho_const = \
                x2 - x1, y2 - y1, - (y2 - y1) * y_ - (x2 - x1) * x_
            # Normalize
            normalizer1 = np.sqrt(coeff_x ** 2 + coeff_y ** 2)
            normalizer2 = np.sqrt(ortho_coeff_x ** 2 + ortho_coeff_y ** 2)

            line_function_coeff.append(
                    np.array([coeff_x, coeff_y, const]) / normalizer1)
            ortho_function_coeff.append(
                    np.array([ortho_coeff_x, ortho_coeff_y, ortho_const]) / normalizer2)

        return np.array(line_function_coeff), np.array(ortho_function_coeff)

    def _dist_between_line_and_box(self, coeffs, boxs):
        """Distance between box center and lines

        Args:
            coeffs (`numpy.ndarray`): normalized linear function
            boxs (list of dict): bounding boxs [{"xmin": 100, "ymin": 100,
                "xmax": 150, "ymax": 150}]
        Returns:
            (numpy.ndarray): each row corresponds each line distance in pixel
        """
        box_array = []
        dist = np.array([self._dist])

        for box in boxs:
            box_c = self._convert_center_box(box)
            b = [box_c["x"], box_c["y"], 1]
            box_array.append(b)

        if len(box_array) > 0:
            dist = abs(np.dot(np.array(box_array), coeffs.T))

        return dist

    def _box_dist(self, box1, box2):
        """Get two boxes distance

        Args:
            box1 (dict): bounding center box {"x": 75,"y": 75, "xmin": 50,
                "ymin": 50, "xmax": 100, "ymax": 100}
            box2 (dict): bounding center box {"x": 125,"y": 125, "xmin": 100,
                "ymin": 150, "xmax": 100, "ymax": 150}

        Returns:
            (float): distance in pixel
        """
        dist = ((box1["x"] - box2["x"]) ** 2 +
                (box1["y"] - box2["y"]) ** 2) ** 0.5

        return dist

    def _convert_center_box(self, box):
        """Convert bounding box to center x, y object

        Args:
            box (dict): bounding box {"xmin": 50, "ymin": 50,
                "xmax": 100, "ymax": 100}
        Returns:
            (dict): {"x": 75,"y": 75, "xmin": 50, "ymin": 50,
                "xmax": 100, "ymax": 100}
        """
        box["x"] = int((box["xmax"] + box["xmin"]) / 2)
        box["y"] = int((box["ymax"] + box["ymin"]) / 2)

        return box

    def _check_object_list(self, boxes, cur_timestamp):
        """Get new object list index of boxes

        check incoming boxes existed in current self._object_list,
        check if box close to lines enough.

        Args:
            boxs (list of dict): bounding boxs [{"xmin": 100, "ymin": 100,
                "xmax": 150, "ymax": 150}]
            cur_timestamp (float): Current timestmap.

        Returns:
            (list of dict): list of triggered objects. Each triggered object
                contains following fields:
                "obj_idx" (int): The index of matched bounding box in input
                    bounding boxes.
                "path_name": (string): The path name that triggered object
                    walks along.
        """
        triggered_objs = []
        dist_1 = self._dist_between_line_and_box(self._line_function_coeff, boxes)
        dist_2 = self._dist_between_line_and_box(self._ortho_function_coeff, boxes)

        for box_idx, box in list(enumerate(boxes)):
            # Update the tracking object list.
            box_c = self._convert_center_box(box)
            self._object_list, car_id = \
                self._check_object_box(self._object_list, box_c, cur_timestamp)

            # Get lines that is touched by the bounding box.
            lane_numbers = self._check_lines(dist_1[box_idx],
                                             dist_2[box_idx],
                                             self._length_of_lines)

            # Handle for each touched line.
            for lane_number in lane_numbers:
                lane_info = self._lane_info_list[lane_number]
                touched_start = self._object_list[car_id]["touched_start"]
                if lane_info["is_start"]:
                    if not lane_number in touched_start:
                        touched_start.append(lane_number)
                else:
                    peer_start_idx = lane_info["peer_idx"]
                    if peer_start_idx in touched_start:
                        touched_start.remove(peer_start_idx)
                        triggered_objs.append({
                            "box_idx": box_idx,
                            "path_name": lane_info["path_name"],
                        })

        return triggered_objs

    def _check_object_box(self, object_list, box_c, cur_timestamp):
        """Update the tracking object list.

        Args:
            object_list (dict): Previous tracking object list.
            box_c (dict): Bounding box with center {"x": 75,"y": 75, "xmin": 50,
                "ymin": 50, "xmax": 100, "ymax": 100}
            cur_timestamp (float): Current timestmap.

        Returns
            (dict, car_id): (Updated tracking object list, The car ID).
        """
        new_object_list = object_list.copy()

        for car_id, car in new_object_list.items():
            dist = self._box_dist(car["box"], box_c)
            if dist < self._dist_thr:
                new_object_list[car_id]["box"] = box_c
                new_object_list[car_id]["timestamp"] = cur_timestamp
                return new_object_list, car_id

        car_id = uuid()
        new_object_list[car_id] = {
            "timestamp": cur_timestamp,
            "box": box_c,
            "touched_start": [],
        }

        return new_object_list, car_id

    def _check_lines(self, dist_1, dist_2, length_of_lane):
        """Get lines that is touched by an object.

        check distance between lane and object is close enough

        Args:
            dist_1, dist_2 (numpy.ndarray): Each row corresponds each line
                distance in pixel
            length_of_lane (list of float): Each row length of line in pixel

        Returns:
            list of int: Indexes of touched lines.
        """
        lane_indexes = []

        for i, (d1, d2, length) in \
                list(enumerate(zip(dist_1, dist_2, length_of_lane))):
            if d1 < self._counting_check_dist and d2 < (length / 2):
                lane_indexes.append(i)

        return lane_indexes

    def _clean_outdated_objects(self, object_list, last_timestamp):
        new_object_list = object_list.copy()

        for car_id, car in object_list.items():
            period = last_timestamp - car["timestamp"]
            if period > self._object_expired_period:
                del new_object_list[car_id]

        return new_object_list

    def _check_triggers(self, detections, timestamps):
        """Find objects that trigger the events from given detected objects.

        Args:
            detections (list of tuple): A list of object detection result
                objects, format of each result is (bboxes, scores, classes).
            timestamps (list of float): A list of timestamp for each detection
                result.

        Returns:
            list of dict: A list of dictionary that specifies the triggered
                objects, format of each dictionary is:
                {
                    "is_triggered": True / False,
                    "bboxes": [...],
                    "scores": [...],
                    "labels": [...],
                    "path_names": [...],
                }
        """
        width, height = self._frame_size

        results = []
        for detection, timestamp in zip(detections, timestamps):
            (bboxes, scores, labels) = detection

            cands = { "bboxes_detect": [] }
            for bbox, label in zip(bboxes, labels):
                cands["bboxes_detect"].append({
                    "xmin": bbox.xmin,
                    "ymin": bbox.ymin,
                    "xmax": bbox.xmax,
                    "ymax": bbox.ymax,
                })

            triggered_objs = []
            w_cands = {
                "is_triggered": False,
                "bboxes": [],
                "scores": [],
                "labels": [],
                "path_names": [],
            }
            if len(cands["bboxes_detect"]) > 0:
                # Check whether object is the same, line, ...etc
                triggered_objs = self._check_object_list(
                    cands["bboxes_detect"],
                    timestamp,
                )
            for triggered_obj in triggered_objs:
                idx = triggered_obj["box_idx"]
                bbox = bboxes[idx]
                label = labels[idx]

                if label in self._triggers:
                    normalized_bbox = [
                        float(bbox.ymin) / height,
                        float(bbox.xmin) / width,
                        float(bbox.ymax) / height,
                        float(bbox.xmax) / width,
                    ]
                    w_cands["is_triggered"] = True
                    w_cands["bboxes"].append(normalized_bbox)
                    w_cands["scores"].append(scores[idx])
                    w_cands["labels"].append(labels[idx])
                    w_cands["path_names"].append(triggered_obj["path_name"])
            results.append(w_cands)

        # Clean outdated objects in the tracking object list.
        self._object_list = self._clean_outdated_objects(
            self._object_list,
            timestamps[-1],
        )

        return results

    def run(self, frames):
        images = [frame.image for frame in frames]
        timestamps = [frame.timestamp for frame in frames]
        f_images = self._client.scatter(images)
        f_detect = gpu_worker.run_model(self._client,
                                        "vehicle_detection",
                                        f_images,
                                        iou_threshold=0.3)
        catched = self._check_triggers(f_detect.result(), timestamps)

        output_frames = []
        for i in range(len(catched)):
            current_metadata = catched[i].copy()
            output_frames.append(EventVideoFrame(frames[i], current_metadata))

        return output_frames


class DirectionalCarCountingPipeline(object):
    """A class used to run the Directional Car Counting pipeline.

    Attributes:
        anal_id (lib.ObjectID): The ObjectID of the analyzer that the pipeline
            was attached with.
        paths (list of dict): Interested paths. Each path is a dictionary with
            following fields:
            "name" (string): The path name.
            "start": (dict): The starting line. A dictionary that contains 4
                fields ("x1", "y1", "x2", "y2") and each field is a floating
                value in range [0, 1].
            "end": (dict): The starting line. A dictionary that contains 4
                fields ("x1", "y1", "x2", "y2") and each field is a floating
                value in range [0, 1].
        triggers (list of string): The target of interest.
        frame_size (tuple): The size of the input image frame with format
            of (width, height).
    """

    def __init__(self, anal_id, paths, triggers, frame_size):
        self._name = "directional_car_counting"
        self._anal_id = anal_id

        transformed_paths = []

        for path in paths:
            transformed_paths.append({
                "name": path["name"],
                "start": transform_lines_format([path["start"]], frame_size)[0],
                "end": transform_lines_format([path["end"]], frame_size)[0],
            })

        # Create a directional car counting detector.
        self._detector = DirectionalCarCounting(
            transformed_paths,
            triggers,
            frame_size,
        )

        # Connect to Notification service
        self._notification = Notification()

        # Connect to Database service
        self._database = Database()

    def _output_event(self, timestamp, triggered, path_name):
        """Output event to notification center and database.

        Args:
            timestamp (float): Event timestamp.
            triggerd (string): The triggerd object of the event.
            path_name: (string): The path name that triggered object walks
                along.
        """
        event_type = "{}.alert".format(self._name)
        content = {
            "triggered": triggered,
            "path": path_name,
        }

        # Save event to database
        event_id = self._database.save_event(self._anal_id,
                                             timestamp,
                                             event_type,
                                             content).result()
        logging.info("Success to save event in database: _id: {}, analyzerId: "
                     "{}, timestamp: {}, type: {}, content: {}"
                     .format(event_id,
                             self._anal_id,
                             timestamp,
                             event_type,
                             content))

        # Push notification
        # Check whether the object near lines or not.
        self._notification.push_anal(event_id,
                                     self._anal_id,
                                     timestamp,
                                     event_type,
                                     content)

    def run(self, frames, motions, is_src_reader_busy):
        """Run Directional Car Counting pipeline.

        Args:
            frames: A list of raw video frames to be detected.
            motions, is_src_reader_busy: not in use
        """
        detected = self._detector.run(frames)

        for frame in detected:
            # check if frame contains cars
            if frame.metadata["is_triggered"]:
                labels = frame.metadata["labels"]
                path_names = frame.metadata["path_names"]
                for label, path_name in zip(labels, path_names):
                    self._output_event(frame.timestamp, label, path_name)

    def release(self):
        pass
