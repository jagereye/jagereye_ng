from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import asyncio
from concurrent.futures import CancelledError
import json
from bson import json_util

from jagereye_ng import logging

from threading import Thread
import gi
gi.require_version('Gst', '1.0')
from gi.repository import Gst, GObject
from jagereye_ng.io.rtsp import GstServer

class AsyncTimer(object):
    def __init__(self, interval, func, *args, **kwargs):
        self._interval = interval
        self._func = func
        self._args = args
        self._kwargs = kwargs
        self._task = None

    async def _timer_job(self):
        try:
            await asyncio.sleep(self._interval)
            self._func(*self._args, **self._kwargs)
            self._task = asyncio.ensure_future(self._timer_job())
        except CancelledError:
            logging.info("AsyncTimer job has been cancelled.")
            raise

    def start(self):
        assert self._task is None, ("Perhaps you call start() twice"
                                    " by accident?")
        self._task = asyncio.ensure_future(self._timer_job())

    def cancel(self):
        if self._task is not None:
            self._task.cancel()
            self._task = None

class RTSP_server(Thread):
    def __init__(self):
        super().__init__()
        self.daemon = True
        # Init Glib
        GObject.threads_init()
        Gst.init(None)
        # Init RTSP server
        self._rtsp_server = GstServer()
        self._loop = GObject.MainLoop.new(None, False)

    def get_instance(self):
        return self._rtsp_server

    def run(self):
        self._loop.run()

def deep_equal(obj1, obj2):
    dump1 = json.dumps(obj1, sort_keys=True, default=json_util.default)
    dump2 = json.dumps(obj2, sort_keys=True, default=json_util.default)

    return dump1 == dump2
