from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import os
import time
import abc
import shutil
from collections import deque
from queue import Queue, Empty as QueueEmpty
from threading import Thread, Lock, Event
import cv2
import copy
import numpy as np
from botocore.client import ClientError
from jagereye_ng.io.streaming import VideoStreamWriter, VideoFrame
from jagereye_ng.util.lists import AdaptiveListSelection
from jagereye_ng.io.obj_storage import ObjectStorageClient
from jagereye_ng import logging

class EventVideoFrame(object):
    """A class used to store VideoFrame object and its event metadata

    Attributes:
        frame (VideoFrame obj): The raw VideoFrame object of the event.
        metadata (dict): The metadata of the frame for the event.
    """
    def __init__(self, frame, metadata=None):
        self._frame = frame
        self.metadata = metadata

    @property
    def frame(self):
        return self._frame

    @property
    def image(self):
        return self._frame.image

    @property
    def timestamp(self):
        return self._frame.timestamp


class SharedEventVideoFrame(object):
    """A class used to store a shared VideoFrame object and its event metadatas.

    Attributes:
        frame (`VideoFrame`): The raw VideoFrame object of the event.
        metadatas (dict of dict): The metadatas of the shared frame for the
            event.
        base_metadata (dict): The base metadata of the shared frame for the
            event.
    """
    def __init__(self, frame, metadatas={}, base_metadata=None):
        self._frame = frame
        self._metadatas = metadatas
        self._base_metadata = base_metadata

    @property
    def frame(self):
        return self._frame

    @property
    def image(self):
        return self._frame.image

    @property
    def timestamp(self):
        return self._frame.timestamp

    @property
    def metadatas(self):
        return self._metadatas

    @property
    def base_metadata(self):
        return self._base_metadata


class _EventVideoWriter(object):
    """A class used to generate event video.

    Attributes:
        video_key (str): The key to represent the video in the object storage.
        metadata_key (str): The key to represent the video metadata in the
            object storage.
        timestamp (timestamp): The start timestamp of the video, it will be
            saved in the video metadata.
        metadata (dict): Base video metadata.
        fps (int): The fps of the output video.
        size (tuple): The size of the output video. The format is
            (width, height).
    """
    def __init__(self, video_key, metadata_key, timestamp, metadata, fps, size, q_size=0, threads=0):
        self._writer = VideoStreamWriter(q_size)
        self._video_key = video_key
        self._metadata_key = metadata_key
        self._tmp_filepath = os.path.join("/tmp", self._video_key)

        # Create the temporary folder, for video and metadata file, if it does
        # not exist.
        tmp_dir = os.path.dirname(self._tmp_filepath)
        if not os.path.exists(tmp_dir):
            os.makedirs(tmp_dir)

        self._metadata = {"fps": fps, "start": timestamp}
        try:
            self._event_name = metadata["event_name"]
            event_custom = metadata["event_custom"]
            self._metadata[self._event_name] = {
                "frames": [],
                "custom": event_custom
            }
        except KeyError:
            raise

        try:
            self._writer.open(self._tmp_filepath, fps, size, threads)
        except RuntimeError:
            raise

        # Connect to Object storage service
        self._obj_storage = ObjectStorageClient()
        self._obj_storage.connect()

    def _write(self, frame):
        self._writer.write(frame)
        self._metadata[self._event_name]["frames"].append(frame.metadata)

    def write(self, frames):
        if isinstance(frames, list):
            for frame in frames:
                self._write(frame)
        else:
            self._write(frames)

    def end(self, timestamp=None):
        self._writer.end()
        self._metadata[self._event_name]["end"] = float(
            timestamp if timestamp is not None else time.time())
        # TODO: this error handling may not needed for AM will handle it
        try:
            self._obj_storage.save_file_obj(self._video_key, self._tmp_filepath)
        except ClientError:
            logging.error("Failed to save file: {}".format(self._video_key))

        # Remove the temporary video file.
        os.remove(self._tmp_filepath)
        logging.info("Saved video: {}".format(self._video_key))

        # Write out video metadata to object storage.
        self._obj_storage.save_json_obj(self._metadata_key, self._metadata)
        logging.info("Saved video metadata: {}".format(self._metadata_key))


class EventVideoPolicy():
    """A metaclass used to define the event video policy interface.

    User should inherent this class to define his own policy object to control
    the flow EventVideoAgent of recording video.
    """

    START_RECORDING = 0
    STOP_RECORDING = 1

    @abc.abstractmethod
    def compute(self, frames):
        pass


class SharedEventVideoPolicy(EventVideoPolicy):
    """A metaclass used to define the shared event video policy interface.

    User should inherent this class to define his own policy object to control
    the flow SharedEventVideoAgent of recording video.
    """
    pass


class AgentEvent(object):
    __slots__ = ["action", "content"]
    def __init__(self, action, content=None):
        self.action = action
        self.content = content


class SharedAgentEvent(object):
    __slots__ = ["agent_events"]
    def __init__(self, agent_events):
        self.agent_events = agent_events


class EventVideoAgent(object):
    """A class used to control event video generation with a policy.

    Attributes:
        policy (object): The policy to control when the agent to start/stop
            recording.
        event_metadata (dict): The base of video metadata. It's a dict with
            following keys:
                event_name: The event name of the video.
                event_custom: The information you want to insert into the video
                    metadata section of the event_name.
        obj_key_prefix (str): The key prefix of the video and metadata.
        frame_size (tuple): The size of the input image frame with format
            of (width, height).
        video_format (str): The output video format.
        fps (int): The output video fps.
        history_len (int): The length, in seconds, of frame history queue. This
            determines when the agent starts to record before policy returning
            action "START_RECORDING".
    """

    STATE_RECORDING = 0
    STATE_PASSTHROUGH = 1

    def __init__(self,
                 policy,
                 event_metadata,
                 obj_key_prefix,
                 frame_size,
                 video_format="mp4",
                 fps=15,
                 history_len=3):
        """Initialize a EventVideoAgent object."""
        self._policy = policy
        self._obj_key_prefix = obj_key_prefix
        self._frame_size = frame_size
        self._event_metadata = event_metadata
        self._video_format = video_format
        self._fps = fps

        max_history_frames = self._fps * history_len
        self._history_q = deque(maxlen=max_history_frames) # queue of EventVideoFrame
        self._current_writer = None
        self._state = EventVideoAgent.STATE_PASSTHROUGH

    def is_recording(self):
        return self._state == EventVideoAgent.STATE_RECORDING

    def process(self, frame):
        """Process the frame to generate event video.
           This function should return fast as nonblocking operation.

        Args:
            frame (`EventVideoFrame`): The frame object to be saved.

        Returns:
            `AgentEvent`
        """
        agent_event = None

        # Determine what action needs to be taken for the incoming frame
        # according to the user-defined policy.
        action = self._policy.compute(frame)
        if self._state == EventVideoAgent.STATE_PASSTHROUGH:
            self._history_q.append(frame)
            if action == EventVideoPolicy.START_RECORDING:
                timestamp = frame.timestamp
                filename = os.path.join(self._obj_key_prefix,
                                        "{}".format(timestamp))
                video_key = "{}.{}".format(filename, self._video_format)
                metadata_key = "{}.json".format(filename)
                self._current_writer = _EventVideoWriter(
                    video_key,
                    metadata_key,
                    timestamp,
                    self._event_metadata,
                    self._fps,
                    self._frame_size)

                # Flush out history queue to event video
                history = self._history_q.copy()
                for _ in range(len(history)):
                    self._current_writer.write(history.popleft())
                self._history_q.clear()

                agent_event = AgentEvent(action, {"video_key": video_key,
                                                  "metadata_key": metadata_key,
                                                  "timestamp": timestamp})
                self._state = EventVideoAgent.STATE_RECORDING
        elif self._state == EventVideoAgent.STATE_RECORDING:
            self._current_writer.write(frame)
            if action == EventVideoPolicy.STOP_RECORDING:
                self._current_writer.end(frame.timestamp)
                agent_event = AgentEvent(action)
                self._state = EventVideoAgent.STATE_PASSTHROUGH

        return agent_event

    def release(self):
        if self._state == EventVideoAgent.STATE_RECORDING:
            self._current_writer.end()
        self._history_q.clear()


class SharedEventVideoAgent(object):
    """A class used to control shared event video generation with a policy.

    Attributes:
        policy (object): The policy to control when the agent to start/stop
            recording.
        event_metadata (dict): The base of video metadata. It's a dict with
            following keys:
                event_name: The event name of the video.
                event_custom: The information you want to insert into the video
                    metadata section of the event_name.
        obj_key_prefix (string): The key prefix of the video and metadata.
        frame_size (tuple): The size of the input image frame with format
            of (width, height).
        video_format (string): The output video format.
        fps (int): The output video fps.
        history_len (int): The length, in seconds, of frame history queue. This
            determines when the agent starts to record before policy returning
            action "START_RECORDING".
            Note that the memory consumption is based on `history_len` *
            `frame size` * `numbers of recording object`.
            please make sure your machine have enough RAM.
    """
    STATE_RECORDING = 0
    STATE_PASSTHROUGH = 1

    def __init__(self,
                 policy,
                 event_metadata,
                 obj_key_prefix,
                 frame_size,
                 video_format="mp4",
                 fps=15,
                 history_len=3):
        """Initialize a SharedEventVideoAgent object."""
        self._policy = policy
        self._obj_key_prefix = obj_key_prefix
        self._frame_size = frame_size
        self._event_metadata = event_metadata
        self._video_format = video_format
        self._fps = fps

        max_history_frames = self._fps * history_len
        self._history_q = deque(maxlen=max_history_frames)
        self._current_writers = {}
        self._states = {}

    def _single_frame(self, key, frame):
        if key in frame.metadatas:
            metadata = frame.metadatas[key]
        else:
            metadata = frame.base_metadata
        return EventVideoFrame(frame.frame, metadata)

    def is_recording(self, key):
        return self._states[key] == EventVideoAgent.STATE_RECORDING

    def process(self, frame):
        """Process the frame to generate event video.
           This function should return fast as nonblocking operation.

        Args:
            frame (`SharedEventVideoFrame`): The frame object to be saved.

        Returns:
            `SharedAgentEvent`
        """
        # Push new frame to history queue.
        self._history_q.append(frame)

        # Determine what actions need to be taken for the incoming frame
        # according to the user-defined policy.
        actions = self._policy.compute(frame)

        agent_events = {}
        for key, action in actions.items():
            if key not in self._states:
                self._states[key] = SharedEventVideoAgent.STATE_PASSTHROUGH

            if self._states[key] == SharedEventVideoAgent.STATE_PASSTHROUGH:
                if action == SharedEventVideoPolicy.START_RECORDING:
                    timestamp = frame.timestamp
                    filename = os.path.join(self._obj_key_prefix,
                                            key,
                                            "{}".format(timestamp))
                    video_key = "{}.{}".format(filename, self._video_format)
                    metadata_key = "{}.json".format(filename)
                    self._current_writers[key] = _EventVideoWriter(
                        video_key,
                        metadata_key,
                        timestamp,
                        self._event_metadata,
                        self._fps,
                        self._frame_size,
                    )

                    # Put all frames from history queue to event video.
                    history = self._history_q.copy()
                    for _ in range(len(history)):
                        self._current_writers[key].write(
                            self._single_frame(key, history.popleft()),
                        )

                    agent_events[key] = AgentEvent(action, {
                        "video_key": video_key,
                        "metadata_key": metadata_key,
                        "timestamp": timestamp,
                    })
                    self._states[key] = SharedEventVideoAgent.STATE_RECORDING
            elif self._states[key] == SharedEventVideoAgent.STATE_RECORDING:
                self._current_writers[key].write(self._single_frame(key, frame))
                if action == SharedEventVideoPolicy.STOP_RECORDING:
                    self._current_writers[key].end(frame.timestamp)
                    self._current_writers.pop(key, None)
                    self._states.pop(key, None)
                    agent_events[key] = AgentEvent(action)

        shared_agent_event = SharedAgentEvent(agent_events)

        return shared_agent_event

    def release(self):
        for key, state in self._states.items():
            if (state == SharedEventVideoAgent.STATE_RECORDING and
                key in self._current_writers):
                self._current_writers[key].end()
        self._history_q.clear()


class SharedPersistentEventVideoAgent(object):
    """A class used to control shared event video generation with a policy.

        In order to perform like `SharedEventVideoAgent` and reduce memory
        usage, This Event Agent is especially build for longer recording time.

        Main thread function `process` keep handling status, frames and return
        `SharedAgentEvent` as a non-blocking operation.

        There are three workers runing in the background:
        1. persist buffer worker:
            This worker keep fetching newest frame, and write buffered frames
            in file system every second. every images will be stored as
            compressed JPEG file in temporal folder.
        2. copy worker:
            Once status has changed to STOP_RECORDING, Worker task will be
            triggered by copy queue, it copy related images to another
            recording folder.
        3. record worder:
            Worker task is triggered by record queue, it will read all the
            images from the recording folder finish recording video file.

        Like this:

        |---------------------------------------------|
        |        Circular buffer image indexes        |
        |---------------------------------------------|
        | 0 | 1 | 2 | ... | ... | self._max_q_size -1 |
        |---------------------------------------------|
        |                              |current|      |
        |---------------------------------------------|
        |       |////////  copy files  ////////|      |
        |---------------------------------------------|
        |       |//////// record video ////////|      |
        |---------------------------------------------|


        Here's disk usage example:
        resolution 1080p, 80 quality jpeg, 60 min, 10 fps files consumed 8GB

    Attributes:
        policy (object): The policy to control when the agent to start/stop
            recording.
        event_metadata (dict): The base of video metadata. It's a dict with
            following keys:
                event_name: The event name of the video.
                event_custom: The information you want to insert into the video
                    metadata section of the event_name.
        obj_key_prefix (string): The key prefix of the video and metadata.
        q_size (int): a queue for _EventVideoWriter, length equals to:
                (output fps) * (recording time in second)
        max_q_size (int): maximun queue size for store input frames buffer,
            longer max_q_size gets better fault tolerance for current algorithom
        frame_size (tuple): The size of the input image frame with format
            of (width, height).
        fps (int): The output video fps.
        video_format (string): The output video format.
        num_copy_worker (int): numbers of copy worker.
        num_record_worker (int): numbers of record worker.
    """

    STATE_RECORDING = 0
    STATE_PASSTHROUGH = 1

    def __init__(self, anal_id,
                 policy,
                 event_metadata,
                 obj_key_prefix,
                 q_size,
                 frame_size,
                 fps=10,
                 video_format='mp4',
                 num_copy_worker=5,
                 num_record_worker=2):
        super().__init__()

        _default_folder = '/tmp/JAGER/SharedPersistentEventVideoBuffer'
        if not os.path.exists(_default_folder):
            os.makedirs(_default_folder)
        self._file_path = os.path.join(_default_folder, anal_id)
        shutil.rmtree(self._file_path, ignore_errors=True)
        os.makedirs(self._file_path)

        self._policy = policy

        assert q_size > 0
        self._q_size = q_size
        self._max_q_size = int(q_size * 3)

        self._encode_params = self._get_encode_params(80)
        self._frame_size = frame_size
        self._video_format = video_format
        self._obj_key_prefix = obj_key_prefix
        self._event_metadata = event_metadata

        self._idx = 0  # range: [0 - self._max_q_size)
        self._lock_idx = Lock()  # for self._idx
        self._is_file_idx_writing = {i: False for i in range(self._max_q_size)}
        self._lock_file_idx = Lock()  # for self._is_file_idx_writing

        # stores data for EventVideoFrame and this agent
        self._states = {}  # {key: objectId, value: state}
        self._timestamps = {}  # {key: objectId(string), value(float)}
        self._queues_metadatas = {}  # {key: objectId(string), value (`Queue`) of metadata}, stores metadata frame by frame
        self._base_metadatas = {}  # {key: objectId(string), value(dict) of base_metadata}
        self._if_add_copy_task = {}
        ''' key: objectId(string), value(boolean)
            stores if asked copy task, make sure excute copy task only once
            during each request
        '''

        # background persist buffer worker
        self._fps = fps
        self._queue_persist = deque()
        self._event_stop_source = Event()
        self._event_stop_persist_buffer_worker = Event()
        self._worker_persist_buffer = Thread(target=self._persist_buffer_task,
                                           daemon=True)
        self._worker_persist_buffer.start()

        # background copy worker
        self._queues_copy = Queue()  # queue of (tuple), stores (idx, key) for copy request
        self._event_stop_copy_worker = Event()
        self._worker_copy = [Thread(target=self._check_copy_task,
                                    daemon=True)
                             for _ in range(num_copy_worker)]
        for t in self._worker_copy:
            t.start()

        # background record worker
        self._queues_record = Queue()  # queue of (tuple), stores (idx, key) for record request
        self._event_stop_record_worker = Event()
        self._worker_record = [Thread(target=self._check_record_task,
                                      daemon=True)
                               for _ in range(num_record_worker)]
        for t in self._worker_record:
            t.start()

    def _get_encode_params(self, jpeg_quality):
        """JPEG parameters for OpenCV imwrite

        Args:
            jpeg_quality (int): value is from 0 to 100 for JPEG encoding
                quality.
        """
        encode_params = [
            int(cv2.IMWRITE_JPEG_OPTIMIZE), 1,
            int(cv2.IMWRITE_JPEG_QUALITY), jpeg_quality
        ]
        return encode_params

    def _get_agent_event_data(self, key, timestamp):
        """Get common filename for agent event"""
        filename = os.path.join(self._obj_key_prefix,
                                key,
                                "{}".format(timestamp))
        video_key = "{}.{}".format(filename, self._video_format)
        metadata_key = "{}.json".format(filename)
        return video_key, metadata_key

    def _persist_buffer_task(self):
        current_timestamp = last_timestamp = 0  # timestamp
        is_init = False
        img_shape = (self._frame_size[1], self._frame_size[0], 3)
        latest_image = np.zeros(img_shape, dtype=np.uint8)
        selector = AdaptiveListSelection()
        while True:
            # Start collect {fps} frames in {write_interval}
            images = []
            while len(images) < self._fps:
                if len(self._queue_persist) > 0:
                    frame = self._queue_persist.popleft()
                    frame_timestamp = frame.timestamp
                    if is_init is False:
                        # time initialization (execute once)
                        current_timestamp = last_timestamp = frame_timestamp
                        is_init = True
                    elif len(images) == 0:
                        if frame_timestamp > last_timestamp + 1:
                            # set current timestamp first if valid
                            current_timestamp = frame_timestamp
                        else:
                            # drop this frame
                            continue

                    # Select proper frame
                    if frame_timestamp >= current_timestamp + 1:
                        # put this frame back to queue
                        self._queue_persist.appendleft(frame)
                        break
                    elif frame_timestamp >= current_timestamp and \
                         frame_timestamp < current_timestamp + 1:
                        # use this frame
                        latest_image = frame.image
                        images.append(frame.image)
                    else:
                        # drop this frame
                        continue
                else:
                    time.sleep(1)
                    if self._event_stop_source.is_set():
                        break

            if len(images) == 0:
                images = [latest_image] * (self._fps)
            else:
                images = selector.select(images, self._fps)

            diff_timestamp_write = current_timestamp - last_timestamp
            while diff_timestamp_write >= 2:  # in second
                # Add latest frames second by second
                images[:] = [images[0]] * self._fps + images[:]
                diff_timestamp_write -= 1

            last_timestamp = current_timestamp

            # Write images as buffer files
            for image in images:
                with self._lock_idx:
                    _idx = self._idx
                    # release lock ASAP
                with self._lock_file_idx:
                    self._is_file_idx_writing[_idx] = True
                    # release lock ASAP
                cv2.imwrite(self._file_path + '/{}.jpg'.format(_idx),
                            image, self._encode_params)
                with self._lock_file_idx:
                    self._is_file_idx_writing[_idx] = False
                    # release lock ASAP
                with self._lock_idx:
                    self._idx = (self._idx + 1) % self._max_q_size
                    # release lock ASAP

            if self._event_stop_persist_buffer_worker.is_set():
                break

    def _check_copy_task(self):
        """The copy queue consumer"""
        while True:
            if self._queues_copy.empty():
                time.sleep(1)
                if self._event_stop_copy_worker.is_set():
                    break
            else:
                idx, key = self._queues_copy.get()  # blocking op
                self._do_copy_task(idx, key)
                self._queues_record.put((idx, key))  # non blocking op

    def _do_copy_task(self, after_trigger_idx, key):
        """Copy all image to temp folder"""
        # Remove related files
        tmp_path = os.path.join(self._file_path, key)
        shutil.rmtree(tmp_path, ignore_errors=True)
        os.makedirs(tmp_path)

        idx_start = after_trigger_idx - self._q_size
        if idx_start < 0:
            idx_start = self._max_q_size - (self._q_size - after_trigger_idx)
            if not os.path.exists(os.path.join(self._file_path, '{}.jpg'.format(idx_start))):
                idx_start = 0

        counter_copy = 0
        while counter_copy < self._q_size:
            idx_new = (idx_start + counter_copy) % self._max_q_size
            file_source = os.path.join(self._file_path, '{}.jpg'.format(idx_new))

            if not os.path.exists(file_source):
                time.sleep(1)  # wait until source buffer exist
                continue

            file_dest = os.path.join(tmp_path, '{}.jpg'.format(counter_copy))

            with self._lock_file_idx:
                is_file_idx_writing = self._is_file_idx_writing[idx_new]
                # release lock ASAP
            if is_file_idx_writing is False:
                shutil.copyfile(file_source, file_dest)
            else:
                time.sleep(1)  # wait until source buffer file finish writing
                continue

            counter_copy += 1

    def _check_record_task(self):
        """The recording queue consumer"""
        while True:
            if self._queues_record.empty():
                time.sleep(1)
                if self._event_stop_record_worker.is_set():
                    break
            else:
                idx, key = self._queues_record.get()  # blocking op
                self._do_record_task(idx, key)

    def _do_record_task(self, after_trigger_idx, key):
        """read all images from disk and write as a video"""
        tmp_path = os.path.join(self._file_path, key)
        timestamp = self._timestamps[key]
        video_key, metadata_key = self._get_agent_event_data(key, timestamp)
        # Initilize _EventVideoWriter with memory, cpu consumption limitation
        writer = _EventVideoWriter(
            video_key,
            metadata_key,
            timestamp,
            self._event_metadata,
            self._fps,
            self._frame_size, q_size=100, threads=2)

        metadatas = {}
        base_metadata = self._base_metadatas[key]
        for idx in range(self._q_size):
            image = cv2.imread('{}/{}.jpg'.format(tmp_path, idx))
            if self._queues_metadatas[key].empty() is True:
                metadatas = base_metadata
            else:
                metadatas = self._queues_metadatas[key].get()

            # TODO use EventVideoFrame is also fine
            sharedFrame = SharedEventVideoFrame(VideoFrame(image),
                                                metadatas, base_metadata)
            writer.write(self._single_frame(key, sharedFrame))
        writer.end(timestamp + self._q_size * self._fps)

        if os.path.exists(tmp_path):
            shutil.rmtree(tmp_path, ignore_errors=True)

        self._queues_metadatas[key].queue.clear()
        self._queues_metadatas.pop(key, None)
        self._timestamps.pop(key, None)
        self._base_metadatas.pop(key, None)
        self._states.pop(key, None)

    def is_recording(self, key):
        state = self._states.get(key, SharedPersistentEventVideoAgent.STATE_PASSTHROUGH)
        return state == SharedPersistentEventVideoAgent.STATE_RECORDING

    def _single_frame(self, key, frame):
        if key in frame.metadatas:
            metadata = frame.metadatas[key]
        else:
            metadata = frame.base_metadata
        return EventVideoFrame(frame.frame, metadata)

    def process(self, frame):
        """Process the frame to generate event video.
           This function should return fast as nonblocking operation.

        Args:
            frame (`SharedEventVideoFrame`): The frame object to be saved.

        Returns:
            `SharedAgentEvent`
        """
        # Determine what actions need to be taken for the incoming frame
        # according to the user-defined policy.

        # TODO handling video streaming source reconnection

        actions = self._policy.compute(frame)

        agent_events = {}
        for key, action in actions.items():
            if key not in self._states:
                self._states[key] = SharedPersistentEventVideoAgent.STATE_PASSTHROUGH

            if self._states[key] == SharedPersistentEventVideoAgent.STATE_PASSTHROUGH:
                if action == SharedEventVideoPolicy.START_RECORDING:
                    timestamp = frame.timestamp
                    video_key, metadata_key = self._get_agent_event_data(key, timestamp)
                    agent_events[key] = AgentEvent(action, {
                        "video_key": video_key,
                        "metadata_key": metadata_key,
                        "timestamp": timestamp,
                    })
                    self._timestamps[key] = timestamp
                    self._if_add_copy_task.pop(key, None)
                    self._states[key] = SharedPersistentEventVideoAgent.STATE_RECORDING
                    self._queues_metadatas[key] = Queue()
                    self._queues_metadatas[key].put(frame.metadatas)
                    self._base_metadatas[key] = frame.base_metadata

            elif self._states[key] == SharedPersistentEventVideoAgent.STATE_RECORDING:
                self._queues_metadatas[key].put(frame.metadatas)
                if action == SharedEventVideoPolicy.STOP_RECORDING:
                    self._stop_recording(key)
                    # TODO add to agent_events while record task has been done
        self._queue_persist.append(frame)
        shared_agent_event = SharedAgentEvent(agent_events)
        return shared_agent_event

    def _stop_recording(self, key):
        if self._states[key] == SharedPersistentEventVideoAgent.STATE_RECORDING:
            # TODO refactor this part in the future
            if self._if_add_copy_task.get(key, False) is False:
                with self._lock_idx:
                    _idx = self._idx
                    # release lock ASAP
                self._queues_copy.put((_idx, key))  # non blocking op
                self._if_add_copy_task[key] = True

    def release(self):
        # Set video source stoped event
        self._event_stop_source.set()
        # Finish all recording request
        for key in self._states:
            self._stop_recording(key)
        """
        Here's a short sleep on the main thread before set the copy worker stop
        event, the copy queue will be consumed by the copy worker to finish all
        the rest of recording task.
        """
        time.sleep(1)

        # Stop copy woker
        self._event_stop_copy_worker.set()
        for t in self._worker_copy:
            t.join()
        self._queues_copy.queue.clear()

        # Then stop record woker
        self._event_stop_record_worker.set()
        for t in self._worker_record:
            t.join()
        self._queues_record.queue.clear()
        for k in self._queues_metadatas:
            self._queues_metadatas[k].queue.clear()

        # Then stop write buffer worker
        self._event_stop_persist_buffer_worker.set()
        self._worker_persist_buffer.join()
        self._queue_persist.clear()
