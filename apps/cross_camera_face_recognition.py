from face_recognition import FaceRecognitionPipeline


class CrossCameraFaceRecognitionPipeline(FaceRecognitionPipeline):
    """A class used to run the Cross Camera Face Recognition pipeline.

    Attributes:
        anal_id (lib.ObjectID): The ObjectID of the analyzer that the pipeline
            was attached with.
        analysis_fps (int): FPS (frame per second) to analyze the input video
            frames. Note that this is different from source FPS and analysis FPS
            is usually lower than source FPS (e.g., source FPS = 15, analysis
            FPS = 5).
        triggers (list of lib.ObjectID): The target of interest. Each item is the
            group ID of faces stored in externals.
        roi_list (list of list of object): A list that contains zero, one or
            more region(s) of interest (roi). The format of each roi is a list
            of object points, such as [{"x": 0.21, "y": 0.33},
            {"x": 0.32, "y": 0.43}, ...]
        face_min_width(int): The minimum face width for detection and
            recognition (in pixel).
        face_min_height(int): The minimum face height for detection and
            recognition (in pixel).
        detn_thold (float): Face detection threshold, range from 0 to 1.
        recog_thold (float): Face recognition threshold, range from 0 to 1.
        next period (int): The period (in seconds) to recognize the same person
            (who was recognized for a while) again.
        output_non_matched (bool): Whether to output faces that are not in the
            database or not.
        frame_size (tuple): The size of the input image frame with format
            of (width, height).
    """
    def __init__(self,
                 anal_id,
                 analysis_fps,
                 triggers,
                 roi_list,
                 face_min_width,
                 face_min_height,
                 detn_thold,
                 recog_thold,
                 next_period,
                 output_non_matched,
                 frame_size):
        super().__init__("cross_camera_face_recognition",
                         anal_id,
                         triggers,
                         "group",
                         analysis_fps,
                         roi_list,
                         face_min_width,
                         face_min_height,
                         detn_thold,
                         recog_thold,
                         next_period,
                         output_non_matched,
                         frame_size)
