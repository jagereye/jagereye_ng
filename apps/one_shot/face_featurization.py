from dask.distributed import get_client
from jagereye_ng.gpu_worker import gpu_worker
from jagereye_ng.gpu_worker.face import crop_align_faces
from jagereye_ng.io.obj_storage import ObjectStorageClient
from jagereye_ng.io.obj_storage import NoSuchKey
from jagereye_ng.util.generic import get_config

from one_shot.one_shot import OneShotExecutor
from one_shot.one_shot import OneShotResult

config = get_config()["apps"]["dask"]
address = config["cluster_ip"] + ":" + config["scheduler_port"]

class _Bundle(object):
    def __init__(self, id, content):
        self._id = id
        self._content = content

    @property
    def id(self):
        return self._id

    @property
    def content(self):
        return self._content


class FaceFeaturization(OneShotExecutor):
    """One-Shot executor for face featurization."""

    def __init__(self):
        super().__init__(max_batch_size=5)

        try:
            # Get Dask client
            self._client = get_client(address=address)
        except ValueError:
            assert False, ("Should connect to Dask scheduler before"
                           " initializing this object.")

        # Connect to Object storage service.
        self._obj_storage = ObjectStorageClient()
        self._obj_storage.connect()

    def __del__(self):
        self._client.close()

    def _fetch_images(self, image_keys):
        images = []
        failures = []

        for image_key in image_keys:
            key = image_key.content
            try:
                image = self._obj_storage.get_image_obj(key)
                images.append(_Bundle(image_key.id, image))
            except NoSuchKey:
                reason = "Image does not exist"
                failures.append(_Bundle(image_key.id, reason))

        return images, failures

    def _detect_faces(self, images):
        faces = []
        failures = []

        input_images = [image.content for image in images]
        f_images = self._client.scatter(input_images)
        f_detected = gpu_worker.run_model(self._client,
                                          "face_detection",
                                          f_images)
        detections = f_detected.result()

        for image, detection in zip(images, detections):
            bboxes, landmarks, _ = detection
            num_faces = len(bboxes)
            if num_faces == 0:
                reason = "No face detected"
                failures.append(_Bundle(image.id, reason))
            elif num_faces > 1:
                reason = "More than one face detected"
                failures.append(_Bundle(image.id, reason))
            else:
                aligned_faces = crop_align_faces(image.content,
                                                 bboxes,
                                                 landmarks)
                # We only get one face for each image.
                face = aligned_faces[0]
                faces.append(_Bundle(image.id, face))

        return faces, failures

    def _calculate_features(self, faces):
        features = []
        failures = []

        input_faces = [face.content for face in faces]
        f_faces = self._client.scatter([input_faces])
        f_recognized = gpu_worker.run_model(self._client,
                                            "face_recognition",
                                            f_faces)
        recognitions = f_recognized.result()[0]

        for face, recognition in zip(faces, recognitions):
            features.append(_Bundle(face.id, recognition))

        return features, failures

    def run(self, tasks):
        results = [None] * len(tasks)
        failed_bundles = []

        # Aggregate the image keys from tasks.
        image_keys = [_Bundle(idx, task.content["image"])
                      for idx, task in enumerate(tasks)]

        # Fetch images.
        images, failures = self._fetch_images(image_keys)
        failed_bundles += failures

        # Detect faces from images.
        faces, failures = self._detect_faces(images)
        failed_bundles += failures

        # Calculate features of faces.
        features, failures = self._calculate_features(faces)
        failed_bundles += failures

        # Aggregate face features to results.
        for feature in features:
            idx = feature.id
            updated = { "feature": feature.content.tolist() }
            results[idx] = OneShotResult(tasks[idx], True, updated=updated)

        # Aggregate failures to results.
        for failed_bundle in failed_bundles:
            idx = failed_bundle.id
            reason = failed_bundle.content
            results[idx] = OneShotResult(tasks[idx], False, reason=reason)

        return results
