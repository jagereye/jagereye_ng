from dask.distributed import get_client
from jagereye_ng.util.generic import get_config

from one_shot.one_shot import OneShotExecutor
from one_shot.one_shot import OneShotResult

config = get_config()["apps"]["dask"]
address = config["cluster_ip"] + ":" + config["scheduler_port"]


class StitchingCalibration(OneShotExecutor):
    """One-Shot executor for calibration."""

    def __init__(self):
        super().__init__(max_batch_size=10)

        # Get Dask client.
        try:
            # Get Dask client
            self._client = get_client(address=address)
        except ValueError:
            assert False, ("Should connect to Dask scheduler before"
                           " initializing this object.")

    def __del__(self):
        self._client.close()

    def run(self, tasks):
        results = [None] * len(tasks)
        for idx, task in enumerate(tasks):
            # TODO add calibration process here in the future if needed
            results[idx] = OneShotResult(tasks[idx], True)

        return results
