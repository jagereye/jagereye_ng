import abc


class OneShotExecutor(object):
    """The base class for one-shot task execution."""
    def __init__(self, max_batch_size=1):
        self._max_batch_size = max_batch_size

    @property
    def max_batch_size(self):
        return self._max_batch_size

    @abc.abstractmethod
    def run(self, tasks):
        """Run the tasks execution."""
        raise NotImplementedError()


class OneShotTask(object):
    def __init__(self, name, content, source_collection, source_id):
        self._name = name
        self._content = content
        self._source_collection = source_collection
        self._source_id = source_id

    @property
    def name(self):
        return self._name

    @property
    def content(self):
        return self._content

    @property
    def source_collection(self):
        return self._source_collection

    @property
    def source_id(self):
        return self._source_id


class OneShotResult(object):
    def __init__(self, task, success, updated={}, reason=""):
        self._task = task
        self._success = success
        self._updated = updated
        self._reason = reason

    @property
    def task(self):
        return self._task

    @property
    def success(self):
        return self._success

    @property
    def updated(self):
        return self._updated

    @property
    def reason(self):
        return self._reason
