from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import numpy as np
import cv2
import os
import time
import copy
import uuid
from dask.distributed import get_client
import matplotlib.path as mplPath

from events import SharedEventVideoFrame, SharedEventVideoPolicy, \
    SharedPersistentEventVideoAgent
from jagereye_ng import image as im
from jagereye_ng import gpu_worker
from jagereye_ng import logging
from jagereye_ng.io.obj_storage import ObjectStorageClient
from jagereye_ng.io.notification import Notification
from jagereye_ng.io.database import Database
from jagereye_ng.io.streaming import VideoFrame
from jagereye_ng.util.generic import get_config
from jagereye_ng.gpu_worker.label import UNATTENDED_OBJECT_CLASSIFICATION_CATEGORIES

UNATTENDED_OBJECT_CLASSIFICATION_TARGET_LIST = [
    'box',
    'cone',
    'pallet',
    'suitcase',
    'trash bag'
]
USE_CLASSIFICATION = True
STANDARD_DEVIATION_THRESHOLD = 15
DISAPPEAR_THRESHOLD = 20
DEBUG = False

config = get_config()["apps"]["dask"]
address = config["cluster_ip"] + ":" + config["scheduler_port"]


def transform_roi_format(roi, frame_size):
    """Transforms roi format.

    Transforms rules:
        absolution point value = relative point value * corresponding frame
        size

    Examples:
        Assume frame size is (100, 100),
        Expect source roi format (list of dict):
            [{"x": 0.21, "y": 0.33}, {"x": 0.32, "y": 0.43}, ...]

        Transformed roi format (list of tuple):
            [(21, 33), (32, 43), ...]

    Args:
        roi (list of objects): Input roi to be transformed.
        frame_size (tuple): The frame size of input image frames with
            format (width, height).

    Returns:
        Transformed roi.
    """
    result = []
    for r in roi:
        if ((r["x"] < 0 or r["x"] > 1) or
                (r["y"] < 0 or r["y"] > 1)):
            raise ValueError("Invalid roi point format, should be a float with"
                             " value between 0 and 1.")
        result.append([int(float(r["x"]) * float(frame_size[0])),
                       int(float(r["y"]) * float(frame_size[1]))])
    return [result]


class _Candidate:
    """Abandoned Object Candidate"""

    STATE_CADIDATE = 0
    STATE_UNATTENDED_OBJECT = 1
    STATE_NONE = 2

    def __init__(self, xmin, ymin, xmax, ymax, start_time, initial_img):
        self.id = -1
        self.state = _Candidate.STATE_NONE
        self.xmin = xmin
        self.ymin = ymin
        self.xmax = xmax
        self.ymax = ymax
        self.start_time = start_time
        self.category = ''
        self.stay_time = 0
        self.is_stable = False
        self.object_img = None
        self.miss_pairing_count = 0
        self.paired = False
        self.classified = False
        self.initial_img = initial_img
        self.disappear_count = 0

    def assign_id(self):
        self.id = str(uuid.uuid4())

    def renew(self, frame, pairing_time, alert_time):
        self.stay_time = time.time() - self.start_time
        self.paired = True
        self.object_img = frame[self.ymin:self.ymax, self.xmin:self.xmax, :]
        if self.stay_time > pairing_time and \
           self.is_stable is False:
            self.is_stable = True
        if self.stay_time > alert_time and \
           self.state == _Candidate.STATE_NONE:
            self.state = _Candidate.STATE_CADIDATE


class _OutputPolicy(SharedEventVideoPolicy):
    def compute(self, frame):
        actions = {}

        for key, metadata in frame.metadatas.items():
            if metadata["mode"] == ObjectEvent.STATE_RECORDING_START:
                action = SharedEventVideoPolicy.START_RECORDING
            elif metadata["mode"] == ObjectEvent.STATE_RECORDING_END:
                action = SharedEventVideoPolicy.STOP_RECORDING
            else:
                action = None
            actions[key] = action

        return actions


class ObjectEvent(object):
    """ Real event for after triggering

        STATE_RECORDING_START (send event, start recording) |
        STATE_NORMAL <= STATE_RECORDING_END <===============|
    """
    STATE_NORMAL = 0
    STATE_RECORDING_START = 1
    STATE_RECORDING_END = 3

    def __init__(self, meta_data, initial_img, expiration_time):
        self.meta_data = meta_data
        self.initial_img = initial_img
        self.state = self.STATE_RECORDING_START
        self.timestamp_start = time.time()
        self.expiration_time = expiration_time

    def check_expire(self, timestamp_now):
        if self.state == self.STATE_RECORDING_START and \
           timestamp_now - self.timestamp_start >= self.expiration_time:
            self.state = self.STATE_RECORDING_END


class UnattendedObjectClassification(object):

    def __init__(self, fps, frame_size, roi,
                 alert_time, record_time_after_alert,
                 object_min_width_ratio=0, object_min_height_ratio=0):
        try:
            # Get Dask client
            self._client = get_client(address=address)
        except ValueError:
            assert False, ("Should connect to Dask scheduler before"
                           " initializing this object.")

        self.frame_size = frame_size
        self.resize = 2
        self.new_w = int(frame_size[0] / self.resize)
        self.new_h = int(frame_size[1] / self.resize)
        # stage 1 (is stable)
        self.pairing_time = 2.5
        # stage 2 (for alert)
        self.alert_time = alert_time
        self._record_time_after_alert = record_time_after_alert
        self.miss_pairing_max = int(fps * 3)  # in second

        self.object_min_width = int(frame_size[0] * object_min_width_ratio)
        self.object_min_height = int(frame_size[1] * object_min_height_ratio)

        self._roi = roi
        self.set_roi_frame()

        self._config_ratio = frame_size[1] / 1080.0
        self.area_limit = int(100 * self._config_ratio)
        self.dist_thr = int(10 * self._config_ratio)

        self._object_list = []

        self.fgbg = cv2.createBackgroundSubtractorKNN(
                        history=3000, detectShadows=False)

        self._global_time = time.time()

        # based on unattended object classification model category
        self._categories = UNATTENDED_OBJECT_CLASSIFICATION_CATEGORIES
        self._target_list = UNATTENDED_OBJECT_CLASSIFICATION_TARGET_LIST
        self._objects_event = {} ## key: objectId, value: (`ObjectEvent`)

        logging.info("Created an Unattended Object Classification app")

    def __del__(self):
        self._client.close()

    @property
    def roi(self):
        return self._roi

    @roi.setter
    def roi(self, value):
        self._roi = value
        self.set_roi_frame()

    def set_roi_frame(self):
        self._roi_frame = np.zeros((self.frame_size[1],
                                    self.frame_size[0], 3), dtype="uint8")
        for c in self._roi:
            c = np.array(c)
            cv2.drawContours(self._roi_frame, [c], -1, (255, 255, 255), -1)
        self._roi_frame = cv2.resize(self._roi_frame, (self.new_w, self.new_h))

    def compute_iou(self, rec1, rec2):
        """
        computing IoU
        :param rec1: (y0, x0, y1, x1), which reflects
                (top, left, bottom, right)
        :param rec2: (y0, x0, y1, x1)
        :return: scale value of IoU
        """
        # computing area of each rectangles
        S_rec1 = (rec1[2] - rec1[0]) * (rec1[3] - rec1[1])
        S_rec2 = (rec2[2] - rec2[0]) * (rec2[3] - rec2[1])

        # computing the sum_area
        sum_area = S_rec1 + S_rec2

        # find the each edge of intersect rectangle
        left_line = max(rec1[1], rec2[1])
        right_line = min(rec1[3], rec2[3])
        top_line = max(rec1[0], rec2[0])
        bottom_line = min(rec1[2], rec2[2])

        # judge if there is an intersect
        if left_line >= right_line or top_line >= bottom_line:
            return 0
        else:
            intersect = (right_line - left_line) * (bottom_line - top_line)
            return intersect / (sum_area - intersect)

    def compute_distence(self, rec1, rec2):
        """
        computing distence
        :param rec1: (y0, x0, y1, x1), which reflects
                (top, left, bottom, right)
        :param rec2: (y0, x0, y1, x1)
        :return: scale value of distence
        """
        rec1_center = [(rec1[3]+rec1[1])/2, (rec1[2]+rec1[0])/2]
        rec2_center = [(rec2[3]+rec2[1])/2, (rec2[2]+rec2[0])/2]
        return ((rec1_center[0]-rec2_center[0])**2 +
                (rec1_center[1]-rec2_center[1])**2)**0.5

    def check_object_size(self, area, width, height):
        is_ok = True
        if area > self.new_w * self.new_h / 8 or \
           area < self.area_limit / (self.resize ** 2):
            is_ok = False
        if self.object_min_width != 0 and self.object_min_height != 0:
            if width < self.object_min_width and \
               height < self.object_min_height:
                is_ok = False
        return is_ok

    def check_object_list_1(self, object_list, new_coord, frame):
        # TODO let object_list immutable, return new object_list
        match = False
        xmin, ymin = new_coord[1], new_coord[0]
        xmax, ymax = new_coord[3], new_coord[2]
        for old in object_list:
            old_coord = (old.ymin, old.xmin, old.ymax, old.xmax)
            dist = self.compute_distence(old_coord, new_coord)
            if dist < self.dist_thr:
                old.renew(frame, self.pairing_time, self.alert_time)
                match = True
                break
            bbpath = mplPath.Path(np.array([[old.xmin, old.ymin],
                                            [old.xmax, old.ymin],
                                            [old.xmax, old.ymax],
                                            [old.xmin, old.ymax]]))
            if sum(bbpath.contains_points([[xmin, ymin], [xmin, ymax], [xmax, ymin], [xmax, ymax]])) == 4:
                match = True
                break
        if match is False:
            initial_img = frame.copy()
            cv2.rectangle(initial_img,
                          (xmin, ymin),
                          (xmax, ymax), (0, 0, 255), 2)

            object_list.append(_Candidate(
                                xmin, ymin, xmax, ymax, time.time(),
                                initial_img))

    def check_object_list_2(self, object_list, frame):
        for obj in object_list:
            if obj.paired is False and obj.is_stable is True:
                new_object_image = frame[obj.ymin:obj.ymax,
                                         obj.xmin:obj.xmax, :]
                std = self.check_stay(new_object_image, obj.object_img)
                if std < STANDARD_DEVIATION_THRESHOLD:
                    obj.renew(frame, self.pairing_time, self.alert_time)

    def check_stay(self, img_1, img_2):
        diff = cv2.absdiff(img_1, img_2)
        std = np.std(diff)
        return std

    def classify(self, img):
        f_images = self._client.scatter([img])
        f_detect = gpu_worker.run_model(
                                self._client,
                                "unattended_object_classification",
                                f_images)

        probabilities = f_detect.result()
        out_index = np.argmax(probabilities[0])
        return self._categories[out_index], probabilities[0][out_index]

    def check_abandoned(self, frame, timestamp, frame_with_boxes, object_list):
        renew_global_time = False

        if USE_CLASSIFICATION is True:
            for idx, obj in reversed(list(enumerate(object_list))):
                if DEBUG:
                    cv2.rectangle(frame_with_boxes,
                                  (obj.xmin, obj.ymin),
                                  (obj.xmax, obj.ymax),
                                  (0, 0, 0), 2)
                object_img = frame[obj.ymin:obj.ymax, obj.xmin:obj.xmax, :]
                if obj.state == _Candidate.STATE_CADIDATE and \
                   obj.classified is False:
                    category, prob = self.classify(object_img)
                    obj.classified = True
                    if category in self._target_list:
                        obj.state = \
                            _Candidate.STATE_UNATTENDED_OBJECT
                        if obj.id == -1:
                            obj.assign_id()
                            obj.object_img = object_img
                            obj.category = category
                            cv2.putText(frame_with_boxes,
                                        obj.category,
                                        (obj.xmin, obj.ymax-5),
                                        cv2.FONT_HERSHEY_TRIPLEX, 1,
                                        (0, 255, 0), 1, cv2.LINE_AA)
                            cv2.rectangle(frame_with_boxes,
                                          (obj.xmin, obj.ymin),
                                          (obj.xmax, obj.ymax),
                                          (0, 0, 255), 2)
                            meta_data = {
                                'xmin': obj.xmin,
                                'xmax': obj.xmax,
                                'ymin': obj.ymin,
                                'ymax': obj.ymax,
                                'timestamp': timestamp,
                                'triggered': category
                            }
                            
                            self._objects_event[obj.id] = \
                                ObjectEvent(meta_data, obj.initial_img,
                                            self._record_time_after_alert)
                    else:
                        del object_list[idx]

                if obj.state == _Candidate.STATE_UNATTENDED_OBJECT and \
                        time.time() - self._global_time > 1:
                    renew_global_time = True
                    std = self.check_stay(obj.object_img, object_img)
                    if std > STANDARD_DEVIATION_THRESHOLD:
                        obj.disappear_count += 1
                        if obj.disappear_count >= DISAPPEAR_THRESHOLD:
                            category, prob = self.classify(object_img)
                            obj.category = category
                            if category in self._target_list:
                                obj.disappear_count = 0
                            else:
                                cv2.putText(frame_with_boxes,
                                            obj.category,
                                            (obj.xmin, obj.ymax-5),
                                            cv2.FONT_HERSHEY_TRIPLEX, 1,
                                            (0, 255, 0), 1, cv2.LINE_AA)
                                cv2.rectangle(frame_with_boxes,
                                              (obj.xmin, obj.ymin),
                                              (obj.xmax, obj.ymax),
                                              (0, 0, 255), 2)
                                del object_list[idx]
                    else:
                        obj.object_img = object_img
                        obj.disappear_count = 0
            if renew_global_time:
                self._global_time = time.time()

        for obj in object_list:
            if obj.state == _Candidate.STATE_UNATTENDED_OBJECT or \
               USE_CLASSIFICATION is False:
                cv2.putText(frame_with_boxes, obj.category,
                            (obj.xmin, obj.ymax-5),
                            cv2.FONT_HERSHEY_TRIPLEX, 1,
                            (0, 255, 0), 1, cv2.LINE_AA)
                cv2.rectangle(frame_with_boxes, (obj.xmin, obj.ymin),
                              (obj.xmax, obj.ymax), (0, 0, 255), 2)

        return frame_with_boxes

    def run(self, frames):
        event_video_frames = []
        unattended_object_frames = []  # list of frame

        for frame in frames:
            image = frame.image
            image_resize = cv2.resize(image, (self.new_w, self.new_h))
            timestamp = frame.timestamp

            if len(self._roi) > 0:
                image_resize[self._roi_frame == 0] = 0

            fgmask = self.fgbg.apply(image_resize)

            kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (3, 3))
            binary = cv2.erode(fgmask, kernel, iterations=1)
            kernel = cv2.getStructuringElement(cv2.MORPH_RECT, (13, 13))
            binary = cv2.morphologyEx(binary, cv2.MORPH_CLOSE, kernel)

            _, contours, hierarchy = cv2.findContours(binary,
                                                      cv2.RETR_EXTERNAL,
                                                      cv2.CHAIN_APPROX_SIMPLE)

            frame_with_boxes = image.copy()
            for c in contours:
                area = cv2.contourArea(c)

                (x, y, w, h) = cv2.boundingRect(c)
                (x, y, w, h) = (x * self.resize, y * self.resize,
                                w * self.resize, h * self.resize)

                x = max(int(x - 0.2 * w), 0)
                y = max(int(y - 0.2 * h), 0)
                w = int(w * 1.4)
                h = int(h * 1.4)

                if self.check_object_size(area, w, h) is False:
                    continue

                new_coord = (y, x, y + h, x + w)  # for compute iou
                self.check_object_list_1(self._object_list, new_coord, image)
            self.check_object_list_2(self._object_list, image)
            for idx, obj in reversed(list(enumerate(self._object_list))):
                if obj.state == _Candidate.STATE_NONE:
                    if obj.paired is False:
                        obj.miss_pairing_count += 1
                        if obj.miss_pairing_count > self.miss_pairing_max:
                            del self._object_list[idx]
                    else:
                        obj.paired = False

            frame_with_boxes = self.check_abandoned(
                    image, timestamp, frame_with_boxes, self._object_list)

            unattended_object_frames.append(
                    VideoFrame(frame_with_boxes, time.time()))
            metadatas = {}
            for k, v in self._objects_event.items():
                v.check_expire(timestamp)
                metadatas[k] = {'mode': v.state} #, 'metadata': metadata}
            base_metadata = {"mode": ObjectEvent.STATE_NORMAL}

            event_video_frames.append(SharedEventVideoFrame(
                copy.deepcopy(frame),
                metadatas=metadatas,
                base_metadata=base_metadata,
            ))
            ## post refresh status
            for k, v in self._objects_event.items():
                if v.state == ObjectEvent.STATE_RECORDING_END:
                    v.state = ObjectEvent.STATE_NORMAL
        return unattended_object_frames, event_video_frames

    def release(self):
        pass


class UnattendedObjectClassificationPipeline(object):

    def __init__(self, anal_id, fps, frame_size, roi, alert_time,
                 object_min_width_ratio, object_min_height_ratio,
                 record_time_before_alert,
                 record_time_after_alert,
                 video_format="mp4",
                 fps_recording=10, fps_source=30):
        self._anal_id = anal_id
        self._name = 'unattendeded_object_classification'
        self._obj_key_prefix = os.path.join(self._name, anal_id)
        self._frame_size = frame_size

        # Create an Unattended Object Classification Instance
        transformed_roi = transform_roi_format(roi, frame_size)
        self._classificator = \
            UnattendedObjectClassification(
                    fps, frame_size, transformed_roi,
                    alert_time, record_time_after_alert,
                    object_min_width_ratio, object_min_height_ratio)
        
        # Create output video agent
        event_video_metadata = {
            "event_name": self._name,
            "event_custom": {"roi": transformed_roi}
        }

        q_size_before = int(fps_recording * record_time_before_alert)
        q_size_after = int(fps_recording * record_time_after_alert)
        q_size = q_size_after + q_size_before
        self._output_agent = SharedPersistentEventVideoAgent(
            anal_id,
            _OutputPolicy(),
            event_video_metadata,
            self._obj_key_prefix,
            q_size,
            frame_size,
            fps_recording,
            fps_source,
            video_format)


        # Connect to Object Storage service.
        self._obj_storage = ObjectStorageClient()
        self._obj_storage.connect()

        # Connect to Notification service.
        self._notification = Notification()
        # Connect to Database service.
        self._database = Database()

    def _take_snapshot(self, filename, frame):
        """Save a frame to an image file and push it to the object storage.

        Args:
            filename (string): The name of the snapshot.
            frame (`SharedEventVideoFrame`): The frame object to be saved.

        Returns:
            string: The key of the snapshot in the object storage.
        """
        snapshot_key = os.path.join(self._obj_key_prefix, "{}.jpg"
                                    .format(filename))
        shrunk_image = im.shrink_image(frame.image)
        self._obj_storage.save_image_obj(snapshot_key, shrunk_image)

        return snapshot_key

    def _save_meta_data(self, filename, meta_data):
        metadata_key = os.path.join(self._obj_key_prefix, "{}.json"
                                    .format(filename))
        self._obj_storage.save_json_obj(metadata_key, meta_data)
        return metadata_key

    def _output_event(self, event, timestamp, metadata_key, snapshot_key, triggered):
        """Output event to notification center and database.

        Args:
            event (`AgentEvent`): The event object to be outputted.
        """
        event_type = '{}.alert'.format(self._name)
        content = {
            "video": event.content["video_key"],
            "metadata_video": event.content["metadata_key"],
            "metadata_snapshot": metadata_key,
            "snapshot": snapshot_key,
            "triggered": triggered,
        }

        # Save event to database.
        event_id = self._database.save_event(self._anal_id, timestamp,
                                             event_type, content)

        logging.info("Success to save event in database: analyzerId: {}, "
                     "timestamp: {}, type: {}, content: {}"
                     .format(self._anal_id, timestamp, event_type, content))

        # Push notification.
        self._notification.push_anal(event_id,
                                     self._anal_id,
                                     timestamp,
                                     event_type,
                                     content)

    def _check_triggers(self, frames, meta_datas):
        out_event_frames = []
        out_event_meta_datas = []
        for i in range(len(frames)):
            if len(meta_datas[i]) > 0:
                out_event_frames.append(frames[i])
                out_event_meta_datas.append(meta_datas[i])

        return out_event_frames, out_event_meta_datas

    def run(self, frames, motions, is_src_reader_busy):
        unattended_object_frames, event_video_frames = \
            self._classificator.run(frames)
        for frame in event_video_frames:
            out_event = self._output_agent.process(frame)
            for objectID, agent_event in out_event.agent_events.items():
                if agent_event.action == SharedEventVideoPolicy.START_RECORDING:
                    logging.info('out_event {}'.format(objectID))
                    timestamp = agent_event.content["timestamp"]
                    thumbnail_filename = os.path.join(objectID,
                                                      str(timestamp))
                    snapshot = VideoFrame(self._classificator._objects_event[objectID].initial_img, timestamp)
                    thumbnail_key = self._take_snapshot(thumbnail_filename,
                                                        snapshot)
                    metadata = self._classificator._objects_event[objectID].meta_data
                    metadata_key = self._save_meta_data(
                        os.path.join(objectID, str(timestamp + 1)),# TODO prevent metadata filename conflict
                        metadata)
                    self._output_event(agent_event,timestamp,
                                        metadata_key, thumbnail_key,
                                        metadata['triggered'])
                elif agent_event.action == SharedEventVideoPolicy.STOP_RECORDING:
                    logging.info("End of event video for {}".format(objectID))
                    # currently SharedPersistentEventVideoAgent not return here
                    #self._classificator._objects_event.pop(objectID, None) # TODO move this to refresh part

        return unattended_object_frames

    def release(self):
        self._classificator.release()
