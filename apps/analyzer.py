from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import inspect
import math
import time, datetime
import asyncio
import multiprocessing
import threading
import json
import os
from threading import Thread
from bson.son import SON
from queue import Empty as QueueEmpty
from queue import Full as QueueFull
from dask.distributed import LocalCluster, Client
from multiprocessing import Process, Pipe, TimeoutError, Pipe
from pymongo import UpdateOne

import one_shot
from one_shot.one_shot import OneShotExecutor
from one_shot.one_shot import OneShotResult
from one_shot.one_shot import OneShotTask
from utils import AsyncTimer
from utils import deep_equal
from utils import RTSP_server
from intrusion_detection import IntrusionDetectionPipeline
from illegal_parking_detection import IllegalParkingDetectionPipeline
from car_counting import CarCountingPipeline
from directional_car_counting import DirectionalCarCountingPipeline
from single_camera_license_plate_recognition \
import SingleCameraLicensePlateRecognitionPipeline
from cross_camera_license_plate_recognition \
import CrossCameraLicensePlateRecognitionPipeline
from single_camera_face_recognition import SingleCameraFaceRecognitionPipeline
from cross_camera_face_recognition import CrossCameraFaceRecognitionPipeline
from stitching.stitching import StitchingPipeline
from unattended_object_classification \
import UnattendedObjectClassificationPipeline

from jagereye_ng import video_proc as vp
from jagereye_ng import gpu_worker
from jagereye_ng import gpu_scheduler
from jagereye_ng.api import APIConnector
from jagereye_ng.io.const import ONE_SHOT_STATUS
from jagereye_ng.io.db_engine import connect_db
from jagereye_ng.io.db_engine import DatabaseError
from jagereye_ng.io.streaming \
import VideoStreamReader, VideoSyncMultiStreamReader
from jagereye_ng.io import io_worker, notification, database
from jagereye_ng.util.generic import get_config, get_messaging
from jagereye_ng.util.generic import is_env_production, is_env_development
from jagereye_ng.util.generic import uuid
from jagereye_ng.util.string import pascal_to_snake
from jagereye_ng import logging
import gi
gi.require_version('Gst', '1.0')
from gi.repository import Gst, GObject
from jagereye_ng.io.rtsp import GstServer

NATS_CHANNEL_NAME = get_messaging()["channels"]["API_TO_ANALYZER"]

ONE_SHOT_COLLECTIONS = [
    "externals",
]

class HotReconfigurationError(Exception):
    def __str__(self):
        return ("Hot re-configuring analyzer is not allowed, please"
                " stop analyzer first before updating it.")


def choose_read_buffer_size(pipelines, default_size):
    buffer_size = default_size

    for p in pipelines:
        type_name = pascal_to_snake(p["type"])
        config = get_config()["apps"][type_name]
        p_size = config.get("read_buffer_size", default_size)
        if p_size < default_size:
            buffer_size = p_size

    return buffer_size


def create_pipeline(anal_id, source, pipelines, frame_size, fps_reader):
    result = []
    stream_output_width = None
    stream_output_height = None
    for p in pipelines:
        params = p["params"]
        if p["type"] == "IntrusionDetection":
            config = get_config()["apps"]["intrusion_detection"]
            result.append(IntrusionDetectionPipeline(
                anal_id,
                params["roi"],
                params["triggers"],
                frame_size,
                config["video_format"],
                config["fps"],
                config["history_len"]))
        elif p["type"] == "CarCounting":
            config = get_config()["apps"]["car_counting"]
            result.append(CarCountingPipeline(
                anal_id,
                params["lines"],
                params["triggers"],
                frame_size))
        elif p["type"] == "DirectionalCarCounting":
            config = get_config()["apps"]["directional_car_counting"]
            result.append(DirectionalCarCountingPipeline(
                anal_id,
                params["paths"],
                params["triggers"],
                frame_size))
        elif p["type"] == "SingleCameraLicensePlateRecognition":
            config = \
                get_config()["apps"]["single_camera_license_plate_recognition"]
            result.append(SingleCameraLicensePlateRecognitionPipeline(
                anal_id,
                params["triggers"],
                frame_size,
                config["video_format"],
                config["fps"],
                config["history_len"]))
        elif p["type"] == "CrossCameraLicensePlateRecognition":
            config = \
                get_config()["apps"]["cross_camera_license_plate_recognition"]
            result.append(CrossCameraLicensePlateRecognitionPipeline(
                anal_id,
                params["triggers"],
                frame_size,
                config["video_format"],
                config["fps"],
                config["history_len"]))
        elif p["type"] == "SingleCameraFaceRecognition":
            config = \
                get_config()["apps"]["single_camera_face_recognition"]
            result.append(SingleCameraFaceRecognitionPipeline(
                anal_id,
                config["analysis_fps"],
                params["roiList"],
                params["faceMinWidth"],
                params["faceMinHeight"],
                params["detectionThreshold"],
                params["recognitionThreshold"],
                params["nextPeriod"],
                params["outputNonMatched"],
                frame_size))
        elif p["type"] == "CrossCameraFaceRecognition":
            config = \
                get_config()["apps"]["cross_camera_face_recognition"]
            result.append(CrossCameraFaceRecognitionPipeline(
                anal_id,
                config["analysis_fps"],
                params["triggers"],
                params["roiList"],
                params["faceMinWidth"],
                params["faceMinHeight"],
                params["detectionThreshold"],
                params["recognitionThreshold"],
                params["nextPeriod"],
                params["outputNonMatched"],
                frame_size))
        if p["type"] == "IllegalParkingDetection":
            config = get_config()["apps"]["illegal_parking_detection"]
            result.append(IllegalParkingDetectionPipeline(
                anal_id,
                params["roiList"],
                params["triggers"],
                params.get("alertTime", 30),
                params.get("recordTimeBeforeAlert", 30),
                params.get("recordTimeAfterAlert", 30),
                frame_size,
                config["video_format"],
                config["fps"]))
        elif p["type"] == "Stitching":
            stitching_pipeline = StitchingPipeline(
                anal_id,
                source,
                str(params["externalId"]),
                params.get("roi", []),
                params["stitchConfig"],
                frame_size)
            stream_output_shape = stitching_pipeline.get_output_frame_shape()
            stream_output_width = stream_output_shape[1]
            stream_output_height = stream_output_shape[0]
            result.append(stitching_pipeline)
        elif p["type"] == "UnattendedObjectClassification":
            config = get_config()["apps"]["unattended_object_classification"]
            result.append(UnattendedObjectClassificationPipeline(
                anal_id,
                config["fps"],
                frame_size,
                params.get("roi", []),
                params.get("alertTime", 3 * 60),
                params.get("objectMinWidthRatio", 0),
                params.get("objectMinHeightRatio", 0),
                params.get("recordTimeBeforeAlert", 30),
                params.get("recordTimeAfterAlert", 30),
            ))

    return result, stream_output_width, stream_output_height


class Driver(object):
    def __init__(self):
        self._driver_process = None
        self._sig_parent = None
        self._sig_child = None

    def start(self, func, *argv):
        # Terminate the previous process if it exists.
        if self._driver_process is not None:
            self.terminate()

        self._sig_parent, self._sig_child = Pipe(duplex=True)
        # Use spawn to create child processes. The child processes will have
        # fresh states when created. (By default, the multiprocessing uses fork
        # method on Unix.)
        context = multiprocessing.get_context('spawn')
        self._driver_process = context.Process(
            target=Driver.run_driver_func,
            args=(func,
                  self._sig_child,
                  argv))
        self._driver_process.daemon = True
        self._driver_process.start()

    def terminate(self, timeout=5):
        assert self._driver_process is not None, "It's an error to attempt to \
            terminate a driver before it has been started."


        try:
            self._driver_process.join(timeout)
        except TimeoutError:
            logging.error("The driver was not terminated for some reason "
                          "(exitcode: {}), force to terminate it."
                          .format(self._driver_process.exitcode))
            self._driver_process.terminate()
            time.sleep(0.1)
        finally:
            if self._driver_process.is_alive():
                self._driver_process.terminate()
                time.sleep(0.1)
            self._sig_parent.close()
            self._sig_child.close()
            self._sig_parent = None
            self._sig_child = None
            self._driver_process = None

    def poll(self, timeout=None):
        if self._sig_parent is not None:
            if timeout is not None:
                return self._sig_parent.poll(timeout)
            else:
                return self._sig_parent.poll()
        else:
            return False

    def send(self, msg):
        self._sig_parent.send(msg)

    def recv(self):
        return self._sig_parent.recv()

    def is_alive(self):
        if self._driver_process is None:
            return False

        return self._driver_process.is_alive()

    @staticmethod
    def run_driver_func(driver_func, signal, *argv):
        try:
            driver_func(signal, *argv[0])
        finally:
            signal.close()


class Analyzer():

    STATUS_STARTING = "starting"
    STATUS_RUNNING = "running"
    STATUS_SRC_DOWN = "source_down"
    STATUS_INTERNAL_ERROR = "internal_error"
    STATUS_STOPPED = "stopped"

    def __init__(self, anal_id, name, source, pipelines,
                    stream_output=False, rtsp_server=None):
        self._id = anal_id
        self._name = name
        self._source = source
        self._pipelines = pipelines
        self._stream_output = stream_output
        self._rtsp_sink = self._rtsp_source = None
        self._rtsp_server = rtsp_server
        self._driver = Driver()
        self._status = Analyzer.STATUS_STOPPED
        self._status_source = self._set_status_source()
        self._status_timer = None

    def _check_hot_reconfiguring(self):
        if (self._status == Analyzer.STATUS_RUNNING or
                self._status == Analyzer.STATUS_STARTING):
            raise HotReconfigurationError()

    @property
    def name(self):
        return self._name

    @name.setter
    def name(self, value):
        self._check_hot_reconfiguring()
        self._name = value

    @property
    def source(self):
        return self._source

    @source.setter
    def source(self, value):
        self._check_hot_reconfiguring()
        self._source = value

    @property
    def pipelines(self):
        return self._pipelines

    @pipelines.setter
    def pipelines(self, value):
        self._check_hot_reconfiguring()
        self._pipelines = value

    @property
    def stream_output(self):
        return self._stream_output

    @stream_output.setter
    def stream_output(self, value):
        self._check_hot_reconfiguring()
        self._stream_output = value

    def _set_status_source(self):
        status_source = ''
        if self._source['mode'] == 'multistreaming':
            status_source = []
        return status_source

    def get_status(self):
        ''' combine analyzer status and source status '''
        return json.dumps({"status": self._status,
                            "status_source": self._status_source})

    def is_stopped(self):
        return self._status == Analyzer.STATUS_STOPPED

    def _refresh_driver_status(self):
        received = None
        received_type = ''
        if self._driver.poll():
            received = self._driver.recv()
            received_type = received.split('::')[0]

        if received_type == 'status_source':
            self._status_source = json.loads(received.split('::')[1])

        if received_type == 'rtsp_setting':
            # Once analyzer send these requirements, ask rtsp start endpoint
            rtsp_setting = json.loads(received.split('::')[1])
            fps = rtsp_setting['fps']
            width = rtsp_setting['width']
            height = rtsp_setting['height']
            read_batch_size = rtsp_setting['read_batch_size']
            self._add_stream_output(fps, width, height, read_batch_size)

        if self._status == Analyzer.STATUS_STARTING:
            if received:
                if received == "analyzer::ready":
                    self._status = Analyzer.STATUS_RUNNING
                elif received == "analyzer::internal_error":
                    self._status = Analyzer.STATUS_INTERNAL_ERROR
                else:
                    self._status = Analyzer.STATUS_SRC_DOWN
            elif self._wait_for_driver_countdown > 0:
                self._wait_for_driver_countdown -= 1
            elif not self._driver.is_alive():
                logging.error("Analyzer is terminated for unknown reason when "
                              "starting: {}".format(self._name))
                self._status = Analyzer.STATUS_INTERNAL_ERROR
            else:
                self._status = Analyzer.STATUS_SRC_DOWN
        elif self._status == Analyzer.STATUS_RUNNING:
            if received:
                if received  == "analyzer::source_down":
                    self._status = Analyzer.STATUS_SRC_DOWN
                elif received == "analyzer::internal_error":
                    self._status = Analyzer.STATUS_INTERNAL_ERROR
            elif not self._driver.is_alive():
                logging.error("Analyzer is terminated for unknown reason when "
                              "running: {}".format(self._name))
                self._status = Analyzer.STATUS_INTERNAL_ERROR
        elif (self._status == Analyzer.STATUS_SRC_DOWN or
              self._status == Analyzer.STATUS_INTERNAL_ERROR):
            # Try to restart the driver process
            self.start()

    def _setup_timer(self):
        if self._status_timer is None:
            self._status_timer = AsyncTimer(1, self._refresh_driver_status)
            self._status_timer.start()

    def _cleanup_timer(self):
        if self._status_timer is not None:
            self._status_timer.cancel()
            self._status_timer = None

    def _cleanup_driver(self):
        self._driver.send("stop")
        self._driver.terminate()

    def _add_stream_output(self, fps, width, height, read_batch_size):
        '''Ask RTSP server add media with endpoint'''
        self._rtsp_server.add_stream(
                self._rtsp_sink, self._id,
                fps, width, height, read_batch_size)

    def _cleanup_pipe_and_remove_stream_output(self, remove_endpoint=False):
        '''Cleanup unused pipe and ask RTSP server remove endpoint'''
        if self._rtsp_sink is not None:
            self._rtsp_sink.close()
        if self._rtsp_source is not None:
            self._rtsp_source.close()
        self._rtsp_sink = self._rtsp_source = None
        if remove_endpoint:
            self._rtsp_server.remove_stream(self._id)

    def start(self):
        if (self._status != Analyzer.STATUS_RUNNING and
                self._status != Analyzer.STATUS_STARTING):
            self._cleanup_pipe_and_remove_stream_output(remove_endpoint=False)
            ''' self._rtsp_sink, self._rtsp_source:
                these two variable are simplex messaging channel between
                process, create a pipe for stream source and sink before fork
                new process.
            '''
            self._rtsp_sink, self._rtsp_source = Pipe(duplex=False)
            self._driver.start(analyzer_main_func,
                               self._id,
                               self._name,
                               self._source,
                               self._pipelines,
                               self._stream_output,
                               self._rtsp_source)
            self._status = Analyzer.STATUS_STARTING
            ''' _wait_for_driver_countdown:
                from analyzer status STATUS_STARTING -> STATUS_STARTING
                the initialization of analyzer_main_func create_pipeline
                progress to ready should takes less than
                _wait_for_driver_countdown sec
            '''
            self._wait_for_driver_countdown = 100
            self._setup_timer()

    def stop(self):
        if not self.is_stopped():
            self._cleanup_timer()
            self._cleanup_driver()
            self._cleanup_pipe_and_remove_stream_output(remove_endpoint=True)
        self._status = Analyzer.STATUS_STOPPED

    def set_debug_mode(self, params):
        if params["debugMode"] == True:
            self._driver.send("turn_on_debug_mode")
        elif params["debugMode"] == False:
            self._driver.send("turn_off_debug_mode")

    def __repr__(self):
        return "Analyzer(" + self._id + ")"

def analyzer_main_func(signal, anal_id, name, source, pipelines,
                        stream_output, rtsp_pipe):
    logging.info("Starts running Analyzer: {}".format(name))

    config = get_config()["apps"]["base"]
    read_batch_size = config["read_batch_size"]

    if source['mode'] == 'streaming' or source['mode'] == 'file':
        buffer_size = choose_read_buffer_size(pipelines, 64)
        fps = 15
        src_reader = VideoStreamReader(source['url'],
                                        fps, buffer_size)
    elif source['mode'] == 'multistreaming':
        read_batch_size = 1
        buffer_size = 10
        fps = 30
        src_reader = VideoSyncMultiStreamReader(source['urls'],
                                                fps, buffer_size)
    else:
        logging.error('Analyzer {} source wrong'.format(anal_id))
        signal.send("analyzer::internal_error")
        return
    status_source, err_open = src_reader.open(3)
    # open video streaming with timeout
    if err_open != None:
        signal.send("analyzer::source_down")
        return
    else:
        # wait for load source info
        time.sleep(1)
        video_info = src_reader.get_video_info()

    try:
        error_message = ''
        pipelines, pipeline_width, pipeline_height = create_pipeline(
            anal_id,
            source,
            pipelines,
            video_info["frame_size"],
            fps)

        # Tell parent process analyzer while pipeline has been created
        signal.send('analyzer::ready')

        # init video source status
        signal.send('status_source::{}'.format(json.dumps(status_source)))

        if stream_output == True:
            stream_output_width = video_info["frame_size"][0]
            stream_output_height = video_info["frame_size"][1]
            # start rtsp streaming output
            rtsp_setting = {
                'fps': fps, # TODO move this config to application level
                'width': pipeline_width if pipeline_width is not None \
                                            else stream_output_width,
                'height': pipeline_height if pipeline_height is not None \
                                            else stream_output_height,
                'read_batch_size': read_batch_size
            }
            signal.send('rtsp_setting::{}'.format(json.dumps(rtsp_setting)))

        while True:
            frames, _status_source, err_read = src_reader.read(
                                                    batch_size=read_batch_size)
            if status_source != _status_source:
                # to prevent keep sending status
                status_source = _status_source
                signal.send('status_source::{}'.format(
                            json.dumps(status_source)))

            if err_read is not None:
                error_message = 'analyzer::source_down'
                break

            is_src_reader_busy = src_reader.is_busy()
            rendered_frames = frames
            for p in pipelines:
                motions = []
                if source['mode'] == 'multistreaming':
                    # TODO stream and multi source motion
                    motions = [[]]
                else:
                    normalized_roi = None
                    if hasattr(p, "normalized_roi"):
                        normalized_roi = p.normalized_roi
                    motions = vp.detect_motion(frames, \
                                config["motion_threshold"], normalized_roi)
                # TODO design a better pipeline flow for input/output frame
                rendered_frames = p.run(rendered_frames, motions, is_src_reader_busy)

            if stream_output and rendered_frames is not None:
                for raw_frame in rendered_frames:
                    rtsp_pipe.send(raw_frame.image)

            if signal.poll():
                recvd_msg = signal.recv()
                if recvd_msg == "stop":
                    break
                elif recvd_msg == "turn_on_debug_mode":
                    logging.set_debug_mode({"debugMode": True})
                elif recvd_msg == "turn_off_debug_mode":
                    logging.set_debug_mode({"debugMode": False})

    except Exception as ex:
        logging.exception('Error occurred when running pipelines: {}'.format(str(ex)))
        error_message = "analyzer::internal_error"
    finally:
        src_reader.release()
        for p in pipelines:
            if hasattr(p, "release"):
                p.release()
        logging.info("Analyzer terminated: {}".format(name))
        if error_message != '':
            signal.send(error_message)


class OneShotRunner(object):
    """Actual runner for one-shot tasks execution."""

    STATUS_RUNNING = "running"
    STATUS_STOPPED = "stopped"

    def __init__(self,
                 runner_id,
                 task_queue,
                 result_queue,
                 health_timer_period,
                 reget_task_period):
        """Initialize a `OneShotRunner` instance.

        Args:
            runner_id (string): Unique ID of the runner.
            task_queue (`multiprocessing.queues.Queue`): Queue to get tasks.
            result_queue (`multiprocessing.queues.Queue`): Queue to insert task
                results.
            health_timer_period (float): The timer period to check and assure
                runner process health (in second).
            reget_task_period (float): The period to try regetting next task
                when task queue is empty (in second).
        """
        self._runner_id = runner_id
        self._task_queue = task_queue
        self._result_queue = result_queue
        self._health_timer_period = health_timer_period
        self._reget_task_period = reget_task_period
        self._status = OneShotRunner.STATUS_STOPPED
        self._driver = Driver()
        self._health_timer = None

    def _is_healthy(self):
        return self._driver.is_alive()

    def _assure_health(self):
        if not self._is_healthy():
            # Restart if the driver process is no healthy.
            self.stop()
            self.start()

    def _setup_timer(self):
        if self._health_timer is None:
            self._health_timer = AsyncTimer(self._health_timer_period,
                                            self._assure_health)
            self._health_timer.start()

    def _cleanup_timer(self):
        if self._health_timer is not None:
            self._health_timer.cancel()
            self._health_timer = None

    def _cleanup_driver(self):
        self._driver.terminate()

    def start(self):
        if self._status == OneShotRunner.STATUS_STOPPED:
            self._driver.start(one_shot_main,
                               self._runner_id,
                               self._task_queue,
                               self._result_queue,
                               self._reget_task_period)
            self._status = OneShotRunner.STATUS_RUNNING
            self._setup_timer()

    def stop(self):
        if self._status == OneShotRunner.STATUS_RUNNING:
            self._cleanup_timer()
            self._cleanup_driver()
        self._status = OneShotRunner.STATUS_STOPPED


class _OneShotTasksBatch(object):
    """Helper class for gathering a one-shot tasks batch."""

    def __init__(self, name, executor):
        """Initialize a `_OneShotTasksBatch` instance.

        Args:
            name (string): The task name for this batch.
            executor (`one_shot.one_shot.OneShotExecutor`): The task executor
                for this batch.
        """
        self._name = name
        self._executor = executor
        self._tasks = []

    @property
    def name(self):
        """string: The task name."""
        return self._name

    @property
    def executor(self):
        """`one_shot.one_shot.OneShotExecutor`: The task executor."""
        return self._executor

    @property
    def tasks(self):
        """list of `one_shot.one_shot.OneShotTask`: The tasks batch."""
        return self._tasks

    def append(self, task):
        self._tasks.append(task)

    def size(self):
        return len(self._tasks)


def one_shot_main(signal,
                  runner_id,
                  task_queue,
                  result_queue,
                  reget_task_period):
    logging.info("Starting running one-shot analyzer, (ID = {})"
                 .format(runner_id))

    # Get all one-shot executors.
    executors = {}
    for name in dir(one_shot):
        definition = getattr(one_shot, name)
        is_executor = (inspect.isclass(definition) and
                       issubclass(definition, OneShotExecutor))

        if is_executor:
            executors[name] = definition()
    next_task = None
    task_batch = None
    to_execute = False

    # Infinite loop for one-shot tasks execution.
    # In the loop, we will gather a batch of tasks, which all have the same task
    # name, then invoke the correspoding executor to run. For example, assume we
    # have 2 types of executors: "A" and "B". If the tasks queue is: (A, A, B),
    # first we will execute a batch with 2 "A" tasks, and then execute a batch
    # with 1 "B" task.
    while True:
        try:
            # Try to get a task from the queue.
            task = task_queue.get_nowait()
        except QueueEmpty:
            # If the task queue is empty, we may have 2 choices:
            # 1. The batch is not gathered yet, then we sleep for a while and
            #    continue to next loop.
            # 2. The batch is gathered, then we need to triiger the batch
            #    execution now.
            if task_batch is None:
                time.sleep(reget_task_period)
                continue
            else:
                to_execute = True
        else:
            # We got a task but it does not have correspoding executor, then we
            # seems it fails and continue to next loop.
            if task.name not in executors:
                reason = "Non-Supported one-shot task: {}".format(task.name)
                result = OneShotResult(task, False, reason=reason)
                result_queue.put(result)
                logging.error(reason)
                continue

            # We got a task and it does has correspoding executor, we may have
            # 3 choices:
            # 1. The batch is not gathered yet, then we create a new batch and
            #    append the task to the batch.
            # 2. The batch is gathered and the task name equals to the batch
            #    task name and the batch is not still full, then we append the
            #    task to the batch.
            # 3. The batch is gathered but thetask name does equal to the batch
            #    task name or the batch is full, then we will feed the task
            #    into "next" batch. In the case, we will also trigger the
            #    execution.
            if task_batch is None:
                task_batch = _OneShotTasksBatch(task.name, executors[task.name])
                task_batch.append(task)
            else:
                if (task_batch.name == task.name and
                    task_batch.size() < task_batch.executor.max_batch_size):
                    task_batch.append(task)
                else:
                    next_task = task
                    to_execute = True

        # Execute the batch if it is triggered.
        if to_execute:
            try:
                results = task_batch.executor.run(task_batch.tasks)
            except Exception as e:
                reason = str(e)
                results = [OneShotResult(task, False, reason=reason)
                           for task in task_batch.tasks]
                logging.exception("Error occurred when running one-shot executor")

            for result in results:
                result_queue.put(result)

            task_batch = None
            to_execute = False

        # Feed the task into next batch if necessary.
        if next_task:
            task_batch = _OneShotTasksBatch(next_task.name,
                                           executors[next_task.name])
            task_batch.append(next_task)
            next_task = None


class OneShotManager(object):
    """Manager of one-shot tasks execution.

    The manager will invoke a series of one-shot runners to execute tasks.
    """

    STATUS_RUNNING = "running"
    STATUS_STOPPED = "stopped"

    def __init__(self,
                 collections,
                 db,
                 min_runners=1,
                 max_runners=8,
                 task_queue_size=100,
                 task_timer_period=1,
                 result_timer_period=1,
                 autoscale_timer_period=1,
                 health_timer_period=1,
                 reget_task_period=1):
        """Initialize a `OneShotRunner` instance.

        Args:
            collections (list of string): The database collection names for
                one-shot tasks.
            db (`pymongo.database.Database`): The database instance.
            min_runners (int): The minimum number of task runners. Defaults to
                1.
            max_runners (int): The maximum number of task runners. Defaults to
                8.
            task_queue_size (int): The maximum length of task queue. Defaults
                to 100.
            task_timer_period (float): The timer period to insert new tasks
                (in second). Defaults to 1.
            result_timer_period (float): The timer period to update task
                results (in second). Defaults to 1.
            autoscale_timer_period (float): The timer period to autoscale the
                number of runners (in second). Default to 1.
            health_timer_period (float): The timer period to check and assure
                runner process health (in second).
            reget_task_period (float): For runner, the period to try regetting
                next task when task queue is empty (in second). Defaults to 1.
        """
        self._collections = collections
        self._db = db
        self._min_runners = min_runners
        self._max_runners = max_runners
        self._task_queue_size = task_queue_size
        self._task_timer_period = task_timer_period
        self._result_timer_period = result_timer_period
        self._autoscale_timer_period = autoscale_timer_period
        self._health_timer_period = health_timer_period
        self._reget_task_period = reget_task_period
        self._runners = {}
        self._task_queue = self._create_queue(maxsize=self._task_queue_size)
        self._result_queue = self._create_queue()
        self._task_timer = None
        self._result_timer = None
        self._autoscale_timer = None
        self._status = OneShotManager.STATUS_STOPPED

    def _gen_runner_id(self):
        return "OneShotRunner-{}".format(uuid())

    def _create_queue(self, maxsize=0):
        context = multiprocessing.get_context("spawn")
        queue = context.Queue(maxsize=maxsize)

        return queue

    def _reset_unfinished_tasks(self):
        for collection in self._collections:
            self._db[collection].update_many({
                "task.status": ONE_SHOT_STATUS.PROCESSING,
            }, {
                "$set": {
                    "task.status": ONE_SHOT_STATUS.PENDING,
                },
            })

    def _insert_collection_tasks(self, collection, limit):
        pending_records = list(self._db[collection].aggregate([{
            "$match": {
                "expired": False,
                "task.status": ONE_SHOT_STATUS.PENDING,
            },
        }, {
            "$sort": SON([
                ("task.createdAt", 1),
            ]),
        }, {
            "$limit": limit,
        }]))

        processing_record_ids = []
        for pending_record in pending_records:
            task = OneShotTask(
                pending_record["task"]["name"],
                pending_record["content"],
                collection,
                pending_record["_id"],
            )
            try:
                self._task_queue.put_nowait(task)
                processing_record_ids.append(pending_record["_id"])
            except QueueFull:
                break

        self._db[collection].update_many({
            "_id": {
                "$in": processing_record_ids,
            },
        }, {
            "$set": {
                "task.status": ONE_SHOT_STATUS.PROCESSING,
            },
        })

    def _insert_tasks(self):
        try:
            limit_total = self._task_queue_size - self._task_queue.qsize()
            limit_avg = int(limit_total / len(self._collections))

            for collection in self._collections:
                self._insert_collection_tasks(collection, limit_avg)
        except Exception as e:
            logging.error("Error occur when assign one-shot tasks: {}"
                          .format(e))

    def _update_result(self):
        requests_dict = {}
        qsize = self._result_queue.qsize()

        for idx in range(qsize):
            try:
                result = self._result_queue.get_nowait()
            except QueueEmpty:
                break

            task = result.task
            search = {
                "_id": task.source_id,
            }

            if not result.success:
                to_update = {
                    "$set": {
                        "task.status": ONE_SHOT_STATUS.FAILED,
                        "task.reason": result.reason,
                    },
                }
            else:
                to_update = {
                    "$set": {
                        "task.status": ONE_SHOT_STATUS.DONE,
                    },
                    "$unset": {
                        "task.reason": "",
                    },
                }
                for key, value in result.updated.items():
                    content_key = "content.{}".format(key)
                    to_update["$set"][content_key] = value

            if task.source_collection not in requests_dict:
                requests_dict[task.source_collection] = []

            request = UpdateOne(search, to_update)
            requests_dict[task.source_collection].append(request)

        try:
            for collection_name, requests in requests_dict.items():
                self._db[collection_name].bulk_write(requests)
        except Exception as e:
            logging.error("Error occur while updating results of one-shot"
                          " tasks: {}".format(e))

    def _autoscale(self):
        # TODO(JiaKuan Su): Implement autoscaling for numbers of runners.
        pass

    def _add_runner(self):
        # Create and start a one-shot runner.
        runner_id = self._gen_runner_id()
        runner = OneShotRunner(runner_id,
                               self._task_queue,
                               self._result_queue,
                               self._health_timer_period,
                               self._reget_task_period)
        runner.start()

        # Update the mapping for runners.
        self._runners[runner_id] = runner

    def _remove_runner(self, runner_id):
        # TODO(JiaKuan Su):
        # The method is unused, but it will be useful when implementing
        # autoscaling and stop.
        if runner_id in self._runners:
            self._runners[runner_id].stop()
            self._runners.pop(runner_id, None)

    def _setup_timers(self):
        if self._task_timer is None:
            # Create and start a timer to insert tasks periodically.
            self._task_timer = AsyncTimer(self._task_timer_period,
                                          self._insert_tasks)
            self._task_timer.start()

        if self._result_timer is None:
            # Create and start a timer to update task results periodically.
            self._result_timer = AsyncTimer(self._result_timer_period,
                                            self._update_result)
            self._result_timer.start()

        if self._autoscale_timer is None:
            # Create and start a timer to autoscale the number of runners.
            self._result_timer = AsyncTimer(self._autoscale_timer_period,
                                            self._autoscale)
            self._result_timer.start()

    def start(self):
        if self._status == OneShotManager.STATUS_STOPPED:
            # Reset all unfinished tasks.
            self._reset_unfinished_tasks()

            # Add runners. The number of added runners is the minimum number of
            # required runners.
            for idx in range(self._min_runners):
                self._add_runner()

            # Setup timers.
            self._setup_timers()

            # Change status to "running".
            self._status = OneShotManager.STATUS_RUNNING


class AnalyzerManager(APIConnector):

    STATUS_INITIAL = "initial"
    STATUS_READY = "ready"

    def __init__(self,
                 old_req,
                 client,
                 cluster,
                 rtsp_server,
                 nats_hosts,
                 db_name,
                 db_host=None,
                 db_authentication_source=None):
        self.status = AnalyzerManager.STATUS_INITIAL
        self._io_loop = asyncio.get_event_loop()
        super().__init__(NATS_CHANNEL_NAME, self._io_loop, nats_hosts)
        self._rtsp_server = rtsp_server
        self._old_req = old_req
        self._client = client
        self._cluster = cluster
        self._analyzers = dict()
        self._db_client, self._db = self._setup_db(
            db_name,
            host=db_host,
            authentication_source=db_authentication_source,
        )
        self._sync_timer = AsyncTimer(3, self._sync_analyzers)
        self._sync_timer.start()

        self._sync_gpu_worker_thread = threading.Thread(
                                            target=self._compare_gpu_worker,
                                            args=(30, ),
                                            daemon=True)
        self._sync_gpu_worker_thread.start()
        self._gpu_worker_closed_status_counter = 0

        # Create a manager to execute one-shot (i.e., short-term) tasks.
        self._one_shot_manager = OneShotManager(ONE_SHOT_COLLECTIONS,
                                                self._db)
        self._one_shot_manager.start()

        self.status = AnalyzerManager.STATUS_READY

    def _setup_db(self, db_name, host=None, authentication_source=None):
        try:
            client, db = connect_db(
                db_name,
                host=host,
                authentication_source=authentication_source,
            )
        except DatabaseError as e:
            logging.error("Database error occured while connecting: {}"
                          .format(str(e)))
            raise

        return client, db

    def _sync_analyzers(self):
        """Synchronize analyzers with data sourcing (database)."""
        # Updated analyzers information that are fetched from database.
        analyzers_updated = list(self._db["analyzers"].find())
        # Keys of current analyzers that are hold by the analyzer manager.
        analyzers_cur_keys = list(self._analyzers.keys())

        for analyzer_updated in analyzers_updated:
            sid = str(analyzer_updated["_id"])
            name = analyzer_updated["name"]
            enabled = analyzer_updated["enabled"]
            source = analyzer_updated["source"]
            pipelines = analyzer_updated["pipelines"]
            stream_output = analyzer_updated["streamOutput"]
            try:
                if not sid in analyzers_cur_keys:
                    # Case 1: The updated analyzer does not exist in current
                    # analyzers. In this case, create a new analyzer and start
                    # it if it is enabled.
                    self.on_create({
                        "id": sid,
                        "name": name,
                        "source": source,
                        "pipelines": pipelines,
                        "streamOutput": stream_output
                    })
                    if enabled:
                        self.on_start(sid)
                else:
                    # Case 2: The udpated analyzer exist in current analyzers.
                    # If the configurations of the updated analyzer and the
                    # corresponding current analyzer, we should change the
                    # parameters of the current analyzer or start/stop it.
                    analyzer_cur = self._analyzers[sid]
                    is_equal = ((name == analyzer_cur.name) and
                           deep_equal(source, analyzer_cur.source) and
                           deep_equal(pipelines, analyzer_cur.pipelines) and
                           stream_output == analyzer_cur.stream_output)

                    if not is_equal:
                        self.on_stop(sid)
                        self.on_update({
                            "id": sid,
                            "params": {
                                "name": name,
                                "source": source,
                                "pipelines": pipelines,
                                "streamOutput": stream_output
                            },
                        })
                        if enabled:
                            self.on_start(sid)
                    elif enabled and analyzer_cur.is_stopped():
                        self.on_start(sid)
                    elif (not enabled) and (not analyzer_cur.is_stopped()):
                        self.on_stop(sid)

                    analyzers_cur_keys.remove(sid)
            except Exception as e:
                logging.error(e)

        if analyzers_cur_keys:
            # Case 3: Analyzer Manager holds some analyzers that are not listed
            # in updated analyzers. In this case, delete the remained current
            # analyzers.
            try:
                self.on_delete(analyzers_cur_keys)
            except Exception as e:
                logging.error(e)

    def _compare_gpu_worker(self, reload_time):
        while True:
            try:
                logging.info('Checking GPU worker status...')
                for model_name in self._old_req:
                    for worker in self._old_req[model_name]['worker']:
                        # worker status includes "running", "closing", "closed"
                        if worker.status == 'closed' and \
                           worker.name.startswith("GPU_WORKER"):
                            self._gpu_worker_closed_status_counter += 1
                            logging.info('GPU worker {} status {}'.format(
                                          worker.name, worker.status))

                # get new GPU worker request
                new_req = gpu_scheduler.find_requirement()
                if self._gpu_worker_closed_status_counter >= 2:
                    logging.info(('GPU worker force restarting...'))
                    # Force stop all gpu worker to prevent start worker timeout
                    for worker in self._cluster.workers:
                        if worker.name.startswith("GPU_WORKER"):
                            self._cluster.stop_worker(worker)
                    settings = gpu_scheduler.find_allocation_settings(new_req)
                    for setting in settings:
                        w = self._cluster.start_worker(
                                    name=setting["name"],
                                    resources=setting["resources"])
                        new_req[setting["model_name"]]["worker"].append(w)
                    self._old_req = new_req
                    # reset timer
                    self._gpu_worker_closed_status_counter = 0

                logging.info('Checking GPU worker requirement...')
                if not self._equal_req(self._old_req, new_req):
                    logging.info(('GPU worker requirement has changed, '
                                  'Start reloading...'))
                    # stop all workers
                    for model_name in self._old_req:
                        for worker in self._old_req[model_name]['worker']:
                            self._cluster.stop_worker(worker)
                    # start workers according to new request
                    settings = gpu_scheduler.find_allocation_settings(new_req)
                    for setting in settings:
                        w = self._cluster.start_worker(
                                    name=setting["name"],
                                    resources=setting["resources"])
                        new_req[setting["model_name"]]["worker"].append(w)
                    self._old_req = new_req
            except Exception as ex:
                logging.exception(('Error occurred while comparing gpu worker:'
                                   ' {}').format(str(ex)))
            finally:
                time.sleep(reload_time)

    def _equal_req(self, old_req, new_req):
        for k1 in old_req.keys():
            if k1 not in new_req:
                return False
            else:
                for key in new_req[k1].keys():
                    if key == "worker":
                        continue
                    if old_req[k1][key] != new_req[k1][key]:
                        return False
        for k2 in new_req.keys():
            if k2 not in old_req:
                return False
        return True

    def on_create(self, params):
        logging.info("Creating Analyzer, params: {}".format(params))
        try:
            sid = params["id"]
            name = params["name"]
            source = params["source"]
            pipelines = params["pipelines"]
            stream_output = params.get("streamOutput", False)

            self._analyzers[sid] = Analyzer(sid, name, source,
                                            pipelines, stream_output,
                                            self._rtsp_server)
        except KeyError as e:
            raise RuntimeError("Invalid request format: {}".format(e.args[0]))
        except Exception as e:
            logging.error(e)

    def _get_analyzer_status(self, sid):
        if sid not in self._analyzers:
            logging.error("Analyzer not found for getting status: {}"
                          .format(sid))
            return Analyzer.STATUS_INTERNAL_ERROR
        return self._analyzers[sid].get_status()

    def on_read(self, params):
        logging.info("Getting Analyzer information, params: {}".format(params))

        if isinstance(params, list):
            result = dict()
            for sid in params:
                result[sid] = self._get_analyzer_status(sid)
        else:
            # TODO: check if params is an ObjectID
            result = self._get_analyzer_status(params)
        return result

    def on_update(self, update):
        logging.info("Updating Analyzer, params: {}".format(update))
        try:
            sid = update["id"]
            params = update["params"]
            analyzer = self._analyzers[sid]
            if "name" in params:
                analyzer.name = params["name"]
            if "source" in params:
                analyzer.source = params["source"]
            if "pipelines" in params:
                analyzer.pipelines = params["pipelines"]
            if "streamOutput" in params:
                analyzer.stream_output = params["streamOutput"]
        except KeyError as e:
            raise RuntimeError("Invalid request format: missing "
                               "field '{}'.".format(e.args[0]))
        except HotReconfigurationError as e:
            raise RuntimeError(str(e))

    def _delete_analyzer(self, sid):
        self._analyzers[sid].stop()
        del self._analyzers[sid]

    def on_delete(self, params):
        logging.info("Deleting Analyzer: {}".format(params))
        try:
            # TODO: Need to make sure the allocated resources for
            #       analyzer "sid" also been deleted completely
            if isinstance(params, list):
                for sid in params:
                    self._delete_analyzer(sid)
            else:
                sid = params
                self._delete_analyzer(sid)
        except KeyError:
            raise RuntimeError("Invalid request foramt")

    def on_start(self, sid):
        logging.info("Starting Analyzer: {}".format(sid))
        if sid not in self._analyzers:
            raise RuntimeError("Analyzer not found")
        else:
            self._analyzers[sid].start()

    def on_stop(self, sid):
        logging.info("Stopping Analyzer: {}".format(sid))
        if sid not in self._analyzers:
            raise RuntimeError("Analyzer not found")
        else:
            self._analyzers[sid].stop()

    def on_is_ready(self):
        if self.status == AnalyzerManager.STATUS_READY:
            return True
        else:
            return False

    def on_change_debug_mode(self, params):
        logging.debug("Change debug mode: {}".format(params))
        # change debug mode for analyzer managers
        logging.set_debug_mode(params)

        # change debug mode for all analyzers
        for _, analyzer in self._analyzers.items():
            analyzer.set_debug_mode(params)

    def on_get_gpu_resource(self):
        logging.debug("Get GPU resource")
        return gpu_scheduler.get_gpu_resource()

    def start(self):
        self._io_loop.run_forever()

if __name__ == "__main__":
    multiprocessing.freeze_support()
    logging.info("Start analyzer manager")
    config = get_config()["apps"]["dask"]
    diagnostics_port = \
        int(config["diagnostics_port"]) if is_env_development() else None
    try:
        cluster = LocalCluster(ip=config["scheduler_ip"], \
                            scheduler_port=int(config["scheduler_port"]), \
                            diagnostics_port=diagnostics_port, \
                            n_workers=0)
    except:
        os._exit(0)

    # Organize GPU worker list and create workers
    requirement = gpu_scheduler.find_requirement()
    gpu_allocation_settings = gpu_scheduler.find_allocation_settings(requirement)
    for setting in gpu_allocation_settings:
        w = cluster.start_worker(name=setting["name"], resources=setting["resources"])
        requirement[setting["model_name"]]["worker"].append(w)
    cluster.start_worker(name="IO_WORKER-1", resources={"IO": 1})

    with Client(cluster.scheduler_address) as client:
        # Registers a setup callback function for all current and future GPU
        # workers.
        client.register_worker_callbacks(setup=gpu_worker.init_worker)
        # Registers a setup callback function for all current and future
        # workers.
        client.register_worker_callbacks(setup=io_worker.init_worker)

        # Get configuration of messaging and database.
        natsconfig = get_config()["services"]["messaging"]
        nats_url = "nats://{}:4222".format(natsconfig['network']['ip'])
        dbconfig = get_config()["services"]["database"]
        db_url = "mongodb://{}:{}@{}:27017".format(dbconfig['env']["username"],
                                                   dbconfig['env']["password"],
                                                   dbconfig['network']['ip'])
        db_name = dbconfig['db_name']

        rtsp_server = RTSP_server()
        rtsp_server.start()

        # Start analyzer manager
        manager = AnalyzerManager(requirement,
                                  client,
                                  cluster,
                                  rtsp_server.get_instance(),
                                  [nats_url],
                                  db_name,
                                  db_host=db_url,
                                  db_authentication_source="admin")
        manager.start()
