import os
from enum import Enum
from threading import Timer

import cv2
from dask.distributed import get_client

from jagereye_ng import image as im
from jagereye_ng import gpu_worker
from jagereye_ng.io.database import Database
from jagereye_ng.io.notification import Notification
from jagereye_ng.io.obj_storage import ObjectStorageClient
from jagereye_ng import logging
from jagereye_ng.util.generic import get_config

from events import SharedEventVideoAgent
from events import SharedEventVideoFrame
from events import SharedEventVideoPolicy


EXTERNAL_TYPE = "cross_camera_license_plate_recognition.plate"

config = get_config()["apps"]["dask"]
address = config["cluster_ip"] + ":" + config["scheduler_port"]

class LICENSE_PLATE_FORMAT(Enum):
    """Enum for plate number formats."""
    # PLAIN: Use the given license plates directly.
    PLAIN = 0
    # EXTERNAL: Use the licese plates from external.
    EXTERNAL = 1


class _PlateRecognizer(object):
    """A class used to recognize license plates events.

    Attributes:
        license_plate_format (int): The format of license plate.
        triggers (list of string, list of `bson.objectid.ObjectId`, list of
            dict): The target of interest.
        max_margin (int): The maximum number of frames to end an event.
    """
    STATE_NORMAL = 0
    STATE_ALERT_START = 1
    STATE_ALERTING = 2
    STATE_ALERT_END = 3

    SYNC_INTERVAL = 3

    def __init__(self, license_plate_format, triggers, max_margin):
        try:
            # Get Dask client
            self._client = get_client(address=address)
        except ValueError:
            assert False, ("Should connect to Dask scheduler before"
                           " initializing this object.")

        # Connect to Database service.
        self._database = Database()

        self._license_plate_format = license_plate_format
        self._max_margin = max_margin
        self._states = {}
        self._margin_counters = {}

        self._update_triggers(license_plate_format, triggers)

    def _normalize_plate_number(self, number):
        normalized = number

        for ch in [" ", "-"]:
            normalized = normalized.replace(ch, "")

        return normalized

    def _update_triggers(self, license_plate_format, triggers):
        self._triggers = self._transform_triggers(license_plate_format,
                                                  triggers)
        self._sync_timer = Timer(_PlateRecognizer.SYNC_INTERVAL,
                                 self._update_triggers,
                                 [license_plate_format, triggers])
        self._sync_timer.start()

    def _transform_triggers(self, license_plate_format, triggers):
        if license_plate_format == LICENSE_PLATE_FORMAT.PLAIN:
            # Get detail for each license plate.
            details = [{
                "id": str(i),
                "group": p.get("group", ""),
                "owner": p["owner"],
                "number": self._normalize_plate_number(p["number"]),
            } for i, p in enumerate(triggers)]
            # Get unique groups.
            groups = list(set([x["group"] for x in details]))
        elif license_plate_format == LICENSE_PLATE_FORMAT.EXTERNAL:
            # Get detail for each license plate.
            query = {
                "type": EXTERNAL_TYPE,
                "group": triggers,
            }
            details = self._database.query_externals(query).result()
            details = [{
                "id": p["_id"],
                "group": p["group"],
                "owner": p["content"]["owner"],
                "number": self._normalize_plate_number(p["content"]["number"]),
            } for p in details]
            # Get unique groups.
            groups = list(set(triggers))

        # Get number for each license plate.
        numbers = [x["number"] for x in details]

        return {
            "groups": groups,
            "numbers": numbers,
            "details": details,
        }

    def _is_triggered(self, plate_number):
        """Check whether a plate number is in the target of interest.

        Args:
            plate_number (string): The plate number to be found.

        Returns:
            True if plate number is in the target and false otherwise.
        """
        is_triggered = (plate_number is not None and
                        plate_number in self._triggers["numbers"])

        return is_triggered

    def _catch_triggers(self, detections, predictions):
        """Catch recognized license plates that are in the target of interest.

        Args:
            detections (list of list): A list of license plates detection
                result, format of each result is (bboxes, bbox_scores).
            predictions (list of list): A list of license plates recognition
                result, format of each result is (plate_numbers, plate_scores).

        Returns:
            list of dict: A list of dictionary that specifies the catched
                license plates, format of each dictonary is:
                {
                    "bboxes": [...],
                    "bbox_scores": [...],
                    "plate_numbers": [...],
                    "plate_scores": [...],
                }
        """
        results = []
        for detection, prediction in zip(detections, predictions):
            bboxes, bbox_scores = detection
            plate_numbers, plate_scores = prediction

            result = {}
            for idx in range(len(bboxes)):
                if self._is_triggered(plate_numbers[idx]):
                    if not bool(result):
                        # For first recognized license plate.
                        result = {
                            "bboxes": [],
                            "bbox_scores": [],
                            "plate_numbers": [],
                            "plate_scores": [],
                        }
                    result["bboxes"].append(bboxes[idx].tolist())
                    result["bbox_scores"].append(bbox_scores[idx])
                    result["plate_numbers"].append(plate_numbers[idx])
                    result["plate_scores"].append(plate_scores[idx])

            results.append(result)

        return results

    def _process_state(self, plate_number, is_catched, num_frames):
        if plate_number not in self._states:
            self._states[plate_number] = _PlateRecognizer.STATE_NORMAL
            self._margin_counters[plate_number] = 0

        state = self._states[plate_number]
        margin_counter = self._margin_counters[plate_number]

        if state == _PlateRecognizer.STATE_NORMAL:
            if is_catched:
                state = _PlateRecognizer.STATE_ALERT_START
                margin_counter = 0
        elif state == _PlateRecognizer.STATE_ALERT_START:
            state = _PlateRecognizer.STATE_ALERTING
        elif state == _PlateRecognizer.STATE_ALERTING:
            if is_catched:
                margin_counter = 0
            elif margin_counter > self._max_margin:
                state = _PlateRecognizer.STATE_ALERT_END
            else:
                margin_counter += num_frames
        elif state  == _PlateRecognizer.STATE_ALERT_END:
            if is_catched:
                state = _PlateRecognizer.STATE_ALERT_START
            else:
                state = _PlateRecognizer.STATE_NORMAL
        else:
            assert False, "Unknown state: {}".format(self._state)

        self._states[plate_number] = state
        self._margin_counters[plate_number] = margin_counter

    def _update_state(self, metadata, plate_number, is_catched):
        metadata_out = metadata.copy()

        # Update the state of that plate number.
        self._process_state(plate_number, is_catched, 1)
        # Set metadata with the latest state.
        metadata_out.update({ "mode": self._states[plate_number] })

        return metadata_out

    def _refresh_states(self):
        for plate_number in self._states.copy():
            if self._states[plate_number] == _PlateRecognizer.STATE_NORMAL:
                self._states.pop(plate_number, None)
                self._margin_counters.pop(plate_number, None)

    def find_ids(self, plate_number):
        triggers = self._triggers.copy()
        return [p["id"]
                for n, p in zip(triggers["numbers"], triggers["details"])
                if n == plate_number]

    def run(self, frames, motions):
        catches = []

        if motions["frames"]:
            images = [frame.image for frame in frames]

            # Run license plate detection.
            f_images = self._client.scatter(images)
            f_detect = gpu_worker.run_model(self._client,
                                            "license_plate_detection",
                                            f_images)
            detections = f_detect.result()

            # Crop the detected license plates.
            plates_list = []
            for image, detection in zip(images, detections):
                plates = []
                bboxes = detection[0]
                for bbox in bboxes:
                    plate = image[bbox.ymin:bbox.ymax, bbox.xmin:bbox.xmax]
                    if plate is not None:
                        plates.append(plate)
                plates_list.append(plates)

            # Run license plate recognition.
            f_plates_list = self._client.scatter(plates_list)
            f_recognize = gpu_worker.run_model(self._client,
                                               "license_plate_recognition",
                                               f_plates_list)
            predictions = f_recognize.result()

            # Catch license plates that are in triggers list.
            catches = self._catch_triggers(detections, predictions)

        event_video_frames = []
        for idx in range(len(frames)):
            current_frame = frames[idx]
            metadatas = {}
            base_metadata = { "mode": _PlateRecognizer.STATE_NORMAL }
            try:
                motion_idx = motions["index"].index(idx)
            except ValueError:
                for plate_number in self._states:
                    metadata = self._update_state({}, plate_number, False)
                    metadatas[plate_number] = metadata
            else:
                if "plate_numbers" in catches[motion_idx]:
                    catched_numbers = catches[motion_idx]["plate_numbers"]
                else:
                    catched_numbers = []
                uncatched_numbers = [number
                                     for number in self._states
                                     if number not in catched_numbers]
                for plate_number in catched_numbers:
                    metadata = self._update_state(catches[motion_idx],
                                                  plate_number,
                                                  True)
                    metadatas[plate_number] = metadata
                for plate_number in uncatched_numbers:
                    metadata = self._update_state({}, plate_number, False)
                    metadatas[plate_number] = metadata
            event_video_frames.append(SharedEventVideoFrame(
                current_frame,
                metadatas=metadatas,
                base_metadata=base_metadata,
            ))

        self._refresh_states()

        return event_video_frames

    def release(self):
        if self._sync_timer is not None:
            self._sync_timer.cancel()


class _OutputPolicy(SharedEventVideoPolicy):
    def compute(self, frame):
        actions = {}

        for key, metadata in frame.metadatas.items():
            if metadata["mode"] == _PlateRecognizer.STATE_ALERT_START:
                action = SharedEventVideoPolicy.START_RECORDING
            elif metadata["mode"] == _PlateRecognizer.STATE_ALERT_END:
                action = SharedEventVideoPolicy.STOP_RECORDING
            else:
                action = None
            actions[key] = action

        return actions


class LicensePlateRecognitionPipeline(object):
    """A base class used to run the pipeline for licnse plate recognition.

    Attributes:
        license_plate_format (int): The format of license plate. It must be
            LICENSE_PLATE_FORMAT.PLAIN or LICENSE_PLATE_FORMAT.EXTERNAL.
        name (string): The pipeline name.
        anal_id (lib.ObjectID): The ObjectID of the analyzer that the pipeline
            was attached with.
        triggers (list of string, list of dict): The target of interest.
        frame_size (tuple): The size of the input image frame with format
            of (width, height).
        video_format (string): The output video format.
        fps (int): The output video fps.
        history_len (int): The length, in seconds, of frame history queue. This
            determines when the agent starts to record before policy returning
            action "START_RECORDING".

    Raises:
        `TypeError`: If the license plate format is not supported.
    """
    def __init__(self,
                 license_plate_format,
                 name,
                 anal_id,
                 triggers,
                 frame_size,
                 video_format="mp4",
                 fps=15,
                 history_len=3):
        if license_plate_format not in [LICENSE_PLATE_FORMAT.PLAIN,
                                        LICENSE_PLATE_FORMAT.EXTERNAL]:
            raise ValueError("Non-Supported license plate format: {}"
                             .format(license_plate_format))

        self._license_plate_format = license_plate_format
        self._name = name
        self._anal_id = anal_id
        self._obj_key_prefix = os.path.join(name, anal_id)

        # Create a license plate recognizer.
        self._recognizer = _PlateRecognizer(license_plate_format,
                                            triggers,
                                            fps * history_len)

        # Create output video agent.
        event_video_metadata = {
            "event_name": name,
            "event_custom": {},
        }
        self._output_agent = SharedEventVideoAgent(
            _OutputPolicy(),
            event_video_metadata,
            self._obj_key_prefix,
            frame_size,
            video_format=video_format,
            fps=fps,
            history_len=history_len,
        )

        # Connect to Object Storage service.
        self._obj_storage = ObjectStorageClient()
        self._obj_storage.connect()

        # Connect to Notification service.
        self._notification = Notification()

        # Connect to Database service.
        self._database = Database()

    def _take_snapshot(self, filename, frame):
        """Save a frame to an image file and push it to the object storage.

        Args:
            filename (string): The name of the snapshot.
            frame (`SharedEventVideoFrame`): The frame object to be saved.

        Returns:
            string: The key of the snapshot in the object storage.
        """
        thumbnail_key = os.path.join(self._obj_key_prefix, "{}.jpg"
                                     .format(filename))
        shrunk_image = im.shrink_image(frame.image)
        self._obj_storage.save_image_obj(thumbnail_key, shrunk_image)

        return thumbnail_key

    def _output_event(self, event, thumbnail_key, triggered):
        """Output event to notification center and database.

        Args:
            event (`AgentEvent`): The event object to be outputted.
            thumbnail_key (string): The key of the thumbnail in object storage.
            triggerd (string): The triggerd object of the event.
        """
        timestamp = event.content["timestamp"]
        event_type = "{}.alert".format(self._name)
        content = {
            "video": event.content["video_key"],
            "metadata": event.content["metadata_key"],
            "thumbnail": thumbnail_key,
            "triggered": triggered,
        }

        if self._license_plate_format == LICENSE_PLATE_FORMAT.EXTERNAL:
            content["externals"] = self._recognizer.find_ids(triggered)

        # Save event to database.
        event_id = self._database.save_event(self._anal_id,
                                             timestamp,
                                             event_type,
                                             content).result()
        logging.info("Success to save event in database: _id: {}, analyzerId: "
                     "{}, timestamp: {}, type: {}, content: {}"
                     .format(event_id,
                             self._anal_id,
                             timestamp,
                             event_type,
                             content))


        # Push notification.
        self._notification.push_anal(event_id,
                                     self._anal_id,
                                     timestamp,
                                     event_type,
                                     content)

    def run(self, frames, motions, is_src_reader_busy):
        recognitions = self._recognizer.run(frames, motions)

        for recognition in recognitions:
            out_event = self._output_agent.process(recognition)
            for plate_number, agent_event in out_event.agent_events.items():
                if agent_event.action == SharedEventVideoPolicy.START_RECORDING:
                    timestamp = agent_event.content["timestamp"]
                    thumbnail_filename = os.path.join(plate_number,
                                                      str(timestamp))
                    thumbnail_key = self._take_snapshot(thumbnail_filename,
                                                        recognition)
                    self._output_event(agent_event, thumbnail_key, plate_number)
                elif agent_event.action == SharedEventVideoPolicy.STOP_RECORDING:
                    logging.info("End of event video for {}".format(plate_number))

    def release(self):
        self._recognizer.release()
        self._output_agent.release()
