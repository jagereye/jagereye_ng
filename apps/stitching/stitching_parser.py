import re

"""This parser is based nopa two config file
    PLEASE check stitch_config and camera_params while changing nopa's version.
"""
# nopa stitch config format
stitch_parsing_list = [
    "source_cameras",
    "pano_cam_grouping",
    "output_framing",
    "mode",
    "output_width",
    "output_height",
    "pano_group_layout",
    "wave_correct",
    "calibration_config",
    "conf_thresh",
    "match_conf",
    "warping_config",
    "radius",
    "h_fov",
    "v_fov",
    "pre_undistort_config",
    "intrinsics_matrix",
    "distortion_coeffs",
    "parallax_flow_config",
    "blending_config",
    "blend_slope"
]

# nopa calibration format
camera_parsing_list = [
    "cameras",
    "distortion_coeffs",
    "height",
    "intrinsics_matrix",
    "width",
    "shots",
    "camera",
    "rotation",
    "rotation_matrix",
    "translation"
]

def snake2camel(name):
    return re.sub(r'(?:^|_)([a-z])', lambda x: x.group(1).upper(), name)

def snake2camelback(name):
    return re.sub(r'_([a-z])', lambda x: x.group(1).upper(), name)

def camel2snake(name):
    return name[0].lower() + re.sub(r'(?!^)[A-Z]', lambda x: '_' + x.group(0).lower(), name[1:])

def camelback2snake(name):
    return re.sub(r'[A-Z]', lambda x: '_' + x.group(0).lower(), name)


def parse_dict_in_list_snake2camelback(dict_input, target_list, dict_output):
    """Parsing dictionary from snake case into camel case in target list

    Examples:
        dict_input: {"hello_world": "123", "throw_excection": true}
        target_list: ["hello_world"]

        After iterating all key in dict_input, the dict_output should be:
            {"helloWorld": "123", "throw_excection": true}

    Args:
        dict_input (dict): a dict's key contain snake case.
        target_list (list of string): list of target to parse, string in
            snake case.
        dict_output (dict): a output dict. all key will be parsed into camel
            case.
    TODO:
        design a better way that is able to tell some key is user-defined in
        the future if possible.
    """
    for k, v in dict_input.items():
        kk = k
        if k in target_list:
            kk = snake2camelback(k)
        if type(v) == dict:
            dict_output[kk] = {}
            parse_dict_in_list_snake2camelback(v, target_list, dict_output[kk])
        else:
            dict_output[kk] = v

def parse_dict_in_list_camelback2snake(dict_input, target_list, dict_output):
    """Parsing dictionary from camel case into snake case in target list

    Examples:
        dict_input: {"helloWorld": "123", "throwExcection": true}
        target_list: ["hello_world"]

        After iterating all key in dict_input, the dict_output should be:
            {"hello_world": "123", "throwExcection": true}

    Args:
        dict_input (dict): a dict's key contain camel case.
        target_list (list of string): list of target to parse, string in
            snake case.
        dict_output (dict): a output dict. all key will be parsed into snake
            case.
    TODO:
        design a better way that is able to tell some key is user-defined in
        the future if possible.
    """
    for k, v in dict_input.items():
        kk = camelback2snake(k)
        if kk not in target_list:
            kk = k
        if type(v) == dict:
            dict_output[kk] = {}
            parse_dict_in_list_camelback2snake(v, target_list, dict_output[kk])
        else:
            dict_output[kk] = v
