from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import numpy as np
import time
import os
import cv2

from jagereye_ng import image as im
from jagereye_ng.io.obj_storage import ObjectStorageClient
from jagereye_ng.io.database import Database
from jagereye_ng import logging
from jagereye_ng.io.streaming import VideoFrame

from stitching.stitching_parser import stitch_parsing_list, \
    camera_parsing_list, parse_dict_in_list_camelback2snake
import nopa
from nopa.stitching.stitching_builder import build_stitching_from_dict
assert nopa.__version__ == '2.1.0'


def transform_roi_format(roi, frame_size):
    """Transforms roi format.

    Transforms rules:
        absolution point value = relative point value * corresponding frame
        size

    Examples:
        Assume frame size is (100, 100),
        Expect source roi format (list of dict):
            [{"x": 0.21, "y": 0.33}, {"x": 0.32, "y": 0.43}, ...]

        Transformed roi format (list of tuple):
            [(21, 33), (32, 43), ...]

    Args:
        roi (list of objects): Input roi to be transformed.
        frame_size (tuple): The frame size of input image frames with
            format (width, height).

    Returns:
        Transformed roi.
    """
    result = []
    for r in roi:
        if ((r["x"] < 0 or r["x"] > 1) or
                (r["y"] < 0 or r["y"] > 1)):
            raise ValueError("Invalid roi point format, should be a float "
                             "with value between 0 and 1.")
        result.append((float(r["x"]) * float(frame_size[0]),
                       float(r["y"]) * float(frame_size[1])))
    return tuple(result)


class _Stitching(object):
    """A class used to stitching frames.

    Attributes:
        source (list of string): The video sources.
        external_id (string): The _id in external which contains camera
            parameters.
        roi (list of object): The region of interest with format of a list
            of object points, such as [{"x": 0.21, "y": 0.33},
            {"x": 0.32, "y": 0.43}, ...]
        stitch_config (dict): Nopa stitcher config.
    """

    def __init__(self, source, external_id, roi, stitch_config):
        # Connect to Database service.
        database = Database()
        external = database.get_external(external_id).result()

        # camera_params: parsing from jager external into nopa original format
        # stitch_json: parsing from jager analyzer into nopa original format
        camera_params = {}
        stitch_json = {}
        parse_dict_in_list_camelback2snake(
                external['content']['cameraParams'],
                camera_parsing_list, camera_params)
        parse_dict_in_list_camelback2snake(
                stitch_config,
                stitch_parsing_list, stitch_json)
        source_cameras = stitch_json['source_cameras']

        self.output_frame_shape = (
                stitch_json['output_framing']['output_height'],
                stitch_json['output_framing']['output_width'],
                3)

        self._use_roi = False
        self._roi = roi
        if len(self._roi) > 0:
            self._use_roi = True
            # set x,y as inverse value for finding upper bound and lower bound
            self._x_ub = 0
            self._x_lb = self.output_frame_shape[1]
            self._y_ub = 0
            self._y_lb = self.output_frame_shape[0]
            for point in self._roi:
                self._x_ub = max(self._x_ub, int(point[0]))
                self._x_lb = min(self._x_lb, int(point[0]))
                self._y_ub = max(self._y_ub, int(point[1]))
                self._y_lb = min(self._y_lb, int(point[1]))

        assert source['mode'] == 'multistreaming' and len(source['urls']) > 0

        # Display black image for anytime error situation occured
        self._black_frame = {}  # (dict): "user-defined id": (`numpy.ndarray`)

        # Mapping source index (key: user-defined id, value: video source idx)
        self._camera_to_url_idx_lookup_table = {}

        for k, v in source_cameras.items():
            for i in range(len(source['urls'])):
                if source['urls'][i] == v:
                    self._camera_to_url_idx_lookup_table[k] = i
                    break
            self._black_frame[k] = np.zeros(
                                (camera_params['cameras'][k]['height'],
                                 camera_params['cameras'][k]['width'], 3),
                                dtype=np.uint8)
        stitch_json['calibration_config']['camera_params'] = camera_params

        self.stitcher, _, _ = build_stitching_from_dict(stitch_json)
        logging.info("Created an Stitching app")

    def get_output_frame_shape(self):
        return self.output_frame_shape

    def __del__(self):
        pass

    def run(self, frames):
        """Do stitch and return stitched frame"""
        stitched_frames = []  # list of stitched frame
        num_source = len(frames)
        if num_source == 0:
            return stitched_frames
        length_frames = len(frames[0])
        for l in range(length_frames):
            avg_timestamp = 0
            sync_frames = [frames[i][l] for i in range(num_source)]

            frames_s = {}
            for k, v in self._camera_to_url_idx_lookup_table.items():
                if sync_frames[v].image.shape == self._black_frame[k].shape:
                    frames_s[k] = sync_frames[v].image
                else:
                    frames_s[k] = cv2.resize(
                            sync_frames[v].image,
                            (self._black_frame[k].shape[1],
                             self._black_frame[k].shape[0]),
                            interpolation=cv2.INTER_LINEAR)
                    logging.error('video source index {} get wrong size {} \
                            before stitch'.format(
                            v, sync_frames[v].image.shape))
            try:
                stitch_img = self.stitcher.stitch(frames_s)
                for frame in sync_frames:
                    avg_timestamp += frame.timestamp
            except Exception as ex:
                logging.error('Stitching progressing exception {}'.format(
                              str(ex)))
                stitch_img = np.zeros(
                    self.output_frame_shape,
                    dtype=np.uint8)

            avg_timestamp //= len(sync_frames)
            if self._use_roi is True:
                stitch_img = stitch_img[
                                    self._y_lb:self._y_ub,
                                    self._x_lb:self._x_ub,
                                    :]
                stitch_img = cv2.resize(stitch_img,
                                        (self.output_frame_shape[1],
                                         self.output_frame_shape[0]),
                                        interpolation=cv2.INTER_LINEAR)
            stitch_frame = VideoFrame(stitch_img, avg_timestamp)
            stitched_frames.append(stitch_frame)
        return stitched_frames

    def release(self):
        pass


class StitchingPipeline(object):
    """A class used to run the Stitching pipeline.

    Attributes:
        anal_id (lib.ObjectID): The ObjectID of the analyzer that the pipeline
            was attached with.
        source (list of string): The video sources.
        external_id (string): The _id in external which contains camera
            parameters.
        roi (list of object): The region of interest with format of a list
            of object points, such as [{"x": 0.21, "y": 0.33},
            {"x": 0.32, "y": 0.43}, ...]
        stitch_config (dict): Nopa stitcher config.
        frame_size (tuple): The size of the input image frame with format
            of (width, height).
    """
    def __init__(self, anal_id, source, external_id, roi, stitch_config,
                 frame_size):
        transformed_roi = transform_roi_format(roi, frame_size)
        # Create an stitching instance
        self._stitching = _Stitching(
            source,
            external_id,
            transformed_roi,
            stitch_config)
        self.snapshot_timestamp = time.time()

        self._name = 'stitching.snapshot'
        self._anal_id = anal_id

        self._obj_key_prefix = os.path.join(self._name, anal_id)

        # Connect to Object Storage service.
        self._obj_storage = ObjectStorageClient()
        self._obj_storage.connect()

    def get_output_frame_shape(self):
        return self._stitching.get_output_frame_shape()

    def _take_snapshot(self, filename, frame):
        """Save a frame to an image file and push it to the object storage.

        Args:
            filename (string): The name of the snapshot.
            frame (`SharedEventVideoFrame`): The frame object to be saved.

        Returns:
            string: The key of the snapshot in the object storage.
        """
        snapshot_key = os.path.join(self._obj_key_prefix, "{}.jpg".format(
                                    filename))
        shrunk_image = im.shrink_image(frame.image)
        self._obj_storage.save_image_obj(snapshot_key, shrunk_image)

        return snapshot_key

    def run(self, frames, motions, is_src_reader_busy):
        """Run Stitching pipeline.

        Args:
            frames (list of VideoFrame): A list of raw sync video frames to
                be detected.
            motions: The motion of the input frames. It should be the output of
                video_proc.detect_motion().
        """
        stitched_frames = self._stitching.run(frames)
        time_now = time.time()
        if time_now - self.snapshot_timestamp > 60:
            if len(stitched_frames) > 0:
                self.snapshot_timestamp = time_now
                snapshot_frame = VideoFrame(
                                    cv2.resize(
                                        stitched_frames[0].image.copy(),
                                        dsize=(320, 240),
                                        interpolation=cv2.INTER_AREA),
                                    time_now)
                self._take_snapshot('320x240', snapshot_frame)
        return stitched_frames

    def release(self):
        pass
