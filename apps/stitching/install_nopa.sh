pip3 uninstall -y nopa && rm -rf $JAGERROOT/apps/stitching/calibration
cd $JAGERROOT/apps
git clone --branch v2.1.0 https://$GITLAB_DEPLOY_ACCOUNT:$GITLAB_DEPLOY_TOKEN@gitlab.com/toppano-mirror/Nopa
cp -r Nopa/calibration $JAGERROOT/apps/stitching && \
    pip3 install --user ./Nopa && \
    rm -rf $JAGERROOT/apps/Nopa
