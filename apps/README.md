## Applications

### Face

### Object Detection

### Stitching

[nopa](https://gitlab.com/toppano-mirror/Nopa)

nopa use some python module like PyOpenGL and PyQT5, numpy which included in setup.py

#### upgrade nopa

1. change nopa version in `$JAGERROOT/apps/stitching/stitching.py`, `Dockerfile`, `$JAGERROOT/apps/stitching/install_nopa.sh`.

``` shell
$ sh $JAGERROOT/apps/stitching/install_nopa.sh
```

2. update `stitching_parser.py` for all config key field for new version. then run unit test for stitching.

3. update `$JAGERROOT/share/pipeline.json` and `externals.json` for stitch config and camera parameters new format.

4. update `stitching/stitching.py` and `stitching/stitching_api.py` format for nopa's stitcher builder function.

5. happy stitching!

#### developing on host

nopa need X server to render stitching graphic

* if you're running nopa over ssh -X, use command below:

``` shell
$ export DISPLAY=:0; nohup ice weasel &>/dev/null &
```

#### developing on docker container

```shell

docker run -it \
        --volume=$XSOCK:$XSOCK:rw \
        --env="DISPLAY" \
    jagereye/analyzer_manager:v.X.X.X \
    analyzer_manager
```

ref:

some example make container connect host's X server:[Using GUI's with Docker](http://wiki.ros.org/docker/Tutorials/GUI)
[nvidia cudagl](https://hub.docker.com/r/nvidia/opengl/)
[Understanding how uid and gid work in Docker containers](https://medium.com/@mccode/understanding-how-uid-and-gid-work-in-docker-containers-c37a01d01cf)

##### some qt issues

* [xcb related](https://stackoverflow.com/questions/31952711/threading-pyqt-crashes-with-unknown-request-in-queue-while-dequeuing)

```python
[xcb] Unknown request in queue while dequeuing
[xcb] Most likely this is a multi-threaded client and XInitThreads has not been called
[xcb] Aborting, sorry about that.
```

add code if needed

```python
QtCore.QCoreApplication.setAttribute(QtCore.Qt.AA_X11InitThreads)
```

##### glib  and qt mainloop might not conflict in the case we use

now we use gst rtsp server on analyzer manager main process,
each anaylzer are running on other process.

this qt env variable prevents Qt from using GLib’s event loop.

```shell
export QT_NO_GLIB=1
```

[qt env](http://www.thelins.se/johan/blog/2008/12/it-is-all-about-the-environment/)

##### MIT-SHM

The MIT Shared Memory Extension or MIT-SHM is a X Window System extension for exchange of image data between client and server using shared memory. 

```shell
export QT_X11_NO_MITSHM=1
```

ref:
[MIT Shared Memory Extension wiki](https://en.wikipedia.org/wiki/MIT-SHM)
[MIT Shared Memory Extension or MIT-SHM over ssh](https://unix.stackexchange.com/questions/136606/mit-shared-memory-extension-or-mit-shm-over-ssh)

---

## RTSP server
[gst rtsp sample](https://github.com/madams1337/python-opencv-gstreamer-examples)

---

## Testing

## run pytest

```shell
$ python3 -m pytest --disable-pytest-warnings -v -s .
```
