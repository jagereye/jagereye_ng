import json
import os

from stitching.stitching_parser \
import stitch_parsing_list, camera_parsing_list, \
parse_dict_in_list_camelback2snake, parse_dict_in_list_snake2camelback

class TestStitching(object):

    def test_stitch_config(self):
        path = os.path.join(os.getcwd(), os.path.dirname(__file__))
        with open(os.path.join(path, 'stitch_config.json')) as json_file:
            stitch_config = json.load(json_file)
        camel = {}
        parse_dict_in_list_snake2camelback(stitch_config,
                                            stitch_parsing_list, camel)
        snake = {}
        parse_dict_in_list_camelback2snake(camel, stitch_parsing_list, snake)
        assert snake == stitch_config

    def test_camera_params(self):
        path = os.path.join(os.getcwd(), os.path.dirname(__file__))
        with open(os.path.join(path, 'camera_params.json')) as json_file:
            camera_params = json.load(json_file)
        camel = {}
        parse_dict_in_list_snake2camelback(camera_params,
                                            stitch_parsing_list, camel)
        snake = {}
        parse_dict_in_list_camelback2snake(camel, stitch_parsing_list, snake)
        assert snake == camera_params
