from distributed.utils_test import gen_cluster
from dask.distributed import Client, LocalCluster
from jagereye_ng.util.generic import get_config

import numpy as np
from car_counting import CarCounting, transform_lines_format

lines = [
    {"x1": 0.26581633, "y1": 0.58518519, "x2": 0.325, "y2": 0.58518519},
    {"x1": 0.27040816, "y1": 0.68657407, "x2": 0.34770408,"y2": 0.6912037},
    {"x1": 0.39107143, "y1": 0.57824074, "x2": 0.44693878, "y2": 0.5837963},
    {"x1": 0.8, "y1": 0.8, "x2": 0.85, "y2": 0.85}
]

lines_t = np.array([[287.0816364, 1123.5555648, 351, 1123.5555648],
                    [292.0408128, 1318.2222144, 375.5204064, 1327.111104],
                    [422.3571444, 1110.2222208, 482.6938824, 1120.888896],
                    [864, 1536, 918, 1632]])

length_of_lines = np.array([63.91836360000002, 83.95150329769055,
                            61.27234214801111, 110.14535850411491])
line_fc = np.array([[-0.00000000e+00, 1.00000000e+00, -1.12355556e+03],
                    [-1.05881244e-01, 9.94378782e-01, -1.27989056e+03],
                    [-1.74086298e-01, 9.84730400e-01, -1.01974298e+03],
                    [-8.71575537e-01, 4.90261240e-01, 0.00000000e+00]])
ortho_fc = np.array([[ 1.00000000e+00, 0.00000000e+00, -3.19040818e+02],
                    [9.94378782e-01, 1.05881244e-01, -4.71949947e+02],
                    [9.84730400e-01, 1.74086298e-01, -6.39818567e+02],
                    [4.90261240e-01, 8.71575537e-01, -1.81739842e+03]])

closed_box_idx = 2 # close to lines[2]

bboxes = [
    {"xmin": 50, "ymin": 50, "xmax": 100, "ymax": 100},
    {"xmin": 100, "ymin": 100, "xmax": 150, "ymax": 150},
    {"xmin": 870, "ymin": 1560, "xmax": 890, "ymax": 1600}
]

bboxes_c = [
    {"x": 75, "y": 75, "xmin": 50, "ymin": 50, "xmax": 100, "ymax": 100},
    {"x": 125, "y": 125, "xmin": 100, "ymin": 100, "xmax": 150, "ymax": 150},
    {"x": 880, "y": 1580, "xmin": 870, "ymin": 1560, "xmax": 890, "ymax": 1600}
]

triggers = ["cars"]
frame_size = (1080, 1920)

# for CarCounting class init client
config = get_config()["apps"]["dask"]
cluster = LocalCluster(ip=config["scheduler_ip"], \
                        scheduler_port=int(config["scheduler_port"]), \
                        diagnostics_port=int(config["diagnostics_port"]), \
                        n_workers=1)

# new CarCounting object
cc = CarCounting(lines_t, triggers, frame_size)

class TestCarCounting(object):
    # TODO decoupling the jager framework for unit test

    def test_transform_lines_format(self):
        r = transform_lines_format(lines, frame_size)
        assert np.allclose(r, lines_t)

    def test_get_length_of_lines(self):
        r = cc._get_length_of_lines(lines_t)
        assert np.allclose(r, length_of_lines)

    def test_get_linear_func_coeff(self):
        r1, r2 = cc._get_linear_func_coeff(lines_t)
        assert np.allclose(r1, line_fc)
        assert np.allclose(r2, ortho_fc)

    def test_dist_between_line_and_box(self):
        r = cc._dist_between_line_and_box(line_fc, bboxes)
        e = np.array([[1048.55556, 1213.25324465, 958.94467235, 28.59857227],
                [998.55556, 1168.82836775, 918.41246725, 47.66428713],
                [456.44444, 198.05242084, 382.93510976, 7.62628664]])
        assert np.allclose(r, e)

    def test_convert_center_box(self):
        r = cc._convert_center_box(bboxes[0])
        assert r == bboxes_c[0]

    def test_box_dist(self):
        r = cc._box_dist(bboxes_c[0], bboxes_c[1])
        e = np.array([70.7106])
        assert np.allclose(np.array([r]), e)

    def test_check_object_list(self):
        cc._object_list = []
        r = cc._check_object_list(bboxes)
        # distance between box_c[2] and lines_t[2] is close enough
        assert r == [closed_box_idx]
        r = cc._check_object_list(bboxes)
        # same bboxes return no new object
        assert r == []
        # generate moved bbox
        moved_bbox = {}
        for k in bboxes[closed_box_idx]:
            # moved pixel in config range (detail pls check CarCounting)
            moved_bbox[k] = bboxes[closed_box_idx][k] + \
                    cc._counting_check_dist / 4
        r = cc._check_object_list([moved_bbox])
        # "track same" bboxes return no new object
        assert r == []
    def test_check_triggers(self):
        # TODO add img to detect then trigger
        pass


class TestCarCountingPipeline(object):
    def test_integration_car_counting(self):
        # TODO
        pass

class TestCarCountingFinished(object):
    def test_close_dask_client(self):
        global cc
        del cc
