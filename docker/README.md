## jagereye/nodejs

---------------------------------------------
## jagereye/tensorflow
Based on [tensorflow v.1.7.1](https://github.com/tensorflow/tensorflow/releases/tag/v1.7.1)

[tensorflow:1.7.1-gpu-py3 build docker instruction](https://github.com/tensorflow/tensorflow/blob/v1.7.1/tensorflow/tools/docker/README.md)

this is for addiction OpenGL requirement:

check if OpenGL works in container environment

```shell
$ apt-get install mesa-utils
$ glxinfo -B
```

ref:
[nvidia-docker opengl support](https://github.com/NVIDIA/nvidia-docker/wiki/Frequently-Asked-Questions#is-opengl-supported)
[cudagl docker image](https://hub.docker.com/r/nvidia/opengl/)
