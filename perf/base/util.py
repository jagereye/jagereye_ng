import time

import GPUtil
import yaml


class NonExistentGPU(Exception):
    pass


def deep_len(lst):
    return sum(deep_len(el) if isinstance(el, list) else 1 for el in lst)


def elapsed(fn, *args, **kwargs):
    start_time = time.time()

    fn(*args, **kwargs)

    elapsed_time = time.time() - start_time

    return elapsed_time


def get_gpu_total_mem(gpu_idx):
    local_devices = GPUtil.getGPUs()

    for local_device in local_devices:
        if str(gpu_idx) == str(local_device.id):
            return local_device.memoryTotal

    raise NonExistentGPU("GPU {} does not exist".format(gpu_idx))


def load_yaml(file_path):
    with open(file_path, "r") as f:
        return yaml.load(f)
