import os


class PerfReport(object):

    def __init__(self, file_name):
        self._file_name = file_name

    def generate(self):
        return ""

    def save(self):
        dir_path = os.path.dirname(os.path.realpath(self._file_name))
        if not os.path.exists(dir_path):
                os.makedirs(dir_path)

        with open(self._file_name, "w") as f:
            f.write(self.generate())
