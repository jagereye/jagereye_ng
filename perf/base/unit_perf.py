from multiprocessing import Manager
from multiprocessing import Process


class UnitPerf(object):

    def run(self, report_result):
        pass


class UnitPerfRunner(object):

    def __init__(self):
        super().__init__()

    def run(self, unit_perf):
        manager = Manager()
        report_result = manager.list()

        process = Process(target=unit_perf.run, args=(report_result,))
        process.start()
        process.join()

        assert process.exitcode == 0, "Failed on running unit perf"
        assert len(report_result) == 1, "Must have exact 1 report result"

        return report_result[0]
