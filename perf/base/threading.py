import signal
from threading import Thread
from threading import Event


class TerminableThread(Thread):

    def __init__(self):
        super().__init__()

        self.daemon = True

        self._terminate_event = Event()

    def is_terminated(self):
        return self._terminate_event.is_set()

    def terminate(self):
        self._terminate_event.set()
