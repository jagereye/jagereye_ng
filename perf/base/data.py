import os
from queue import Queue
from queue import Full as QueueFull

import cv2
import numpy as np

from base.threading import TerminableThread
from base.util import deep_len


class InvalidDataType(Exception):
    pass


class InvalidDataSource(Exception):
    pass


class DataReader(TerminableThread):

    def __init__(self, data, input_shape, queue_maxsize=16, timeout=1):
        super().__init__()

        self._data = data
        self._input_shape = input_shape
        self._queue_maxsize = queue_maxsize
        self._timeout = timeout

        self._img_set = self._capture(data)
        self._img_set_size = deep_len(self._img_set)
        self._queue = Queue(maxsize=queue_maxsize)

    def _capture_video(self, data_src):
        img_set = []

        reader = cv2.VideoCapture(data_src)

        if not reader.isOpened():
            raise InvalidDataSource("Non-Existent data source: {}"
                                   .format(data_src))

        while True:
            success, img = reader.read()
            if not success:
                break
            img_set.append(img)

        return img_set

    def _capture_imgs(self, data_src):
        img_set = []

        try:
            file_names = os.listdir(data_src)
        except FileNotFoundError:
            raise InvalidDataSource("Non-Existent data source: {}"
                                   .format(data_src))

        for file_name in file_names:
            file_path = os.path.join(data_src, file_name)
            img = cv2.imread(file_path)
            img_set.append(img)

        return img_set

    def _capture(self, data):
        data_src = data["src"]
        data_type = data["type"]

        if data_type == "video":
            img_set = self._capture_video(data_src)
        elif data_type == "images":
            img_set = self._capture_imgs(data_src)
        else:
            raise InvalidDataType("Non-Supported data type: {}"
                                  .format(data_type))

        return img_set

    def _deep_assign(self, idx_lst):
        result = []

        for el in idx_lst:
            if isinstance(el, list):
                result.append(self._deep_assign(el))
            else:
                result.append(self._img_set[el].copy())

        return result

    def _sample(self):
        idx_lst =  np.random.randint(self._img_set_size,
                                     size=self._input_shape)

        return self._deep_assign(idx_lst.tolist())

    def run(self):
        while not self.is_terminated():
            imgs = self._sample()
            try:
                self._queue.put(imgs, timeout=self._timeout)
            except QueueFull:
                continue

    def sample(self):
        return self._queue.get()
