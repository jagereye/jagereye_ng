import os

from jagereye_ng.util.generic import get_config

from base.unit_perf import UnitPerfRunner
from base.util import load_yaml
from model_mem.model_mem import ModelMemUnitPerf
from model_mem.report import ModelMemTotalReport


def run_model_mem(output_base_dir):
    models_config = get_config(config_file="models.yml")["models"]
    model_mem_config = load_yaml("model_mem/config.yml")

    gpu_idx = model_mem_config["base"]["gpu_index"]

    unit_perf_runner = UnitPerfRunner()

    reports = []

    for model_name, run_config in model_mem_config["models"].items():
        model_config = models_config[model_name]
        model_version = model_config["version"]
        model_format = model_config["format"]
        data = run_config["data"]
        input_shape = run_config["input_shape"]
        duration = run_config["duration"]

        file_name = os.path.join(output_base_dir, "{}.txt".format(model_name))
        report = ModelMemTotalReport(file_name,
                                     gpu_idx,
                                     model_name,
                                     model_version,
                                     model_format,
                                     data,
                                     input_shape,
                                     duration)

        for run in run_config["runs"]:
            model_mem = run["model_mem"]
            models_num = run["models_num"]

            model_mem_perf = ModelMemUnitPerf(gpu_idx,
                                              model_name,
                                              model_version,
                                              model_format,
                                              model_mem,
                                              models_num,
                                              data,
                                              input_shape,
                                              duration)
            unit_report = unit_perf_runner.run(model_mem_perf)
            report.append_unit(unit_report)

        reports.append(report)

    for report in reports:
        print(report.generate())
        report.save()
