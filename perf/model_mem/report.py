import textwrap

from tabulate import tabulate

from base.report import PerfReport


UNIT_HEADERS = [
    "Model Memory",
    "Models Number",
    "Inference Time Avg.",
    "Inference Time Avg. (Each Model)"
]


class ModelMemUnitReport(object):

    def __init__(self, model_mem, models_num, infer_times):
        self._model_mem = model_mem
        self._models_num = models_num
        self._infer_times = infer_times

    def _gen_time_str(self, time_float):
        return "{:.3f} ms".format(time_float)

    def _gen_mem_str(self, mem):
        return "{} MiB".format(mem)

    def _gen_num_str(self, num):
        return str(num)

    def to_readable(self):
        model_mem = self._gen_mem_str(self._model_mem)

        models_num = self._gen_num_str(self._models_num)

        avg_infer_time = sum(self._infer_times) / len(self._infer_times)
        avg_infer_time = self._gen_time_str(avg_infer_time)

        avg_infer_time_each = [self._gen_time_str(t) for t in self._infer_times]
        avg_infer_time_each = ",".join(avg_infer_time_each)

        return [
            model_mem,
            models_num,
            avg_infer_time,
            avg_infer_time_each,
        ]


class ModelMemTotalReport(PerfReport):

    def __init__(self,
                 file_name,
                 gpu_idx,
                 model_name,
                 model_version,
                 model_format,
                 data,
                 input_shape,
                 duration):
        super().__init__(file_name)

        self._gpu_idx = gpu_idx
        self._model_name = model_name
        self._model_version = model_version
        self._model_format = model_format
        self._data = data
        self._input_shape = input_shape
        self._duration = duration
        self._unit_reports = []

    def _info_str(self):
        info_str = textwrap.dedent("""
            [ Performance Report: {model_name} ]
            GPU Index: {gpu_idx}
            Model Version: {model_version}
            Model Format: {model_format}
            Data: {data_src} ({data_type})
            Input Shape: {input_shape}
            Duration: {duration} second(s)
        """.format(gpu_idx=self._gpu_idx,
                   model_name=self._model_name,
                   model_version=self._model_version,
                   model_format=self._model_format,
                   data_src=self._data["src"],
                   data_type=self._data["type"],
                   input_shape=self._input_shape,
                   duration=self._duration))

        return info_str

    def _units_str(self):
        unit_lists = [u.to_readable() for u in self._unit_reports]
        units_str = tabulate(unit_lists,
                             headers=UNIT_HEADERS,
                             tablefmt="fancy_grid")

        return units_str

    def generate(self):
        output = "{}\n\n{}".format(self._info_str(), self._units_str())

        return output

    def append_unit(self, unit_report):
        self._unit_reports.append(unit_report)
