import textwrap
import time

import asyncio
from dask.distributed import Client
from dask.distributed import LocalCluster
from dask.distributed import get_client
from jagereye_ng import gpu_worker

from base.unit_perf import UnitPerf
from base.data import DataReader
from base.threading import TerminableThread
from base.util import deep_len
from base.util import elapsed
from base.util import get_gpu_total_mem
from model_mem.report import ModelMemUnitReport


class _ModelRunnerThread(TerminableThread):

    def __init__(self, data_reader, model_name):
        super().__init__()

        self._data_reader = data_reader
        self._model_name = model_name

        self._client = get_client()
        self._completed_num = 0
        self._total_elapsed = 0.0

    @property
    def completed_num(self):
        return self._completed_num

    @property
    def total_elapsed(self):
        return self._total_elapsed

    def _run_model(self, imgs):
        f_imgs = self._client.scatter(imgs)
        f_result = gpu_worker.run_model(self._client,
                                        self._model_name,
                                        f_imgs)
        f_result.result()

    def run(self):
        while True:
            imgs = self._data_reader.sample()

            try:
                elapsed_time = elapsed(self._run_model, imgs)
            except asyncio.CancelledError:
                # FIXME(JiaKuan Su):
                # The model running request sometimes occurs cancelled error
                # for unkown reason. It may cause from the requests are too
                # frequently. Currently, we can only handle such case by
                # catching the cancelled error.
                continue

            if self.is_terminated():
                break

            self._completed_num += deep_len(imgs)
            self._total_elapsed += elapsed_time


class _ModelRunner(object):

    def __init__(self, data, model_name, input_shape):
        self._data = data
        self._model_name = model_name
        self._input_shape = input_shape

        self._data_reader = DataReader(self._data, self._input_shape)
        self._runner = _ModelRunnerThread(self._data_reader, self._model_name)

    def start(self):
        self._data_reader.start()
        self._runner.start()

    def terminate(self):
        self._runner.terminate()
        self._data_reader.terminate()
        self._runner.join()
        self._data_reader.join()

    def get_result(self):
        return {
            "completed_num": self._runner.completed_num,
            "total_elapsed": self._runner.total_elapsed,
        }


class ModelMemUnitPerf(UnitPerf):

    def __init__(self,
                 gpu_idx,
                 model_name,
                 model_version,
                 model_format,
                 model_mem,
                 models_num,
                 data,
                 input_shape,
                 duration):
        super().__init__()

        self._gpu_idx = gpu_idx
        self._model_name = model_name
        self._model_version = model_version
        self._model_format = model_format
        self._model_mem = model_mem
        self._models_num = models_num
        self._data = data
        self._input_shape = input_shape
        self._duration = duration

        gpu_total_mem = get_gpu_total_mem(self._gpu_idx)
        self._gpu_fraction = self._model_mem / gpu_total_mem

    def _config_str(self):
        config_str = textwrap.dedent("""
            == Perf unit for model_mem: {model_name} ==
            GPU Index: {gpu_idx}
            Model Version: {model_version}
            Model Format: {model_format}
            Model Memory: {models_num}
            Models Number: {models_num}
            Data: {data_src} ({data_type})
            Input Shape: {input_shape}
            Duration: {duration} second(s)
        """.format(gpu_idx=self._gpu_idx,
                   model_name=self._model_name,
                   model_version=self._model_version,
                   model_format=self._model_format,
                   model_mem=self._model_mem,
                   models_num=self._models_num,
                   data_src=self._data["src"],
                   data_type=self._data["type"],
                   input_shape=self._input_shape,
                   duration=self._duration))

        return config_str

    def _progress_str(self, sec_cur):
        progress_str = "Progress: {} / {} second(s)".format(
            sec_cur,
            self._duration,
        )

        return progress_str

    def _find_allocation_settings(self):
        allocation_settings = []

        for worker_idx in range(self._models_num):
            allocation_settings.append({
                "name": "GPU_WORKER_{}:{}:{}:{}:{}:{}".format(
                    worker_idx,
                    self._model_name.upper(),
                    self._model_version,
                    self._model_format,
                    self._gpu_idx,
                    self._gpu_fraction,
                ),
                "resources": {
                    "GPU:{}".format(self._model_name.upper()): 1,
                },
            })

        return allocation_settings

    def _gen_report(self, results):
        infer_times = []
        for result in results:
            infer_time = (result["total_elapsed"] * 1000) / result["completed_num"]
            infer_times.append(infer_time)

        unit_report = ModelMemUnitReport(self._model_mem,
                                         self._models_num,
                                         infer_times)

        return unit_report

    def run(self, report_result):
        allocation_settings = self._find_allocation_settings()

        cluster = LocalCluster(n_workers=0)
        client = Client(cluster.scheduler_address)

        workers = []

        for setting in allocation_settings:
            worker = cluster.start_worker(name=setting["name"],
                                          resources=setting["resources"])
            workers.append(worker)

        with cluster, client:
            results = client.run(gpu_worker.init_worker)
            assert all([v == "OK" for _, v in results.items()]), \
                   "Failed to initialize GPU workers"

            runners = []
            for idx in range(self._models_num):
                runner = _ModelRunner(self._data,
                                      self._model_name,
                                      self._input_shape)
                runner.start()
                runners.append(runner)

            print(self._config_str())

            for sec_cur in range(self._duration):
                print(self._progress_str(sec_cur), end="\r")
                time.sleep(1)

            results = []
            for runner in runners:
                runner.terminate()
                results.append(runner.get_result())

            unit_report = self._gen_report(results)

            for worker in workers:
                cluster.stop_worker(worker)

        client.close()
        cluster.close()

        report_result.append(unit_report)
