import argparse
import os
import shutil

from model_mem.run import run_model_mem
from model_batch.run import run_model_batch


OUTPUT_ROOT = "report"

TARGET_ALL = "all"
TARGETS_FN = {
    "model_mem": run_model_mem,
    "model_batch": run_model_batch,
}


def _get_output_base_dir(perf_type):
    return os.path.join(OUTPUT_ROOT, perf_type)


def _parse_args():
    parser = argparse.ArgumentParser(
        "Performance testing tool for JagerEye",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )

    target_choices = [TARGET_ALL] + list(TARGETS_FN.keys())

    parser.add_argument("-t",
                        "--target",
                        type=str,
                        default=TARGET_ALL,
                        dest="target",
                        help="Testing target",
                        choices=target_choices)

    return parser.parse_args()


def _run(name):
    output_base_dir = _get_output_base_dir(name)

    TARGETS_FN[name](output_base_dir)


def main(target):
    # Delete the output root folder.
    shutil.rmtree(OUTPUT_ROOT, ignore_errors=True)

    # Run the target function(s).
    if target != TARGET_ALL:
        _run(target)
    else:
        for name in TARGETS_FN.keys():
            _run(name)


if __name__ == "__main__":
    args = _parse_args()

    main(args.target)
