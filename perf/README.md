# JagerEye Performance Testing Tool

The directory contains performance testing tools for JagerEye.

## Prerequisites

* Install [JagerEye Framework](framework)

* Install required Python packages:

```bash
pip3 install requirements.txt
```

## Usage

The entry point is `perf.py`. Use following command to run it:

```bash
python3 perf.py [-h | --help] [-t TARGET | --target TARGET]
```

Where `TARGET` can be one of following:

- `all`: Run all testings.
- `model_mem`: Run [Model Momery](#model-memory) testing only.
- `model_batch`: Run [Model Batch](#model-batch) testing only.

After running successfully, the result report will stored in `report` folder.

## Performance Testing

The tool supports following performance testing:

### Model Memory

The testing measures how a model (such as `object_detection`) performs under different memory settings. The testing will run multple same model instances simultaneously and feed them images for inference repeatly. The testing will then output average inference time of an image.

You can specify settings by modifying the [configuration file](perf/model_mem/config.yml). The structure is shown as follows:

```yaml
base:
    # GPU index to run the testing
    gpu_index: ${gpu_index}
models:
    # The model name to run
    ${Model name}:
        data:
            # Data source
            src: ${data_src}
            #Data type, can be `video` or `images`
            type: ${data_type}
        # Shape of input images number (array of integer)
        input_shape: ${input_shape}
        # Duration (in seconds) to run a setting
        duration: ${duration}
        # Running Settings (Array of object)
        runs:
            - model_mem: ${model_mem} # Model memory in MiB
              models_num: ${models_num} # Number of models to run simultaneously
```

The report will show result as below:

- `Inference Time Avg.`: The overall average inference time of an image.
- `Inference Time Avg. (Each Model)` The average time of an image for each model instance.

### Model Batch

The testing measures how a model (such as `license_plate_recognition`) performs under different batch size settings. The testing will run multple one model instance and feed it images for inference repeatly. The testing will then output average inference time of an image.

You can specify settings by modifying the [configuration file](perf/model_batch/config.yml). The structure is shown as follows:

```yaml
base:
    # GPU index to run the testing
    gpu_index: ${gpu_index}
models:
    # The model name to run
    ${Model name}:
        data:
            # Data source
            src: ${data_src}
            #Data type, can be `video` or `images`
            type: ${data_type}
        # Shape of input images number (array of integer)
        input_shape: ${input_shape}
        # Duration (in seconds) to run a setting
        duration: ${duration}
        # Running Settings (Array of object)
        runs:
            - batch_size: ${batch_size} # Model batch size
```

The report will show result as below:

- `Inference Time Avg.`: The overall average inference time of an image.
