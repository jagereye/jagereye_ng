import os

from jagereye_ng.util.generic import get_config

from base.util import load_yaml
from base.unit_perf import UnitPerfRunner
from model_batch.model_batch import ModelBatchUnitPerf
from model_batch.report import ModelBatchTotalReport


def run_model_batch(output_base_dir):
    models_config = get_config(config_file="models.yml")["models"]
    model_batch_config = load_yaml("model_batch/config.yml")

    gpu_idx = model_batch_config["base"]["gpu_index"]

    unit_perf_runner = UnitPerfRunner()

    reports = []

    for model_name, run_config in model_batch_config["models"].items():
        model_config = models_config[model_name]
        model_version = model_config["version"]
        model_format = model_config["format"]
        model_mem = model_config["gpu_usage"]
        data = run_config["data"]
        input_shape = run_config["input_shape"]
        duration = run_config["duration"]

        file_name = os.path.join(output_base_dir, "{}.txt".format(model_name))
        report = ModelBatchTotalReport(file_name,
                                       gpu_idx,
                                       model_name,
                                       model_version,
                                       model_format,
                                       model_mem,
                                       data,
                                       input_shape,
                                       duration)

        for run in run_config["runs"]:
            batch_size = run["batch_size"]

            model_mem_perf = ModelBatchUnitPerf(gpu_idx,
                                                model_name,
                                                model_version,
                                                model_format,
                                                model_mem,
                                                batch_size,
                                                data,
                                                input_shape,
                                                duration)
            unit_report = unit_perf_runner.run(model_mem_perf)
            report.append_unit(unit_report)

        reports.append(report)

    for report in reports:
        print(report.generate())
        report.save()
