import textwrap

from tabulate import tabulate

from base.report import PerfReport


UNIT_HEADERS = [
    "Batch Size",
    "Inference Time Avg.",
]


class ModelBatchUnitReport(object):

    def __init__(self, batch_size, avg_infer_time):
        self._batch_size = batch_size
        self._avg_infer_time = avg_infer_time

    def _gen_time_str(self, time_float):
        return "{:.3f} ms".format(time_float)

    def _gen_batch_size_str(self, batch_size):
        return str(batch_size)

    def to_readable(self):
        batch_size = self._gen_batch_size_str(self._batch_size)

        avg_infer_time = self._gen_time_str(self._avg_infer_time)

        return [
            batch_size,
            avg_infer_time,
        ]


class ModelBatchTotalReport(PerfReport):

    def __init__(self,
                 file_name,
                 gpu_idx,
                 model_name,
                 model_version,
                 model_format,
                 model_mem,
                 data,
                 input_shape,
                 duration):
        super().__init__(file_name)

        self._gpu_idx = gpu_idx
        self._model_name = model_name
        self._model_version = model_version
        self._model_format = model_format
        self._model_mem = model_mem
        self._data = data
        self._input_shape = input_shape
        self._duration = duration

        self._unit_reports = []

    def _info_str(self):
        info_str = textwrap.dedent("""
            [ Performance Report: {model_name} ]
            GPU Index: {gpu_idx}
            Model Version: {model_version}
            Model Format: {model_format}
            Model Memory: {model_mem}
            Data: {data_src} ({data_type})
            Input Shape: {input_shape}
            Duration: {duration} second(s)
        """.format(gpu_idx=self._gpu_idx,
                   model_name=self._model_name,
                   model_version=self._model_version,
                   model_format=self._model_format,
                   model_mem=self._model_mem,
                   data_src=self._data["src"],
                   data_type=self._data["type"],
                   input_shape=self._input_shape,
                   duration=self._duration))

        return info_str

    def _units_str(self):
        unit_lists = [u.to_readable() for u in self._unit_reports]
        units_str = tabulate(unit_lists,
                             headers=UNIT_HEADERS,
                             tablefmt="fancy_grid")

        return units_str

    def generate(self):
        output = "{}\n\n{}".format(self._info_str(), self._units_str())

        return output

    def append_unit(self, unit_report):
        self._unit_reports.append(unit_report)
