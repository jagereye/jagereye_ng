import textwrap
import time

from jagereye_ng.gpu_worker import models
from jagereye_ng.util.string import snake_to_pascal

from base.data import DataReader
from base.threading import TerminableThread
from base.unit_perf import UnitPerf
from base.util import deep_len
from base.util import elapsed
from base.util import get_gpu_total_mem
from model_batch.report import ModelBatchUnitReport


class _ModelRunnerThread(TerminableThread):

    def __init__(self, data_reader, model):
        super().__init__()

        self._data_reader = data_reader
        self._model = model

        self._completed_num = 0
        self._total_elapsed = 0.0

    @property
    def completed_num(self):
        return self._completed_num

    @property
    def total_elapsed(self):
        return self._total_elapsed

    def _run_model(self, imgs):
        self._model.run(imgs)

    def run(self):
        while True:
            imgs = self._data_reader.sample()

            elapsed_time = elapsed(self._run_model, imgs)

            if self.is_terminated():
                break

            self._completed_num += deep_len(imgs)
            self._total_elapsed += elapsed_time


class _ModelRunner(object):

    def __init__(self, data, input_shape, model):
        self._data = data
        self._input_shape = input_shape
        self._model = model

        self._data_reader = DataReader(self._data, self._input_shape)
        self._runner = _ModelRunnerThread(self._data_reader, self._model)

    def start(self):
        self._data_reader.start()
        self._runner.start()

    def terminate(self):
        self._runner.terminate()
        self._data_reader.terminate()
        self._runner.join()
        self._data_reader.join()

    def get_result(self):
        return {
            "completed_num": self._runner.completed_num,
            "total_elapsed": self._runner.total_elapsed,
        }


class ModelBatchUnitPerf(UnitPerf):

    def __init__(self,
                 gpu_idx,
                 model_name,
                 model_version,
                 model_format,
                 model_mem,
                 batch_size,
                 data,
                 input_shape,
                 duration):
        super().__init__()

        self._gpu_idx = gpu_idx
        self._model_name = model_name
        self._model_version = model_version
        self._model_format = model_format
        self._model_mem = model_mem
        self._batch_size = batch_size
        self._data = data
        self._input_shape = input_shape
        self._duration = duration

        gpu_total_mem = get_gpu_total_mem(self._gpu_idx)
        self._gpu_fraction = self._model_mem / gpu_total_mem

    def _config_str(self):
        config_str = textwrap.dedent("""
            == Perf unit for model_batch: {model_name} ==
            GPU Index: {gpu_idx}
            Model Version: {model_version}
            Model Format: {model_format}
            Model Memory: {model_mem}
            Batch Size: {batch_size}
            Data: {data_src} ({data_type})
            Input Shape: {input_shape}
            Duration: {duration} second(s)
        """.format(gpu_idx=self._gpu_idx,
                   model_name=self._model_name,
                   model_version=self._model_version,
                   model_format=self._model_format,
                   model_mem=self._model_mem,
                   batch_size=self._batch_size,
                   data_src=self._data["src"],
                   data_type=self._data["type"],
                   input_shape=self._input_shape,
                   duration=self._duration))

        return config_str

    def _progress_str(self, sec_cur):
        progress_str = "Progress: {} / {} second(s)".format(
            sec_cur,
            self._duration,
        )

        return progress_str

    def _get_model(self):
        model_class = getattr(models, snake_to_pascal(self._model_name))
        model = model_class(
            self._model_name,
            self._model_version,
            self._model_format,
            batch_size=self._batch_size,
        )
        model.load(self._gpu_idx, self._gpu_fraction)

        return model

    def _gen_report(self, result):
        avg_infer_time = \
            (result["total_elapsed"] * 1000) / result["completed_num"]

        unit_report = ModelBatchUnitReport(self._batch_size, avg_infer_time)

        return unit_report

    def run(self, report_result):
        model = self._get_model()

        runner = _ModelRunner(self._data, self._input_shape, model)
        runner.start()

        print(self._config_str())

        for sec_cur in range(self._duration):
            print(self._progress_str(sec_cur), end="\r")
            time.sleep(1)

        runner.terminate()

        unit_report = self._gen_report(runner.get_result())

        report_result.append(unit_report)
