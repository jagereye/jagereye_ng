const cloneDeep = require('lodash/cloneDeep')
const mongoose = require('mongoose')
const Schema = mongoose.Schema
const fs = require('fs')
const JSONPath = require('JSONPath')
const crypto = require('crypto')

const DATABASE_SCHEMA_URL = '../../shared/database.json'
const EXTERNALS_SCHEMA_URL = '../../shared/externals.json'
const PIPELINE_SCHEMA_URL = '../../shared/pipeline.json'
const SYSTEMS_SCHEMA_URL = '../../shared/systems.json'

const config = require('./config')
// Initialize mongoose connection
const dbURL = `mongodb://${config.services.database.network.ip}:27017/` +
                `${config.services.database.db_name}`
const options = {
    user: config.services.database.env.username,
    pass: config.services.database.env.password,
    auth: { authSource: "admin" },
    useNewUrlParser: true,
    reconnectTries: Number.MAX_VALUE, // Never stop trying to redbect
    reconnectInterval: 500, // Reconnect every 500ms
}

global.mongoose = mongoose
mongoose.connect(dbURL, options);
mongoose.set('useCreateIndex', true);

mongoose.Promise = global.Promise;

const db = mongoose.connection;
db.on('connecting', () => {
    console.log('MongoDB connecting')
})
db.on('error', (err) => {
    console.error(err)
    // Handle the case for failure on first dbection.
    if (err.message && err.message.match(/failed to connect to server .* on first connect/)) {
        setTimeout(() => {
            console.log('Retrying first connect to MongoDB server')
            db.openUri(dbURL, options).catch(() => {})
        }, 3 * 1000)
    }
})
db.on('connected', () => {
    console.log('MongoDB connected')
})
db.once('open', () => {
    console.log(`MongoDB opened, MongoDB server: ${dbURL}`)
})
db.on('reconnected', () => {
    console.log('MongoDB reconnected')
})
db.on('disconnecting', () => {
    console.log('MongoDB disconnecting')
})
db.on('disconnected', () => {
    console.log('MongoDB disconnected')
})

function createSchema(schemaRep, options = {}) {
    const newSchemaRep = cloneDeep(schemaRep) // prevent to be updated
    const newOptions = cloneDeep(options) // prevent to be updated
    let index

    if (newSchemaRep.DISCRIMINATOR) {
        newOptions.discriminatorKey = newSchemaRep.DISCRIMINATOR.key
        delete newSchemaRep.DISCRIMINATOR
    }
    if (newSchemaRep.INDEX) {
        index = newSchemaRep.INDEX
        delete newSchemaRep.INDEX
    }

    const schemaObj = new Schema(newSchemaRep, newOptions)

    if (index) {
        schemaObj.index(index.fields, index.options)
    }

    return schemaObj
}

/*
  @param {Object} schemaJSON contains subdoc
  @return mainDocObj without subdoc
*/
function updateSUBDOC2Schema(schemaJSON) {
    const schemaCloneJSON = cloneDeep(schemaJSON) // prevent to be updated
    const mainDocSchemaList = Object.keys(schemaCloneJSON).filter((key) => {
        return !key.startsWith('subdoc_')
    })
    const subDocSchemaList = Object.keys(schemaCloneJSON).filter((key) => {
        return key.startsWith('subdoc_')
    })
    const mainDocObj = {}
    const subDocSchemaObj = {}
    // Import subdocument schema first
    subDocSchemaList.forEach((item) => {
        // Validate schema
        const result = JSONPath({json: schemaJSON[item], path: '$..*@string()'}).filter((value) => {
            return value.startsWith('SUBDOC_')
        })
        if (result && result.length > 0) {
            throw new Error('Subdocument of subdocument is not allow')
        }

        // Create subdocument object
        subDocSchemaObj[item] = createSchema(schemaJSON[item])
    })

    // For each main document schema, replace the subdocument placeholder
    // with real subdocument object and then create the schema object
    mainDocSchemaList.forEach((item) => {
        const propertyList = JSONPath({json: schemaCloneJSON[item], path: '$..*@string()', resultType: 'all'})
        const subDocPropertyList = propertyList.filter((property) => {
            return property.value.startsWith('SUBDOC_')
        })
        subDocPropertyList.forEach((property) => {
            // Find matching subdocument schema object
            const match = Object.keys(subDocSchemaObj).find((obj) => {
                return obj.toUpperCase() === property.value
            })
            if (!match) {
                throw new Error(`Unable to find definition for subdocument placeholder "${property.value}" in main document schema "${item}"`)
            }

            // Replace subdocument placeholder with matched subdocument object
            const pathArray = JSONPath.toPathArray(property.path).slice(0) // prevent to be updated
            const parentProperty = pathArray.slice(1, -1).reduce((accu, curr) => {
                return accu ? accu[curr] : null
            }, schemaCloneJSON[item])
            parentProperty[pathArray.pop()] = subDocSchemaObj[match]
        })
        mainDocObj[item] = schemaCloneJSON[item]
    })
    return mainDocObj
}

function importSchema(schemaUrl) {
    const schemaJSON = JSON.parse(fs.readFileSync(schemaUrl, 'utf8'))
    const mainDocObj = updateSUBDOC2Schema(schemaJSON)
    const mainDocSchemaObj = {}
    for (const item in mainDocObj) {
        mainDocSchemaObj[item] = createSchema(mainDocObj[item])
    }
    return mainDocSchemaObj
}

function importDiscriminators(model, wrapperKey, schemaUrl) {
    const schemaJSON = JSON.parse(fs.readFileSync(schemaUrl, 'utf8'))
    const mainDocObj = updateSUBDOC2Schema(schemaJSON)

    for (const item in mainDocObj) {
        const itemSchema = cloneDeep(mainDocObj[item]) // prevent to be updated
        const schemaRep = {}
        schemaRep[wrapperKey] = itemSchema
        // deleting stuff not defined in mongoose
        if (itemSchema.ONE_SHOT) {
            delete itemSchema.ONE_SHOT
        }
        if (itemSchema.PROJECTIONS) {
            delete itemSchema.PROJECTIONS
        }
        // add your discriminator without _id
        model.discriminator(item, createSchema(schemaRep, {
            _id: false,
        }))
    }
}

class UserClass {
    encryptPassword(password) {
        return crypto.pbkdf2Sync(password, this.salt, 10000, 512, 'sha512').toString('hex')
    }

    checkPassword(password) {
        return this.encryptPassword(password) === this.hashedPassword
    }

    set password(password) {
        this._plainPassword = password
        this.salt = crypto.randomBytes(128).toString('hex')
        this.hashedPassword = this.encryptPassword(password)
    }

    get password() {
        return this._plainPassword
    }
}

const databaseSchemaObj = importSchema(DATABASE_SCHEMA_URL)

// analyzers pipelines params schema is different for every type 
const docArray = databaseSchemaObj['analyzers'].path('pipelines')
importDiscriminators(docArray, 'params', PIPELINE_SCHEMA_URL)

const Models = {}
Object.keys(databaseSchemaObj).forEach((objName) => {
    if(objName === "users") {
       databaseSchemaObj[objName].loadClass(UserClass)
    }
    Models[objName] = mongoose.connection.model(objName, databaseSchemaObj[objName], objName)
})

importDiscriminators(Models.externals, 'content', EXTERNALS_SCHEMA_URL)
importDiscriminators(Models.systems, 'content', SYSTEMS_SCHEMA_URL);

module.exports = Models
