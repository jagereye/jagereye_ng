const models = require('./database')
const config = require('./config')

module.exports = {
    models,
    config
}
